﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
// Review the values of the assembly attributes
[assembly: AssemblyTitle("ProGammaX")]
[assembly: AssemblyDescription("Frontend for DOSBox 0.74.")]
[assembly: AssemblyCompany("Marek Hakl")]
[assembly: AssemblyProduct("ProGammaX")]
[assembly: AssemblyCopyright("Copyright © 2009-2015 Marek Hakl")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(false)]

//The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("275c2de3-f8af-413f-b71d-5f12abaa0103")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// <Assembly: AssemblyVersion("1.0.*")>
[assembly: AssemblyVersion("2.1.0.0")]
[assembly: AssemblyFileVersion("2.1.0.0")]
[assembly: NeutralResourcesLanguageAttribute("")]