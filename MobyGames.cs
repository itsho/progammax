﻿//Imports System.Data
//Imports System.Net
//Imports System.IO
namespace ProGammaX
{
    using System;
    using System.Collections;
    using System.Text.RegularExpressions;
    using System.Windows.Forms;

    public partial class MobyGames
     {

        private static MobyGames MobyGamesRef;
        public static MobyGames mobyGamesRef
            {
            get
                {
                return MobyGames.MobyGamesRef;
                }
            }


        #region Fields

        private ArrayList ArrayLinks = null;
        private bool EnterPressedInSearchBox = false;
        private string HTML = string.Empty;

        #endregion Fields

        #region Constructors

        public MobyGames()
        {
            InitializeComponent();

            ProGammaX.MainForm.mainFormRef.applySelectedTheme(this);
            
            MobyGamesRef = this;

            this.Load += new System.EventHandler(MobyGames_Load);
            this.Load += new System.EventHandler(OnLoaded);
            this.TextBox_GetInfoFromMobyGames_Searchbox.KeyDown += new System.Windows.Forms.KeyEventHandler(TextBox_GetInfoFromMobyGames_Searchbox_KeyDown);
            this.TextBox_GetInfoFromMobyGames_Searchbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(TextBox_GetInfoFromMobyGames_Searchbox_KeyPress);
            this.ComboBox_GetInfoFromMobyGames_Filter.SelectedIndexChanged += new System.EventHandler(ComboBox_GetInfoFromMobyGames_Filter_SelectedIndexChanged);
            this.CheckBox_GetInfoFromMobyGames_GameName.CheckedChanged += new System.EventHandler(CheckBox_GetInfoFromMobyGames_GameName_CheckedChanged);
            this.CheckBox_GetInfoFromMobyGames_Genre.CheckedChanged += new System.EventHandler(CheckBox_GetInfoFromMobyGames_Genre_CheckedChanged);
            this.CheckBox_GetInfoFromMobyGames_Developer.CheckedChanged += new System.EventHandler(CheckBox_GetInfoFromMobyGames_Developer_CheckedChanged);
            this.CheckBox_GetInfoFromMobyGames_Publisher.CheckedChanged += new System.EventHandler(CheckBox_GetInfoFromMobyGames_Publisher_CheckedChanged);
            this.CheckBox_GetInfoFromMobyGames_www.CheckedChanged += new System.EventHandler(CheckBox_GetInfoFromMobyGames_www_CheckedChanged);
            this.CheckBox_GetInfoFromMobyGames_Year.CheckedChanged += new System.EventHandler(CheckBox_GetInfoFromMobyGames_Year_CheckedChanged);
            this.CheckBox_GetInfoFromMobyGames_Comment.CheckedChanged += new System.EventHandler(CheckBox_GetInfoFromMobyGames_Comment_CheckedChanged);
            this.ListView_GetInfoFromMobyGames_Results.SelectedIndexChanged += new System.EventHandler(ListView_GetInfoFromMobyGames_Results_SelectedIndexChanged);
            this.ListView_GetInfoFromMobyGames_Results.ColumnClick += ListView_GetInfoFromMobyGames_Results_ColumnClick;
        }

        #endregion Constructors

        #region Methods




        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);

            if (this.Visible) this.Refresh();
        }





        public void GBA(string strSource, string strStart, string strEnd, int startPos = 0)
        {
            listView lstAdd = this.ListView_GetInfoFromMobyGames_Results;
            int Start, End;
            string strResult = string.Empty;
            if (strSource.Contains(strStart) && strSource.Contains(strEnd))
            {
                Start = strSource.IndexOf(strStart, startPos) + strStart.Length;
                End = strSource.IndexOf(strEnd, Start);

                strResult = strSource.Substring(Start, End - Start);
                
                lstAdd.Items.Add(strResult);
                lstAdd.Items[lstAdd.Items.Count - 1].Tag = "http://www.mobygames.com" + getBetween(strStart, Convert.ToChar(34).ToString(), Convert.ToChar(34).ToString()); //get address between ""
                lstAdd.Items[lstAdd.Items.Count - 1].ToolTipText = (string)lstAdd.Items[lstAdd.Items.Count - 1].Tag;

            }
            else
            {
                
            }
            lstAdd.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
        }

        //public void GBA( string strSource,  string strStart,  string strEnd, ListView lstAdd, int startPos = 0)
        //{
        //    int iPos = 0;
        //    int iEnd;
        //    string strResult;
        //    int lenStart = strStart.Length;

        //    while (iPos != -1)
        //    {
        //        strResult = string.Empty;
        //        iPos = strSource.IndexOf(strStart, startPos);
        //        iEnd = strSource.IndexOf(strEnd, iPos + lenStart);
        //        if (iPos != -1 && iEnd != -1)
        //        {
        //            strResult = strSource.Substring(iPos + lenStart, iEnd - (iPos + lenStart));
        //            lstAdd.Items.Add(strResult);
        //            lstAdd.Items[lstAdd.Items.Count - 1].Tag = "http://www.mobygames.com" + ParseLinks(strStart).IndexOf(0).ToString(); //Item(0) som nahradil IndexOf(0)!!!
        //            lstAdd.Items[lstAdd.Items.Count - 1].ToolTipText = (string)lstAdd.Items[lstAdd.Items.Count - 1].Tag;
        //            startPos = iPos + lenStart;
        //        }
        //    }
        //}

        //public string GBA_function(string strSource, string strStart, string strEnd, int startPos = 0)
        //{
        //    int iPos;
        //    int iEnd;
        //    string strResult;
        //    int lenStart = strStart.Length;

        //    //Do Until iPos = -1
        //    strResult = string.Empty;
        //    iPos = strSource.IndexOf(strStart, startPos);
        //    iEnd = strSource.IndexOf(strEnd, iPos + lenStart);
        //    if (iPos != -1 && iEnd != -1)
        //    {
        //        strResult = strSource.Substring(iPos + lenStart, iEnd - (iPos + lenStart));
        //        //startPos = iPos + lenStart
        //    }
        //    //Loop
        //    return strResult;
        //}
        private ListViewColumnSorter listViewColumnSorter = new ListViewColumnSorter();

       private void ListView_GetInfoFromMobyGames_Results_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            this.ListView_GetInfoFromMobyGames_Results.ListViewItemSorter = listViewColumnSorter;

            // Determine if clicked column is already the column that is being sorted.
            if (e.Column == listViewColumnSorter.SortColumn)
            {
                // Reverse the current sort direction for this column.
                if (listViewColumnSorter.Order == SortOrder.Ascending)
                {
                    listViewColumnSorter.Order = SortOrder.Descending;
                }
                else
                {
                    listViewColumnSorter.Order = SortOrder.Ascending;
                }
            }
            else
            {
                // Set the column number that is to be sorted; default to ascending.
                listViewColumnSorter.SortColumn = e.Column;
                listViewColumnSorter.Order = SortOrder.Ascending;
            }

            // Perform the sort with these new sort options.
            this.ListView_GetInfoFromMobyGames_Results.Sort();

            this.ListView_GetInfoFromMobyGames_Results.ListViewItemSorter = null;

        }







        public string GetPageHTML(string URL, int TimeoutSeconds = 10)
        {
            // Retrieves the HTML from the specified URL,
            // using a default timeout of 10 seconds
            System.Net.WebRequest objRequest;
            System.Net.WebResponse objResponse;
            System.IO.Stream objStreamReceive;
            System.Text.Encoding objEncoding;
            System.IO.StreamReader objStreamRead;
            string returnValue = string.Empty;

            try
            {
                // Setup our Web request
                objRequest = System.Net.WebRequest.Create(URL);
                objRequest.Timeout = TimeoutSeconds * 1000;
                // Retrieve data from request
                objResponse = objRequest.GetResponse();
                objStreamReceive = objResponse.GetResponseStream();
                objEncoding = System.Text.Encoding.GetEncoding("utf-8");
                objStreamRead = new System.IO.StreamReader(objStreamReceive, objEncoding);
                // Set function return value
                returnValue = objStreamRead.ReadToEnd();
                // Check if available, then close response
                if (objResponse != null)
                {
                    objResponse.Close();
                }
                return returnValue;
            }
            catch
            {
                // Error occured grabbing data, simply return nothing
                return string.Empty;
            }
        }

        //private MainForm mainForm;
        //    public MobyGames(MainForm otherForm)
        //    {
        //        InitializeComponent();
        //        this.mainForm = otherForm;
        //    }
        // ERROR: Handles clauses are not supported in C#
        public void MobyGames_Load(System.Object sender, System.EventArgs e)
        {
            this.TopMost = true;

            try
            {
                if (MainForm.mainFormRef.TabControl_MAIN.SelectedTab == MainForm.mainFormRef.TabPage_WIZARD )
                {

                        switch (MainForm.mainFormRef.ComboBox_WIZARD_AddNewProfileStep1_SelectOS.SelectedIndex)
                        {

                            case 1:
                                // "WINDOWS", "WIN"
                                this.ComboBox_GetInfoFromMobyGames_Filter.SelectedIndex = 2;
                                break;
                            case 0:
                                // "DOS"
                                this.ComboBox_GetInfoFromMobyGames_Filter.SelectedIndex = 1;
                                break;
                            default:
                                this.ComboBox_GetInfoFromMobyGames_Filter.SelectedIndex = 0;
                                break;

                        }

                }
                        else
                {

                    switch (MainForm.mainFormRef.GetOSVersionFrom_MAIN_LIST_Column3())
                        {

                            case "WINDOWS":
                                break;
                            case "WIN":
                                this.ComboBox_GetInfoFromMobyGames_Filter.SelectedIndex = 2;
                                break;
                            case "DOS":
                                this.ComboBox_GetInfoFromMobyGames_Filter.SelectedIndex = 1;
                                break;
                            default:
                                this.ComboBox_GetInfoFromMobyGames_Filter.SelectedIndex = 0;
                                break;

                        }
                }

            }
            catch
            {
            }

            //Me.ListView_GetInfoFromMobyGames_Results.GridLines = Form1.ToolStripMenuItem_MAIN_View_ShowGridInDetails.Checked

            this.TextBox_GetInfoFromMobyGames_GameName.Text = string.Empty;
            this.TextBox_GetInfoFromMobyGames_Comment.Text = string.Empty;
            this.TextBox_GetInfoFromMobyGames_Publisher.Text = string.Empty;
            this.TextBox_GetInfoFromMobyGames_Developer.Text = string.Empty;
            this.TextBox_GetInfoFromMobyGames_Genre.Text = string.Empty;
            this.TextBox_GetInfoFromMobyGames_Year.Text = string.Empty;
            this.TextBox_GetInfoFromMobyGames_www.Text = string.Empty;
            this.ListView_GetInfoFromMobyGames_Results.Items.Clear();
            this.Button_GetInfoFromMobyGames.Enabled = false;

            HTML = null;

            this.TextBox_GetInfoFromMobyGames_Searchbox.Focus();

            
        }

        public ArrayList ParseImages(string HTML)
        {
            // Remember to add the following at top of class:
            // - Imports System.Text.RegularExpressions
            System.Text.RegularExpressions.Regex objRegEx;
            System.Text.RegularExpressions.Match objMatch;
            System.Collections.ArrayList arrLinks = new System.Collections.ArrayList();
            // Create regular expression
            objRegEx = new System.Text.RegularExpressions.Regex("img.*src\\s*=\\s*(?:\"(?<1>[^\"]*)\"|(?<1>\\S+))", System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.Compiled);
            // Match expression to HTML
            objMatch = objRegEx.Match(HTML);
            // Loop through matches and add <1> to ArrayList
            while (objMatch.Success)
            {
                string strMatch;
                strMatch = objMatch.Groups[1].ToString();
                arrLinks.Add(strMatch);
                objMatch = objMatch.NextMatch();
            }
            // Pass back results
            return arrLinks;
        }

        public static string getBetween(string strSource, string strStart, string strEnd)
        {
            int Start, End;
            if (strSource.Contains(strStart) && strSource.Contains(strEnd))
            {
                Start = strSource.IndexOf(strStart, 0) + strStart.Length;
                End = strSource.IndexOf(strEnd, Start);
                return strSource.Substring(Start, End - Start);
            }
            else
            {
                return "";
            }
        }

        public ArrayList ParseLinks(string HTML)
        {
            // Remember to add the following at top of class:
            // - Imports System.Text.RegularExpressions
            System.Text.RegularExpressions.Regex objRegEx;
            System.Text.RegularExpressions.Match objMatch;
            System.Collections.ArrayList arrLinks = new System.Collections.ArrayList();
            // Create regular expression
            try
            {
                objRegEx = new System.Text.RegularExpressions.Regex("a.*href\\s*=\\s*(?:\"(?<1>[^\"]*)\"|(?<1>\\S+))", System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.Compiled);
                // Match expression to HTML
                objMatch = objRegEx.Match(HTML);
                // Loop through matches and add <1> to ArrayList
                while (objMatch.Success)
                {
                    
                    string strMatch;
                    strMatch = objMatch.Groups[1].ToString();
                    //arrLinks.Add(getBetween(strMatch, Convert.ToChar(34).ToString(), Convert.ToChar(34).ToString()));
                    arrLinks.Add(strMatch);
                    objMatch = objMatch.NextMatch();
                    //MessageBox.Show(getBetween(strMatch, Convert.ToChar(34).ToString(), Convert.ToChar(34).ToString()));
                }

            }
            catch
            {
            }

            // Pass back results
            return arrLinks;
        }

        public string StripTags(string HTML)
        {
            // Removes tags from passed HTML
            //System.Text.RegularExpressions.Regex objRegEx = null;

            return Regex.Replace(HTML, "<.*?>", string.Empty);
        }

        private void ApplyFilter()
        {
            try
            {

                foreach (ListViewItem TempItem in this.ListView_GetInfoFromMobyGames_Results.Items)
                {
                    switch (this.ComboBox_GetInfoFromMobyGames_Filter.SelectedIndex)
                    {

                        case 0:
                            // All games
                            return;

                        case 1:
                            // DOS
                            //If Not CStr(TempItem.Tag).Contains("/dos/") Then TempItem.Remove()
                            if (((string)TempItem.Tag).Contains("/3do/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/amiga/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/amiga-cd32/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/cpc/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/android/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/apple2/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/apple2gs/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/atari-2600/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/atari-5200/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/atari-7800/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/atari-8-bit/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/atari-st/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/bbc-micro_/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/brew/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/blackberry/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/browser/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/cd-i/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/channel-f/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/colecovision/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/c128/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/c64/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/pet/"))
                                TempItem.Remove();
                            //If CStr(TempItem.Tag).Contains("/dos/") Then TempItem.Remove()
                            if (((string)TempItem.Tag).Contains("/doja/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/dragon-3264/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/dreamcast/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/electron/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/exen/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/gameboy/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/gameboy-advance/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/gameboy-color/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/game-gear/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/game-com/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/gamecube/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/genesis/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/gizmondo/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/intellivision/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/j2me/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/jaguar/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/linux/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/lynx/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/msx/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/macintosh/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/mophun/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/ngage/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/ngage2/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/nes/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/neo-geo/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/neo-geo-cd/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/neo-geo-pocket/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/neo-geo-pocket-color/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/n64/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/nintendo-ds/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/nintendo-dsi/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/odyssey/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/odyssey-2/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/pc-booter/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/pc88/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/pc98/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/pc-fx/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/psp/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/palmos/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/playstation/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/ps2/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/ps3/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/sega-32x/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/sega-cd/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/sega-master-system/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/sega-saturn/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/snes/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/spectravideo/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/symbian/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/ti-994a/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/trs-80/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/trs-80-coco/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/turbografx-cd/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/turbo-grafx/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/vsmile/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/vic-20/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/vectrex/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/virtual-boy/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/wii/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/windows/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/win3x/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/windowsmobile/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/wonderswan/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/wonderswan-color/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/xbox/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/xbox360/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/zx-spectrum/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/zeebo/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/zodiac/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/ipad/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/iphone/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/ipod-classic/"))

                                TempItem.Remove();
                            break;

                        case 2:
                            // WINDOWS
                            //If Not CStr(TempItem.Tag).Contains("/windows/") Then TempItem.Remove()
                            if (((string)TempItem.Tag).Contains("/3do/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/amiga/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/amiga-cd32/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/cpc/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/android/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/apple2/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/apple2gs/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/atari-2600/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/atari-5200/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/atari-7800/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/atari-8-bit/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/atari-st/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/bbc-micro_/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/brew/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/blackberry/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/browser/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/cd-i/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/channel-f/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/colecovision/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/c128/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/c64/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/pet/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/dos/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/doja/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/dragon-3264/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/dreamcast/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/electron/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/exen/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/gameboy/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/gameboy-advance/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/gameboy-color/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/game-gear/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/game-com/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/gamecube/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/genesis/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/gizmondo/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/intellivision/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/j2me/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/jaguar/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/linux/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/lynx/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/msx/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/macintosh/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/mophun/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/ngage/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/ngage2/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/nes/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/neo-geo/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/neo-geo-cd/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/neo-geo-pocket/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/neo-geo-pocket-color/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/n64/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/nintendo-ds/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/nintendo-dsi/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/odyssey/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/odyssey-2/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/pc-booter/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/pc88/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/pc98/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/pc-fx/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/psp/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/palmos/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/playstation/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/ps2/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/ps3/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/sega-32x/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/sega-cd/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/sega-master-system/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/sega-saturn/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/snes/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/spectravideo/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/symbian/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/ti-994a/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/trs-80/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/trs-80-coco/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/turbografx-cd/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/turbo-grafx/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/vsmile/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/vic-20/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/vectrex/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/virtual-boy/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/wii/"))
                                TempItem.Remove();
                            //If CStr(TempItem.Tag).Contains("/windows/") Then TempItem.Remove()
                            //If CStr(TempItem.Tag).Contains("/win3x/") Then TempItem.Remove()
                            if (((string)TempItem.Tag).Contains("/windowsmobile/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/wonderswan/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/wonderswan-color/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/xbox/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/xbox360/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/zx-spectrum/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/zeebo/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/zodiac/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/ipad/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/iphone/"))
                                TempItem.Remove();
                            if (((string)TempItem.Tag).Contains("/ipod-classic/"))
                                TempItem.Remove();
                            break;
                    }

                }

            }
            catch
            {
            }
        }

        // ERROR: Handles clauses are not supported in C#
        private void Button_GetInfoFromMobyGames_Click(System.Object sender, System.EventArgs e)
        {
            if (this.TextBox_GetInfoFromMobyGames_GameName.Enabled == false & this.TextBox_GetInfoFromMobyGames_Comment.Enabled == false & this.TextBox_GetInfoFromMobyGames_Publisher.Enabled == false & this.TextBox_GetInfoFromMobyGames_Developer.Enabled == false & this.TextBox_GetInfoFromMobyGames_Genre.Enabled == false & this.TextBox_GetInfoFromMobyGames_Year.Enabled == false & this.TextBox_GetInfoFromMobyGames_www.Enabled == false)
            {
                return;
            }

            string tempHTML2 = GetPageHTML(this.ListView_GetInfoFromMobyGames_Results.SelectedItems[0].Tag.ToString());
            
            tempHTML2 = StripTags(tempHTML2).Replace("&nbsp;", " ");
            tempHTML2 = tempHTML2.Replace("&quot;", null);
            tempHTML2 = tempHTML2.Replace("&amp;", "&");

            try
            {
                //ListBox_GetInfoFromMobyGames_Results.DataSource = ArrayLinks
                //Try
                //    MsgBox("http://www.mobygames.com" & ArrayImages(3).ToString)
                //    Me.PictureBox1.ImageLocation = "http://www.mobygames.com" & ArrayImages(3).ToString '"http://progammax.webs.com/data/images/icon.png"
                //Catch ex1 As Exception
                //    Me.PictureBox1.Image = Nothing
                //End Try
                if (this.TextBox_GetInfoFromMobyGames_GameName.Enabled == true)
                {
                    this.TextBox_GetInfoFromMobyGames_GameName.Text = this.ListView_GetInfoFromMobyGames_Results.SelectedItems[0].Text;
                }
                if (this.TextBox_GetInfoFromMobyGames_Comment.Enabled == true)
                {
                    this.TextBox_GetInfoFromMobyGames_Comment.Text = getBetween(tempHTML2, "Description", "[");
                }
                if (this.TextBox_GetInfoFromMobyGames_Publisher.Enabled == true)
                {
                    this.TextBox_GetInfoFromMobyGames_Publisher.Text = getBetween(tempHTML2, "Published by", "Developed by");
                }
                if (this.TextBox_GetInfoFromMobyGames_Developer.Enabled == true)
                {
                    this.TextBox_GetInfoFromMobyGames_Developer.Text = getBetween(tempHTML2, "Developed by", "Released");
                }
                if (this.TextBox_GetInfoFromMobyGames_Genre.Enabled == true)
                {
                    this.TextBox_GetInfoFromMobyGames_Genre.Text = getBetween(tempHTML2, "Genre", "Perspective");
                }
                if (this.TextBox_GetInfoFromMobyGames_Year.Enabled == true)
                {
                    this.TextBox_GetInfoFromMobyGames_Year.Text = getBetween(tempHTML2, "Released", "Platform");
                }
                if (this.TextBox_GetInfoFromMobyGames_www.Enabled == true)
                {
                    this.TextBox_GetInfoFromMobyGames_www.Text = this.ListView_GetInfoFromMobyGames_Results.SelectedItems[0].Tag.ToString();
                }

            }
            catch
            {
            }
        }

        // ERROR: Handles clauses are not supported in C#
        private void Button_SearchMobyGames_Click(System.Object sender, System.EventArgs e)
        {
            PrepareToSearchMobyGames();
        }

        // ERROR: Handles clauses are not supported in C#
        private void Cancel_Button_Click(System.Object sender, System.EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        // ERROR: Handles clauses are not supported in C#
        private void CheckBox_GetInfoFromMobyGames_Comment_CheckedChanged(System.Object sender, System.EventArgs e)
        {
            this.TextBox_GetInfoFromMobyGames_Comment.Enabled = this.CheckBox_GetInfoFromMobyGames_Comment.Checked;
        }

        // ERROR: Handles clauses are not supported in C#
        private void CheckBox_GetInfoFromMobyGames_Developer_CheckedChanged(System.Object sender, System.EventArgs e)
        {
            this.TextBox_GetInfoFromMobyGames_Developer.Enabled = this.CheckBox_GetInfoFromMobyGames_Developer.Checked;
        }

        // ERROR: Handles clauses are not supported in C#
        private void CheckBox_GetInfoFromMobyGames_GameName_CheckedChanged(System.Object sender, System.EventArgs e)
        {
            this.TextBox_GetInfoFromMobyGames_GameName.Enabled = this.CheckBox_GetInfoFromMobyGames_GameName.Checked;
        }

        // ERROR: Handles clauses are not supported in C#
        private void CheckBox_GetInfoFromMobyGames_Genre_CheckedChanged(System.Object sender, System.EventArgs e)
        {
            this.TextBox_GetInfoFromMobyGames_Genre.Enabled = this.CheckBox_GetInfoFromMobyGames_Genre.Checked;
        }

        // ERROR: Handles clauses are not supported in C#
        private void CheckBox_GetInfoFromMobyGames_Publisher_CheckedChanged(System.Object sender, System.EventArgs e)
        {
            this.TextBox_GetInfoFromMobyGames_Publisher.Enabled = this.CheckBox_GetInfoFromMobyGames_Publisher.Checked;
        }

        // ERROR: Handles clauses are not supported in C#
        private void CheckBox_GetInfoFromMobyGames_www_CheckedChanged(System.Object sender, System.EventArgs e)
        {
            this.TextBox_GetInfoFromMobyGames_www.Enabled = this.CheckBox_GetInfoFromMobyGames_www.Checked;
        }

        // ERROR: Handles clauses are not supported in C#
        private void CheckBox_GetInfoFromMobyGames_Year_CheckedChanged(System.Object sender, System.EventArgs e)
        {
            this.TextBox_GetInfoFromMobyGames_Year.Enabled = this.CheckBox_GetInfoFromMobyGames_Year.Checked;
        }

        // ERROR: Handles clauses are not supported in C#
        private void ComboBox_GetInfoFromMobyGames_Filter_SelectedIndexChanged(System.Object sender, System.EventArgs e)
        {
            SearchMobyGames();
            
            //ApplyFilter()
        }

        // ERROR: Handles clauses are not supported in C#
        private void ListView_GetInfoFromMobyGames_Results_SelectedIndexChanged(System.Object sender, System.EventArgs e)
        {
            try
            {
                this.ToolTip1.Show(this.ListView_GetInfoFromMobyGames_Results.SelectedItems[0].ToolTipText, this.ListView_GetInfoFromMobyGames_Results);
            }
            catch
            {
                ToolTip1.RemoveAll();
            }

            if (this.ListView_GetInfoFromMobyGames_Results.SelectedItems.Count == 0)
            {
                this.Button_GetInfoFromMobyGames.Enabled = false;
            }
            else
            {
                this.Button_GetInfoFromMobyGames.Enabled = true;
            }
        }

        // ERROR: Handles clauses are not supported in C#
        private void OK_Button_Click(System.Object sender, System.EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            //MsgBox("ok")
            this.Close();
        }

        // ERROR: Handles clauses are not supported in C#
        private void OnLoaded(object sender, System.EventArgs e)
        {
            // Sets keyboard focus on the search Button.
            this.Button_SearchMobyGames.Select();
        }

        private void PrepareToSearchMobyGames()
        {
            HTML = GetPageHTML("http://www.mobygames.com/search/quick?ajax=1&sFilter=1&p=-1&sG=on&q=" + Convert.ToChar(34).ToString() + this.TextBox_GetInfoFromMobyGames_Searchbox.Text + Convert.ToChar(34).ToString().Replace("\\", null)).Replace(">", ">" + "\n\r");
            ArrayLinks = ParseLinks(HTML);
            
            SearchMobyGames();
        }

        private void RemoveDuplicates(listView lstView)
        {
            ListViewItem itemI;
            ListViewItem itemJ;
            int progress = 0;
            int count = 0;
            int ProgressDupCounter = lstView.Items.Count;

            for (int i = lstView.Items.Count - 1; i >= 0; i += -1)
            {
                itemI = lstView.Items[i];

                progress = progress + 1;

                // start one after hence +1

                for (int z = i + 1; z <= lstView.Items.Count - 1; z += 1)
                {
                    itemJ = lstView.Items[z];

                    if (itemI.Text == itemJ.Text & (string)itemI.Tag == (string)itemJ.Tag)
                    {
                        //duplicate found, now delete duplicate
                        lstView.Items.Remove(itemJ);

                        count = count + 1;

                        break; // TODO: might not be correct. Was : Exit For

                    }

                }

            }
        }

        private void SearchMobyGames()
        {
            string TempHTML = HTML;
            int TempCount = 0;
            string tempStartString1 = string.Empty;

            if (string.IsNullOrEmpty(TempHTML))
            {
                return;
            }

            this.ListView_GetInfoFromMobyGames_Results.BeginUpdate();
            this.ListView_GetInfoFromMobyGames_Results.Items.Clear();
            

            TempHTML = TempHTML.Replace( "\n\r", null);
            TempHTML = TempHTML.Replace( "&#xB2;", ":");
            TempHTML = TempHTML.Replace( "&#x27;", "'");
            TempHTML = TempHTML.Replace( "&#x26;", "&");
            TempHTML = TempHTML.Replace("&#x22;", Convert.ToChar(34).ToString());
            
            try
            {
                //Loop through results

                for (TempCount = 0; TempCount <= ArrayLinks.Count - 1; TempCount++)
                {
                    tempStartString1 = "Game: <a href=" + ArrayLinks[TempCount].ToString();
                    //MessageBox.Show(tempStartString1);
                    //MessageBox.Show(HTML);
                    GBA(TempHTML, tempStartString1, "</a>");

                }

                RemoveDuplicates(this.ListView_GetInfoFromMobyGames_Results);
                ApplyFilter();
            }
            catch
            {
            }

            this.ListView_GetInfoFromMobyGames_Results.EndUpdate();
            this.Refresh();
        }

        // ERROR: Handles clauses are not supported in C#
        private void TextBox_GetInfoFromMobyGames_Searchbox_KeyDown(System.Object sender, System.Windows.Forms.KeyEventArgs e)
        {
            EnterPressedInSearchBox = false;
            this.TextBox_GetInfoFromMobyGames_Searchbox.Focus();
            if (e.KeyCode == Keys.Enter)
            {
                //Me.Button_SearchMobyGames.Focus()
                EnterPressedInSearchBox = true;
            }
        }

        // to prevent characters from entering the control.
        // ERROR: Handles clauses are not supported in C#
        private void TextBox_GetInfoFromMobyGames_Searchbox_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            // Check for the flag being set in the KeyDown event.

            if (EnterPressedInSearchBox == true)
            {
                PrepareToSearchMobyGames();

                // Stop the character from being entered into the control since it is non-numerical.
                e.Handled = true;
            }
        }

        #endregion Methods

        #region Other

        //TextBox_GetInfoFromMobyGames_Searchbox_KeyPress
        //Private Sub ListView_GetInfoFromMobyGames_Results_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles ListView_GetInfoFromMobyGames_Results.MouseMove
        //    Dim TempListViewItem As ListViewItem = Nothing
        //    Try
        //        TempListViewItem = Me.ListView_GetInfoFromMobyGames_Results.GetItemAt(e.X, e.Y)
        //        If (TempListViewItem Is Nothing) Then
        //            ToolTip1.RemoveAll()
        //        Else
        //            If (ToolTip1.GetToolTip(Me.ListView_GetInfoFromMobyGames_Results) <> TempListViewItem.Tag.ToString) Then
        //                ToolTip1.SetToolTip(Me.ListView_GetInfoFromMobyGames_Results, TempListViewItem.Tag.ToString)
        //            End If
        //        End If
        //    Catch ex As Exception
        //        ToolTip1.RemoveAll()
        //    End Try
        //End Sub

        #endregion Other
    }
}