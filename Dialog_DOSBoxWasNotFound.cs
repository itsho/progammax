﻿namespace ProGammaX
{
    using System;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;

    public partial class Dialog_DOSBoxWasNotFound
    {
        #region Fields
        private static Dialog_DOSBoxWasNotFound Dialog_DOSBoxWasNotFoundRef;
        public static Dialog_DOSBoxWasNotFound dialog_DOSBoxWasNotFoundRef
        {
        get
            {
            return Dialog_DOSBoxWasNotFoundRef;
            }
        }

        //private const UInt32 SWP_NOMOVE = 0x2;
        //private const UInt32 SWP_NOSIZE = 0x1;

        //readonly IntPtr HWND_BOTTOM = new IntPtr(1);
        //readonly IntPtr HWND_TOP = new IntPtr(0);

        #endregion Fields

        #region Constructors

        public Dialog_DOSBoxWasNotFound()
        {
            InitializeComponent();
            ProGammaX.MainForm.mainFormRef.applySelectedTheme(this);
            Dialog_DOSBoxWasNotFoundRef = this;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Dialog5_FormClosed);
            this.Load += new System.EventHandler(this.Dialog5_Load);
            this.TopMost = true;
        }

        #endregion Constructors

        #region Methods

        // send window to the top function
        //[DllImport("user32.dll", SetLastError = true)]
        //private static bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, UInt32 uFlags)
        //{
        //    return true;
        //}

        // ERROR: Handles clauses are not supported in C#
        private void Button1_Click(System.Object sender, System.EventArgs e)
        {
            string Folder = MainForm.mainFormRef.MakePath_Full_FromPathRelativeTo_ApplicationDirectory(this.TextBox1.Text);

            //If Not My.Computer.FileSystem.DirectoryExists(Folder) Then Folder = Me.TextBox_OPTIONS_ApplicationPaths_DosBoxDirectory.Text

            if (!System.IO.Directory.Exists(Folder))
            {
                Folder = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles);// My.Computer.FileSystem.SpecialDirectories.ProgramFiles;
            }
            //If Not My.Computer.FileSystem.DirectoryExists(Folder) Then Folder = ApplicationDirectory

            //If Folder = Nothing Or My.Computer.FileSystem.DirectoryExists(Folder) Then Folder = ApplicationDirectory

            // "Select DOSBox directory:"
            // ERROR: Not supported in C#: WithStatement

            if (this.FolderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.TextBox1.Text = MainForm.mainFormRef.MakePathRelativeOrFull_To_ApplicationDirectory(FolderBrowserDialog1.SelectedPath);
            }
        }

        // ERROR: Handles clauses are not supported in C#
        private void Cancel_Button_Click(System.Object sender, System.EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        // ERROR: Handles clauses are not supported in C#
        private void Dialog5_FormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e)
        {
            this.Show();
        }

        // ERROR: Handles clauses are not supported in C#
        private void Dialog5_Load(System.Object sender, System.EventArgs e)
        {
            // send window to the top
            //UInt32 flags = SWP_NOMOVE | SWP_NOSIZE;
            //SetWindowPos(this.Handle, HWND_TOP, 0, 0, 0, 0, flags);
            this.Hide();
        }

        // ERROR: Handles clauses are not supported in C#
        private void OK_Button_Click(System.Object sender, System.EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        #endregion Methods
    }
}