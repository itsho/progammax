﻿    namespace ProGammaX
    {
        #region Directives
        using System;
        using System.Windows.Forms;
        using System.Runtime.InteropServices;
        using System.Drawing;
        #endregion

        [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust")]
        public class cMyRichTextBox : IDisposable
        {
            #region Fields
            private IntPtr _hMyRichTextBoxWnd = IntPtr.Zero;
            private cInternalScrollBar _cInternalScroll;
            #endregion

            #region Constructor
            public cMyRichTextBox(IntPtr handle, Bitmap hztrack, Bitmap hzarrow, Bitmap hzthumb, Bitmap vttrack, Bitmap vtarrow, Bitmap vtthumb, Bitmap fader, bool forceRepaint = true)
            {
                if (handle == IntPtr.Zero)
                    throw new Exception("The MyRichTextBox handle is invalid.");
                _hMyRichTextBoxWnd = handle;
                if (hztrack != null && hzarrow != null && hzthumb != null && vttrack != null && vtarrow != null && vtthumb != null)
                    _cInternalScroll = new cInternalScrollBar(_hMyRichTextBoxWnd, hztrack, hzarrow, hzthumb, vttrack, vtarrow, vtthumb, fader, forceRepaint);
                else
                    throw new Exception("The MyRichTextBox image(s) are invalid");
            }

            public void Dispose()
            {
                try
                {
                    if (_cInternalScroll != null) _cInternalScroll.Dispose();
                }
                catch { }
            }
            #endregion






    }


}
