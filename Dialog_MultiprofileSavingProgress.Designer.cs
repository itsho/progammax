﻿namespace ProGammaX
{
    partial class Dialog_MultiprofileSavingProgress : System.Windows.Forms.Form
    {

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.Label__Dialog_MultiprofileSavingProgress_CurrentlySavingProfile = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label__Dialog_MultiprofileSavingProgress_Progress = new System.Windows.Forms.Label();
            this.ProgressBar__Dialog_MultiprofileSavingProgress = new ProGammaX.myProgressBar();
            this.OK_Button = new ProGammaX.button();
            this.Cancel_Button = new ProGammaX.button();
            this.TableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.TableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Label__Dialog_MultiprofileSavingProgress_CurrentlySavingProfile
            // 
            this.Label__Dialog_MultiprofileSavingProgress_CurrentlySavingProfile.AutoSize = true;
            this.Label__Dialog_MultiprofileSavingProgress_CurrentlySavingProfile.Location = new System.Drawing.Point(48, 96);
            this.Label__Dialog_MultiprofileSavingProgress_CurrentlySavingProfile.Name = "Label__Dialog_MultiprofileSavingProgress_CurrentlySavingProfile";
            this.Label__Dialog_MultiprofileSavingProgress_CurrentlySavingProfile.Size = new System.Drawing.Size(116, 13);
            this.Label__Dialog_MultiprofileSavingProgress_CurrentlySavingProfile.TabIndex = 5;
            this.Label__Dialog_MultiprofileSavingProgress_CurrentlySavingProfile.Text = "Currently saving profile:";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(48, 80);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(95, 13);
            this.Label2.TabIndex = 6;
            this.Label2.Text = "Currently is saving:";
            // 
            // Label__Dialog_MultiprofileSavingProgress_Progress
            // 
            this.Label__Dialog_MultiprofileSavingProgress_Progress.AutoSize = true;
            this.Label__Dialog_MultiprofileSavingProgress_Progress.Location = new System.Drawing.Point(48, 24);
            this.Label__Dialog_MultiprofileSavingProgress_Progress.Name = "Label__Dialog_MultiprofileSavingProgress_Progress";
            this.Label__Dialog_MultiprofileSavingProgress_Progress.Size = new System.Drawing.Size(51, 13);
            this.Label__Dialog_MultiprofileSavingProgress_Progress.TabIndex = 7;
            this.Label__Dialog_MultiprofileSavingProgress_Progress.Text = "Progress:";
            // 
            // ProgressBar__Dialog_MultiprofileSavingProgress
            // 
            this.ProgressBar__Dialog_MultiprofileSavingProgress.Location = new System.Drawing.Point(48, 40);
            this.ProgressBar__Dialog_MultiprofileSavingProgress.Name = "ProgressBar__Dialog_MultiprofileSavingProgress";
            this.ProgressBar__Dialog_MultiprofileSavingProgress.Size = new System.Drawing.Size(328, 23);
            this.ProgressBar__Dialog_MultiprofileSavingProgress.TabIndex = 4;
            this.ProgressBar__Dialog_MultiprofileSavingProgress.TransitionGraphic = null;
            // 
            // OK_Button
            // 
            this.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.OK_Button.Location = new System.Drawing.Point(3, 3);
            this.OK_Button.Name = "OK_Button";
            this.OK_Button.Size = new System.Drawing.Size(67, 23);
            this.OK_Button.TabIndex = 0;
            this.OK_Button.Text = "OK";
            this.OK_Button.TransitionGraphic = null;
            this.OK_Button.UseVisualStyleBackColor = true;
            this.OK_Button.Visible = false;
            this.OK_Button.Click += new System.EventHandler(this.OK_Button_Click);
            // 
            // Cancel_Button
            // 
            this.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Cancel_Button.Location = new System.Drawing.Point(76, 3);
            this.Cancel_Button.Name = "Cancel_Button";
            this.Cancel_Button.Size = new System.Drawing.Size(67, 23);
            this.Cancel_Button.TabIndex = 1;
            this.Cancel_Button.Text = "Close";
            this.Cancel_Button.TransitionGraphic = null;
            this.Cancel_Button.UseVisualStyleBackColor = true;
            this.Cancel_Button.Click += new System.EventHandler(this.Cancel_Button_Click);
            // 
            // TableLayoutPanel1
            // 
            this.TableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.TableLayoutPanel1.ColumnCount = 2;
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableLayoutPanel1.Controls.Add(this.OK_Button, 0, 0);
            this.TableLayoutPanel1.Controls.Add(this.Cancel_Button, 1, 0);
            this.TableLayoutPanel1.Location = new System.Drawing.Point(269, 138);
            this.TableLayoutPanel1.Name = "TableLayoutPanel1";
            this.TableLayoutPanel1.RowCount = 1;
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableLayoutPanel1.Size = new System.Drawing.Size(146, 29);
            this.TableLayoutPanel1.TabIndex = 3;
            // 
            // Dialog_MultiprofileSavingProgress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 188);
            this.Controls.Add(this.Label__Dialog_MultiprofileSavingProgress_CurrentlySavingProfile);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label__Dialog_MultiprofileSavingProgress_Progress);
            this.Controls.Add(this.ProgressBar__Dialog_MultiprofileSavingProgress);
            this.Controls.Add(this.TableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Dialog_MultiprofileSavingProgress";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Multiple profiles are saving, please wait...";
            this.TableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        #endregion

        internal System.Windows.Forms.Label Label__Dialog_MultiprofileSavingProgress_CurrentlySavingProfile;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label__Dialog_MultiprofileSavingProgress_Progress;
        internal myProgressBar ProgressBar__Dialog_MultiprofileSavingProgress;
        internal button OK_Button;
        internal button Cancel_Button;

        internal System.Windows.Forms.TableLayoutPanel TableLayoutPanel1;
    }
}