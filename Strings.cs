﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProGammaX
    {
    class Strings
        {

            public static bool IsNumeric(object expression)
            {
            if (expression == null)
                return false;

            double testDouble;
            if (double.TryParse(expression.ToString(), out testDouble))
                return true;

            //VB's 'IsNumeric' returns true for any boolean value:
            bool testBool;
            if (bool.TryParse(expression.ToString(), out testBool))
                return true;

            return false;
            }

            public static int InStr(string expression, string searchedExpression)
            {
            if (string.IsNullOrEmpty(expression))
                return 0;
            else
                return (expression.IndexOf(searchedExpression) + 1);
            }

            public static int InStr(int startNumber, string expression, string searchedExpression)
                {
                if (string.IsNullOrEmpty(expression))
                    return 0;
                else
                    return (expression.IndexOf(searchedExpression, startNumber - 1) + 1);
                }

                public static string Right(string expression, int length)
                    {
                    if (string.IsNullOrEmpty(expression))
                        return string.Empty;
                    else if (length >= expression.Length)
                        return expression;
                    else
                        return expression.Substring(expression.Length - length);
                    }
              

            public static string LCase(string expression)
                {
                if (string.IsNullOrEmpty(expression))
                    return string.Empty;
                else
                    return expression.ToLower();
                }

            public static string UCase(string expression)
                {
                if (string.IsNullOrEmpty(expression))
                    return string.Empty;
                else
                    return expression.ToUpper();
                }

            public static string RTrim(string expression)
                {
                if (string.IsNullOrEmpty(expression))
                    return string.Empty;
                else
                    return expression.TrimEnd();
                }

            public static string LTrim(string expression)
                {
                if (string.IsNullOrEmpty(expression))
                    return string.Empty;
                else
                    return expression.TrimStart();
                }

            public static string Trim(string expression)
                {
                if (string.IsNullOrEmpty(expression))
                    return string.Empty;
                else
                    return expression.Trim();
                }

            public static string Replace(string expression, string oldValue, string newValue)
                {
                if (string.IsNullOrEmpty(expression))
                    return string.Empty;
                else
                    return expression.Replace(oldValue, newValue);
                }

            public static string Left ( string expression, int length )
                {
                if(string.IsNullOrEmpty ( expression ))
                    return string.Empty;
                else if(length >= expression.Length)
                    return expression;
                else
                    return expression.Substring ( 0, length );
                }

            public static string Mid(string expression,int start, int length = 0)
                {
                if (string.IsNullOrEmpty(expression))
                    return string.Empty;
                else
                    {
                    //start = start + 1;

                    if (start > expression.Length + 1)
                        {
                        return string.Empty;
                        }
                    if (start < 1)
                        {
                        start = 1;
                        }
                    if (length <= 0 || length >= ((expression.Length + 1) - start))
                    {
                        length = (expression.Length + 1) - start;
                    }


                    return expression.Substring(start - 1, length);
                    }
                }

            public static bool IsNumeric ( string expression)
                {
                if(string.IsNullOrEmpty ( expression ))
                    return false;
                else
                    return expression.ToCharArray ( ).Where ( x => !Char.IsDigit ( x ) ).Count ( ) == 0;
                }

            public static int Len(string expression)
                {
                if(string.IsNullOrEmpty ( expression ))
                    {
                    return 0;
                    }
                else
                    {
                    return expression.Length;
                    }
                }



            public static double Val ( string expression )
                {
                if(expression == null)
                    return 0;

                //try the entire string, then progressively smaller
                //substrings to simulate the behavior of VB's 'Val',
                //which ignores trailing characters after a recognizable value:
                for(int size = expression.Length; size > 0; size--)
                    {
                    double testDouble;
                    if(double.TryParse ( expression.Substring ( 0, size ), out testDouble ))
                        return testDouble;
                    }

                //no value is recognized, so return 0:
                return 0;
                }


            public static double Val ( object expression )
                {
                if(expression == null)
                    return 0;

                double testDouble;
                if(double.TryParse ( expression.ToString ( ), out testDouble ))
                    return testDouble;

                //VB's 'Val' function returns -1 for 'true':
                bool testBool;
                if(bool.TryParse ( expression.ToString ( ), out testBool ))
                    return testBool ? -1 : 0;

                //VB's 'Val' function returns the day of the month for dates:
                System.DateTime testDate;
                if(System.DateTime.TryParse ( expression.ToString ( ), out testDate ))
                    return testDate.Day;

                //no value is recognized, so return 0:
                return 0;
                }

            public static int Val ( char expression )
                {
                int testInt;
                if(int.TryParse ( expression.ToString ( ), out testInt ))
                    return testInt;
                else
                    return 0;
                }



            public static Array ResizeArray(Array oldArray, int newSize)
            {

                //usage:
                //tempParameters = (string[])Strings.ResizeArray(tempParameters, numberOfElements);

                int oldSize = oldArray.Length;
                Type elementType = oldArray.GetType().GetElementType();
                Array newArray = Array.CreateInstance(elementType, newSize);

                int preserveLength = Math.Min(oldSize, newSize);

                if (preserveLength > 0)
                    Array.Copy(oldArray, newArray, preserveLength);

                return newArray;
            }


            public static string[] ResizeStringArray(Array oldArray, int newSize)
            {

                //usage:
                //tempParameters = (string[])Strings.ResizeArray(tempParameters, numberOfElements);

                int oldSize = oldArray.Length;
                Type elementType = oldArray.GetType().GetElementType();
                Array newArray = Array.CreateInstance(elementType, newSize);

                int preserveLength = Math.Min(oldSize, newSize);

                if (preserveLength > 0)
                    Array.Copy(oldArray, newArray, preserveLength);

                return (string[])newArray;
            } 








        }
    }
