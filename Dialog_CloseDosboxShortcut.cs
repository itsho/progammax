﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProGammaX
{
    public partial class Dialog_CloseDosboxShortcut : Form
    {
        public Dialog_CloseDosboxShortcut()
        {
            InitializeComponent();

            ProGammaX.MainForm.mainFormRef.applySelectedTheme(this);

            Dialog_CloseDosboxShortcutRef = this;
        }


        private static Dialog_CloseDosboxShortcut Dialog_CloseDosboxShortcutRef;
        public static Dialog_CloseDosboxShortcut dialog_CloseDosboxShortcutRef
        {
            get
            {
                return Dialog_CloseDosboxShortcut.Dialog_CloseDosboxShortcutRef;
            }
            set
            {
                Dialog_CloseDosboxShortcut.Dialog_CloseDosboxShortcutRef = value;
            }
        }




        private void Close_Button_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }
    }
}
