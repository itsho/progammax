﻿//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Drawing;
//using System.Drawing.Drawing2D;
//using System.Linq;
//using System.Runtime.InteropServices;
//using System.Text;
//using System.Windows.Forms;

//namespace ProGammaX
//{
//    class myComboBox : ComboBox
//    {
//        public myComboBox()
//            : base()
//        {
//            // add event handlers
//            this.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
//            this.DrawItem += new DrawItemEventHandler(ComboBoxWrap_DrawItem);
//            this.MeasureItem += new MeasureItemEventHandler(ComboBoxWrap_MeasureItem);
//            // This call is required by the Windows.Forms Form Designer.
//            //InitializeComponent();

//            initialDropDownWidth = this.DropDownWidth;

//            this.HandleCreated += new EventHandler(BetterComboBox_HandleCreated);
//        }



//        private void UpdateDropDownWidth()
//        {
//            //Create a GDI+ drawing surface to measure string widths
//            System.Drawing.Graphics ds = this.CreateGraphics();

//            //Float to hold largest single item width
//            float maxWidth = 0;

//            //Iterate over each item, measuring the maximum width
//            //of the DisplayMember strings
//            foreach (object item in this.Items)
//            {
//                maxWidth = Math.Max(maxWidth, ds.MeasureString(item.ToString(), this.Font).Width);
//            }

//            //Add a buffer for some white space
//            //around the text
//            maxWidth += 30;

//            //round maxWidth and cast to an int
//            int newWidth = (int)Decimal.Round((decimal)maxWidth, 0);

//            //If the width is bigger than the screen, ensure
//            //we stay within the bounds of the screen
//            if (newWidth > Screen.GetWorkingArea(this).Width)
//            {
//                newWidth = Screen.GetWorkingArea(this).Width;
//            }

//            //Only change the default width if it's smaller
//            //than the newly calculated width
//            if (newWidth > initialDropDownWidth)
//            {
//                this.DropDownWidth = newWidth;
//            }

//            //Clean up the drawing surface
//            ds.Dispose();
//        }


//        //Store the default width to perform check in UpdateDropDownWidth.
//        private int initialDropDownWidth = 0;



//        private void BetterComboBox_HandleCreated(object sender, EventArgs e)
//        {
//            UpdateDropDownWidth();
//        }



//        //protected override void WndProc(ref Message m)
//        //{
//        //    base.WndProc(ref m);
//        //}


//        private const UInt32 WM_CTLCOLORLISTBOX = 0x0134;



//        //protected override void WndProc(ref Message m)
//        //{
//        //    if (m.Msg == WM_CTLCOLORLISTBOX)
//        //    {
//        //        //TODO: Position Drop-down
//        //    }

//        //    base.WndProc(ref m);
//        //}


//        //protected override void WndProc(ref Message m)
//        //{
//        //    if (m.Msg == WM_CTLCOLORLISTBOX)
//        //    {
//        //        // Make sure we are inbounds of the screen
//        //        int left = this.PointToScreen(new Point(0, 0)).X;

//        //        //Only do this if the dropdown is going off right edge of screen
//        //        if (this.DropDownWidth > Screen.PrimaryScreen.WorkingArea.Width - left)
//        //        {
//        //            //TODO: Position Drop-down
//        //        }
//        //    }

//        //    base.WndProc(ref m);
//        //}



//        [DllImport("user32.dll")]
//        static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int
//       X, int Y, int cx, int cy, uint uFlags);

//        [DllImport("gdi32.dll")]
//        static extern uint SetBkColor(IntPtr hdc, int crColor);


//        [DllImport("gdi32.dll", EntryPoint = "CreateSolidBrush", SetLastError = true)]

//        public static extern IntPtr CreateSolidBrush(int crColor);


//        [DllImport("gdi32")]
//        static extern int SetTextColor(IntPtr hdc, int color);


//        [DllImport("user32.dll")]
//        static extern IntPtr GetSysColorBrush(int nIndex);
//        private const int SWP_NOSIZE = 0x1;


//        protected override void WndProc(ref Message m)
//        {
//            if (m.Msg == WM_CTLCOLORLISTBOX)
//            {
//                // Make sure we are inbounds of the screen
//                int left = this.PointToScreen(new Point(0, 0)).X;

//                //Only do this if the dropdown is going off right edge of screen
//                if (this.DropDownWidth > Screen.PrimaryScreen.WorkingArea.Width - left)
//                {
//                    // Get the current combo position and size
//                    Rectangle comboRect = this.RectangleToScreen(this.ClientRectangle);

//                    int dropHeight = 0;
//                    int topOfDropDown = 0;
//                    int leftOfDropDown = 0;

//                    //Calculate dropped list height
//                    for (int i = 0; (i < this.Items.Count && i < this.MaxDropDownItems); i++)
//                    {
//                        dropHeight += this.ItemHeight;
//                    }

//                    //Set top position of the dropped list if 
//                    //it goes off the bottom of the screen
//                    if (dropHeight > Screen.PrimaryScreen.WorkingArea.Height -
//                       this.PointToScreen(new Point(0, 0)).Y)
//                    {
//                        topOfDropDown = comboRect.Top - dropHeight - 2;
//                    }
//                    else
//                    {
//                        topOfDropDown = comboRect.Bottom;
//                    }

//                    //Calculate shifted left position
//                    leftOfDropDown = comboRect.Left - (this.DropDownWidth -
//                       (Screen.PrimaryScreen.WorkingArea.Width - left));

//                    // Postioning/sizing the drop-down
//                    //SetWindowPos(HWND hWnd,
//                    //      HWND hWndInsertAfter,
//                    //      int X,
//                    //      int Y,
//                    //      int cx,
//                    //      int cy,
//                    //      UINT uFlags);
//                    //when using the SWP_NOSIZE flag, cx and cy params are ignored
//                    SetWindowPos(m.LParam,
//                       IntPtr.Zero,
//                       leftOfDropDown,
//                       topOfDropDown,
//                       0,
//                       0,
//                       SWP_NOSIZE);
//                }
//            }


//            //if (m.Msg == 0x0013 || m.Msg == 0x0014)
//            //{
//            //    IntPtr hdc = GetDCEx(m.HWnd, (IntPtr)1, 1 | 0x0013);

//            //    if (hdc != IntPtr.Zero)
//            //    {
//            //        Graphics graphics = Graphics.FromHdc(hdc);
//            //        Color borderColor = Color.Blue;
//            //        Rectangle rectangle = new Rectangle(0, 0, this.Width, this.Height);
//            //        ControlPaint.DrawSelectionFrame(graphics, false,rectangle, rectangle, Color.OrangeRed);
//            //        m.Result = (IntPtr)1;
//            //        ReleaseDC(m.HWnd, hdc);
//            //    }
//            //}

//            if (m.Msg == 0x000F) // this is the WM_PAINT message 
//            {

//                using (Graphics g = CreateGraphics())
//                {
//                    if (string.IsNullOrEmpty(Text) && !Focused)
//                    {
//                        SizeF size = g.MeasureString("Marek", Font);
//                        //draw background text  
//                        g.DrawString("Marek", Font, Brushes.LightGray, new PointF(0, (Height - size.Height) / 2));
//                    }
//                }  
//            }

//            if (m.Msg == WM_CTLCOLOREDIT)
//            {



//                IntPtr hdcEdit = m.WParam;
//                IntPtr hEdit = m.LParam;
//                if (hEdit != IntPtr.Zero && hdcEdit != IntPtr.Zero)
//                {
//                    //DRAWITEMSTRUCT tItem = default(DRAWITEMSTRUCT);
//                    //CopyMemory(hdcEdit, hEdit, Strings.Len(hdcEdit));
//                    //lBackBrush = System.Convert.ToInt64(CreateSolidBrush(ColorToCOLORREF(Color.Red)));



//                        //IntPtr hdc = GetDCEx(m.HWnd, (IntPtr)1, 1 | 0x0020);
//                        //if (hdc != IntPtr.Zero)
//                        //{
//                        //    Graphics graphics = Graphics.FromHdc(hdc);
//                        //    Color borderColor = Color.Blue;
//                        //    Rectangle rectangle = new Rectangle(0, 0, this.Width, this.Height);
//                        //    ControlPaint.DrawSelectionFrame(graphics, false, rectangle, rectangle, Color.OrangeRed);
//                        //}

//                        SetBkColor(m.WParam, ColorToCOLORREF(ThemedColors.WindowColor));
//                        SetTextColor(m.WParam, ColorToCOLORREF(ThemedColors.WindowTextColor));     //font color is red

//                    //
//                    //FillRect(tItem, new RECT(), System.Convert.ToInt64(CreateSolidBrush(ColorToCOLORREF(Color.Red))));
//                    m.Result = hBrush;
//                    return;
//                }
//            }



//            if (this.Enabled)
//            {
//                this.BackColor = ThemedColors.WindowColor;
//            }
//            else
//            {
//                this.BackColor = ThemedColors.BackColor;
//            }

//            base.WndProc(ref m);
//        }

//        //public DRAWITEMSTRUCT tItem = default(DRAWITEMSTRUCT);
//        //public string sItem = default(string);







//        protected override void OnLostFocus(System.EventArgs e)
//        {
//            base.OnLostFocus(e);
//            this.Invalidate();
//        }

//        protected override void OnGotFocus(System.EventArgs e)
//        {
//            base.OnGotFocus(e);
//            this.Invalidate();
//        }
//        protected override void OnResize(EventArgs e)
//        {
//            base.OnResize(e);
//            this.Invalidate();
//        }









//        void ComboBoxWrap_MeasureItem(object sender, MeasureItemEventArgs e)
//        {
//            // set the height of the item, using MeasureString with the font and control width
//            //myComboBox ddl = (myComboBox)sender;
//            //string text = ddl.Items[e.Index].ToString();
//            //SizeF size = e.Graphics.MeasureString(text, this.Font, ddl.DropDownWidth); 
//            //e.ItemHeight = (int)Math.Ceiling(size.Height) + 1;  // plus one for the border
//            //e.ItemWidth = ddl.DropDownWidth;
//            //System.Diagnostics.Trace.WriteLine(String.Format("Height {0}, Text {1}", e.ItemHeight, text));


//            Font TempFont = this.Font;
//            Font bFont = new Font(TempFont, FontStyle.Bold);
//            //Dim lFont As New Font("Arial", 12, FontStyle.Bold)
//            SizeF siText;
//            // set the height of the item, using MeasureString with the font and control width
//            myComboBox ddl = (myComboBox)sender;
//            string text = ddl.Items[e.Index].ToString();
//            if (ddl.Items[e.Index] == ddl.SelectedItem)
//            {
//                siText = e.Graphics.MeasureString((string)ddl.Items[e.Index], bFont);
//            }
//            else
//            {
//                siText = e.Graphics.MeasureString((string)ddl.Items[e.Index], TempFont);
//            }

//            e.ItemHeight = (int)siText.Height;
//            e.ItemWidth = (int)siText.Width;





//        }
//        protected override void OnBackColorChanged(EventArgs e)
//        {
//            base.OnBackColorChanged(e);
//        }
//        void ComboBoxWrap_DrawItem(object sender, DrawItemEventArgs e)
//        {



//            if (e.Index < 0 || e.State == DrawItemState.ComboBoxEdit)
//            {
//                return;
//            }


//            e.DrawBackground();
//            //e.DrawFocusRectangle();

//            Brush b = null;

//            // draw a lighter blue selected BG colour, the dark blue default has poor contrast with black text on a dark blue background
//            //if (e.State == DrawItemState.Selected)
//            //{
//            //    e.Graphics.FillRectangle(new SolidBrush(ThemedColors.SelectionColor), e.Bounds);
//            //    b = new SolidBrush(ThemedColors.SelectionForeColor);
//            //}
//            //else
//            //{
//            //    e.Graphics.FillRectangle(new SolidBrush(ThemedColors.WindowColor), e.Bounds);
//            //    b = new SolidBrush(ThemedColors.WindowTextColor);
//            //}
//                // draw the separator line
//            if (e.State == DrawItemState.Selected)
//            {
//                Rectangle separatorRect = e.Bounds;

//                // fill the background behind the separator
//                using (Brush br = new SolidBrush(ThemedColors.SelectionColor))
//                {
//                    e.Graphics.FillRectangle(br, separatorRect);
//                }
//            }
//            // get the text of the item
//            myComboBox ddl = (myComboBox)sender;
//            string text = ddl.Items[e.Index].ToString();
//            b = new SolidBrush(ThemedColors.WindowTextColor);
//            e.Graphics.DrawString(text, this.Font, b, e.Bounds, StringFormat.GenericDefault);

//            //// draw a light grey border line to separate the items
//            //Pen p = new Pen(new SolidBrush(ThemedColors.WindowColor), 1);
//            //e.Graphics.DrawLine(p, new Point(e.Bounds.Left, e.Bounds.Bottom - 1), new Point(e.Bounds.Right, e.Bounds.Bottom - 1));
//            //p.Dispose();





//        }







//        int WM_CTLCOLOREDIT = 0x133;
//        IntPtr hBrush = IntPtr.Zero;

//        public static int ColorToCOLORREF(Color color)
//        {
//            return ((color.R | (color.G << 8)) | (color.B << 0x10));
//        }





//        //protected override void WndProc(ref Message m)
//        //{
//        //    if (m.Msg == WM_CTLCOLOREDIT)
//        //    {
//        //        IntPtr hdcEdit = m.WParam;
//        //        IntPtr hEdit = m.LParam;
//        //        if (hEdit != IntPtr.Zero && hdcEdit != IntPtr.Zero)
//        //        {
//        //            hBrush = CreateSolidBrush(ColorToCOLORREF(Color.Blue));
//        //            SetBkColor(hdcEdit, ColorToCOLORREF(Color.Blue));
//        //            m.Result = hBrush;
//        //            return;
//        //        }
//        //    }
//        //    base.WndProc(ref m);
//        //}



//        //[DllImport("user32", ExactSpelling = true, CharSet = CharSet.Ansi, SetLastError = true)]
//        //public static extern long FillRect(long hdc, RECT lpRect, long hBrush);



//        //public struct RECT
//        //{

//        //    private long Left;

//        //    private long Top;

//        //    private long Right;

//        //    private long Bottom;
//        //}
//        //public struct DRAWITEMSTRUCT
//        //{

//        //    private long CtlType;

//        //    private long CtlID;

//        //    private long itemID;

//        //    private long itemAction;

//        //    private long itemState;

//        //    private long hwndItem;

//        //    private long hdc;

//        //    private RECT rcItem;

//        //    private long itemData;
//        //}


//        //[DllImport("kernel32.dll", EntryPoint = "RtlMoveMemory")]
//        //public static extern void CopyMemory(string Destination, string Source, long Length);

//        //private static int WM_NCPAINT = 0x0085;
//        //private static int WM_ERASEBKGND = 0x0013;
//        //private static int WM_PAINT = 0x000F;

//        //[DllImport("user32.dll")]
//        //static extern IntPtr GetDCEx(IntPtr hwnd, IntPtr hrgnclip, uint fdwOptions);

//        //[DllImport("user32.dll")]
//        //static extern int ReleaseDC(IntPtr hwnd, IntPtr hDC);






//        //[DllImport("kernel32", EntryPoint = "RtlMoveMemory", ExactSpelling = true, CharSet = CharSet.Ansi, SetLastError = true)]
//        //public static extern void CopyMemory(Any Destination, Any Source, long Length);

////        [DllImport("user32", EntryPoint = "SetWindowLongA", ExactSpelling = true, CharSet = CharSet.Ansi, SetLastError = true)]
////        public static extern long SetWindowLong(long hwnd, long nIndex, long dwNewLong);
////        [DllImport("user32", EntryPoint = "CallWindowProcA", ExactSpelling = true, CharSet = CharSet.Ansi, SetLastError = true)]
////        public static extern long CallWindowProc(long lpPrevWndFunc, long hwnd, long Msg, long wParam, long lParam);
////        [DllImport("user32", EntryPoint = "SendMessageA", ExactSpelling = true, CharSet = CharSet.Ansi, SetLastError = true)]
////        public static extern long SendMessage(long hwnd, long wMsg, long wParam, Any lParam);
////        [DllImport("gdi32", ExactSpelling = true, CharSet = CharSet.Ansi, SetLastError = true)]
////        public static extern long CreateSolidBrush(long crColor);
////        [DllImport("user32", ExactSpelling = true, CharSet = CharSet.Ansi, SetLastError = true)]
////        public static extern long FillRect(long hdc, RECT lpRect, long hBrush);
////        [DllImport("gdi32", ExactSpelling = true, CharSet = CharSet.Ansi, SetLastError = true)]
////        public static extern long DeleteObject(long hObject);
////        [DllImport("gdi32", ExactSpelling = true, CharSet = CharSet.Ansi, SetLastError = true)]
////        public static extern long SetBkColor(long hdc, long crColor);
////        [DllImport("gdi32", ExactSpelling = true, CharSet = CharSet.Ansi, SetLastError = true)]
////        public static extern long SetTextColor(long hdc, long crColor);
////        [DllImport("gdi32", EntryPoint = "TextOutA", ExactSpelling = true, CharSet = CharSet.Ansi, SetLastError = true)]
////        public static extern long TextOut(long hdc, long x, long y, string lpString, long nCount);
////        [DllImport("user32", ExactSpelling = true, CharSet = CharSet.Ansi, SetLastError = true)]
////        public static extern long DrawFocusRect(long hdc, RECT lpRect);
////        [DllImport("user32", ExactSpelling = true, CharSet = CharSet.Ansi, SetLastError = true)]
////        public static extern long GetSysColor(long nIndex);

////        public const int COLOR_HIGHLIGHT = 13;
////        public const int COLOR_HIGHLIGHTTEXT = 14;
////        public const int COLOR_WINDOW = 5;
////        public const int COLOR_WINDOWTEXT = 8;
////        public const int LB_GETTEXT = 0x189;
////        public const int WM_DRAWITEM = 0x2B;
////        public const object GWL_WNDPROC = (-4);
////        public const int ODS_FOCUS = 0x10;
////        public const int ODT_LISTBOX = 2;

////        public long lPrevWndProc;

////        public long SubClassedList(long hwnd, long Msg, long wParam, long lParam)
////{
////    long returnValue = default(long);
////    DRAWITEMSTRUCT tItem = default(DRAWITEMSTRUCT);
////    string sBuff = default(string);
////    string sItem = default(string);
////    long lBack = default(long);

////    if (Msg == WM_DRAWITEM)
////    {
////        //Redraw the listbox
////        //This function only passes the Address of the DrawItem Structure, so we need to
////        //use the CopyMemory API to Get a Copy into the Variable we setup:
////        CopyMemory(tItem, ByVal lParam, Strings.Len(tItem));
////        //Make sure we're dealing with a Listbox
////        if (tItem.CtlType == ODT_LISTBOX)
////        {
////            //Get the Item Text
////            SendMessage(tItem.hwndItem, LB_GETTEXT, tItem.itemID, ByVal sBuff);
////            sItem = sBuff.Substring(0, sBuff.IndexOf(('\0').ToString()) + 0);
////            if (tItem.itemState & ODS_FOCUS)
////            {
////                //Item has Focus, Highlight it, I'm using the Default Focus
////                //Colors for this example.
////                lBack = System.Convert.ToInt64(CreateSolidBrush(GetSysColor(COLOR_HIGHLIGHT)));
////                FillRect(tItem.hdc, tItem.rcItem, lBack);
////                SetBkColor(tItem.hdc, GetSysColor(COLOR_HIGHLIGHT));
////                SetTextColor(tItem.hdc, GetSysColor(COLOR_HIGHLIGHTTEXT));
////                TextOut (tItem.hdc, tItem.rcItem.Left, tItem.rcItem.Top, ByVal sItem, sItem.Length);
////                DrawFocusRect (tItem.hdc, tItem.rcItem);
////            }
////            else
////            {
////                //Item Doesn't Have Focus, Draw it's Colored Background
////                //Create a Brush using the Color we stored in ItemData
////                lBack = System.Convert.ToInt64(CreateSolidBrush(tItem.itemData));
////                //Paint the Item Area
////                FillRect(tItem.hdc, tItem.rcItem, lBack);
////                //Set the Text Colors
////                SetBkColor(tItem.hdc, tItem.itemData);
////                SetTextColor(tItem.hdc, (tItem.itemData == vbBlack ? vbWhite : vbBlack));
////                //Display the Item Text
////                TextOut (tItem.hdc, tItem.rcItem.Left, tItem.rcItem.Top, ByVal sItem, sItem.Length);
////            }
////            DeleteObject(lBack);
////            //Don't Need to Pass a Value on as we've just handled the Message ourselves
////            returnValue = 0;
////            return returnValue;

////        }

////    }
////    returnValue = System.Convert.ToInt64(CallWindowProc(lPrevWndProc, hwnd, Msg, wParam, lParam));
////    return returnValue;
////}












//    }











//}












using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
namespace ProGammaX
{



    public class myComboBox : ComboBox
    {


        #region Constants
        // messages
        private const int WM_MOUSEMOVE = 0x200;
        private const int WM_MOUSELEAVE = 0x2A3;
        private const int WM_NCMOUSEMOVE = 0xA0;
        private const int WM_LBUTTONDOWN = 0x201;
        private const int WM_RBUTTONDOWN = 0x204;
        private const int WM_DRAWITEM = 0x002B;
        private const int WM_NCPAINT = 0x0085;    // WM_NCPAINT message
        private const int WM_ERASEBKGND = 0x0014; // WM_ERASEBKGND message
        private const int WM_PAINT = 0x000F;      // WM_PAINT message
        private const int WM_COMMAND = 0x0111;
        private const int WM_USER = 0x0400;
        private const int WM_REFLECT = WM_USER + 0x1C00;
        private const int CBN_DROPDOWN = 7;
        private const int CB_GETDROPPEDSTATE = 0x157;


        // combobox messages
        private const int CB_GETCOMBOBOXINFO = 0x164;
        // message handler
        private static IntPtr MSG_HANDLED = new IntPtr(1);
        #endregion

        #region Enums
        private enum COMBOBOXSTYLESTATES
        {
            CBXS_NORMAL = 1,
            CBXS_HOT = 2,
            CBXS_PRESSED = 3,
            CBXS_DISABLED = 4,
        };

        private enum ComboBoxButtonState
        {
            STATE_SYSTEM_NONE = 0,
            STATE_SYSTEM_INVISIBLE = 0x00008000,
            STATE_SYSTEM_PRESSED = 0x00000008
        }
        #endregion

        #region Structs
        [StructLayout(LayoutKind.Sequential)]
        private struct PAINTSTRUCT
        {
            internal IntPtr hdc;
            internal int fErase;
            internal RECT rcPaint;
            internal int fRestore;
            internal int fIncUpdate;
            internal int Reserved1;
            internal int Reserved2;
            internal int Reserved3;
            internal int Reserved4;
            internal int Reserved5;
            internal int Reserved6;
            internal int Reserved7;
            internal int Reserved8;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            internal RECT(int X, int Y, int Width, int Height)
            {
                this.Left = X;
                this.Top = Y;
                this.Right = Width;
                this.Bottom = Height;
            }
            internal int Left;
            internal int Top;
            internal int Right;
            internal int Bottom;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct COMBOBOXINFO
        {
            internal Int32 cbSize;
            internal RECT rcItem;
            internal RECT rcButton;
            internal ComboBoxButtonState buttonState;
            internal IntPtr hwndCombo;
            internal IntPtr hwndEdit;
            internal IntPtr hwndList;
        }
        #endregion
        #region API
        //[DllImport("user32.dll")]
        //private static extern IntPtr BeginPaint(IntPtr hWnd, ref PAINTSTRUCT ps);

        //[DllImport("user32.dll")]
        //[return: MarshalAs(UnmanagedType.Bool)]
        //private static extern bool EndPaint(IntPtr hWnd, ref PAINTSTRUCT ps);

        //[DllImport("user32.dll")]
        //[return: MarshalAs(UnmanagedType.Bool)]
        //private static extern bool ValidateRect(IntPtr hWnd, ref RECT lpRect);

        [DllImport("gdi32.dll")]
        private static extern IntPtr SelectObject(IntPtr hdc, IntPtr hObject);

        //[DllImport("gdi32.dll")]
        //[return: MarshalAs(UnmanagedType.Bool)]
        //private static extern bool BitBlt(IntPtr hdc, int nXDest, int nYDest, int nWidth, int nHeight, IntPtr hdcSrc, int nXSrc, int nYSrc, int dwRop);

        //[DllImport("gdi32.dll")]
        //[return: MarshalAs(UnmanagedType.Bool)]
        //private static extern bool StretchBlt(IntPtr hDest, int X, int Y, int nWidth, int nHeight, IntPtr hdcSrc,
        //int sX, int sY, int nWidthSrc, int nHeightSrc, int dwRop);

        [DllImport("user32.dll")]
        private static extern IntPtr GetDC(IntPtr handle);

        [DllImport("user32.dll")]
        private static extern int ReleaseDC(IntPtr handle, IntPtr hdc);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll")]
        private static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, ref COMBOBOXINFO lParam);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetWindowRect(IntPtr hWnd, ref RECT lpRect);

        [DllImport("user32.dll")]
        private extern static int OffsetRect(ref RECT lpRect, int x, int y);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetCursorPos(ref Point lpPoint);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool PtInRect([In] ref RECT lprc, Point pt);

        [DllImport("user32.dll")]
        private static extern bool GetComboBoxInfo(IntPtr hWnd, ref COMBOBOXINFO pcbi);

        //[DllImport("gdi32.dll")]
        //static extern uint SetBkColor(IntPtr hdc, int crColor);


        //[DllImport("gdi32.dll", EntryPoint = "CreateSolidBrush", SetLastError = true)]
        //public static extern IntPtr CreateSolidBrush(int crColor);


        //[DllImport("gdi32")]
        //static extern int SetTextColor(IntPtr hdc, int color);

        private static int HIWORD(int lParam)
        {
            return ((lParam >> 16) & 0xffff);
        }

        #endregion
        #region Fields
        //private bool _bPainting = false;
        private bool _bMoved = false;
        //private bool _bFading = false;
        private IntPtr _hComboWnd = IntPtr.Zero;
        private cStoreDc _cComboboxDc = new cStoreDc();
        private Bitmap _oComboboxBitmap;
        private Bitmap _oMask;


        /// <summary>
        /// Gets a handle to the combobox
        /// </summary>
        private IntPtr HwndCombo
        {
            get
            {
                COMBOBOXINFO pcbi = new COMBOBOXINFO();
                pcbi.cbSize = System.Runtime.InteropServices.Marshal.SizeOf(pcbi);
                GetComboBoxInfo(this.Handle, ref pcbi);
                return pcbi.hwndCombo;
            }
        }
        /// <summary>
        /// Gets a handle to the combo's drop-down list
        /// </summary>
        private IntPtr HwndDropDown
        {
            get
            {
                COMBOBOXINFO pcbi = new COMBOBOXINFO();
                pcbi.cbSize = System.Runtime.InteropServices.Marshal.SizeOf(pcbi);
                GetComboBoxInfo(this.Handle, ref pcbi);
                return pcbi.hwndList;
            }
        }

        //private NativeListWindow listControl;

        #endregion

        #region Properties
        /// <summary>
        /// Get/Set Combobox bitmap.
        /// </summary>
        public Bitmap ComboboxGraphic
        {
            get { return _oComboboxBitmap; }
            set
            {
                _oComboboxBitmap = value;
                if (_cComboboxDc.Hdc != IntPtr.Zero)
                {
                    _cComboboxDc.Dispose();
                    _cComboboxDc = new cStoreDc();
                }
                _cComboboxDc.Width = _oComboboxBitmap.Width;
                _cComboboxDc.Height = _oComboboxBitmap.Height;
                SelectObject(_cComboboxDc.Hdc, _oComboboxBitmap.GetHbitmap());

            }
        }


        /// <summary>
        /// Fader graphic
        /// </summary>
        private Bitmap TransitionGraphic
        {
            get { return _oMask; }
            set { _oMask = value; }
        }
        #endregion
        //private Brush BorderBrush = new SolidBrush(ThemedColors.WindowColor);
        //private Brush ArrowBrush = new SolidBrush(ThemedColors.SelectionForeColor);
        //private Brush DropButtonBrush = new SolidBrush(ThemedColors.SelectionColor);

        //private Color _ButtonColor = ThemedColors.WindowColor;
        //public Color ButtonColor
        //{
        //    get { return _ButtonColor; }
        //    set
        //    {
        //        _ButtonColor = value;
        //        DropButtonBrush = new SolidBrush(this.ButtonColor);
        //        this.Invalidate();
        //    }
        //}
        public static int ColorToCOLORREF(Color color)
        {
            return ((color.R | (color.G << 8)) | (color.B << 0x10));

        }
        //private const int WM_PAINT = 0xf;
        private const int WM_CTLCOLOREDIT = 0x133;
        private SolidBrush _backBrush;
        //internal cInternalScrollBar vScrollBar;
        NativeListWindow listControl;



        public myComboBox()
            : base()
        {



            //if (MainForm.mainFormRef.checkBox_OPTIONS_Themes_UseSkinOnControls.Checked)
            //{
                this.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
                //this.DrawItem += new DrawItemEventHandler(ComboBoxWrap_DrawItem);
                //force repaint comboboxes:
                this.FlatStyle = System.Windows.Forms.FlatStyle.Flat;

                base.SetStyle(ControlStyles.AllPaintingInWmPaint, true);

            //}

            ////BackColor = ThemedColors.WindowColor;
            //DestroyBrush();
            this.DoubleBuffered = true;
            //this.SetStyle(ControlStyles.UserPaint, true);


            //base.SetStyle(ControlStyles.DoubleBuffer, true);

            //base.SetStyle(ControlStyles.UserPaint, true);

            //this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            //base.SetStyle(ControlStyles.ResizeRedraw, true);

            //Application.Idle += Application_Idle;


            //this.SetStyle(ControlStyles.UserPaint | ControlStyles.OptimizedDoubleBuffer |
            //    ControlStyles.AllPaintingInWmPaint | ControlStyles.SupportsTransparentBackColor,
            //    true);



            //this.Invalidate();
            //this.Refresh();
        }

        //internal cComboBox combobox;
        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);
            COMBOBOXINFO combInfo = new COMBOBOXINFO();
            combInfo.cbSize = Marshal.SizeOf(combInfo);
            GetComboBoxInfo(this.Handle, ref combInfo);

            listControl = new NativeListWindow(this, combInfo.hwndList);





            RECT comboBoxRectangle = new RECT();
            GetWindowRect((IntPtr)this.HwndCombo, ref comboBoxRectangle);

            // get coordinates of combo's drop down list
            RECT dropDownListRectangle = new RECT();
            GetWindowRect((IntPtr)this.HwndDropDown, ref dropDownListRectangle);




            try
            {






                ComboboxGraphic = MainForm.mainFormRef.vienna_combo;
                Bitmap fader = MainForm.mainFormRef.fader;

                if (fader != null)
                    TransitionGraphic = fader;


            }
            catch
            {


            }


            //try
            //{

            //    combobox = new cComboBox(this.Handle, ProGammaX.MainForm.mainFormRef.vienna_combo, ProGammaX.MainForm.mainFormRef.vienna_ScrollHorzShaft, ProGammaX.MainForm.mainFormRef.vienna_ScrollHorzArrow, ProGammaX.MainForm.mainFormRef.vienna_ScrollHorzThumb, ProGammaX.MainForm.mainFormRef.vienna_ScrollVertShaft, ProGammaX.MainForm.mainFormRef.vienna_ScrollVertArrow, ProGammaX.MainForm.mainFormRef.vienna_ScrollVertThumb, ProGammaX.MainForm.mainFormRef.fader);

            //}
            //catch
            //{

            //}


        }

        protected override void OnTextChanged(EventArgs e)
        {
            base.OnTextChanged(e);

            if (this.Visible) this.Refresh();
        }
        protected override void OnClick(EventArgs e)
        {
            
            base.OnClick(e);
            if (this.Visible) this.Refresh();
        }


        //IntPtr hBrush = IntPtr.Zero;







        //private void drawControl(bool focused)
        //{
        //    drawBorder(focused == true ? COLOR_FOCUSED : COLOR_NOT_FOCUSED);
        //    drawButton(focused == true ? COLOR_FOCUSED : COLOR_NOT_FOCUSED);
        //}

        //private void drawButton(Color color)
        //{
        //    Graphics grfx = Graphics.FromHwnd(this.Handle);
        //    Rectangle rect = this.ClientRectangle;

        //    if (color == COLOR_FOCUSED)
        //        drawBorder(COLOR_FOCUSED);
        //    else
        //    {
        //        drawBorder(COLOR_NOT_FOCUSED);
        //        Rectangle child = new Rectangle(rect.Right - 16, rect.Top, 15,
        //          rect.Height);
        //        ControlPaint.DrawComboButton
        //          (grfx, rect.Right - 16, rect.Top + 2, 15, 20, ButtonState.Flat);
        //    }
        //}
        private void drawBorder(Color color)
        {


            Graphics g = Graphics.FromHwnd(this.Handle);
            Rectangle client = this.ClientRectangle;
            Rectangle child = new Rectangle(client.Right - 16, client.Top, 16,
              client.Height);
            //SolidBrush brush;

            //if (this.Focused)
            //    brush = new SolidBrush(ThemedColors.BackColor);
            //else
            //    brush = new SolidBrush(ThemedColors.ForeColor);

            //if (color == COLOR_FOCUSED)
            //{
            //    // fill the drop down arrow box and fill the arrow with the right color
            //    g.FillRectangle(new SolidBrush(COLOR_FOCUSED_FILL), child);
            //    g.FillPolygon(brush, new Point[]
            //    {
            //      new Point(client.Right - 11, client.Top + 11),
            //      new Point(client.Right - 6, client.Top + 11),
            //      new Point(client.Right - 9, client.Top + 14),
            //      new Point(client.Right - 11, client.Top + 11)
            //    });
            //}

            ControlPaint.DrawBorder(g, client, color, ButtonBorderStyle.Solid);
            ControlPaint.DrawBorder(g, child, color, ButtonBorderStyle.Solid);

            g.Dispose();


        }



        #region Methods
        private void drawCombo()
        {




            if (_oComboboxBitmap == null)
            {
                try
                {
                    ComboboxGraphic = MainForm.mainFormRef.vienna_combo;
                }
                catch
                {
                    return;
                }
                


            }

            isPainting = true;
            if (this.Handle == IntPtr.Zero) return;
            int offset = 0;
            int width = _oComboboxBitmap.Width / 4;
            RECT tr = new RECT();
            GetWindowRect(this.Handle, ref tr);
            OffsetRect(ref tr, -tr.Left, -tr.Top);
            Rectangle cr = new Rectangle(0, 0, tr.Right, tr.Bottom);
            ComboBox cb = (ComboBox)Control.FromHandle(this.Handle);








            // get button size
            tr = comboButton();
            // backfill and border
            //using (Graphics g = Graphics.FromHwnd(this.Handle))
            //{
            //    using (Brush flatBrush = new SolidBrush(cb.Enabled ? ThemedColors.SelectionColor : ThemedColors.DisabledMenuTextColor))
            //        g.FillRectangle(flatBrush, cr);
            //    cr.Height--;
            //    cr.Width--;
            //    using (Pen borderPen = new Pen(ThemedColors.ForeColor, 0.5f))
            //    {
            //        g.DrawLine(borderPen, cr.X, cr.Y + 1, cr.X, cr.Height);
            //        g.DrawLine(borderPen, cr.X, cr.Height, cr.Width, cr.Height);
            //    }
            //    using (Pen borderPen = new Pen(ThemedColors.BackColor, 0.5f))
            //    {
            //        g.DrawLine(borderPen, cr.X, cr.Y, cr.Width, cr.Y);
            //        g.DrawLine(borderPen, cr.Width, cr.Y, cr.Width, cr.Height - 1);
            //    }
            //}


            //drawItemBackGround();
            

            if (!cb.Enabled)
            {
                drawBorder(ThemedColors.ForeColor);
                offset = width * 3;
            }
            else
            {
                drawBorder(ThemedColors.ForeColor);
                if (cb.DroppedDown)
                {
                    offset = width * 1;






                }
                else if (cb.Focused || _bMoved)
                    offset = width * 2;
                else
                    offset = 0;
            }
            // draw the image
            IntPtr hdc = GetDC(this.Handle);



            using (StretchImage si = new StretchImage(_cComboboxDc.Hdc, hdc, new Rectangle(offset, 0, width, _cComboboxDc.Height), new Rectangle(tr.Left, tr.Top, tr.Right - tr.Left, tr.Bottom - tr.Top), 2, StretchModeEnum.STRETCH_HALFTONE)) { }


            //if (!this.DroppedDown)
            //    drawItem();


            ReleaseDC(this.Handle, hdc);

            
            isPainting = false;

        }


        //public static void DrawText(IDeviceContext dc, string text, Font font, Point pt, Color foreColor);





        private RECT comboButton()
        {

            int width = _oComboboxBitmap.Width / 4;
            COMBOBOXINFO cbi = new COMBOBOXINFO();
            RECT tr = new RECT();

            // get button size
            cbi.cbSize = Marshal.SizeOf(cbi);
            if (SendMessage(this.Handle, CB_GETCOMBOBOXINFO, 0, ref cbi) != 0)
            {
                tr = cbi.rcButton;
            }
            else
            {
                ComboBox cb = (ComboBox)Control.FromHandle(this.Handle);
                tr = new RECT(cb.ClientRectangle.Width - width, 1, width, cb.ClientRectangle.Height - 2);
            }
            return tr;
        }

        private bool overButton()
        {
            RECT wr = new RECT();
            Point pos = new Point();

            GetWindowRect(this.Handle, ref wr);
            RECT tr = comboButton();
            OffsetRect(ref tr, wr.Left, wr.Top);
            GetCursorPos(ref pos);
            if (PtInRect(ref tr, pos))
                return true;
            return false;
        }
        #endregion

        //private Color COLOR_FOCUSED = ThemedColors.ForeColor;
        //private Color COLOR_NOT_FOCUSED = ThemedColors.BackColor;
        //private Color COLOR_FOCUSED_FILL = ThemedColors.SelectionForeColor;





        //private bool focused = false;
        protected override void OnVisibleChanged(EventArgs e)
        {
            base.OnVisibleChanged(e);
            if (this.Visible)
            {
                drawCombo();
                this.Refresh();
            }
        }
        protected override void OnDropDownClosed(EventArgs e)
        {
            base.OnDropDownClosed(e);
            this.Parent.Refresh();
        }
        protected override void OnEnabledChanged(EventArgs e)
        {
            base.OnEnabledChanged(e);
            this.Refresh();
        }

        //protected override void OnMouseHover(EventArgs e)
        //{
        //    base.OnMouseHover(e);
        //    focused = this.Focused;
        //}
        //protected override void OnMouseUp(MouseEventArgs e)
        //{
        //    base.OnMouseUp(e);
        //    focused = this.Focused;
        //}

        //protected override void OnLostFocus(EventArgs e)
        //{
        //    base.OnLostFocus(e);
        //    focused = false;
        //}

        //protected override void OnMouseEnter(System.EventArgs e)
        //{

        //    base.OnMouseEnter(e);
        //    focused = this.Focused;
        //}

        //protected override void OnMouseLeave(System.EventArgs e)
        //{
        //    base.OnMouseLeave(e);
        //    focused = this.Focused;
        //}

        //protected override void OnGotFocus(System.EventArgs e)
        //{
        //    base.OnGotFocus(e);
        //    focused = this.Focused;
        //}


        //protected override void OnClick(System.EventArgs e)
        //{
        //    base.OnClick(e);
        //    focused = false;

        //}
        //protected override void OnDropDown(EventArgs e)
        //{
        //    base.OnDropDown(e);
        //    focused = true;

        //}








        //protected override void OnPaint(PaintEventArgs e)
        //{
        //    if (DroppedDown)
        //        ButtonRenderer.DrawButton(CreateGraphics(), new System.Drawing.Rectangle(ClientRectangle.X - 1, ClientRectangle.Y - 1, ClientRectangle.Width + 2, ClientRectangle.Height + 2), PushButtonState.Pressed);
        //    else
        //        ButtonRenderer.DrawButton(CreateGraphics(), new System.Drawing.Rectangle(ClientRectangle.X - 1, ClientRectangle.Y - 1, ClientRectangle.Width + 2, ClientRectangle.Height + 2), PushButtonState.Normal);
        //    if (SelectedIndex != -1)
        //    {
        //        Font font;
        //        if (SelectedItem.ToString().Equals("lol"))
        //            font = new Font(this.Font, FontStyle.Bold);
        //        else
        //            font = new Font(this.Font, FontStyle.Regular);
        //        e.Graphics.DrawString(Text, font, new SolidBrush(Color.Red), 3, 3);
        //    }
        //    if (DroppedDown)
        //        this.CreateGraphics().DrawImageUnscaled(new Bitmap(_oComboboxBitmap), ClientRectangle.Width - 13, ClientRectangle.Height - 12);
        //    else
        //        this.CreateGraphics().DrawImageUnscaled(new Bitmap(_oComboboxBitmap), ClientRectangle.Width - 13, ClientRectangle.Height - 12);
        //    base.OnPaint(e);
        //}





        private bool isPainting = false;
        protected override void WndProc(ref Message m)
        {


            //if (m.Msg == (WM_REFLECT + WM_COMMAND))
            //{
            //    if (HIWORD((int)m.WParam) == CBN_DROPDOWN)
            //    {
            //        COMBOBOXINFO combInfo = new COMBOBOXINFO();
            //        combInfo.cbSize = Marshal.SizeOf(combInfo);
            //        GetComboBoxInfo(this.Handle, ref combInfo);

            //        drawCombo();
            //        return;
            //    }
            //}
            //if( m.Msg == 0x2B)  m.Msg = 0x202B;
            //try
            //{
            //    if (MainForm.mainFormRef.backgroundWorkerIsWorking == true)
            //    {
            //        base.WndProc(ref m);
            //        return;
            //    }
            //}
            //catch
            //{
            //    base.WndProc(ref m);
            //    return;
            //}
            base.WndProc(ref m);

            try
            {
                if (!MainForm.mainFormRef.checkBox_OPTIONS_Themes_UseSkinOnControls.Checked)
                {
                    return;

                }
            }
            catch
            {

            }




            switch (m.Msg)
            {

                //case WM_DRAWITEM:

                //    try
                //    {
                //        Graphics graphics = Graphics.FromHdc(m.WParam);
                //        ComboBox cb = (ComboBox)Control.FromHandle(this.Handle);

                //        Brush backcolorBrush = new SolidBrush(BackColor);

                //        graphics.FillRectangle(backcolorBrush, cb.Bounds);
                //        Rect bounds = new Rect(cb.Bounds);
                //        IntPtr hdc = graphics.GetHdc();

                //        SetTextColor(hdc, ColorTranslator.ToWin32(Color.SteelBlue));
                //        SetBkColor(hdc, ColorTranslator.ToWin32(Color.LemonChiffon));

                //        int flags = DT_CENTER | DT_VCENTER | DT_SINGLELINE;
                //        DrawText(hdc, cb.Text, cb.Text.Length, ref bounds, flags);
                //        graphics.ReleaseHdc(hdc);
                //    }
                //    catch
                //    {

                //    }
                case WM_PAINT:
                case WM_NCPAINT:
                    //



                    ////    //// determine if the combobox is dropped down
                    ////    //    Point p = this.PointToScreen(this.Location);
                    ////    //    int locationControl = p.Y; // location on the Y axis
                    ////    //    int screenHeight = Screen.GetBounds(new Point(0,0)).Bottom; // lowest point
                    ////    //    if ((screenHeight - locationControl) < this.DropDownHeight)
                    ////    //    {
                    ////    //        focused = true;
                    ////    //    }



                    //if (focused)
                    //{
                    //    drawCombo();

                    //    focused = false;

                    //}
                    //else
                    //{

                    //    focused = true;
                    //}
                    //
                    

                    if (this.Visible && isPainting == false)
                    {



                       

                       drawCombo();

                       //Thread.Sleep(1);



                    }
                    else
                    {
                        
                        //base.WndProc(ref m);
                        
                    }

                    Thread.Sleep(1);

                    break;
                case WM_ERASEBKGND:
                    //base.WndProc(ref m);

                    //this.DrawItem -= new DrawItemEventHandler(ComboBoxWrap_DrawItem);
                    

                    //else
                    //{
                    //    this.BackColor = ThemedColors.WindowColor;
                    //    this.ForeColor = ThemedColors.WindowTextColor;
                    //}

                    if (this.Visible && isPainting == false)
                    {
                        isPainting = true;

                        if ((this.Enabled && this.BackColor != ThemedColors.WindowColor) || (this.Enabled && this.ForeColor != ThemedColors.WindowTextColor))
                        {
                            this.BackColor = ThemedColors.WindowColor;
                            this.ForeColor = ThemedColors.WindowTextColor;

                            _backBrush = new SolidBrush(ThemedColors.WindowColor);

                        }
                        //else if (this.Focused && !this.DroppedDown && (this.Enabled && this.BackColor != ThemedColors.SelectionColor) || (this.Enabled && this.ForeColor != ThemedColors.SelectionForeColor))
                        //    {

                        //        this.BackColor = ThemedColors.SelectionColor;
                        //        this.ForeColor = ThemedColors.SelectionForeColor;

                        //    }


                        //else if ((!this.Enabled && this.BackColor != ThemedColors.BackColor) || (!this.Enabled && this.ForeColor != ThemedColors.DisabledMenuTextColor))
                        //{
                        //    this.BackColor = ThemedColors.BackColor;
                        //    this.ForeColor = ThemedColors.DisabledMenuTextColor;

                        //}

                        //if (!this.DroppedDown)
                        //{
                        if (this.Focused)
                            _backBrush = new SolidBrush(ThemedColors.SelectionColor);
                        else
                            _backBrush = new SolidBrush(ThemedColors.WindowColor);
                        //this.BackColor = ThemedColors.SelectionColor;
                        //this.ForeColor = ThemedColors.SelectionForeColor;
                        //}

                        try
                        {
                            //Graphics g = Graphics.FromImage(buffer);
                            //PaintEventArgs ex = new PaintEventArgs(g, this.ClientRectangle);
                            //base.OnPaintBackground(ex);
                            //using (Graphics g = Graphics.FromHdc(m.WParam))
                            using (Graphics g = Graphics.FromHdc(m.WParam))
                            {
                                //PaintEventArgs ex = new PaintEventArgs(g, this.ClientRectangle);
                                //base.OnPaintBackground(ex);
                                if (_backBrush != null)
                                {

                                    g.FillRectangle(_backBrush, ClientRectangle);
                                }
                            }


                        }
                        catch
                        {



                        }



                        drawCombo();
                        //drawCombo();
                        //
                        

                    }
                    else
                    {
                      //this.Invalidate();
                      //base.WndProc(ref m);
                      
                    }
                    
                    //this.drawCombo();
                    Thread.Sleep(1);
                    
                    //base.WndProc(ref m);
                    //drawControl(focused);
                    break;
                default:

                    //base.WndProc(ref m);

                    break; // TODO: might not be correct. Was : Exit Select


            }
        }


        //void Application_Idle(object sender, EventArgs e)
        //{
        //    Invalidate();
        //    this.Update();
        //}

        //public override Color BackColor
        //{
        //    get { return base.BackColor; }
        //    set
        //    {
        //        base.BackColor = value;
        //        DestroyBrush();
        //        Invalidate();
        //    }
        //}

        //protected override void WndProc(ref Message m)
        //{

        //    if (m.Msg == WM_ERASEBKGND)
        //    {
        //        using (Graphics g = Graphics.FromHdc(m.WParam))
        //        {
        //            if (_backBrush == null)
        //            {
        //                _backBrush = new SolidBrush(Color.Red);
        //            }
        //            g.FillRectangle(_backBrush, ClientRectangle);
        //        }
        //    }
        //    else
        //    {
        //        base.WndProc(ref m);
        //    }
        //}
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing) { DestroyBrush(); }
        //    base.Dispose(disposing);
        //}
        //private void DestroyBrush()
        //{
        //    if (_backBrush != null)
        //    {
        //        _backBrush.Dispose();
        //        _backBrush = null;
        //    }
        //}

        //protected override void OnPaintBackground(PaintEventArgs pevent)
        //{
        //    base.OnPaintBackground(pevent);

        //    if ((this.Enabled && this.BackColor != ThemedColors.WindowColor) || (this.Enabled && this.ForeColor != ThemedColors.WindowTextColor))
        //    {
        //        this.BackColor = ThemedColors.BackColor;
        //        this.ForeColor = ThemedColors.ForeColor;
        //    }
        //    //else if ((!this.Enabled && this.BackColor != ThemedColors.BackColor) || (!this.Enabled && this.ForeColor != ThemedColors.ForeColor))
        //    //{
        //    //    this.BackColor = ThemedColors.BackColor;
        //    //    this.ForeColor = ThemedColors.DisabledMenuTextColor;
        //    //}
        //    try
        //    {
        //        using (Graphics g = Graphics.FromHwnd(this.Handle))
        //        {
        //            if (_backBrush == null)
        //            {
        //                _backBrush = new SolidBrush(BackColor);
        //            }
        //            g.FillRectangle(_backBrush, ClientRectangle);
        //            DestroyBrush();
        //        }
        //    }
        //    catch
        //    {



        //    }



        //}


        //private Bitmap buffer = null;
        //protected override void OnResize(EventArgs e)
        //{
        //    base.OnResize(e);
        //    if (this.Width > 0 && this.Height > 0)
        //    {
        //        if (this.buffer != null)
        //            this.buffer.Dispose();
        //        this.buffer = new Bitmap(this.Width, this.Height);
        //    }
        //    // make sure it redraws on resize    
        //    this.Invalidate();
        //}
        //protected override void OnPaintBackground(PaintEventArgs pevent)
        //{

        //    Graphics g = Graphics.FromImage(buffer);
        //    PaintEventArgs ex = new PaintEventArgs(g, pevent.ClipRectangle);
        //    base.OnPaintBackground(ex);
        //}

        //not used!!!
        private void drawItemBackGround()
        {
            
            if (this.DroppedDown) return;


            // Get handle to form.
            IntPtr hwnd = this.Handle;

            // Create new graphics object using handle to window.
            Graphics graphics = Graphics.FromHwnd(hwnd);




            if (this.Enabled)
            {
                if (this.Focused)
                {

                    // fill the background behind the separator
                    using (Brush br = new SolidBrush(ThemedColors.SelectionColor))
                    {
                        graphics.FillRectangle(br, this.Bounds);
                    }


                }
                else
                {

                    // fill the background behind the separator
                    using (Brush br = new SolidBrush(ThemedColors.WindowColor))
                    {
                        graphics.FillRectangle(br, this.Bounds);
                    }



                }

            }
            else
            {
                // cobmobox is disabled


                // fill the background behind the separator
                using (Brush br = new SolidBrush(ThemedColors.WindowColor))
                {
                    graphics.FillRectangle(br, this.Bounds);
                }


            }



        }




        // not used!!!
        private void drawItem()
        {
            
            ComboBox cb = (ComboBox)Control.FromHandle(this.Handle);

            if (cb.SelectedIndex < 0 || !cb.Visible)
            {
                return;
            }
            else
            {


                // Get handle to form.
                IntPtr hwnd = this.Handle;

                // Create new graphics object using handle to window.
                Graphics graphics = Graphics.FromHwnd(hwnd);




                if (this.Enabled)
                {
                    if (this.Focused)
                    {


                        TextRenderer.DrawText(graphics, (string)cb.SelectedItem, this.Font, new Point(this.DefaultMargin.Left - 1, this.DefaultMargin.Top), ThemedColors.SelectionForeColor);

                    }
                    else
                    {


                        TextRenderer.DrawText(graphics, (string)cb.SelectedItem, this.Font, new Point(this.DefaultMargin.Left - 1, this.DefaultMargin.Top), ThemedColors.WindowTextColor);

                    }

                }
                else
                {
                    // cobmobox is disabled




                    TextRenderer.DrawText(graphics, (string)cb.SelectedItem, this.Font, new Point(this.DefaultMargin.Left - 1, this.DefaultMargin.Top), ThemedColors.DisabledMenuTextColor);

                }

            }
        }


        //private DrawItemState drawItemState;
        //private int drawItemIndex;
        //private Rectangle drawItemBounds;
        //private string drawItemString;
        //private void ComboBoxWrap_DrawItem(object sender, DrawItemEventArgs e)
        //{

        //    drawItemState = e.State;
        //    drawItemIndex = e.Index;
        //    drawItemBounds = e.Bounds;

        //    myComboBox ddl = (myComboBox)sender;
        //    drawItemString = ddl.Items[e.Index].ToString();

        //    if (e.Index < 0 || e.State == DrawItemState.ComboBoxEdit)
        //    {
        //        return;
        //    }

        //    e.DrawBackground();

        //    this.drawItem();






        //}








        //protected override CreateParams CreateParams
        //{
        //    get
        //    {
        //        var parms = base.CreateParams;
        //        parms.Style &= ~0x02000000;  // Turn off WS_CLIPCHILDREN
        //        return parms;
        //    }
        //}









        protected override void OnDrawItem(DrawItemEventArgs e)
        {
            //

            try
            {
                if (!MainForm.mainFormRef.checkBox_OPTIONS_Themes_UseSkinOnControls.Checked)
                {
                    return;

                }
            }
            catch
            {

            }


            //drawItemState = e.State;
            //drawItemIndex = e.Index;
            //drawItemBounds = e.Bounds;

            if (e.Index < 0 || e.State == DrawItemState.ComboBoxEdit)
            {
                //e.DrawBackground();
                //e.DrawFocusRectangle();
                return;
            }


            isPainting = true;

            e.DrawBackground();

            //IntPtr hdc = GetDC(this.Handle);
            //SendMessage(this.Handle, WM_ERASEBKGND, hdc, IntPtr.Zero);


            //if ((this.Enabled && this.BackColor != ThemedColors.WindowColor) || (this.Enabled && this.ForeColor != ThemedColors.WindowTextColor))
            //{
            //    this.BackColor = ThemedColors.WindowColor;
            //    this.ForeColor = ThemedColors.WindowTextColor;
            //}
            ////else if ((!this.Enabled && this.BackColor != ThemedColors.BackColor) || (!this.Enabled && this.ForeColor != ThemedColors.ForeColor))
            ////{
            ////    this.BackColor = ThemedColors.BackColor;
            ////    this.ForeColor = ThemedColors.DisabledMenuTextColor;
            ////}
            //try
            //{
            //    using (Graphics g = Graphics.FromHwnd(this.Handle))
            //    {
            //        if (_backBrush == null)
            //        {
            //            _backBrush = new SolidBrush(BackColor);
            //        }
            //        g.FillRectangle(_backBrush, ClientRectangle);
            //    }
            //}
            //catch
            //{



            //}







            e.DrawFocusRectangle();










            Brush b = new SolidBrush(ThemedColors.WindowTextColor);
            // get the text of the item
            ComboBox cb = (ComboBox)Control.FromHandle(this.Handle);
            string text = cb.Items[e.Index].ToString();

            //drawItemString = text;


            // draw a lighter blue selected BG colour, the dark blue default has poor contrast with black text on a dark blue background
            //if (e.State == DrawItemState.Selected)
            //{
            //    e.Graphics.FillRectangle(new SolidBrush(ThemedColors.SelectionColor), e.Bounds);
            //    b = new SolidBrush(ThemedColors.SelectionForeColor);
            //}
            //else
            //{
            //    e.Graphics.FillRectangle(new SolidBrush(ThemedColors.WindowColor), e.Bounds);
            //    b = new SolidBrush(ThemedColors.WindowTextColor);
            //}
            // draw the separator line



            if (this.Enabled)
            {
                if (e.State != (DrawItemState.NoAccelerator | DrawItemState.NoFocusRect))
                {

                    //if (this.DroppedDown == false && (e.State & DrawItemState.ComboBoxEdit) != DrawItemState.ComboBoxEdit)
                    //{

                    //    b = new SolidBrush(ThemedColors.SelectionForeColor);
                    //    // fill the background behind the separator
                    //    using (Brush br = new SolidBrush(ThemedColors.SelectionColor))
                    //    {
                    //        e.Graphics.FillRectangle(br, e.Bounds);
                    //    }
                    //}

                    if (this.DroppedDown && this.Focused && e.State != DrawItemState.Inactive)
                    {

                        b = new SolidBrush(ThemedColors.SelectionForeColor);
                        // fill the background behind the separator
                        using (Brush br = new SolidBrush(ThemedColors.SelectionColor))
                        {
                            e.Graphics.FillRectangle(br, e.Bounds);
                        }

                    }
                    else if (this.Focused && e.State != DrawItemState.Inactive)
                    {
                        b = new SolidBrush(ThemedColors.SelectionForeColor);
                        // fill the background behind the separator
                        using (Brush br = new SolidBrush(ThemedColors.SelectionColor))
                        {
                            e.Graphics.FillRectangle(br, e.Bounds);
                        }

                    }
                    else
                    {
                        b = new SolidBrush(ThemedColors.WindowTextColor);
                        // fill the background behind the separator
                        using (Brush br = new SolidBrush(ThemedColors.WindowColor))
                        {
                            e.Graphics.FillRectangle(br, e.Bounds);
                        }

                    }

                }






                e.Graphics.DrawString(text, this.Font, b, e.Bounds, StringFormat.GenericDefault);

                //// draw a light grey border line to separate the items
                //Pen p = new Pen(new SolidBrush(ThemedColors.WindowColor), 1);
                //e.Graphics.DrawLine(p, new Point(e.Bounds.Left, e.Bounds.Bottom - 1), new Point(e.Bounds.Right, e.Bounds.Bottom - 1));
                //p.Dispose();


            }
            else
            {
                // combobox is disabled

                // fill the background behind the separator
                using (Brush br = new SolidBrush(ThemedColors.WindowColor))
                {
                    e.Graphics.FillRectangle(br, e.Bounds);
                }

                b = new SolidBrush(ThemedColors.DisabledMenuTextColor);
                e.Graphics.DrawString(text, this.Font, b, e.Bounds, StringFormat.GenericDefault);
            }









            //isPainting = false;


            drawCombo();






        }

















        //private void ComboBoxWrap_DrawItem(object sender, DrawItemEventArgs e)
        //{


        //    //drawItemState = e.State;
        //    //drawItemIndex = e.Index;
        //    //drawItemBounds = e.Bounds;

        //    if (e.Index < 0 || e.State == DrawItemState.ComboBoxEdit)
        //    {

        //        return;
        //    }


        //    e.DrawBackground();

        //    //e.DrawFocusRectangle();

        //    Brush b = new SolidBrush(ThemedColors.WindowTextColor);
        //    // get the text of the item
        //    myComboBox ddl = (myComboBox)sender;
        //    string text = ddl.Items[e.Index].ToString();

        //    //drawItemString = text;


        //    // draw a lighter blue selected BG colour, the dark blue default has poor contrast with black text on a dark blue background
        //    //if (e.State == DrawItemState.Selected)
        //    //{
        //    //    e.Graphics.FillRectangle(new SolidBrush(ThemedColors.SelectionColor), e.Bounds);
        //    //    b = new SolidBrush(ThemedColors.SelectionForeColor);
        //    //}
        //    //else
        //    //{
        //    //    e.Graphics.FillRectangle(new SolidBrush(ThemedColors.WindowColor), e.Bounds);
        //    //    b = new SolidBrush(ThemedColors.WindowTextColor);
        //    //}
        //    // draw the separator line
        //    if (!this.DroppedDown)
        //    {


        //        e.Graphics.DrawString(text, this.Font, b, e.Bounds, StringFormat.GenericDefault);

        //    }




        //    if (this.Enabled)
        //    {
        //        if (e.State != (DrawItemState.NoAccelerator | DrawItemState.NoFocusRect))
        //        {

        //            //if (this.DroppedDown == false && (e.State & DrawItemState.ComboBoxEdit) != DrawItemState.ComboBoxEdit)
        //            //{

        //            //    b = new SolidBrush(ThemedColors.SelectionForeColor);
        //            //    // fill the background behind the separator
        //            //    using (Brush br = new SolidBrush(ThemedColors.SelectionColor))
        //            //    {
        //            //        e.Graphics.FillRectangle(br, e.Bounds);
        //            //    }
        //            //}

        //            if (this.DroppedDown && this.Focused && e.State != DrawItemState.Inactive)
        //            {

        //                b = new SolidBrush(ThemedColors.SelectionForeColor);
        //                // fill the background behind the separator
        //                using (Brush br = new SolidBrush(ThemedColors.SelectionColor))
        //                {
        //                    e.Graphics.FillRectangle(br, e.Bounds);
        //                }

        //            }
        //            else if (this.Focused && e.State != DrawItemState.Inactive)
        //            {
        //                b = new SolidBrush(ThemedColors.SelectionForeColor);
        //                // fill the background behind the separator
        //                using (Brush br = new SolidBrush(ThemedColors.SelectionColor))
        //                {
        //                    e.Graphics.FillRectangle(br, e.Bounds);
        //                }

        //            }
        //            else
        //            {
        //                b = new SolidBrush(ThemedColors.WindowTextColor);
        //                // fill the background behind the separator
        //                using (Brush br = new SolidBrush(ThemedColors.WindowColor))
        //                {
        //                    e.Graphics.FillRectangle(br, e.Bounds);
        //                }

        //            }

        //        }






        //        e.Graphics.DrawString(text, this.Font, b, e.Bounds, StringFormat.GenericDefault);

        //        //// draw a light grey border line to separate the items
        //        //Pen p = new Pen(new SolidBrush(ThemedColors.WindowColor), 1);
        //        //e.Graphics.DrawLine(p, new Point(e.Bounds.Left, e.Bounds.Bottom - 1), new Point(e.Bounds.Right, e.Bounds.Bottom - 1));
        //        //p.Dispose();


        //    }
        //    else
        //    {
        //        // cobmobox is disabled

        //        b = new SolidBrush(ThemedColors.DisabledMenuTextColor);
        //        e.Graphics.DrawString(text, this.Font, b, e.Bounds, StringFormat.GenericDefault);
        //    }


        //}




    }







    // MyNativeWindow class to create a window given a class name.
    [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust")]
    internal class NativeListWindow : NativeWindow
    {
        // Constant values were found in the "windows.h" header file.
        private const int WS_CHILD = 0x40000000,
                          WS_VISIBLE = 0x10000000,
                          WM_ACTIVATEAPP = 0x001C;

        private int windowHandle;

        private myComboBox parent;
        private cInternalScrollBar scrollBar;
        public NativeListWindow(myComboBox owner, IntPtr handle)
        {
            AssignHandle(handle);
            parent = owner;

        }

        // Listen to when the handle changes to keep the variable in sync
        [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust")]
        protected override void OnHandleChange()
        {
            windowHandle = (int)this.Handle;
        }

        private void AdjustClientRect(ref ProGammaX.myComboBox.RECT rect)
        {
            rect.Right -= 23;
        }


        //private bool visible = false;
        [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust")]
        protected override void WndProc(ref Message message)
        {
            // Listen for messages that are sent to the button window. Some messages are sent 
            // to the parent window instead of the button's window. 
            try
            {
                if (message.Msg == 0x0083 && parent.DroppedDown && MainForm.mainFormRef.checkBox_OPTIONS_Themes_UseSkinOnControls.Checked) //NCCALCSIZE = 0x0083, WM_CREATE = 0x0001, DRAWITEM = 0x002B, MEASUREITEM = 0x002C, CTLCOLOREDIT  = 0x0133, WM_PAINT = 0x000F
                {




                    //visible = true;
                    //cInternalScrollBar(IntPtr hWnd, Bitmap hztrack, Bitmap hzarrow, Bitmap hzthumb, Bitmap vttrack, Bitmap vtarrow, Bitmap vtthumb, Bitmap fader)
                    scrollBar = new cInternalScrollBar(this.Handle, MainForm.mainFormRef.vienna_ScrollVertShaft, MainForm.mainFormRef.vienna_ScrollHorzArrow, MainForm.mainFormRef.vienna_ScrollHorzThumb, MainForm.mainFormRef.vienna_ScrollHorzShaft, MainForm.mainFormRef.vienna_ScrollVertArrow, MainForm.mainFormRef.vienna_ScrollVertThumb, null, true);


                }
                else
                {
                    //if (this.Visible == false)
                    //{

                    //scrollBar.Dispose();
                    //scrollBar = null;
                    //}


                }


            }
            catch
            {

            }


            //switch (message.Msg)
            //{
            //case Win32.NCCALCSIZE:
            //    {
            //        //base.WndProc(ref message);
            //        //if (message.WParam != IntPtr.Zero)
            //        //{
            //        //    NCCALCSIZE_PARAMS rcsize = (NCCALCSIZE_PARAMS)Marshal.PtrToStructure(message.LParam, typeof(NCCALCSIZE_PARAMS));
            //        //    AdjustClientRect(ref rcsize.rect0);
            //        //    Marshal.StructureToPtr(rcsize, message.LParam, false);
            //        //}
            //        //else
            //        //{
            //        //    RECT rcsize = (RECT)Marshal.PtrToStructure(message.LParam, typeof(RECT));
            //        //    AdjustClientRect(ref rcsize);
            //        //    Marshal.StructureToPtr(rcsize, message.LParam, false);
            //        //}

            //        //MainForm form = new MainForm();
            //        //form.loadControlImages(0);
            //        //form._cRcm = new ScrollBar_2___Pokus.cRCM(this.Handle);
            //        //form.loadFrameImages(0);
            //        //form.loadControlImages(0);
            //        //form._cRcm.Start();
            //        //form.Refresh();
            //        vScrollBar = new cInternalScrollBar(this.Handle, MainForm.mainFormRef.vienna_combo, MainForm.mainFormRef.vienna_ScrollHorzArrow, MainForm.mainFormRef.vienna_ScrollHorzThumb, MainForm.mainFormRef.vienna_ScrollVertShaft, MainForm.mainFormRef.vienna_ScrollVertArrow, MainForm.mainFormRef.vienna_ScrollVertThumb, MainForm.mainFormRef.vienna_ScrollHorzShaft);
            //        //message.Result = new IntPtr(0);
            //        //Win32.SendMessage(parent.vScrollBar.Handle, (uint)message.Msg, message.WParam, message.LParam);

            //        break;
            //    }

            //case Win32.WM_NCMOUSEMOVE:
            //    {
            //        //base.WndProc(ref message);
            //        //Win32.SendMessage(parent.vScrollBar.Handle, (uint)message.Msg, message.WParam, message.LParam);
            //        vScrollBar = new cInternalScrollBar(this.Handle, ScrollBar_2___Pokus.Properties.Resources.vienna_combo, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollHorzArrow, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollHorzThumb, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollVertShaft, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollVertArrow, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollVertThumb, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollHorzShaft);
            //        break;
            //    }
            //case Win32.WM_NCLBUTTONDOWN:
            //    {
            //        //base.WndProc(ref message);
            //        //Win32.SendMessage(parent.vScrollBar.Handle, (uint)message.Msg, message.WParam, message.LParam);
            //        vScrollBar = new cInternalScrollBar(this.Handle, ScrollBar_2___Pokus.Properties.Resources.vienna_combo, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollHorzArrow, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollHorzThumb, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollVertShaft, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollVertArrow, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollVertThumb, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollHorzShaft);
            //        break;
            //    }
            //case Win32.WM_NCACTIVATE:
            //    {
            //        //base.WndProc(ref message);
            //        //Win32.SendMessage(parent.vScrollBar.Handle, (uint)message.Msg, message.WParam, message.LParam);
            //        vScrollBar = new cInternalScrollBar(this.Handle, ScrollBar_2___Pokus.Properties.Resources.vienna_combo, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollHorzArrow, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollHorzThumb, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollVertShaft, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollVertArrow, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollVertThumb, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollHorzShaft);
            //        break;
            //    }
            //case Win32.WM_NCMOUSELEAVE:
            //    {
            //        //base.WndProc(ref message);
            //        //Win32.SendMessage(parent.vScrollBar.Handle, (uint)message.Msg, message.WParam, message.LParam);
            //        vScrollBar = new cInternalScrollBar(this.Handle, ScrollBar_2___Pokus.Properties.Resources.vienna_combo, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollHorzArrow, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollHorzThumb, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollVertShaft, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollVertArrow, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollVertThumb, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollHorzShaft);
            //        break;
            //    }
            //case Win32.WM_NCLBUTTONUP:
            //    {
            //        //base.WndProc(ref message);
            //        //Win32.SendMessage(parent.vScrollBar.Handle, (uint)message.Msg, message.WParam, message.LParam);
            //        vScrollBar = new cInternalScrollBar(this.Handle, ScrollBar_2___Pokus.Properties.Resources.vienna_combo, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollHorzArrow, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollHorzThumb, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollVertShaft, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollVertArrow, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollVertThumb, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollHorzShaft);
            //        break;
            //    }
            //case Win32.WM_NCHITTEST:
            //    {
            //        //base.WndProc(ref message);
            //        //Win32.SendMessage(parent.vScrollBar.Handle, (uint)message.Msg, message.WParam, message.LParam);
            //        vScrollBar = new cInternalScrollBar(this.Handle, ScrollBar_2___Pokus.Properties.Resources.vienna_combo, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollHorzArrow, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollHorzThumb, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollVertShaft, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollVertArrow, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollVertThumb, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollHorzShaft);
            //        break;
            //    }
            //case Win32.WM_MOUSEMOVE:
            //    {
            //        //base.WndProc(ref message);
            //        vScrollBar = new cInternalScrollBar(this.Handle, ScrollBar_2___Pokus.Properties.Resources.vienna_combo, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollHorzArrow, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollHorzThumb, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollVertShaft, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollVertArrow, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollVertThumb, ScrollBar_2___Pokus.Properties.Resources.vienna_ScrollHorzShaft);
            //        //if ((int)message.LParam > 0)
            //        //{
            //        //    int x = Win32.LOWORD((int)message.LParam);
            //        //    int y = Win32.HIWORD((int)message.LParam);
            //        //    RECT rect = new RECT(); ;
            //        //    Win32.GetWindowRect(new HandleRef(parent.vScrollBar, parent.vScrollBar.Handle), out rect);
            //        //    Rectangle rc = new Rectangle(parent.Location.X, parent.Location.Y,
            //        //        (rect.right - rect.left), (rect.bottom - rect.top));
            //        //    if (rc.Contains(new Point(x, y)))
            //        //    {
            //        //        Win32.SetFocus(parent.vScrollBar.Handle);
            //        //        Win32.SetWindowPos(parent.vScrollBar.Handle, HWND.TopMost, 155, 1, 23, 105, SetWindowPosFlags.SWP_SHOWWINDOW);
            //        //        Win32.SendMessage(parent.vScrollBar.Handle, (uint)message.Msg, message.WParam, message.LParam);
            //        //    }
            //        //}
            //        break;
            //    }
            //}

            base.WndProc(ref message);
        }





 



















    }





















    [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust")]
    class cScrollBar : NativeWindow, IDisposable
    {
        #region Constants
        // showwindow
        private const int SW_HIDE = 0x0;
        private const int SW_NORMAL = 0x1;
        // window styles
        private const int GWL_STYLE = (-16);
        private const int GWL_EXSTYLE = (-20);
        private const int WS_EX_TOPMOST = 0x8;
        private const int WS_EX_TOOLWINDOW = 0x80;
        private const int WS_CHILD = 0x40000000;
        private const int WS_OVERLAPPED = 0x0;
        private const int WS_CLIPCHILDREN = 0x2000000;
        private const int WS_CLIPSIBLINGS = 0x4000000;
        private const int WS_VISIBLE = 0x10000000;
        private const int SS_OWNERDRAW = 0xD;
        // size/move
        private const uint SWP_NOSIZE = 0x0001;
        private const uint SWP_NOMOVE = 0x0002;
        private const uint SWP_NOZORDER = 0x0004;
        private const uint SWP_NOREDRAW = 0x0008;
        private const uint SWP_NOACTIVATE = 0x0010;
        private const uint SWP_FRAMECHANGED = 0x0020;
        private const uint SWP_SHOWWINDOW = 0x0040;
        private const uint SWP_HIDEWINDOW = 0x0080;
        private const uint SWP_NOCOPYBITS = 0x0100;
        private const uint SWP_NOOWNERZORDER = 0x0200;
        private const uint SWP_NOSENDCHANGING = 0x0400;
        // setwindowpos
        static readonly IntPtr HWND_NOTOPMOST = new IntPtr(-2);
        static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
        static readonly IntPtr HWND_TOP = new IntPtr(0);
        static readonly IntPtr HWND_BOTTOM = new IntPtr(1);
        // scroll messages
        private const int WM_HSCROLL = 0x114;
        private const int WM_VSCROLL = 0x115;
        private const int SB_LINEUP = 0;
        private const int SB_LINEDOWN = 1;
        private const int SB_LINELEFT = 0;
        private const int SB_LINERIGHT = 1;
        private const int SB_PAGEUP = 2;
        private const int SB_PAGEDOWN = 3;
        private const int SB_PAGELEFT = 2;
        private const int SB_PAGERIGHT = 3;
        // mouse buttons
        private const int VK_LBUTTON = 0x1;
        private const int VK_RBUTTON = 0x2;
        // redraw
        private const int RDW_INVALIDATE = 0x0001;
        private const int RDW_INTERNALPAINT = 0x0002;
        private const int RDW_ERASE = 0x0004;
        private const int RDW_VALIDATE = 0x0008;
        private const int RDW_NOINTERNALPAINT = 0x0010;
        private const int RDW_NOERASE = 0x0020;
        private const int RDW_NOCHILDREN = 0x0040;
        private const int RDW_ALLCHILDREN = 0x0080;
        private const int RDW_UPDATENOW = 0x0100;
        private const int RDW_ERASENOW = 0x0200;
        private const int RDW_FRAME = 0x0400;
        private const int RDW_NOFRAME = 0x0800;
        // scroll bar messages
        private const int SB_HORZ = 0x0;
        private const int SB_VERT = 0x1;
        private const int SBM_SETPOS = 0x00E0;
        private const int SBM_GETPOS = 0x00E1;
        private const int SBM_SETRANGE = 0x00E2;
        private const int SBM_SETRANGEREDRAW = 0x00E6;
        private const int SBM_GETRANGE = 0x00E3;
        private const int SBM_ENABLE_ARROWS = 0x00E4;
        private const int SBM_SETSCROLLINFO = 0x00E9;
        private const int SBM_GETSCROLLINFO = 0x00EA;
        private const int SBM_GETSCROLLBARINFO = 0x00EB;
        private const int SIF_RANGE = 0x0001;
        private const int SIF_PAGE = 0x0002;
        private const int SIF_POS = 0x0004;
        private const int SIF_DISABLENOSCROLL = 0x0008;
        private const int SIF_TRACKPOS = 0x0010;
        private const int SIF_ALL = (SIF_RANGE | SIF_PAGE | SIF_POS | SIF_TRACKPOS);
        // scrollbar states
        private const int STATE_SYSTEM_INVISIBLE = 0x00008000;
        private const int STATE_SYSTEM_OFFSCREEN = 0x00010000;
        private const int STATE_SYSTEM_PRESSED = 0x00000008;
        private const int STATE_SYSTEM_UNAVAILABLE = 0x00000001;
        private const uint OBJID_HSCROLL = 0xFFFFFFFA;
        private const uint OBJID_VSCROLL = 0xFFFFFFFB;
        private const uint OBJID_CLIENT = 0xFFFFFFFC;
        // window messages
        private const int WM_PAINT = 0xF;
        private const int WM_MOUSEMOVE = 0x200;
        private const int WM_MOUSELEAVE = 0x2A3;
        private const int WM_LBUTTONDOWN = 0x201;
        private const int WM_LBUTTONUP = 0x202;
        private const int WM_LBUTTONDBLCLK = 0x203;
        private const int WM_RBUTTONDOWN = 0x204;
        private const int WM_RBUTTONUP = 0x205;
        private const int WM_RBUTTONDBLCLK = 0x206;
        private const int WM_MBUTTONDOWN = 0x207;
        private const int WM_MBUTTONUP = 0x208;
        private const int WM_MBUTTONDBLCLK = 0x209;
        private const int WM_MOUSEWHEEL = 0x20A;
        private const int WM_SIZE = 0x5;
        private const int WM_MOVE = 0x3;
        // message handler
        private static IntPtr MSG_HANDLED = new IntPtr(1);
        #endregion

        #region Enums
        private enum SB_HITEST : int
        {
            offControl = 0,
            topArrow,
            bottomArrow,
            leftArrow,
            rightArrow,
            button,
            track
        }

        private enum SYSTEM_METRICS : int
        {
            SM_CXSCREEN = 0,
            SM_CYSCREEN = 1,
            SM_CXVSCROLL = 2,
            SM_CYHSCROLL = 3,
            SM_CYCAPTION = 4,
            SM_CXBORDER = 5,
            SM_CYBORDER = 6,
            SM_CYVTHUMB = 9,
            SM_CXHTHUMB = 10,
            SM_CXICON = 11,
            SM_CYICON = 12,
            SM_CXCURSOR = 13,
            SM_CYCURSOR = 14,
            SM_CYMENU = 15,
            SM_CXFULLSCREEN = 16,
            SM_CYFULLSCREEN = 17,
            SM_CYKANJIWINDOW = 18,
            SM_MOUSEPRESENT = 19,
            SM_CYVSCROLL = 20,
            SM_CXHSCROLL = 21,
            SM_SWAPBUTTON = 23,
            SM_CXMIN = 28,
            SM_CYMIN = 29,
            SM_CXSIZE = 30,
            SM_CYSIZE = 31,
            SM_CXFRAME = 32,
            SM_CYFRAME = 33,
            SM_CXMINTRACK = 34,
            SM_CYMINTRACK = 35,
            SM_CYSMCAPTION = 51,
            SM_CXMINIMIZED = 57,
            SM_CYMINIMIZED = 58,
            SM_CXMAXTRACK = 59,
            SM_CYMAXTRACK = 60,
            SM_CXMAXIMIZED = 61,
            SM_CYMAXIMIZED = 62
        }
        #endregion

        #region Structs
        [StructLayout(LayoutKind.Sequential)]
        private struct PAINTSTRUCT
        {
            internal IntPtr hdc;
            internal int fErase;
            internal RECT rcPaint;
            internal int fRestore;
            internal int fIncUpdate;
            internal int Reserved1;
            internal int Reserved2;
            internal int Reserved3;
            internal int Reserved4;
            internal int Reserved5;
            internal int Reserved6;
            internal int Reserved7;
            internal int Reserved8;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct RECT
        {
            internal RECT(int X, int Y, int Width, int Height)
            {
                this.Left = X;
                this.Top = Y;
                this.Right = Width;
                this.Bottom = Height;
            }
            internal int Left;
            internal int Top;
            internal int Right;
            internal int Bottom;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct SCROLLINFO
        {
            internal uint cbSize;
            internal uint fMask;
            internal int nMin;
            internal int nMax;
            internal uint nPage;
            internal int nPos;
            internal int nTrackPos;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct SCROLLBARINFO
        {
            internal int cbSize;
            internal RECT rcScrollBar;
            internal int dxyLineButton;
            internal int xyThumbTop;
            internal int xyThumbBottom;
            internal int reserved;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            internal int[] rgstate;
        }
        #endregion

        #region API
        [DllImport("user32.dll")]
        private static extern IntPtr BeginPaint(IntPtr hWnd, ref PAINTSTRUCT ps);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool EndPaint(IntPtr hWnd, ref PAINTSTRUCT ps);

        [DllImport("gdi32.dll")]
        private static extern IntPtr SelectObject(IntPtr hdc, IntPtr hObject);

        [DllImport("gdi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool BitBlt(IntPtr hdc, int nXDest, int nYDest, int nWidth, int nHeight, IntPtr hdcSrc, int nXSrc, int nYSrc, int dwRop);

        [DllImport("gdi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool StretchBlt(IntPtr hDest, int X, int Y, int nWidth, int nHeight, IntPtr hdcSrc,
        int sX, int sY, int nWidthSrc, int nHeightSrc, int dwRop);

        [DllImport("user32.dll")]
        private static extern IntPtr GetDC(IntPtr handle);

        [DllImport("user32.dll")]
        private static extern int ReleaseDC(IntPtr handle, IntPtr hdc);

        [DllImport("user32.dll")]
        private static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImport("user32.dll")]
        private static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, ref SCROLLBARINFO lParam);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetWindowRect(IntPtr hWnd, ref RECT lpRect);

        [DllImport("user32.dll")]
        private extern static int OffsetRect(ref RECT lpRect, int x, int y);

        [DllImport("user32.dll")]
        private static extern bool ValidateRect(IntPtr hWnd, ref RECT lpRect);

        [DllImport("user32.dll")]
        private static extern int GetSystemMetrics(SYSTEM_METRICS smIndex);

        [DllImport("uxtheme.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private extern static bool IsAppThemed();

        [DllImport("uxtheme.dll", ExactSpelling = true, CharSet = CharSet.Unicode)]
        private static extern int SetWindowTheme(IntPtr hWnd, String pszSubAppName, String pszSubIdList);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool RedrawWindow(IntPtr hWnd, IntPtr lprcUpdate, IntPtr hrgnUpdate, uint flags);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetCursorPos(ref Point lpPoint);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool PtInRect([In] ref RECT lprc, Point pt);

        [DllImport("user32.dll")]
        private static extern int ScreenToClient(IntPtr hwnd, ref Point lpPoint);

        [DllImport("user32.dll")]
        private static extern short GetKeyState(int nVirtKey);

        [DllImport("user32.dll")]
        private static extern int SetScrollPos(IntPtr hWnd, int nBar, int nPos, bool bRedraw);

        [DllImport("user32.dll")]
        private static extern int GetScrollPos(IntPtr hWnd, int nBar);

        [DllImport("user32.dll")]
        private static extern IntPtr GetParent(IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern IntPtr CreateWindowEx(int exstyle, string lpClassName, string lpWindowName, int dwStyle,
            int x, int y, int nWidth, int nHeight, IntPtr hwndParent, IntPtr Menu, IntPtr hInstance, IntPtr lpParam);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool DestroyWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndAfter, int x, int y, int cx, int cy, uint flags);

        [DllImport("user32.dll")]
        static extern bool EqualRect([In] ref RECT lprc1, [In] ref RECT lprc2);

        [DllImport("user32.dll")]
        private static extern int GetWindowLong(IntPtr hwnd, int nIndex);

        [DllImport("user32.dll")]
        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
        #endregion

        #region Fields
        public bool _bPainting = false;
        //private bool _bMoved = false;
        //private bool _bFading = false;
        private IntPtr _hMaskWnd = IntPtr.Zero;
        private int _iArrowCx = 14;
        private int _iArrowCy = 14;
        private Orientation _eScrollbarOrientation;
        private IntPtr _hScrollBarWnd = IntPtr.Zero;
        private cStoreDc _cArrowDc = new cStoreDc();
        private cStoreDc _cThumbDc = new cStoreDc();
        private cStoreDc _cTrackDc = new cStoreDc();
        private Bitmap _oArrowBitmap;
        private Bitmap _oThumbBitmap;
        private Bitmap _oTrackBitmap;
        private Bitmap _oMask;
        #endregion

        #region Constructor
        public cScrollBar(IntPtr hWnd, Orientation orientation, Bitmap thumb, Bitmap track, Bitmap arrow, Bitmap fader)
        {
            if (hWnd == IntPtr.Zero)
                throw new Exception("The scrollbar handle is invalid.");
            ArrowGraphic = arrow;
            ThumbGraphic = thumb;
            TrackGraphic = track;
            _hScrollBarWnd = hWnd;
            Direction = orientation;
            scrollbarMetrics();
            if (Environment.OSVersion.Version.Major > 5)
            {
                if (IsAppThemed())
                    SetWindowTheme(_hScrollBarWnd, "", "");
            }
            createScrollBarMask();
            ScrollBar sc = (ScrollBar)Control.FromHandle(_hScrollBarWnd);
            sc.Scroll += new ScrollEventHandler(sc_Scroll);
            sc.MouseEnter += new EventHandler(sc_MouseEnter);
            Control ct = Control.FromHandle(GetParent(_hScrollBarWnd));
            ct.Paint += new PaintEventHandler(ct_Paint);
            this.AssignHandle(hWnd);
            if (fader != null)
                TransitionGraphic = fader;
        }

        void sc_MouseEnter(object sender, EventArgs e)
        {
            scrollFader();
        }

        public void Dispose()
        {
            try
            {
                this.ReleaseHandle();
                if (_oArrowBitmap != null) _oArrowBitmap.Dispose();
                if (_cArrowDc != null) _cArrowDc.Dispose();
                if (_oThumbBitmap != null) _oThumbBitmap.Dispose();
                if (_cThumbDc != null) _cThumbDc.Dispose();
                if (_oTrackBitmap != null) _oTrackBitmap.Dispose();
                if (_cTrackDc != null) _cTrackDc.Dispose();
                if (_hMaskWnd != IntPtr.Zero) DestroyWindow(_hMaskWnd);
                //if (_oMask != null) _oMask.Dispose();
            }
            catch { }
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Events
        private void ct_Paint(object sender, PaintEventArgs e)
        {
            invalidateWindow(false);
        }

        private void sc_Scroll(object sender, ScrollEventArgs e)
        {
            invalidateWindow(false);
        }
        #endregion

        #region Properties
        private Bitmap ArrowGraphic
        {
            get { return _oArrowBitmap; }
            set
            {
                _oArrowBitmap = value;
                if (_cArrowDc.Hdc != IntPtr.Zero)
                {
                    _cArrowDc.Dispose();
                    _cArrowDc = new cStoreDc();
                }
                _cArrowDc.Width = _oArrowBitmap.Width;
                _cArrowDc.Height = _oArrowBitmap.Height;
                SelectObject(_cArrowDc.Hdc, _oArrowBitmap.GetHbitmap());

            }
        }

        private Bitmap ThumbGraphic
        {
            get { return _oThumbBitmap; }
            set
            {
                _oThumbBitmap = value;
                if (_cThumbDc.Hdc != IntPtr.Zero)
                {
                    _cThumbDc.Dispose();
                    _cThumbDc = new cStoreDc();
                }
                _cThumbDc.Width = _oThumbBitmap.Width;
                _cThumbDc.Height = _oThumbBitmap.Height;
                SelectObject(_cThumbDc.Hdc, _oThumbBitmap.GetHbitmap());

            }
        }

        private Bitmap TrackGraphic
        {
            get { return _oTrackBitmap; }
            set
            {
                _oTrackBitmap = value;
                if (_cTrackDc.Hdc != IntPtr.Zero)
                {
                    _cTrackDc.Dispose();
                    _cTrackDc = new cStoreDc();
                }
                _cTrackDc.Width = _oTrackBitmap.Width;
                _cTrackDc.Height = _oTrackBitmap.Height;
                SelectObject(_cTrackDc.Hdc, _oTrackBitmap.GetHbitmap());

            }
        }

        private Bitmap TransitionGraphic
        {
            get { return _oMask; }
            set { _oMask = value; }
        }

        private Orientation Direction
        {
            get { return _eScrollbarOrientation; }
            set { _eScrollbarOrientation = value; }
        }

        private int HScrollPos
        {
            get { return GetScrollPos((IntPtr)this.Handle, SB_HORZ); }
            set { SetScrollPos((IntPtr)this.Handle, SB_HORZ, value, true); }
        }

        private int VScrollPos
        {
            get { return GetScrollPos((IntPtr)this.Handle, SB_VERT); }
            set { SetScrollPos((IntPtr)this.Handle, SB_VERT, value, true); }
        }
        #endregion

        #region Methods
        private void checkBarState()
        {
            if ((GetWindowLong(_hScrollBarWnd, GWL_STYLE) & WS_VISIBLE) == WS_VISIBLE)
                ShowWindow(_hMaskWnd, SW_NORMAL);
            else
                ShowWindow(_hMaskWnd, SW_HIDE);
        }

        private void createScrollBarMask()
        {
            Type t = typeof(cScrollBar);
            Module m = t.Module;
            IntPtr hInstance = Marshal.GetHINSTANCE(m);
            IntPtr hParent = GetParent(_hScrollBarWnd);
            RECT tr = new RECT();
            Point pt = new Point();

            GetWindowRect(_hScrollBarWnd, ref tr);
            pt.X = tr.Left;
            pt.Y = tr.Top;
            ScreenToClient(hParent, ref pt);

            _hMaskWnd = CreateWindowEx(WS_EX_TOPMOST | WS_EX_TOOLWINDOW,
                "STATIC", "",
                SS_OWNERDRAW | WS_CHILD | WS_CLIPSIBLINGS | WS_OVERLAPPED | WS_VISIBLE,
                pt.X, pt.Y,
                (tr.Right - tr.Left), (tr.Bottom - tr.Top),
                hParent,
                IntPtr.Zero, hInstance, IntPtr.Zero);

            // set z-order
            SetWindowPos(_hMaskWnd, HWND_TOP,
                0, 0,
                0, 0,
                SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE | SWP_NOOWNERZORDER);
        }

        public void drawScrollBar()
        {
            SCROLLBARINFO sbi = new SCROLLBARINFO();
            RECT tr = new RECT();
            cStoreDc tempDc = new cStoreDc();
            int offset = 0;
            int width = 0;
            int section = 0;

            GetWindowRect(_hScrollBarWnd, ref tr);
            OffsetRect(ref tr, -tr.Left, -tr.Top);
            tempDc.Width = tr.Right;
            tempDc.Height = tr.Bottom;
            SB_HITEST hitTest = scrollbarHitTest();

            sbi.cbSize = Marshal.SizeOf(sbi);
            SendMessage(_hScrollBarWnd, SBM_GETSCROLLBARINFO, 0, ref sbi);

            if (Direction == Orientation.Horizontal)
            {
                // draw the track
                using (StretchImage si = new StretchImage(_cTrackDc.Hdc, tempDc.Hdc, new Rectangle(0, 0, _cTrackDc.Width, _cTrackDc.Height), new Rectangle(_iArrowCx, 0, tr.Right - (2 * _iArrowCx), tr.Bottom), 2, StretchModeEnum.STRETCH_HALFTONE)) { }
                // draw the arrows
                section = 7;
                width = _cArrowDc.Width / section;
                // left arrow
                if (hitTest == SB_HITEST.leftArrow)
                {
                    if (leftKeyPressed())
                        offset = 2;
                    else
                        offset = 1;
                }
                else
                {
                    offset = 0;
                }
                using (StretchImage si = new StretchImage(_cArrowDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cArrowDc.Height), new Rectangle(0, 0, _iArrowCx, tr.Bottom), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                // right arrow
                if (hitTest == SB_HITEST.rightArrow)
                {
                    if (leftKeyPressed())
                        offset = 5;
                    else
                        offset = 4;
                }
                else
                {
                    offset = 3;
                }
                using (StretchImage si = new StretchImage(_cArrowDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cArrowDc.Height), new Rectangle(tr.Right - _iArrowCx, 0, _iArrowCx, tr.Bottom), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                // draw the thumb
                section = 3;
                width = _cThumbDc.Width / section;
                if (hitTest == SB_HITEST.button)
                {
                    if (leftKeyPressed())
                        offset = 2;
                    else
                        offset = 1;
                }
                else
                {
                    offset = 0;
                }
                Point pst = getScrollBarThumb();
                using (StretchImage si = new StretchImage(_cThumbDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cThumbDc.Height), new Rectangle(pst.X, 0, pst.Y - pst.X, tr.Bottom), 2, StretchModeEnum.STRETCH_HALFTONE)) { }
            }
            else
            {
                // draw the track
                using (StretchImage si = new StretchImage(_cTrackDc.Hdc, tempDc.Hdc, new Rectangle(0, 0, _cTrackDc.Width, _cTrackDc.Height), new Rectangle(0, _iArrowCy, tr.Right, tr.Bottom - (2 * _iArrowCy)), 2, StretchModeEnum.STRETCH_HALFTONE)) { }
                section = 6;
                width = _cArrowDc.Width / section;

                // top arrow
                if (hitTest == SB_HITEST.topArrow)
                {
                    if (leftKeyPressed())
                        offset = 2;
                    else
                        offset = 1;
                }
                else
                {
                    offset = 0;
                }
                using (StretchImage si = new StretchImage(_cArrowDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cArrowDc.Height), new Rectangle(0, 0, tr.Right, _iArrowCy), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                // bottom arrow
                if (hitTest == SB_HITEST.bottomArrow)
                {
                    if (leftKeyPressed())
                        offset = 5;
                    else
                        offset = 4;
                }
                else
                {
                    offset = 3;
                }
                using (StretchImage si = new StretchImage(_cArrowDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cArrowDc.Height), new Rectangle(0, tr.Bottom - _iArrowCy, tr.Right, _iArrowCy), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                // draw the thumb
                section = 3;
                width = _cThumbDc.Width / section;
                if (hitTest == SB_HITEST.button)
                {
                    if (leftKeyPressed())
                        offset = 2;
                    else
                        offset = 1;
                }
                else
                {
                    offset = 0;
                }
                Point pst = getScrollBarThumb();
                using (StretchImage si = new StretchImage(_cThumbDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cThumbDc.Height), new Rectangle(0, pst.X, _iArrowCx, pst.Y - pst.X), 2, StretchModeEnum.STRETCH_HALFTONE)) { }
            }
            IntPtr hdc = GetDC(_hMaskWnd);
            BitBlt(hdc, 0, 0, tr.Right, tr.Bottom, tempDc.Hdc, 0, 0, 0xCC0020);
            ReleaseDC(_hMaskWnd, hdc);
            tempDc.Dispose();
        }

        private RECT focusedPartSize()
        {
            RECT tr = new RECT();
            GetWindowRect(_hScrollBarWnd, ref tr);
            OffsetRect(ref tr, -tr.Left, -tr.Top);

            switch (scrollbarHitTest())
            {
                case SB_HITEST.leftArrow:
                    tr = new RECT(0, 0, _iArrowCx, tr.Bottom);
                    break;

                case SB_HITEST.rightArrow:
                    tr = new RECT(tr.Right - _iArrowCx, 0, tr.Right, tr.Bottom);
                    break;

                case SB_HITEST.topArrow:
                    tr = new RECT(0, 0, tr.Right, _iArrowCy);
                    break;

                case SB_HITEST.bottomArrow:
                    tr = new RECT(0, tr.Bottom - _iArrowCy, tr.Right, _iArrowCy);
                    break;

                case SB_HITEST.button:
                    Point pst = getScrollBarThumb();
                    if (Direction == Orientation.Horizontal)
                        tr = new RECT(pst.X, 2, pst.Y - pst.X, tr.Bottom);
                    else
                        tr = new RECT(0, pst.X, _iArrowCx, pst.Y - pst.X);
                    break;

            }
            return tr;
        }

        private RECT getScrollBarRect()
        {
            SCROLLBARINFO sbi = new SCROLLBARINFO();
            sbi.cbSize = Marshal.SizeOf(sbi);
            SendMessage(_hScrollBarWnd, SBM_GETSCROLLBARINFO, 0, ref sbi);
            return sbi.rcScrollBar;
        }

        public void sizeCheck()
        {
            RECT tr = new RECT();
            RECT tw = new RECT();
            GetWindowRect(_hMaskWnd, ref tw);
            GetWindowRect(_hScrollBarWnd, ref tr);
            if (!EqualRect(ref tr, ref tw))
                SetWindowPos(_hMaskWnd, IntPtr.Zero, tr.Left, tr.Top, tr.Right - tr.Left, tr.Bottom - tr.Top, SWP_NOACTIVATE | SWP_NOOWNERZORDER | SWP_NOZORDER);
        }

        private Point getScrollBarThumb()
        {
            Point pt = new Point();
            ScrollBar sc = (ScrollBar)Control.FromHandle(_hScrollBarWnd);
            float incr = 0;

            if (sc.ClientRectangle.Width > sc.ClientRectangle.Height)
            {
                incr = ((float)(sc.ClientRectangle.Width - (_iArrowCx * 2)) / sc.Maximum);
                pt.X = (int)(incr * sc.Value) + _iArrowCx;
                pt.Y = pt.X + (int)(incr * sc.LargeChange);
            }
            else
            {
                incr = ((float)(sc.ClientRectangle.Height - (_iArrowCy * 2)) / sc.Maximum);
                pt.X = (int)(incr * sc.Value) + _iArrowCy;
                pt.Y = pt.X + (int)(incr * sc.LargeChange);
            }
            // fix for ?bug? in scrollbar
            if (sc.Value < sc.Maximum)
            {
                sc.Value++;
                sc.Value--;
            }
            else
            {
                sc.Value--;
                sc.Value++;
            }
            invalidateWindow(true);
            return pt;
        }

        private void invalidateWindow(bool messaged)
        {
            if (messaged)
                RedrawWindow(_hScrollBarWnd, IntPtr.Zero, IntPtr.Zero, RDW_INTERNALPAINT);
            else
                RedrawWindow(_hScrollBarWnd, IntPtr.Zero, IntPtr.Zero, RDW_INVALIDATE | RDW_UPDATENOW);
        }

        private bool leftKeyPressed()
        {
            if (mouseButtonsSwitched())
                return (GetKeyState(VK_RBUTTON) < 0);
            else
                return (GetKeyState(VK_LBUTTON) < 0);
        }

        private bool mouseButtonsSwitched()
        {
            return (GetSystemMetrics(SYSTEM_METRICS.SM_SWAPBUTTON) != 0);
        }

        private SB_HITEST scrollbarHitTest()
        {
            Point pt = new Point();
            RECT tr = new RECT();

            GetWindowRect(_hScrollBarWnd, ref tr);
            OffsetRect(ref tr, -tr.Left, -tr.Top);

            RECT tp = tr;
            GetCursorPos(ref pt);
            ScreenToClient(_hScrollBarWnd, ref pt);

            if (Direction == Orientation.Horizontal)
            {
                if (PtInRect(ref tr, pt))
                {
                    // left arrow
                    tp.Right = _iArrowCx;
                    if (PtInRect(ref tp, pt))
                        return SB_HITEST.leftArrow;
                    // right arrow
                    tp.Left = tr.Right - _iArrowCx;
                    tp.Right = tr.Right;
                    if (PtInRect(ref tp, pt))
                        return SB_HITEST.rightArrow;
                    // button
                    Point pb = getScrollBarThumb();
                    tp.Left = pb.X;
                    tp.Right = pb.Y;
                    if (PtInRect(ref tp, pt))
                        return SB_HITEST.button;
                    // track
                    return SB_HITEST.track;
                }
            }
            else
            {
                if (PtInRect(ref tr, pt))
                {
                    // top arrow
                    tp.Bottom = _iArrowCy;
                    if (PtInRect(ref tp, pt))
                        return SB_HITEST.topArrow;
                    // bottom arrow
                    tp.Top = tr.Bottom - _iArrowCy;
                    tp.Bottom = tr.Bottom;
                    if (PtInRect(ref tp, pt))
                        return SB_HITEST.bottomArrow;
                    // button
                    Point pb = getScrollBarThumb();
                    tp.Top = pb.X;
                    tp.Bottom = pb.Y;
                    if (PtInRect(ref tp, pt))
                        return SB_HITEST.button;
                    // track
                    return SB_HITEST.track;
                }
            }
            return SB_HITEST.offControl;
        }

        private void scrollbarMetrics()
        {
            if (Direction == Orientation.Horizontal)
            {
                _iArrowCx = GetSystemMetrics(SYSTEM_METRICS.SM_CXHSCROLL);
                _iArrowCy = GetSystemMetrics(SYSTEM_METRICS.SM_CYHSCROLL);
            }
            else
            {
                _iArrowCx = GetSystemMetrics(SYSTEM_METRICS.SM_CXVSCROLL);
                _iArrowCy = GetSystemMetrics(SYSTEM_METRICS.SM_CYVSCROLL);
            }
        }

        private void scrollHorizontal(bool Right)
        {
            if (Right)
                SendMessage(_hScrollBarWnd, WM_HSCROLL, SB_LINERIGHT, 0);
            else
                SendMessage(_hScrollBarWnd, WM_HSCROLL, SB_LINELEFT, 0);

        }

        private void scrollVertical(bool Down)
        {
            if (Down)
                SendMessage(_hScrollBarWnd, WM_VSCROLL, SB_LINEDOWN, 0);
            else
                SendMessage(_hScrollBarWnd, WM_VSCROLL, SB_LINEUP, 0);
        }

        private void scrollFader()
        {

            SB_HITEST hitTest = scrollbarHitTest();
            if (hitTest == SB_HITEST.button)
            {
                Point pst = getScrollBarThumb();
                if (TransitionGraphic != null)
                {
                    cTransition ts;
                    RECT tr = new RECT();
                    if (Direction == Orientation.Horizontal)
                    {
                        GetWindowRect(_hScrollBarWnd, ref tr);
                        ts = new cTransition(_hMaskWnd, _hScrollBarWnd, TransitionGraphic, new Rectangle(pst.X, 0, pst.Y - pst.X, tr.Bottom));
                    }
                    else
                    {
                        ts = new cTransition(_hMaskWnd, _hScrollBarWnd, TransitionGraphic, new Rectangle(0, pst.X, _iArrowCx, pst.Y - pst.X));
                    }
                }
            }
        }
        #endregion

        #region WndProc
        protected override void WndProc(ref Message m)
        {
            PAINTSTRUCT ps = new PAINTSTRUCT();

            switch (m.Msg)
            {

                case 0x83:
                case WM_PAINT:
                    if (!_bPainting)
                    {
                        _bPainting = true;
                        // start painting engine
                        BeginPaint(m.HWnd, ref ps);
                        drawScrollBar();
                        ValidateRect(m.HWnd, ref ps.rcPaint);
                        // done
                        EndPaint(m.HWnd, ref ps);
                        _bPainting = false;
                        m.Result = MSG_HANDLED;
                    }
                    else
                    {
                        base.WndProc(ref m);
                    }
                    break;

                case WM_SIZE:
                case WM_MOVE:
                    sizeCheck();
                    base.WndProc(ref m);
                    break;

                default:
                    base.WndProc(ref m);
                    break;
            }
        }
        #endregion
    }








































     class cInternalScrollBar : NativeWindow, IDisposable
    {
        #region Constants
        // style
        private const int GWL_STYLE = (-16);
        private const int GWL_EXSTYLE = (-20);
        private const int WS_EX_TOPMOST = 0x8;
        private const int WS_EX_TOOLWINDOW = 0x80;
        private const int WS_CHILD = 0x40000000;
        private const int WS_OVERLAPPED = 0x0;
        private const int WS_CLIPSIBLINGS = 0x4000000;
        private const int WS_VISIBLE = 0x10000000;
        private const int WS_HSCROLL = 0x100000;
        private const int WS_VSCROLL = 0x200000;
        private const int SS_OWNERDRAW = 0xD;
        // showwindow
        private const int SW_HIDE = 0x0;
        private const int SW_NORMAL = 0x1;
        // size/move
        private const uint SWP_NOSIZE = 0x0001;
        private const uint SWP_NOMOVE = 0x0002;
        private const uint SWP_NOZORDER = 0x0004;
        private const uint SWP_NOREDRAW = 0x0008;
        private const uint SWP_NOACTIVATE = 0x0010;
        private const uint SWP_FRAMECHANGED = 0x0020;
        private const uint SWP_SHOWWINDOW = 0x0040;
        private const uint SWP_HIDEWINDOW = 0x0080;
        private const uint SWP_NOCOPYBITS = 0x0100;
        private const uint SWP_NOOWNERZORDER = 0x0200;
        private const uint SWP_NOSENDCHANGING = 0x0400;
        // setwindowpos
        static readonly IntPtr HWND_NOTOPMOST = new IntPtr(-2);
        static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
        static readonly IntPtr HWND_TOP = new IntPtr(0);
        static readonly IntPtr HWND_BOTTOM = new IntPtr(1);
        // scroll messages
        private const int WM_HSCROLL = 0x114;
        private const int WM_VSCROLL = 0x115;
        private const int SB_LINEUP = 0;
        private const int SB_LINEDOWN = 1;
        private const int SB_LINELEFT = 0;
        private const int SB_LINERIGHT = 1;
        private const int SB_PAGEUP = 2;
        private const int SB_PAGEDOWN = 3;
        private const int SB_PAGELEFT = 2;
        private const int SB_PAGERIGHT = 3;
        // mouse buttons
        private const int VK_LBUTTON = 0x1;
        private const int VK_RBUTTON = 0x2;
        // redraw
        private const int RDW_INVALIDATE = 0x0001;
        private const int RDW_INTERNALPAINT = 0x0002;
        private const int RDW_ERASE = 0x0004;
        private const int RDW_VALIDATE = 0x0008;
        private const int RDW_NOINTERNALPAINT = 0x0010;
        private const int RDW_NOERASE = 0x0020;
        private const int RDW_NOCHILDREN = 0x0040;
        private const int RDW_ALLCHILDREN = 0x0080;
        private const int RDW_UPDATENOW = 0x0100;
        private const int RDW_ERASENOW = 0x0200;
        private const int RDW_FRAME = 0x0400;
        private const int RDW_NOFRAME = 0x0800;
        // scroll bar messages
        private const int SB_HORZ = 0x0;
        private const int SB_VERT = 0x1;
        private const int SBM_SETPOS = 0x00E0;
        private const int SBM_GETPOS = 0x00E1;
        private const int SBM_SETRANGE = 0x00E2;
        private const int SBM_SETRANGEREDRAW = 0x00E6;
        private const int SBM_GETRANGE = 0x00E3;
        private const int SBM_ENABLE_ARROWS = 0x00E4;
        private const int SBM_SETSCROLLINFO = 0x00E9;
        private const int SBM_GETSCROLLINFO = 0x00EA;
        private const int SBM_GETSCROLLBARINFO = 0x00EB;
        private const int SIF_RANGE = 0x0001;
        private const int SIF_PAGE = 0x0002;
        private const int SIF_POS = 0x0004;
        private const int SIF_DISABLENOSCROLL = 0x0008;
        private const int SIF_TRACKPOS = 0x0010;
        private const int SIF_ALL = (SIF_RANGE | SIF_PAGE | SIF_POS | SIF_TRACKPOS);
        // scrollbar states
        private const int STATE_SYSTEM_INVISIBLE = 0x00008000;
        private const int STATE_SYSTEM_OFFSCREEN = 0x00010000;
        private const int STATE_SYSTEM_PRESSED = 0x00000008;
        private const int STATE_SYSTEM_UNAVAILABLE = 0x00000001;
        private const uint OBJID_HSCROLL = 0xFFFFFFFA;
        private const uint OBJID_VSCROLL = 0xFFFFFFFB;
        private const uint OBJID_CLIENT = 0xFFFFFFFC;
        // window messages
        private const int WM_PAINT = 0xF;
        private const int WM_NCPAINT = 0x85;
        private const int WM_NCMOUSEMOVE = 0xA0;
        private const int WM_MOUSEMOVE = 0x200;
        private const int WM_MOUSELEAVE = 0x2A3;
        private const int WM_LBUTTONDOWN = 0x201;
        private const int WM_LBUTTONUP = 0x202;
        private const int WM_LBUTTONDBLCLK = 0x203;
        private const int WM_RBUTTONDOWN = 0x204;
        private const int WM_RBUTTONUP = 0x205;
        private const int WM_RBUTTONDBLCLK = 0x206;
        private const int WM_MBUTTONDOWN = 0x207;
        private const int WM_MBUTTONUP = 0x208;
        private const int WM_MBUTTONDBLCLK = 0x209;
        private const int WM_MOUSEWHEEL = 0x20A;
        private const int WM_MOUSEHWHEEL = 0x020E;
        private const int WM_STYLECHANGED = 0x7D;
        private const int WM_SIZE = 0x5;
        private const int WM_MOVE = 0x3;
        // message handler
        private static IntPtr MSG_HANDLED = new IntPtr(1);
        #endregion

        #region Enums
        private enum SB_HITEST : int
        {
            offControl = 0,
            topArrow,
            bottomArrow,
            leftArrow,
            rightArrow,
            button,
            track
        }

        private enum SYSTEM_METRICS : int
        {
            SM_CXSCREEN = 0,
            SM_CYSCREEN = 1,
            SM_CXVSCROLL = 2,
            SM_CYHSCROLL = 3,
            SM_CYCAPTION = 4,
            SM_CXBORDER = 5,
            SM_CYBORDER = 6,
            SM_CYVTHUMB = 9,
            SM_CXHTHUMB = 10,
            SM_CXICON = 11,
            SM_CYICON = 12,
            SM_CXCURSOR = 13,
            SM_CYCURSOR = 14,
            SM_CYMENU = 15,
            SM_CXFULLSCREEN = 16,
            SM_CYFULLSCREEN = 17,
            SM_CYKANJIWINDOW = 18,
            SM_MOUSEPRESENT = 19,
            SM_CYVSCROLL = 20,
            SM_CXHSCROLL = 21,
            SM_SWAPBUTTON = 23,
            SM_CXMIN = 28,
            SM_CYMIN = 29,
            SM_CXSIZE = 30,
            SM_CYSIZE = 31,
            SM_CXFRAME = 32,
            SM_CYFRAME = 33,
            SM_CXMINTRACK = 34,
            SM_CYMINTRACK = 35,
            SM_CYSMCAPTION = 51,
            SM_CXMINIMIZED = 57,
            SM_CYMINIMIZED = 58,
            SM_CXMAXTRACK = 59,
            SM_CYMAXTRACK = 60,
            SM_CXMAXIMIZED = 61,
            SM_CYMAXIMIZED = 62
        }
        #endregion

        #region Structs
        [StructLayout(LayoutKind.Sequential)]
        private struct PAINTSTRUCT
        {
            internal IntPtr hdc;
            internal int fErase;
            internal RECT rcPaint;
            internal int fRestore;
            internal int fIncUpdate;
            internal int Reserved1;
            internal int Reserved2;
            internal int Reserved3;
            internal int Reserved4;
            internal int Reserved5;
            internal int Reserved6;
            internal int Reserved7;
            internal int Reserved8;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct RECT
        {
            internal RECT(int X, int Y, int Width, int Height)
            {
                this.Left = X;
                this.Top = Y;
                this.Right = Width;
                this.Bottom = Height;
            }
            internal int Left;
            internal int Top;
            internal int Right;
            internal int Bottom;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct SCROLLINFO
        {
            internal uint cbSize;
            internal uint fMask;
            internal int nMin;
            internal int nMax;
            internal uint nPage;
            internal int nPos;
            internal int nTrackPos;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct SCROLLBARINFO
        {
            internal int cbSize;
            internal RECT rcScrollBar;
            internal int dxyLineButton;
            internal int xyThumbTop;
            internal int xyThumbBottom;
            internal int reserved;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            internal int[] rgstate;
        }
        #endregion

        #region API
        [DllImport("user32.dll")]
        private static extern IntPtr BeginPaint(IntPtr hWnd, ref PAINTSTRUCT ps);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool EndPaint(IntPtr hWnd, ref PAINTSTRUCT ps);

        [DllImport("gdi32.dll")]
        private static extern IntPtr SelectObject(IntPtr hdc, IntPtr hObject);

        [DllImport("gdi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool BitBlt(IntPtr hdc, int nXDest, int nYDest, int nWidth, int nHeight, IntPtr hdcSrc, int nXSrc, int nYSrc, int dwRop);

        [DllImport("gdi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool StretchBlt(IntPtr hDest, int X, int Y, int nWidth, int nHeight, IntPtr hdcSrc,
        int sX, int sY, int nWidthSrc, int nHeightSrc, int dwRop);

        [DllImport("user32.dll")]
        private static extern IntPtr GetDC(IntPtr handle);

        [DllImport("user32.dll")]
        private static extern int ReleaseDC(IntPtr handle, IntPtr hdc);

        [DllImport("user32.dll")]
        private static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImport("user32.dll")]
        private static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, ref SCROLLBARINFO lParam);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetWindowRect(IntPtr hWnd, ref RECT lpRect);

        [DllImport("user32.dll")]
        private extern static int OffsetRect(ref RECT lpRect, int x, int y);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool ValidateRect(IntPtr hWnd, ref RECT lpRect);

        [DllImport("user32.dll")]
        private static extern int GetSystemMetrics(SYSTEM_METRICS smIndex);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool RedrawWindow(IntPtr hWnd, IntPtr lprcUpdate, IntPtr hrgnUpdate, uint flags);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetCursorPos(ref Point lpPoint);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool PtInRect([In] ref RECT lprc, Point pt);

        [DllImport("user32.dll")]
        private static extern int ScreenToClient(IntPtr hwnd, ref Point lpPoint);

        [DllImport("user32.dll")]
        private static extern short GetKeyState(int nVirtKey);

        [DllImport("user32.dll")]
        private static extern int SetScrollPos(IntPtr hWnd, int nBar, int nPos, bool bRedraw);

        [DllImport("user32.dll")]
        private static extern int GetScrollPos(IntPtr hWnd, int nBar);

        [DllImport("user32.dll")]
        private static extern IntPtr GetParent(IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern IntPtr CreateWindowEx(int exstyle, string lpClassName, string lpWindowName, int dwStyle,
            int x, int y, int nWidth, int nHeight, IntPtr hwndParent, IntPtr Menu, IntPtr hInstance, IntPtr lpParam);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool DestroyWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndAfter, int x, int y, int cx, int cy, uint flags);


        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool EqualRect([In] ref RECT lprc1, [In] ref RECT lprc2);

        [DllImport("user32.dll")]
        private static extern int GetWindowLong(IntPtr hwnd, int nIndex);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll")]
        private static extern int GetScrollBarInfo(IntPtr hWnd, uint idObject, ref SCROLLBARINFO psbi);
        #endregion

        #region Fields
        public bool _bTrackingMouse = false;
        private int _iArrowCx = 0;
        private int _iArrowCy = 0;
        private IntPtr _hVerticalMaskWnd = IntPtr.Zero;
        private IntPtr _hHorizontalMaskWnd = IntPtr.Zero;
        private IntPtr _hSizerMaskWnd = IntPtr.Zero;
        private IntPtr _hControlWnd = IntPtr.Zero;
        private cStoreDc _cHorizontalArrowDc = new cStoreDc();
        private cStoreDc _cHorizontalThumbDc = new cStoreDc();
        private cStoreDc _cHorizontalTrackDc = new cStoreDc();
        private cStoreDc _cVerticalArrowDc = new cStoreDc();
        private cStoreDc _cVerticalThumbDc = new cStoreDc();
        private cStoreDc _cVerticalTrackDc = new cStoreDc();
        private Bitmap _oHorizontalArrowBitmap = null;
        private Bitmap _oHorizontalThumbBitmap = null;
        private Bitmap _oHorizontalTrackBitmap = null;
        private Bitmap _oVerticalArrowBitmap;
        private Bitmap _oVerticalThumbBitmap;
        private Bitmap _oVerticalTrackBitmap;
        private Bitmap _oMask;
        #endregion

        #region Constructor
        public cInternalScrollBar(IntPtr hWnd, Bitmap hztrack, Bitmap hzarrow, Bitmap hzthumb, Bitmap vttrack, Bitmap vtarrow, Bitmap vtthumb, Bitmap fader, bool visible)
        {
            //if (hWnd == IntPtr.Zero)
            //    throw new Exception("The control handle is invalid.");
            //if (hztrack == null)
            //    throw new Exception("The Horizontal Track image is invalid.");
            //if (hzarrow == null)
            //    throw new Exception("The Horizontal Arrow image is invalid.");
            //if (hzthumb == null)
            //    throw new Exception("The Horizontal Thumb image is invalid.");
            //if (vttrack == null)
            //    throw new Exception("The Vertical Track image is invalid.");
            //if (vtarrow == null)
            //    throw new Exception("The Vertical Arrow image is invalid.");
            //if (vtthumb == null)
            //    throw new Exception("The Vertical Thumb image is invalid.");
            try
            {

                //bool isHwndVisible = ((Control.FromHandle(hWnd).Visible));// || (Control.FromHandle(hWnd).GetType() == typeof(myPanel)))); //(IsScrollbarVisible(hWnd) && );
                //Form.FromHandle(hWnd).Text = Control.FromHandle(hWnd).Name;
                //bool hwndContainsProperControls = (Control.FromHandle(hWnd).GetType() == typeof(myComboBox));  // (Control.FromHandle(hWnd).GetType() == typeof(richTextBox) || Control.FromHandle(hWnd).GetType() == typeof(textBox) || Control.FromHandle(hWnd).GetType() == typeof(listBox) || Control.FromHandle(hWnd).GetType() == typeof(listView) || Control.FromHandle(hWnd).GetType() == typeof(treeView) || Control.FromHandle(hWnd).GetType() == typeof(myPanel) || Control.FromHandle(hWnd).GetType() == typeof(myTabControl));

                if (visible)
                {
                    //HorizontalArrowGraphic = hzarrow;
                    //HorizontalThumbGraphic = hzthumb;
                    //HorizontalTrackGraphic = hztrack;
                    VerticalArrowGraphic = vtarrow;
                    VerticalThumbGraphic = vtthumb;
                    VerticalTrackGraphic = vttrack;

                    // the fader for this class would require
                    // some additional code, with the fader inclass
                    if (fader != null)
                        TransitionGraphic = fader;
                    scrollbarMetrics();
                    _hControlWnd = hWnd;
                    createScrollBarMask();
                    this.AssignHandle(_hControlWnd);
                }
                else
                {


                }
            }
            catch
            {

            }
        }
        #endregion

        #region Properties

        //private static bool IsScrollbarVisible(IntPtr hWnd)
        //{
        //    // Determines whether the vertical scrollbar is visible for the specified control
        //    bool bVisible = false;
        //    int nMessage = GWL_STYLE;
        //    int nStyle = GetWindowLong(hWnd, nMessage);
        //    bVisible = ((nStyle & nMessage) != 0);
        //    return bVisible;
        //}



        //private Bitmap HorizontalArrowGraphic
        //{
        //    get { return _oHorizontalArrowBitmap; }
        //    set
        //    {
        //        _oHorizontalArrowBitmap = value;
        //        if (_cHorizontalArrowDc.Hdc != IntPtr.Zero)
        //        {
        //            _cHorizontalArrowDc.Dispose();
        //            _cHorizontalArrowDc = new cStoreDc();
        //        }
        //        _cHorizontalArrowDc.Width = _oHorizontalArrowBitmap.Width;
        //        _cHorizontalArrowDc.Height = _oHorizontalArrowBitmap.Height;
        //        //SelectObject(_cHorizontalArrowDc.Hdc, _oHorizontalArrowBitmap.GetHbitmap());

        //        try
        //        { 
        //        Thread selectBmp = new Thread(() => SelectObject(_cHorizontalArrowDc.Hdc, _oHorizontalArrowBitmap.GetHbitmap()));
        //        selectBmp.Start();
        //        selectBmp.Join();
        //        }
        //        catch
        //        {

        //        }
        //    }
        //}

        //private Bitmap HorizontalThumbGraphic
        //{
        //    get { return _oHorizontalThumbBitmap; }
        //    set
        //    {
        //        _oHorizontalThumbBitmap = value;
        //        if (_cHorizontalThumbDc.Hdc != IntPtr.Zero)
        //        {
        //            _cHorizontalThumbDc.Dispose();
        //            _cHorizontalThumbDc = new cStoreDc();
        //        }
        //        _cHorizontalThumbDc.Width = _oHorizontalThumbBitmap.Width;
        //        _cHorizontalThumbDc.Height = _oHorizontalThumbBitmap.Height;
        //        //SelectObject(_cHorizontalThumbDc.Hdc, _oHorizontalThumbBitmap.GetHbitmap());

        //        try
        //        { 
        //        Thread selectBmp = new Thread(() => SelectObject(_cHorizontalThumbDc.Hdc, _oHorizontalThumbBitmap.GetHbitmap()));
        //        selectBmp.Start();
        //        selectBmp.Join();
        //        }
        //        catch
        //        {

        //        }
        //    }
        //}

        //private Bitmap HorizontalTrackGraphic
        //{
        //    get { return _oHorizontalTrackBitmap; }
        //    set
        //    {
        //        _oHorizontalTrackBitmap = value;
        //        if (_cHorizontalTrackDc.Hdc != IntPtr.Zero)
        //        {
        //            _cHorizontalTrackDc.Dispose();
        //            _cHorizontalTrackDc = new cStoreDc();
        //        }
        //        _cHorizontalTrackDc.Width = _oHorizontalTrackBitmap.Width;
        //        _cHorizontalTrackDc.Height = _oHorizontalTrackBitmap.Height;
        //        //SelectObject(_cHorizontalTrackDc.Hdc, _oHorizontalTrackBitmap.GetHbitmap());


        //        try
        //        { 

        //        Thread selectBmp = new Thread(() => SelectObject(_cHorizontalTrackDc.Hdc, _oHorizontalTrackBitmap.GetHbitmap()));
        //        selectBmp.Start();
        //        selectBmp.Join();
        //        }
        //        catch
        //        {

        //        }
        //    }
        //}

        private Bitmap VerticalArrowGraphic
        {
            get { return _oVerticalArrowBitmap; }
            set
            {
                _oVerticalArrowBitmap = value;
                if (_cVerticalArrowDc.Hdc != IntPtr.Zero)
                {
                    _cVerticalArrowDc.Dispose();
                    _cVerticalArrowDc = new cStoreDc();
                }
                _cVerticalArrowDc.Width = _oVerticalArrowBitmap.Width;
                _cVerticalArrowDc.Height = _oVerticalArrowBitmap.Height;
                //SelectObject(_cVerticalArrowDc.Hdc, _oVerticalArrowBitmap.GetHbitmap());
                try
                { 
                Thread selectBmp = new Thread(() => SelectObject(_cVerticalArrowDc.Hdc, _oVerticalArrowBitmap.GetHbitmap()));
                selectBmp.Start();
                selectBmp.Join();
                }
                catch
                {

                }

            }
        }

        private Bitmap VerticalThumbGraphic
        {
            get { return _oVerticalThumbBitmap; }
            set
            {
                _oVerticalThumbBitmap = value;
                if (_cVerticalThumbDc.Hdc != IntPtr.Zero)
                {
                    _cVerticalThumbDc.Dispose();
                    _cVerticalThumbDc = new cStoreDc();
                }
                _cVerticalThumbDc.Width = _oVerticalThumbBitmap.Width;
                _cVerticalThumbDc.Height = _oVerticalThumbBitmap.Height;
                //SelectObject(_cVerticalThumbDc.Hdc, _oVerticalThumbBitmap.GetHbitmap());

                try
                {
                    Thread selectBmp = new Thread(() => SelectObject(_cVerticalThumbDc.Hdc, _oVerticalThumbBitmap.GetHbitmap()));
                    selectBmp.Start();
                    selectBmp.Join();
                }
                catch
                {

                }

            }
        }

        private Bitmap VerticalTrackGraphic
        {
            get { return _oVerticalTrackBitmap; }
            set
            {
                _oVerticalTrackBitmap = value;
                if (_cVerticalTrackDc.Hdc != IntPtr.Zero)
                {
                    _cVerticalTrackDc.Dispose();
                    _cVerticalTrackDc = new cStoreDc();
                }
                _cVerticalTrackDc.Width = _oVerticalTrackBitmap.Width;
                _cVerticalTrackDc.Height = _oVerticalTrackBitmap.Height;
                //SelectObject(_cVerticalTrackDc.Hdc, _oVerticalTrackBitmap.GetHbitmap());

                try
                { 
                Thread selectBmp = new Thread(() => SelectObject(_cVerticalTrackDc.Hdc, _oVerticalTrackBitmap.GetHbitmap()));
                selectBmp.Start();
                selectBmp.Join();
                }
                catch
                {

                }
            }
        }

        private Bitmap TransitionGraphic
        {
            get { return _oMask; }
            set { _oMask = value; }
        }

        private int HScrollPos
        {
            get { return GetScrollPos((IntPtr)this.Handle, SB_HORZ); }
            set { SetScrollPos((IntPtr)this.Handle, SB_HORZ, value, true); }
        }

        private int VScrollPos
        {
            get { return GetScrollPos((IntPtr)this.Handle, SB_VERT); }
            set { SetScrollPos((IntPtr)this.Handle, SB_VERT, value, true); }
        }
        #endregion

        #region Methods
        public void checkBarState()
        {
            if ((GetWindowLong(_hControlWnd, GWL_STYLE) & WS_VISIBLE) == WS_VISIBLE)
            {
                if (hasHorizontal())
                    ShowWindow(_hHorizontalMaskWnd, SW_NORMAL);
                else
                    ShowWindow(_hHorizontalMaskWnd, SW_HIDE);

                if (hasVertical())
                    ShowWindow(_hVerticalMaskWnd, SW_NORMAL);
                else
                    ShowWindow(_hVerticalMaskWnd, SW_HIDE);

                if (hasSizer())
                    ShowWindow(_hSizerMaskWnd, SW_NORMAL);
                else
                    ShowWindow(_hSizerMaskWnd, SW_HIDE);
            }
            else
            {
                ShowWindow(_hHorizontalMaskWnd, SW_HIDE);
                ShowWindow(_hVerticalMaskWnd, SW_HIDE);
                ShowWindow(_hSizerMaskWnd, SW_HIDE);
            }
        }

        private void createScrollBarMask()
        {
            Type t = typeof(cScrollBar);
            Module m = t.Module;
            IntPtr hInstance = Marshal.GetHINSTANCE(m);
            IntPtr hParent = GetParent(_hControlWnd);
            RECT tr = new RECT();
            Point pt = new Point();
            SCROLLBARINFO sb = new SCROLLBARINFO();
            sb.cbSize = Marshal.SizeOf(sb);

            // vertical scrollbar
            // get the size and position
            GetScrollBarInfo(_hControlWnd, OBJID_VSCROLL, ref sb);
            tr = sb.rcScrollBar;
            pt.X = tr.Left;
            pt.Y = tr.Top;
            ScreenToClient(hParent, ref pt);

            // create the window
            _hVerticalMaskWnd = CreateWindowEx(WS_EX_TOPMOST | WS_EX_TOOLWINDOW,
                "STATIC", "",
                SS_OWNERDRAW | WS_CHILD | WS_CLIPSIBLINGS | WS_OVERLAPPED | WS_VISIBLE,
                pt.X, pt.Y,
                (tr.Right - tr.Left), (tr.Bottom - tr.Top),
                hParent,
                IntPtr.Zero, hInstance, IntPtr.Zero);

            // set z-order
            SetWindowPos(_hVerticalMaskWnd, HWND_TOP,
                0, 0,
                0, 0,
                SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE | SWP_NOOWNERZORDER);

            // horizontal scrollbar
            GetScrollBarInfo(_hControlWnd, OBJID_HSCROLL, ref sb);
            tr = sb.rcScrollBar;
            pt.X = tr.Left;
            pt.Y = tr.Top;
            ScreenToClient(hParent, ref pt);

            _hHorizontalMaskWnd = CreateWindowEx(WS_EX_TOPMOST | WS_EX_TOOLWINDOW,
                "STATIC", "",
                SS_OWNERDRAW | WS_CHILD | WS_CLIPSIBLINGS | WS_OVERLAPPED | WS_VISIBLE,
                pt.X, pt.Y,
                (tr.Right - tr.Left), (tr.Bottom - tr.Top),
                hParent,
                IntPtr.Zero, hInstance, IntPtr.Zero);

            SetWindowPos(_hHorizontalMaskWnd, HWND_TOP,
                0, 0,
                0, 0,
                SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE | SWP_NOOWNERZORDER);

            // sizer
            _hSizerMaskWnd = CreateWindowEx(WS_EX_TOPMOST | WS_EX_TOOLWINDOW,
                "STATIC", "",
                SS_OWNERDRAW | WS_CHILD | WS_CLIPSIBLINGS | WS_OVERLAPPED | WS_VISIBLE,
                pt.X + (tr.Right - tr.Left), pt.Y,
                _iArrowCx, _iArrowCy,
                hParent,
                IntPtr.Zero, hInstance, IntPtr.Zero);

            SetWindowPos(_hSizerMaskWnd, HWND_TOP,
                0, 0,
                0, 0,
                SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE | SWP_NOOWNERZORDER);
            reSizeMask();
        }

        public void drawScrollBar()
        {
            RECT tr = new RECT();
            Point pst = new Point();
            cStoreDc tempDc = new cStoreDc();
            IntPtr hdc = IntPtr.Zero;
            int offset = 0;
            int width = 0;
            int section = 0;
            SCROLLBARINFO sb = new SCROLLBARINFO();
            sb.cbSize = Marshal.SizeOf(sb);

            if (hasHorizontal())
            {
                GetScrollBarInfo(_hControlWnd, OBJID_HSCROLL, ref sb);
                tr = sb.rcScrollBar;
                OffsetRect(ref tr, -tr.Left, -tr.Top);
                tempDc.Width = tr.Right;
                tempDc.Height = tr.Bottom;
                SB_HITEST hitTest = scrollbarHitTest(Orientation.Horizontal);

                // draw the track
                using (StretchImage si = new StretchImage(_cHorizontalTrackDc.Hdc, tempDc.Hdc, new Rectangle(0, 0, _cHorizontalTrackDc.Width, _cHorizontalTrackDc.Height), new Rectangle(_iArrowCx, 0, tr.Right - (2 * _iArrowCx), tr.Bottom), 2, StretchModeEnum.STRETCH_HALFTONE)) { }
                // draw the arrows
                section = 7;
                width = _cHorizontalArrowDc.Width / section;
                // left arrow
                if (hitTest == SB_HITEST.leftArrow)
                {
                    if (leftKeyPressed())
                        offset = 2;
                    else
                        offset = 1;

                }
                else
                {
                    offset = 0;
                }
                using (StretchImage si = new StretchImage(_cHorizontalArrowDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cHorizontalArrowDc.Height), new Rectangle(0, 0, _iArrowCx, tr.Bottom), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                // right arrow
                if (hitTest == SB_HITEST.rightArrow)
                {
                    if (leftKeyPressed())
                        offset = 5;
                    else
                        offset = 4;

                }
                else
                {
                    offset = 3;
                }
                using (StretchImage si = new StretchImage(_cHorizontalArrowDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cHorizontalArrowDc.Height), new Rectangle(tr.Right - _iArrowCx, 0, _iArrowCx, tr.Bottom), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                // draw the thumb
                section = 3;
                width = _cHorizontalThumbDc.Width / section;
                if (hitTest == SB_HITEST.button)
                {
                    if (leftKeyPressed())
                        offset = 2;
                    else
                        offset = 1;

                }
                else
                {
                    offset = 0;
                }
                pst.X = sb.xyThumbTop;
                pst.Y = sb.xyThumbBottom;







                using (StretchImage si = new StretchImage(_cHorizontalThumbDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cHorizontalThumbDc.Height), new Rectangle(pst.X, 2, pst.Y - pst.X, tr.Bottom), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                hdc = GetDC(_hHorizontalMaskWnd);
                BitBlt(hdc, 0, 0, tr.Right, tr.Bottom, tempDc.Hdc, 0, 0, 0xCC0020);
                ReleaseDC(_hHorizontalMaskWnd, hdc);
            }

            if (hasSizer())
            {
                tempDc.Width = _iArrowCx;
                tempDc.Height = _iArrowCy;
                offset = 6;
                section = 7;
                width = _cHorizontalArrowDc.Width / section;

                using (StretchImage si = new StretchImage(_cHorizontalArrowDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cHorizontalArrowDc.Height), new Rectangle(0, 0, _iArrowCx, _iArrowCy), 0, StretchModeEnum.STRETCH_HALFTONE)) { }
                hdc = GetDC(_hSizerMaskWnd);
                BitBlt(hdc, 0, 0, _iArrowCx, _iArrowCy, tempDc.Hdc, 0, 0, 0xCC0020);
                ReleaseDC(_hSizerMaskWnd, hdc);
            }

            if (hasVertical())
            {
                GetScrollBarInfo(_hControlWnd, OBJID_VSCROLL, ref sb);
                tr = sb.rcScrollBar;
                OffsetRect(ref tr, -tr.Left, -tr.Top);
                tempDc.Width = tr.Right;
                tempDc.Height = tr.Bottom;
                SB_HITEST hitTest = scrollbarHitTest(Orientation.Vertical);

                // draw the track
                using (StretchImage si = new StretchImage(_cVerticalTrackDc.Hdc, tempDc.Hdc, new Rectangle(0, 0, _cVerticalTrackDc.Width, _cVerticalTrackDc.Height), new Rectangle(0, _iArrowCy, tr.Right, tr.Bottom - (2 * _iArrowCy)), 2, StretchModeEnum.STRETCH_HALFTONE)) { }
                section = 6;
                width = _cVerticalArrowDc.Width / section;

                // top arrow
                if (hitTest == SB_HITEST.topArrow)
                {
                    if (leftKeyPressed())
                        offset = 2;
                    else
                        offset = 1;

                }
                else
                {
                    offset = 0;
                }
                using (StretchImage si = new StretchImage(_cVerticalArrowDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cVerticalArrowDc.Height), new Rectangle(0, 0, tr.Right, _iArrowCy), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                // bottom arrow
                if (hitTest == SB_HITEST.bottomArrow)
                {
                    if (leftKeyPressed())
                        offset = 5;
                    else
                        offset = 4;

                }
                else
                {
                    offset = 3;
                }
                using (StretchImage si = new StretchImage(_cVerticalArrowDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cVerticalArrowDc.Height), new Rectangle(0, tr.Bottom - _iArrowCy, tr.Right, _iArrowCy), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                // draw the thumb
                section = 3;
                width = _cVerticalThumbDc.Width / section;
                if (hitTest == SB_HITEST.button)
                {
                    if (leftKeyPressed())
                        offset = 2;
                    else
                        offset = 1;

                }
                else
                {
                    offset = 0;
                }

                pst.X = sb.xyThumbTop;
                pst.Y = sb.xyThumbBottom;
                using (StretchImage si = new StretchImage(_cVerticalThumbDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cVerticalThumbDc.Height), new Rectangle(0, pst.X, _iArrowCx, pst.Y - pst.X), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                hdc = GetDC(_hVerticalMaskWnd);
                BitBlt(hdc, 0, 0, tr.Right, tr.Bottom, tempDc.Hdc, 0, 0, 0xCC0020);
                ReleaseDC(_hVerticalMaskWnd, hdc);
            }
            tempDc.Dispose();
        }

        public void scrollFader()
        {
            if (TransitionGraphic != null)
            {
                SB_HITEST hitTest;
                SCROLLBARINFO sb = new SCROLLBARINFO();
                sb.cbSize = Marshal.SizeOf(sb);
                if (hasHorizontal())
                {
                    hitTest = scrollbarHitTest(Orientation.Horizontal);

                    if ((hitTest == SB_HITEST.button) && (!leftKeyPressed()))
                    {
                        GetScrollBarInfo(_hControlWnd, OBJID_HSCROLL, ref sb);
                        // start the transition routines here
                        // size of mask - new Rectangle(sb.xyThumbTop, 2, sb.xyThumbBottom - sb.xyThumbTop, sb.rcScrollBar.Bottom)
                    }
                }
                if (hasVertical())
                {
                    hitTest = scrollbarHitTest(Orientation.Vertical);

                    if ((hitTest == SB_HITEST.button) && (!leftKeyPressed()))
                    {
                        GetScrollBarInfo(_hControlWnd, OBJID_VSCROLL, ref sb);
                        // start the transition routines here
                        // size of mask - new Rectangle(sb.xyThumbTop, 2, sb.xyThumbBottom - sb.xyThumbTop, sb.rcScrollBar.Bottom)
                    }
                }
            }


        }

        private bool hasHorizontal()
        {
            return ((GetWindowLong(_hControlWnd, GWL_STYLE) & WS_HSCROLL) == WS_HSCROLL);
        }

        private bool hasSizer()
        {
            return (hasHorizontal() && hasVertical());
        }

        private bool hasVertical()
        {
            return ((GetWindowLong(_hControlWnd, GWL_STYLE) & WS_VSCROLL) == WS_VSCROLL);
        }

        public void invalidateWindow(bool messaged)
        {
            if (messaged)
                RedrawWindow(_hControlWnd, IntPtr.Zero, IntPtr.Zero, RDW_INTERNALPAINT);
            else
                RedrawWindow(_hControlWnd, IntPtr.Zero, IntPtr.Zero, RDW_INVALIDATE | RDW_UPDATENOW);
        }

        private bool leftKeyPressed()
        {
            if (mouseButtonsSwitched())
                return (GetKeyState(VK_RBUTTON) < 0);
            else
                return (GetKeyState(VK_LBUTTON) < 0);
        }

        private bool mouseButtonsSwitched()
        {
            return (GetSystemMetrics(SYSTEM_METRICS.SM_SWAPBUTTON) != 0);
        }

        private SB_HITEST scrollbarHitTest(Orientation orient)
        {
            Point pt = new Point();
            RECT tr = new RECT();
            RECT tp = new RECT();
            SCROLLBARINFO sb = new SCROLLBARINFO();
            sb.cbSize = Marshal.SizeOf(sb);

            GetCursorPos(ref pt);

            if (orient == Orientation.Horizontal)
            {
                GetScrollBarInfo(_hControlWnd, OBJID_HSCROLL, ref sb);
                tr = sb.rcScrollBar;
                //OffsetRect(ref tr, -tr.Left, -tr.Top);
                tp = tr;
                if (PtInRect(ref tr, pt))
                {
                    // left arrow
                    tp.Right = tp.Left + _iArrowCx;
                    if (PtInRect(ref tp, pt))
                        return SB_HITEST.leftArrow;
                    // right arrow
                    tp.Left = tr.Right - _iArrowCx;
                    tp.Right = tr.Right;
                    if (PtInRect(ref tp, pt))
                        return SB_HITEST.rightArrow;
                    // button
                    tp.Left = tr.Left + sb.xyThumbTop;
                    tp.Right = tr.Left + sb.xyThumbBottom;
                    if (PtInRect(ref tp, pt))
                        return SB_HITEST.button;
                    // track
                    return SB_HITEST.track;
                }
            }
            else
            {
                GetScrollBarInfo(_hControlWnd, OBJID_VSCROLL, ref sb);
                tr = sb.rcScrollBar;
                tp = tr;

                if (PtInRect(ref tr, pt))
                {
                    // top arrow
                    tp.Bottom = tr.Top + _iArrowCy;
                    if (PtInRect(ref tp, pt))
                        return SB_HITEST.topArrow;
                    // bottom arrow
                    tp.Top = tr.Bottom - _iArrowCy;
                    tp.Bottom = tr.Bottom;
                    if (PtInRect(ref tp, pt))
                        return SB_HITEST.bottomArrow;
                    // button
                    tp.Top = tr.Top + sb.xyThumbTop;
                    tp.Bottom = tr.Bottom + sb.xyThumbBottom;
                    if (PtInRect(ref tp, pt))
                        return SB_HITEST.button;
                    // track
                    return SB_HITEST.track;
                }
            }
            return SB_HITEST.offControl;
        }

        public void reSizeMask()
        {
            RECT tr = new RECT();
            SCROLLBARINFO sb = new SCROLLBARINFO();
            sb.cbSize = Marshal.SizeOf(sb);
            IntPtr hParent = GetParent(_hControlWnd);
            Point pt = new Point();

            if (hasVertical())
            {
                GetScrollBarInfo(_hControlWnd, OBJID_VSCROLL, ref sb);
                tr = sb.rcScrollBar;
                pt.X = tr.Left;
                pt.Y = tr.Top;
                ScreenToClient(hParent, ref pt);
                SetWindowPos(_hVerticalMaskWnd, IntPtr.Zero, pt.X, pt.Y, tr.Right - tr.Left, tr.Bottom - tr.Top, SWP_NOACTIVATE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_SHOWWINDOW);
            }
            if (hasHorizontal())
            {
                GetScrollBarInfo(_hControlWnd, OBJID_HSCROLL, ref sb);
                tr = sb.rcScrollBar;
                pt.X = tr.Left;
                pt.Y = tr.Top;
                ScreenToClient(hParent, ref pt);
                SetWindowPos(_hHorizontalMaskWnd, IntPtr.Zero, pt.X, pt.Y, tr.Right - tr.Left, tr.Bottom - tr.Top, SWP_NOACTIVATE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_SHOWWINDOW);
            }
            if (hasSizer())
            {
                GetScrollBarInfo(_hControlWnd, OBJID_HSCROLL, ref sb);
                tr = new RECT(sb.rcScrollBar.Right, sb.rcScrollBar.Top, sb.rcScrollBar.Right + _iArrowCx, sb.rcScrollBar.Bottom);
                pt.X = tr.Left;
                pt.Y = tr.Top;
                ScreenToClient(hParent, ref pt);
                SetWindowPos(_hSizerMaskWnd, IntPtr.Zero, pt.X, pt.Y, tr.Right - tr.Left, tr.Bottom - tr.Top, SWP_NOACTIVATE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_SHOWWINDOW);
            }
        }

        private void scrollbarMetrics()
        {
            _iArrowCx = GetSystemMetrics(SYSTEM_METRICS.SM_CXVSCROLL);
            _iArrowCy = GetSystemMetrics(SYSTEM_METRICS.SM_CYVSCROLL);
        }

        private void scrollHorizontal(bool Right)
        {
            if (Right)
                SendMessage(_hControlWnd, WM_HSCROLL, SB_LINERIGHT, 0);
            else
                SendMessage(_hControlWnd, WM_HSCROLL, SB_LINELEFT, 0);

        }

        private void scrollVertical(bool Down)
        {
            if (Down)
                SendMessage(_hControlWnd, WM_VSCROLL, SB_LINEDOWN, 0);
            else
                SendMessage(_hControlWnd, WM_VSCROLL, SB_LINEUP, 0);
        }

        public void Dispose()
        {
            try
            {
                this.ReleaseHandle();
                if (_oVerticalArrowBitmap != null) _oVerticalArrowBitmap.Dispose();
                if (_cVerticalArrowDc != null) _cVerticalArrowDc.Dispose();
                if (_oVerticalThumbBitmap != null) _oVerticalThumbBitmap.Dispose();
                if (_cVerticalThumbDc != null) _cVerticalThumbDc.Dispose();
                if (_oVerticalTrackBitmap != null) _oVerticalTrackBitmap.Dispose();
                if (_cVerticalTrackDc != null) _cVerticalTrackDc.Dispose();
                if (_oHorizontalArrowBitmap != null) _oHorizontalArrowBitmap.Dispose();
                if (_cHorizontalArrowDc != null) _cHorizontalArrowDc.Dispose();
                if (_oHorizontalThumbBitmap != null) _oHorizontalThumbBitmap.Dispose();
                if (_cHorizontalThumbDc != null) _cHorizontalThumbDc.Dispose();
                if (_oHorizontalTrackBitmap != null) _oHorizontalTrackBitmap.Dispose();
                if (_cHorizontalTrackDc != null) _cHorizontalTrackDc.Dispose();
                if (_hVerticalMaskWnd != IntPtr.Zero) DestroyWindow(_hVerticalMaskWnd);
                if (_hHorizontalMaskWnd != IntPtr.Zero) DestroyWindow(_hHorizontalMaskWnd);
                if (_hSizerMaskWnd != IntPtr.Zero) DestroyWindow(_hSizerMaskWnd);
            }
            catch { }
            GC.SuppressFinalize(this);
        }
        #endregion


        ////  Scrollbar direction
        ////  All these constents can be found in WinUser.h
        //// 
        //private const int SBS_HORZ = 0;
        //private const int SBS_VERT = 1;
        ////  Windows Messages
        ////  All these constents can be found in WinUser.h
        //// 
        //private const int WM_VSCROLL = 277;
        //private const int WM_HSCROLL = 276;
        //private const int SB_THUMBPOSITION = 4;
        //public enum eScrollAction
        //{

        //    Jump = 0,

        //    Relative = 1,
        //}
        //public enum eScrollDirection
        //{

        //    Vertical = 0,

        //    Horizontal = 1,
        //}


        ////  API Function: GetScrollPos
        ////  Returns an integer of the position of the scrollbar
        //// 
        //[DllImport("user32.dll")]
        //private static extern int GetScrollPos(IntPtr hWnd, int nBar);

        ////  API Function: SetScrollPos
        ////  Sets ONLY the scrollbar DOES NOT change the control object
        //// 
        //[DllImport("user32.dll")]
        //private static extern int SetScrollPos(IntPtr hWnd, int nBar, int nPos, bool bRedraw);

        //  API Function: PostMessageA
        //  Sends a message to a control (We are going to tell it to synch
        //  with the scrollbar)
        // 
        //[DllImport("user32.dll")]
        //private static extern bool PostMessageA(IntPtr hwnd, int wMsg, int wParam, int lParam);

        ////      Sub: scrollControl
        ////  Purpose: All functions to control the scroll are in here
        //// 
        //private void scrollControl(IntPtr hWnd, eScrollDirection Direction, eScrollAction Action, int Amount)
        //{
        //    int position;
        //    //  What direction are we going
        //    if ((Direction == eScrollDirection.Horizontal))
        //    {
        //        //  What action are we taking (Jumping or Relative)
        //        if ((Action == eScrollAction.Relative))
        //        {
        //            position = (GetScrollPos(hWnd, SBS_HORZ) + Amount);
        //        }
        //        else
        //        {
        //            position = Amount;
        //        }
        //        //  Make it so
        //        if ((SetScrollPos(hWnd, SBS_HORZ, position, true) != -1))
        //        {
        //            PostMessageA(hWnd, WM_HSCROLL, (SB_THUMBPOSITION + (65536 * position)), 0);
        //        }
        //        else
        //        {
        //            //MsgBox(("Can\'t set info (Err: "
        //            //                + (GetLastWin32Error() + ")")));
        //        }
        //    }
        //    else
        //    {
        //        //  What action are we taking (Jumping or Relative)
        //        if ((Action == eScrollAction.Relative))
        //        {
        //            position = (GetScrollPos(hWnd, SBS_VERT) + Amount);
        //        }
        //        else
        //        {
        //            position = Amount;
        //        }
        //        //  Make it so
        //        if ((SetScrollPos(hWnd, SBS_VERT, position, true) != -1))
        //        {
        //            PostMessageA(hWnd, WM_VSCROLL, (SB_THUMBPOSITION + (65536 * position)), 0);
        //        }
        //        else
        //        {
        //            //MsgBox(("Can\'t set info (Err: "
        //            //                + (GetLastWin32Error() + ")")));
        //        }
        //    }
        //}




        //#region WndProc
        //protected override void WndProc(ref Message m)
        //{

        //    switch (m.Msg)
        //    {

        //        case WM_VSCROLL:
        //            this.WndProc(ref m);



        //            break;
        //        case WM_HSCROLL:

        //            this.WndProc(ref m);



        //            break;
        //        default:
        //            this.WndProc(ref m);
        //            break;
        //    }
        //}
        //#endregion


        //public bool PreFilterMessage(ref Message m)
        //{
        //    switch (m.Msg)
        //    {
        //        case WM_MOUSEWHEEL:   // 0x020A
        //        case WM_MOUSEHWHEEL:  // 0x020E
        //            IntPtr hControlUnderMouse = WindowFromPoint(new Point((int)m.LParam));
        //            if (hControlUnderMouse == m.HWnd)
        //                return false; // already headed for the right control
        //            else
        //            {
        //                // redirect the message to the control under the mouse
        //                SendMessage(hControlUnderMouse, m.Msg, m.WParam, m.LParam);
        //                return true;
        //            }
        //        default:
        //            return false;
        //    }
        //}








        //protected override void OnResize(EventArgs e)
        //{
        //    this.OnResize(e);
        //    VisibleScrollbars = GetVisibleScrollbars(this);
        //}




        #region WndProc
        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {

                //case 0x000F: //WM_PAINT
                case 0x0083:
                    reSizeMask();
                    base.WndProc(ref m);

                    break;
                case WM_NCPAINT:
                    drawScrollBar();
                    base.WndProc(ref m);
                    break;

                case WM_MOUSEHWHEEL:  // 0x020E
                case WM_HSCROLL:
                    //scrollControl(m.WParam, eScrollDirection.Vertical, eScrollAction.Relative, 5);
                    _bTrackingMouse = true;
                    drawScrollBar();
                    scrollFader();

                    base.WndProc(ref m);
                    break;
                case WM_MOUSEWHEEL:   // 0x020A
                case WM_VSCROLL:
                    //scrollControl(m.WParam, eScrollDirection.Vertical, eScrollAction.Relative, -5);

                    _bTrackingMouse = true;
                    drawScrollBar();
                    scrollFader();
                    base.WndProc(ref m);
                    break;
                case WM_NCMOUSEMOVE:
                    _bTrackingMouse = true;
                    drawScrollBar();
                    scrollFader();
                    base.WndProc(ref m);
                    break;

                case WM_MOUSEMOVE:
                    if (_bTrackingMouse)
                        _bTrackingMouse = false;
                    base.WndProc(ref m);
                    break;


                case WM_SIZE:
                case WM_MOVE:
                    reSizeMask();
                    base.WndProc(ref m);
                    break;

                default:
                    checkBarState();
                    base.WndProc(ref m);
                    break;
            }
        }
        #endregion



    }









     #region Enums
     public enum StretchModeEnum : int
     {
         STRETCH_ANDSCANS = 1,
         STRETCH_ORSCANS = 2,
         STRETCH_DELETESCANS = 3,
         STRETCH_HALFTONE = 4,
     }
     #endregion

     #region GraphicsMode
     public class GraphicsMode : IDisposable
     {
         #region Instance Fields
         private Graphics _cGraphicCopy;
         private SmoothingMode _eOldMode;
         #endregion

         #region Identity
         /// <summary>
         /// Initialize a new instance of the class.
         /// </summary>
         /// <param name="g">Graphics instance.</param>
         /// <param name="mode">Desired Smoothing mode.</param>
         public GraphicsMode(Graphics g, SmoothingMode mode)
         {
             _cGraphicCopy = g;
             _eOldMode = _cGraphicCopy.SmoothingMode;
             _cGraphicCopy.SmoothingMode = mode;
         }

         /// <summary>
         /// Revert the SmoothingMode to original setting.
         /// </summary>
         public void Dispose()
         {
             _cGraphicCopy.SmoothingMode = _eOldMode;
         }
         #endregion
     }
     #endregion

     #region StretchMode
     public class StretchMode : IDisposable
     {
         #region API
         [DllImport("gdi32.dll")]
         [return: MarshalAs(UnmanagedType.Bool)]
         private static extern bool SetStretchBltMode(IntPtr hdc, StretchModeEnum iStretchMode);

         [DllImport("gdi32.dll")]
         private static extern int GetStretchBltMode(IntPtr hdc);
         #endregion

         #region Fields
         private StretchModeEnum _eOldMode = StretchModeEnum.STRETCH_ANDSCANS;
         private IntPtr _pHdc = IntPtr.Zero;
         #endregion

         public StretchMode(IntPtr hdc, StretchModeEnum mode)
         {
             _eOldMode = (StretchModeEnum)GetStretchBltMode(hdc);
             _pHdc = hdc;
             SetStretchBltMode(hdc, mode);
         }

         public void Dispose()
         {
             SetStretchBltMode(_pHdc, _eOldMode);
         }
     }
     #endregion

     #region StretchImage
     public class StretchImage : IDisposable
     {
         #region API
         [DllImport("gdi32.dll")]
         [return: MarshalAs(UnmanagedType.Bool)]
         private static extern bool SetStretchBltMode(IntPtr hdc, StretchModeEnum eStretchMode);

         [DllImport("gdi32.dll")]
         private static extern int GetStretchBltMode(IntPtr hdc);

         [DllImport("gdi32.dll")]
         [return: MarshalAs(UnmanagedType.Bool)]
         private static extern bool StretchBlt(IntPtr hDest, int X, int Y, int nWidth, int nHeight, IntPtr hdcSrc,
         int sX, int sY, int nWidthSrc, int nHeightSrc, int dwRop);
         #endregion

         #region Fields
         private StretchModeEnum _eOldMode = StretchModeEnum.STRETCH_ANDSCANS;
         private IntPtr _pHdc = IntPtr.Zero;
         #endregion

         public StretchImage(IntPtr sourceDc, IntPtr destDc, Rectangle src, Rectangle dest, int depth, StretchModeEnum eStretchMode)
         {
             _eOldMode = (StretchModeEnum)GetStretchBltMode(sourceDc);
             _pHdc = sourceDc;
             SetStretchBltMode(sourceDc, eStretchMode);

             // left
             StretchBlt(destDc, dest.Left, dest.Top, depth, dest.Height, sourceDc, src.Left, 0, depth, src.Height, 0xCC0020);
             // right
             StretchBlt(destDc, dest.Right - depth, dest.Top, depth, dest.Height, sourceDc, src.Right - depth, 0, depth, src.Height, 0xCC0020);
             // top
             StretchBlt(destDc, dest.Left + depth, dest.Top, dest.Width - (2 * depth), depth, sourceDc, src.Left + depth, 0, src.Width - (2 * depth), depth, 0xCC0020);
             // bottom
             StretchBlt(destDc, dest.Left + depth, dest.Bottom - depth, dest.Width - (2 * depth), depth, sourceDc, src.Left + depth, src.Bottom - depth, src.Width - (2 * depth), depth, 0xCC0020);
             // center
             StretchBlt(destDc, dest.Left + depth, dest.Top + depth, dest.Width - (2 * depth), dest.Height - (2 * depth), sourceDc, src.Left + depth, depth, src.Width - (2 * depth), src.Height - (2 * depth), 0xCC0020);
         }

         public void Dispose()
         {
             SetStretchBltMode(_pHdc, _eOldMode);
         }
     }
     #endregion

     #region AlphaStretch
     public class AlphaStretch : IDisposable
     {
         private const byte AC_SRC_OVER = 0x00;
         private const byte AC_SRC_ALPHA = 0x01;

         [StructLayout(LayoutKind.Sequential)]
         private struct BLENDFUNCTION
         {
             byte BlendOp;
             byte BlendFlags;
             byte SourceConstantAlpha;
             byte AlphaFormat;

             internal BLENDFUNCTION(byte op, byte flags, byte alpha, byte format)
             {
                 BlendOp = op;
                 BlendFlags = flags;
                 SourceConstantAlpha = alpha;
                 AlphaFormat = format;
             }
         }

         [DllImport("gdi32.dll", EntryPoint = "GdiAlphaBlend")]
         private static extern bool AlphaBlend(IntPtr hdcDest, int nXOriginDest, int nYOriginDest, int nWidthDest, int nHeightDest,
         IntPtr hdcSrc, int nXOriginSrc, int nYOriginSrc, int nWidthSrc, int nHeightSrc, BLENDFUNCTION blendFunction);

         public AlphaStretch(IntPtr sourceDc, IntPtr destDc, Rectangle src, Rectangle dest, int depth, byte opacity)
         {
             BLENDFUNCTION bf = new BLENDFUNCTION(AC_SRC_OVER, 0x0, opacity, 0x0);
             // left
             AlphaBlend(destDc, dest.Left, dest.Top, depth, dest.Height, sourceDc, src.Left, 0, depth, src.Height, bf);
             // right
             AlphaBlend(destDc, dest.Right - depth, dest.Top, depth, dest.Height, sourceDc, src.Right - depth, 0, depth, src.Height, bf);
             // top
             AlphaBlend(destDc, dest.Left + depth, dest.Top, dest.Width - (2 * depth), depth, sourceDc, src.Left + depth, 0, src.Width - (2 * depth), depth, bf);
             // bottom
             AlphaBlend(destDc, dest.Left + depth, dest.Bottom - depth, dest.Width - (2 * depth), depth, sourceDc, src.Left + depth, src.Bottom - depth, src.Width - (2 * depth), depth, bf);
             // center
             AlphaBlend(destDc, dest.Left + depth, dest.Top + depth, dest.Width - (2 * depth), dest.Height - (2 * depth), sourceDc, src.Left + depth, depth, src.Width - (2 * depth), src.Height - (2 * depth), bf);
         }

         public void Dispose()
         {
             //
         }
     }
     #endregion




     #region StoreDc
     [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust")]
     public class cStoreDc
     {
         [DllImport("gdi32.dll")]
         private static extern IntPtr CreateDCA([MarshalAs(UnmanagedType.LPStr)]string lpszDriver, [MarshalAs(UnmanagedType.LPStr)]string lpszDevice, [MarshalAs(UnmanagedType.LPStr)]string lpszOutput, int lpInitData);

         [DllImport("gdi32.dll")]
         private static extern IntPtr CreateDCW([MarshalAs(UnmanagedType.LPWStr)]string lpszDriver, [MarshalAs(UnmanagedType.LPWStr)]string lpszDevice, [MarshalAs(UnmanagedType.LPWStr)]string lpszOutput, int lpInitData);

         [DllImport("gdi32.dll")]
         private static extern IntPtr CreateDC(string lpszDriver, string lpszDevice, string lpszOutput, int lpInitData);

         [DllImport("gdi32.dll")]
         private static extern IntPtr CreateCompatibleDC(IntPtr hdc);

         [DllImport("gdi32.dll")]
         private static extern IntPtr CreateCompatibleBitmap(IntPtr hdc, int nWidth, int nHeight);

         [DllImport("gdi32.dll")]
         [return: MarshalAs(UnmanagedType.Bool)]
         private static extern bool DeleteDC(IntPtr hdc);

         [DllImport("gdi32.dll", PreserveSig = true)]
         private static extern IntPtr SelectObject(IntPtr hdc, IntPtr hgdiobj);

         [DllImport("gdi32.dll")]
         [return: MarshalAs(UnmanagedType.Bool)]
         private static extern bool DeleteObject(IntPtr hObject);

         private int _iHeight = 0;
         private int _iWidth = 0;
         private IntPtr _pHdc = IntPtr.Zero;
         private IntPtr _pBmp = IntPtr.Zero;
         private IntPtr _pBmpOld = IntPtr.Zero;

         public IntPtr Hdc
         {
             get { return _pHdc; }
         }

         public IntPtr HBmp
         {
             get { return _pBmp; }
         }

         public int Height
         {
             get { return _iHeight; }
             set
             {
                 if (_iHeight != value)
                 {
                     _iHeight = value;
                     ImageCreate(_iWidth, _iHeight);
                 }
             }
         }

         public int Width
         {
             get { return _iWidth; }
             set
             {
                 if (_iWidth != value)
                 {
                     _iWidth = value;
                     ImageCreate(_iWidth, _iHeight);
                 }
             }
         }

         private void ImageCreate(int width, int height)
         {
             IntPtr pHdc = IntPtr.Zero;

             ImageDestroy();
             pHdc = CreateDCA("DISPLAY", "", "", 0);
             _pHdc = CreateCompatibleDC(pHdc);
             _pBmp = CreateCompatibleBitmap(pHdc, _iWidth, _iHeight);
             _pBmpOld = SelectObject(_pHdc, _pBmp);
             if (_pBmpOld == IntPtr.Zero)
             {
                 ImageDestroy();
             }
             else
             {
                 _iWidth = width;
                 _iHeight = height;
             }
             DeleteDC(pHdc);
             pHdc = IntPtr.Zero;
         }

         private void ImageDestroy()
         {
             if (_pBmpOld != IntPtr.Zero)
             {
                 SelectObject(_pHdc, _pBmpOld);
                 _pBmpOld = IntPtr.Zero;
             }
             if (_pBmp != IntPtr.Zero)
             {
                 DeleteObject(_pBmp);
                 _pBmp = IntPtr.Zero;
             }
             if (_pHdc != IntPtr.Zero)
             {
                 DeleteDC(_pHdc);
                 _pHdc = IntPtr.Zero;
             }
         }

         public void Dispose()
         {
             ImageDestroy();
         }
     }
     #endregion















     [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust")]
     public class cTransition : NativeWindow, IDisposable
     {
         #region Constants
         private const int WM_PAINT = 0xF;
         private const int WM_NCPAINT = 0x85;
         private const int WM_TIMER = 0x113;
         private const int WM_NCMOUSEMOVE = 0xA0;
         private const int WM_MOUSEMOVE = 0x200;
         private const int WM_MOUSELEAVE = 0x2A3;
         private const int WM_LBUTTONDOWN = 0x201;
         private const int WM_RBUTTONDOWN = 0x204;
         #endregion

         #region Structs
         [StructLayout(LayoutKind.Sequential)]
         internal struct RECT
         {
             internal RECT(int X, int Y, int Width, int Height)
             {
                 this.Left = X;
                 this.Top = Y;
                 this.Right = Width;
                 this.Bottom = Height;
             }
             internal int Left;
             internal int Top;
             internal int Right;
             internal int Bottom;
         }

         [StructLayout(LayoutKind.Sequential)]
         private struct PAINTSTRUCT
         {
             internal IntPtr hdc;
             internal int fErase;
             internal RECT rcPaint;
             internal int fRestore;
             internal int fIncUpdate;
             internal int Reserved1;
             internal int Reserved2;
             internal int Reserved3;
             internal int Reserved4;
             internal int Reserved5;
             internal int Reserved6;
             internal int Reserved7;
             internal int Reserved8;
         }
         #endregion

         #region API
         [DllImport("user32.dll")]
         private static extern IntPtr BeginPaint(IntPtr hWnd, ref PAINTSTRUCT ps);

         [DllImport("user32.dll")]
         [return: MarshalAs(UnmanagedType.Bool)]
         private static extern bool EndPaint(IntPtr hWnd, ref PAINTSTRUCT ps);

         [DllImport("user32.dll")]
         private static extern IntPtr GetDC(IntPtr handle);

         [DllImport("user32.dll")]
         private static extern int ReleaseDC(IntPtr handle, IntPtr hdc);

         [DllImport("gdi32.dll")]
         [return: MarshalAs(UnmanagedType.Bool)]
         private static extern bool BitBlt(IntPtr hdc, int nXDest, int nYDest, int nWidth, int nHeight, IntPtr hdcSrc, int nXSrc, int nYSrc, int dwRop);

         [DllImport("user32.dll")]
         [return: MarshalAs(UnmanagedType.Bool)]
         private static extern bool PtInRect([In] ref RECT lprc, Point pt);

         [DllImport("user32.dll")]
         [return: MarshalAs(UnmanagedType.Bool)]
         private static extern bool GetCursorPos(ref Point lpPoint);

         [DllImport("user32.dll")]
         private static extern int ScreenToClient(IntPtr hwnd, ref Point lpPoint);

         [DllImport("gdi32.dll")]
         private static extern IntPtr SelectObject(IntPtr hdc, IntPtr hObject);

         [DllImport("user32.dll")]
         private static extern IntPtr GetDesktopWindow();

         [DllImport("user32.dll")]
         [return: MarshalAs(UnmanagedType.Bool)]
         private static extern bool GetWindowRect(IntPtr hWnd, ref RECT lpRect);

         [DllImport("user32.dll")]
         private extern static int OffsetRect(ref RECT lpRect, int x, int y);

         [DllImport("user32.dll")]
         private static extern IntPtr SetTimer(IntPtr hWnd, int nIDEvent, uint uElapse, IntPtr lpTimerFunc);

         [DllImport("user32.dll")]
         [return: MarshalAs(UnmanagedType.Bool)]
         private static extern bool KillTimer(IntPtr hWnd, uint uIDEvent);

         [DllImport("user32.dll")]
         [return: MarshalAs(UnmanagedType.Bool)]
         private static extern bool ValidateRect(IntPtr hWnd, ref RECT lpRect);
         #endregion

         #region Fields
         private bool _bPainting = false;
         private bool _bFadeIn = false;
         private int _maskTimer = 0;
         private int _safeTimer = 0;
         private Rectangle _areaRect;
         private IntPtr _hControlWnd = IntPtr.Zero;
         private IntPtr _hParentWnd = IntPtr.Zero;
         private Bitmap _oMask;
         private cStoreDc _cMaskDc = new cStoreDc();
         private cStoreDc _cTransitionDc = new cStoreDc();
         #endregion

         #region Events
         public delegate void DisposingDelegate();
         public event DisposingDelegate Disposing;
         #endregion

         #region Constructor
         public cTransition(IntPtr hWnd, IntPtr hParent, Bitmap mask, Rectangle area)
         {
             if (hWnd == IntPtr.Zero)
                 throw new Exception("The control handle is invalid.");
             _hControlWnd = hWnd;
             if (mask != null)
             {
                 TransitionGraphic = mask;
                 _areaRect = area;
                 if (hParent != IntPtr.Zero)
                 {
                     _hParentWnd = hParent;
                     this.AssignHandle(_hParentWnd);
                 }
                 else
                 {
                     _hParentWnd = _hControlWnd;
                     this.AssignHandle(_hControlWnd);
                 }
                 startTimer();
             }
         }

         public void Dispose()
         {
             this.ReleaseHandle();
             if (Disposing != null) Disposing();
             stopTimer();
             if (_cTransitionDc != null) _cTransitionDc.Dispose();
             if (_cMaskDc != null) _cMaskDc.Dispose();
             GC.SuppressFinalize(this);
         }
         #endregion

         #region Properties
         private Bitmap TransitionGraphic
         {
             get { return _oMask; }
             set
             {
                 _oMask = value;
                 if (_cTransitionDc.Hdc != IntPtr.Zero)
                 {
                     _cTransitionDc.Dispose();
                     _cTransitionDc = new cStoreDc();
                 }
                 _cTransitionDc.Width = _oMask.Width;
                 _cTransitionDc.Height = _oMask.Height;
                 SelectObject(_cTransitionDc.Hdc, _oMask.GetHbitmap());
             }
         }
         #endregion

         #region Methods
         private void startTimer()
         {
             if (_safeTimer > 0)
                 stopTimer();
             SetTimer(_hParentWnd, 66, 25, IntPtr.Zero);
         }

         private void stopTimer()
         {
             if (_safeTimer > 0)
             {
                 KillTimer(_hParentWnd, 66);
                 _safeTimer = 0;
             }
         }

         private void fadeIn()
         {
             if (_bFadeIn == false)
             {
                 _bFadeIn = true;
                 captureDc();
             }
             if (_maskTimer < 10)
                 _maskTimer++;
             drawMask();

         }

         private void fadeOut()
         {
             if (_bFadeIn == true)
                 _bFadeIn = false;
             if (_maskTimer > 1)
             {
                 _maskTimer--;
                 drawMask();
             }
             else
             {
                 Control ct = Control.FromHandle(_hParentWnd);
                 if (ct != null)
                     ct.Refresh();
                 this.Dispose();
             }
         }

         private void drawMask()
         {
             byte bt = 0;
             IntPtr hdc = IntPtr.Zero;
             cStoreDc tempDc = new cStoreDc();
             RECT tr = new RECT();

             GetWindowRect(_hControlWnd, ref tr);
             OffsetRect(ref tr, -tr.Left, -tr.Top);

             Rectangle bounds = new Rectangle(_areaRect.Left, _areaRect.Top, _areaRect.Right - _areaRect.Left, _areaRect.Bottom - _areaRect.Top);
             tempDc.Width = tr.Right;
             tempDc.Height = tr.Bottom;

             bt = (byte)(_maskTimer * 15);
             BitBlt(tempDc.Hdc, 0, 0, tr.Right, tr.Bottom, _cMaskDc.Hdc, 0, 0, 0xCC0020);
             using (AlphaStretch al = new AlphaStretch(_cTransitionDc.Hdc, tempDc.Hdc, new Rectangle(0, 0, _cTransitionDc.Width, _cTransitionDc.Height), bounds, 2, bt)) { }
             hdc = GetDC(_hControlWnd);
             BitBlt(hdc, 0, 0, tr.Right, tr.Bottom, tempDc.Hdc, 0, 0, 0xCC0020);
             ReleaseDC(_hControlWnd, hdc);
             tempDc.Dispose();
             ValidateRect(_hControlWnd, ref tr);
         }

         private void captureDc()
         {
             RECT tr = new RECT();
             GetWindowRect(_hControlWnd, ref tr);

             _cMaskDc.Width = tr.Right - tr.Left;
             _cMaskDc.Height = tr.Bottom - tr.Top;
             if (_cMaskDc.Hdc != IntPtr.Zero)
             {
                 using (Graphics g = Graphics.FromHdc(_cMaskDc.Hdc))
                     g.CopyFromScreen(tr.Left, tr.Top, 0, 0, new Size(_cMaskDc.Width, _cMaskDc.Height), CopyPixelOperation.SourceCopy);
             }
         }

         private bool inArea()
         {
             Point pt = new Point();
             RECT tr = new RECT(_areaRect.Left, _areaRect.Top, _areaRect.Right, _areaRect.Bottom);
             GetCursorPos(ref pt);
             ScreenToClient(_hParentWnd, ref pt);
             if (PtInRect(ref tr, pt))
                 return true;
             return false;
         }
         #endregion

         #region WndProc
         protected override void WndProc(ref Message m)
         {
             PAINTSTRUCT ps = new PAINTSTRUCT();

             switch (m.Msg)
             {
                 case WM_PAINT:
                     if (!_bPainting && _maskTimer > 1)
                     {
                         _bPainting = true;
                         // start painting engine
                         BeginPaint(m.HWnd, ref ps);
                         drawMask();
                         // done
                         EndPaint(m.HWnd, ref ps);
                         _bPainting = false;
                     }
                     else
                     {
                         base.WndProc(ref m);
                     }
                     break;

                 case WM_TIMER:
                     if ((_safeTimer > 50) && (!inArea()))
                     {
                         stopTimer();
                     }
                     else
                     {
                         if (_bFadeIn == true)
                             fadeIn();
                         else
                             fadeOut();
                     }
                     _safeTimer++;
                     base.WndProc(ref m);
                     break;

                 case WM_MOUSEMOVE:
                     if (inArea())
                         fadeIn();
                     else
                         fadeOut();
                     base.WndProc(ref m);
                     break;

                 case WM_MOUSELEAVE:
                     fadeOut();
                     base.WndProc(ref m);
                     break;

                 case WM_LBUTTONDOWN:
                     Dispose();
                     base.WndProc(ref m);
                     break;

                 default:
                     base.WndProc(ref m);
                     break;
             }
         }
         #endregion






     }







    













}









//namespace ProGammaX
//{
//    using System;
//    using System.Drawing;
//    using System.Reflection;
//    using System.Windows.Forms;

//    public class myComboBox : ComboBox
//    {



//        static Color COLOR_FOCUSED = Color.Blue;
//        static Color COLOR_NOT_FOCUSED = Color.White;
//        static Color COLOR_FOCUSED_FILL = Color.FromArgb(173, 173, 209);

//        public myComboBox()
//        {
//            this.DrawMode = DrawMode.OwnerDrawVariable;
//            SetStyle(ControlStyles.UserPaint, true);
//        }

//        protected override sealed void OnPaint(PaintEventArgs pea)
//        {
//            base.OnPaint(pea);
//            drawControl(this.Focused);
//        }

//        protected override sealed void OnPaintBackground(PaintEventArgs pea)
//        {
//            base.OnPaintBackground(pea);
//            drawControl(this.Focused);
//        }

//        protected override void OnMove(System.EventArgs e)
//        {
//            base.OnMove(e);
//        }

//        protected override void OnMouseEnter(System.EventArgs e)
//        {
//            Invalidate();
//            drawControl(true);
//        }

//        protected override void OnMouseLeave(System.EventArgs e)
//        {
//            Invalidate();
//            if (this.Focused == false)
//                drawControl(false);
//        }

//        protected override void OnMouseHover(System.EventArgs e)
//        {
//            Invalidate();
//            drawControl(true);
//        }

//        protected override void OnGotFocus(System.EventArgs e)
//        {
//            Invalidate();
//            drawControl(this.Focused);
//        }

//        protected override void OnLostFocus(System.EventArgs e)
//        {
//            Invalidate();
//            drawControl(this.Focused);
//        }

//        protected override void OnClick(System.EventArgs e)
//        {
//            drawControl(true);
//        }

//        protected override void OnTextChanged(EventArgs e)
//        {
//            //base.OnTextChanged(e);
//            drawControl(true);
//        }

//        protected override void OnDrawItem(DrawItemEventArgs Args)
//        {

//            Args.DrawBackground();

//            Int32 Index = Args.Index;

//            if (Index < 0 || Index >= Items.Count)
//            {
//                return;
//            }

//            using (Brush ItemBrush = new SolidBrush(Color.Red))
//            {

//                Rectangle ItemRectangle = new Rectangle();
//                ItemRectangle.X = Args.Bounds.Left;
//                ItemRectangle.Y = Args.Bounds.Top + ((Args.Bounds.Height - ItemHeight) / 2);
//                ItemRectangle.Width = Args.Bounds.Width;
//                ItemRectangle.Height = ItemHeight;
//                Args.Graphics.DrawString(Items[Args.Index].ToString(), Args.Font, ItemBrush, ItemRectangle);

//            }

//            Args.DrawFocusRectangle();
//            return;

//        }
//        private void drawBorder(Color color)
//        {
//            Graphics g = Graphics.FromHwnd(this.Handle);
//            Rectangle client = this.ClientRectangle;
//            Rectangle child = new Rectangle(client.Right - 16, client.Top, 16,
//              client.Height);
//            SolidBrush brush;

//            if (this.Focused)
//                brush = new SolidBrush(Color.White);
//            else
//                brush = new SolidBrush(Color.Black);

//            if (color == COLOR_FOCUSED)
//            {
//                // fill the drop down arrow box and fill the arrow with the right color
//                g.FillRectangle(new SolidBrush(COLOR_FOCUSED_FILL), child);
//                g.FillPolygon(brush, new Point[]
//        {
//          new Point(client.Right - 11, client.Top + 11),
//          new Point(client.Right - 6, client.Top + 11),
//          new Point(client.Right - 9, client.Top + 14),
//          new Point(client.Right - 11, client.Top + 11)
//        });
//            }

//            ControlPaint.DrawBorder(g, client, color, ButtonBorderStyle.Solid);
//            ControlPaint.DrawBorder(g, child, color, ButtonBorderStyle.Solid);

//            brush.Dispose();
//            g.Dispose();
//        }

//        private void drawButton(Color color)
//        {
//            Graphics grfx = Graphics.FromHwnd(this.Handle);
//            Rectangle rect = this.ClientRectangle;

//            if (color == COLOR_FOCUSED)
//                drawBorder(COLOR_FOCUSED);
//            else
//            {
//                drawBorder(COLOR_NOT_FOCUSED);
//                Rectangle child = new Rectangle(rect.Right - 16, rect.Top, 15,
//                  rect.Height);
//                ControlPaint.DrawComboButton
//                  (grfx, rect.Right - 16, rect.Top + 2, 15, 20, ButtonState.Flat);
//            }
//            grfx.Dispose();
//        }

//        private void drawControl(bool focused)
//        {
//            drawBorder(focused == true ? COLOR_FOCUSED : COLOR_NOT_FOCUSED);
//            drawButton(focused == true ? COLOR_FOCUSED : COLOR_NOT_FOCUSED);
//        }

//    }

//}