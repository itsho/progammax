﻿using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Drawing.Drawing2D;
using System.Threading;
using System.Reflection;


namespace ProGammaX
{




    public class ListViewColumnSorter : IComparer
    {
        /// <summary>
        /// Specifies the column to be sorted
        /// </summary>
        private int ColumnToSort;
        /// <summary>
        /// Specifies the order in which to sort (i.e. 'Ascending').
        /// </summary>
        private SortOrder OrderOfSort;
        /// <summary>
        /// Case insensitive comparer object
        /// </summary>
        private CaseInsensitiveComparer ObjectCompare;

        /// <summary>
        /// Class constructor.  Initializes various elements
        /// </summary>
        public ListViewColumnSorter()
        {
            // Initialize the column to '0'
            ColumnToSort = 0;

            // Initialize the sort order to 'none'
            OrderOfSort = SortOrder.None;

            // Initialize the CaseInsensitiveComparer object
            ObjectCompare = new CaseInsensitiveComparer();
        }

        /// <summary>
        /// This method is inherited from the IComparer interface.  It compares the two objects passed using a case insensitive comparison.
        /// </summary>
        /// <param name="x">First object to be compared</param>
        /// <param name="y">Second object to be compared</param>
        /// <returns>The result of the comparison. "0" if equal, negative if 'x' is less than 'y' and positive if 'x' is greater than 'y'</returns>
        public int Compare(object x, object y)
        {
            int compareResult;
            ListViewItem listviewX, listviewY;

            // Cast the objects to be compared to ListViewItem objects
            listviewX = (ListViewItem)x;
            listviewY = (ListViewItem)y;

            // Compare the two items
            compareResult = ObjectCompare.Compare(listviewX.SubItems[ColumnToSort].Text, listviewY.SubItems[ColumnToSort].Text);

            // Calculate correct return value based on object comparison
            if (OrderOfSort == SortOrder.Ascending)
            {
                // Ascending sort is selected, return normal result of compare operation
                return compareResult;
            }
            else if (OrderOfSort == SortOrder.Descending)
            {
                // Descending sort is selected, return negative result of compare operation
                return (-compareResult);
            }
            else
            {
                // Return '0' to indicate they are equal
                return 0;
            }
        }

        /// <summary>
        /// Gets or sets the number of the column to which to apply the sorting operation (Defaults to '0').
        /// </summary>
        public int SortColumn
        {
            set
            {
                ColumnToSort = value;
            }
            get
            {
                return ColumnToSort;
            }
        }

        /// <summary>
        /// Gets or sets the order of sorting to apply (for example, 'Ascending' or 'Descending').
        /// </summary>
        public SortOrder Order
        {
            set
            {
                OrderOfSort = value;
            }
            get
            {
                return OrderOfSort;
            }
        }

    }
















    class listView : ListView
    {



        public listView()
        {
            this.OwnerDraw = true;
            this.HideSelection = false;
            this.DoubleBuffered = true;
            this.DrawColumnHeader += myListView_DrawColumnHeader;
            this.DrawItem += myListView_DrawItem;


        }


        protected override void DefWndProc(ref System.Windows.Forms.Message m)
        {
            //Don't turn selected item's backcolor to gray when losing focus:

            if (m.Msg != 8)
            {
                base.DefWndProc(ref m);
            }

        }




        /// <summary>
        /// Add method for ListView control.
        /// </summary>
        /// <param name="ct">Control type [ListView]</param>
        /// <param name="header">Header image</param>
        /// <param name="hztrack">Horizontal track image</param>
        /// <param name="hzarrow">Horizontal arrow image</param>
        /// <param name="hzthumb">Horizontal thumb image</param>
        /// <param name="vttrack">Vertical track image</param>
        /// <param name="vtarrow">Vertical arrow image</param>
        /// <param name="vtthumb">Vertical thumb image</param>
        /// 
        //private Dictionary<IntPtr, cListView> _oListviewSkin;
        private Bitmap _oTransitionMask;
        //private void Add(string ctlname, Bitmap header, Bitmap hztrack, Bitmap hzarrow, Bitmap hzthumb, Bitmap vttrack, Bitmap vtarrow, Bitmap vtthumb)
        //{
        //    List<IntPtr> list = GetChildWindows(this.FindForm().Handle);
        //    StringBuilder nameBldr = new StringBuilder(100);
        //    //string ctlname = ct.ToString().ToLower();
        //    //if (ctlname == "listview")
        //    //{
        //        if (_oListviewSkin == null)
        //            _oListviewSkin = new Dictionary<IntPtr, cListView>();

        //        for (int i = 0; i < list.Count; i++)
        //        {
        //            if (list[i] != IntPtr.Zero)
        //            {
        //                Control ctl = Control.FromHandle(list[i]);
        //                if (ctl != null)
        //                {
        //                    Type t = ctl.GetType();
        //                    string name = t.Name.ToLower();
        //                    if (name == ctlname)
        //                    {
        //                        _oListviewSkin.Add(ctl.Handle, new cListView(ctl.Handle, header, hztrack, hzarrow, hzthumb, vttrack, vtarrow, vtthumb, TransitionGraphic));
        //                        ctl.Refresh();
        //                    }
        //                }
        //            }
        //        }
        //    //}
        //}


        //private List<IntPtr> GetChildWindows(IntPtr parent)
        //{
        //    List<IntPtr> result = new List<IntPtr>();
        //    GCHandle listHandle = GCHandle.Alloc(result);
        //    try
        //    {
        //        EnumWindowProc childProc = new EnumWindowProc(EnumWindow);
        //        EnumChildWindows(parent, childProc, GCHandle.ToIntPtr(listHandle));
        //    }
        //    finally
        //    {
        //        if (listHandle.IsAllocated)
        //            listHandle.Free();
        //    }
        //    return result;
        //}


        //[DllImport("user32")]
        //[return: MarshalAs(UnmanagedType.Bool)]
        //private static extern bool EnumChildWindows(IntPtr window, EnumWindowProc callback, IntPtr i);

        //public delegate bool EnumWindowProc(IntPtr hWnd, IntPtr parameter);


        //private bool EnumWindow(IntPtr handle, IntPtr pointer)
        //{
        //    GCHandle gch = GCHandle.FromIntPtr(pointer);
        //    List<IntPtr> list = gch.Target as List<IntPtr>;

        //    if (list != null)
        //    {
        //        list.Add(handle);
        //        return true;
        //    }
        //    return false;
        //}


        public Bitmap TransitionGraphic
        {
            get { return _oTransitionMask; }
            set { _oTransitionMask = value; }
        }














        // window messages
        private const int WM_PAINT = 0xF;
        private const int WM_NCPAINT = 0x85;
        private const int WM_NCMOUSEMOVE = 0xA0;
        private const int WM_MOUSEMOVE = 0x200;
        private const int WM_MOUSELEAVE = 0x2A3;
        private const int WM_LBUTTONDOWN = 0x201;
        private const int WM_LBUTTONUP = 0x202;
        private const int WM_LBUTTONDBLCLK = 0x203;
        private const int WM_RBUTTONDOWN = 0x204;
        private const int WM_RBUTTONUP = 0x205;
        private const int WM_RBUTTONDBLCLK = 0x206;
        private const int WM_MBUTTONDOWN = 0x207;
        private const int WM_MBUTTONUP = 0x208;
        private const int WM_MBUTTONDBLCLK = 0x209;
        private const int WM_MOUSEWHEEL = 0x20A;
        private const int WM_MOUSEHWHEEL = 0x020E;
        private const int WM_STYLECHANGED = 0x7D;
        private const int WM_SIZE = 0x5;
        private const int WM_MOVE = 0x3;



        //bool visible = false;
        //protected override void OnVisibleChanged(System.EventArgs e)
        //{
        //    visible = this.Visible;

        //    base.OnVisibleChanged(e);
        //}


        //ProGammaX.cRCM _cRcm;


        //internal cInternalScrollBar scrollBar;
        //private bool handleCreated = false;
        protected override void OnHandleCreated(EventArgs e)
        {


            base.OnHandleCreated(e);

            //handleCreated = true;
            //dispose();
            //scrollBar = new cInternalScrollBar(this.Handle, MainForm.mainFormRef.vienna_ScrollHorzShaft, MainForm.mainFormRef.vienna_ScrollHorzArrow, MainForm.mainFormRef.vienna_ScrollHorzThumb, MainForm.mainFormRef.vienna_ScrollVertShaft, MainForm.mainFormRef.vienna_ScrollVertArrow, MainForm.mainFormRef.vienna_ScrollVertThumb, MainForm.mainFormRef.vienna_ScrollHorzShaft);


        }

        protected override void OnClick(EventArgs e)
        {
            base.OnClick(e);

            this.Refresh();
        }

        protected override void OnHandleDestroyed(EventArgs e)
        {
            try
            {
                //scrollBar.Dispose();
            }
            catch
            {

            }
            //handleCreated = false;
            base.OnHandleDestroyed(e);
        }


        private const int WM_VSCROLL = 0x0115;
        private const int WM_HSCROLL = 0x0114;

        

        protected override void OnVisibleChanged(EventArgs e)
        {
            base.OnVisibleChanged(e);
            try
            {
                
                //if (handleCreated && this.Visible)
                //{
                //    scrollBar = new cInternalScrollBar(this.Handle, MainForm.mainFormRef.vienna_ScrollHorzShaft, MainForm.mainFormRef.vienna_ScrollHorzArrow, MainForm.mainFormRef.vienna_ScrollHorzThumb, MainForm.mainFormRef.vienna_ScrollVertShaft, MainForm.mainFormRef.vienna_ScrollVertArrow, MainForm.mainFormRef.vienna_ScrollVertThumb, MainForm.mainFormRef.vienna_ScrollHorzShaft);
                //    handleCreated = false;


                //}


                if (this.FindForm().Visible)
                {
                    //ProGammaX.MainForm.mainFormRef._cRcm = null;
                    start();
                }
                else
                {
                    //
                }

                if (this.Visible)
                { 
                    this.Refresh(); 
                }


            }
            catch
            {


            }
            

        }

        private void start()
        {




            try
            {
                if (!MainForm.mainFormRef.checkBox_OPTIONS_Themes_UseSkinOnControls.Checked)
                {
                    return;

                }
            }
            catch
            {
                return;
            }




            try
            {
                if (this.Visible)
                {
                    //this.Refresh();
                    //ProGammaX.MainForm.mainFormRef.loadControlColors();
                    //ProGammaX.MainForm.mainFormRef.loadControlImages(1, this.FindForm());
                    //ProGammaX.MainForm.mainFormRef._cRcm = new ProGammaX.cRCM(this.FindForm().Handle);
                    // skin controls in containers [tabcontrol]
                    //SkinChildControls = true;
                    // fade graphic
                    TransitionGraphic = ProGammaX.MainForm.mainFormRef.fader;
                    // use custom tooltips
                    //UseCustomTips = true;

                    //Add("ListView", ProGammaX.MainForm.mainFormRef.vienna_header, ProGammaX.MainForm.mainFormRef.vienna_ScrollHorzShaft, ProGammaX.MainForm.mainFormRef.vienna_ScrollHorzArrow, ProGammaX.MainForm.mainFormRef.vienna_ScrollHorzThumb, ProGammaX.MainForm.mainFormRef.vienna_ScrollVertShaft, ProGammaX.MainForm.mainFormRef.vienna_ScrollVertArrow, ProGammaX.MainForm.mainFormRef.vienna_ScrollVertThumb);
                    new cListView(this.Handle, ProGammaX.MainForm.mainFormRef.vienna_header, ProGammaX.MainForm.mainFormRef.vienna_ScrollHorzShaft, ProGammaX.MainForm.mainFormRef.vienna_ScrollHorzArrow, ProGammaX.MainForm.mainFormRef.vienna_ScrollHorzThumb, ProGammaX.MainForm.mainFormRef.vienna_ScrollVertShaft, ProGammaX.MainForm.mainFormRef.vienna_ScrollVertArrow, ProGammaX.MainForm.mainFormRef.vienna_ScrollVertThumb, TransitionGraphic);
                    //ProGammaX.MainForm.mainFormRef._cRcm.Add(ProGammaX.ControlType.ListView, ProGammaX.MainForm.mainFormRef.vienna_header);

                }
            }
            catch
            {

            }

        }

    
    protected override void WndProc(ref Message m)
    {

        

        //if (scrollBar != null && this.Visible)
        //{




        //    switch (m.Msg)
        //    {




        //        //case 0x0083:
        //        //    //case 0x000F: //WM_PAINT
        //        //    scrollBar.checkBarState();

        //        //    base.WndProc(ref m);
        //        //    break;

        //        case WM_NCPAINT:

        //            //dispose();
        //            scrollBar.drawScrollBar();
        //            //base.WndProc(ref m);

        //            break;

        //        case WM_MOUSEHWHEEL:  // 0x020E
        //        case WM_HSCROLL:
        //            //scrollControl(m.WParam, eScrollDirection.Vertical, eScrollAction.Relative, 5);
        //            scrollBar._bTrackingMouse = true;
        //            scrollBar.drawScrollBar();
        //            scrollBar.scrollFader();

        //            //base.WndProc(ref m);
        //            break;
        //        case WM_MOUSEWHEEL:   // 0x020A
        //        case WM_VSCROLL:
        //            //scrollControl(m.WParam, eScrollDirection.Vertical, eScrollAction.Relative, -5);

        //            scrollBar._bTrackingMouse = true;
        //            scrollBar.drawScrollBar();
        //            scrollBar.scrollFader();
        //            //base.WndProc(ref m);
        //            break;
        //        case WM_NCMOUSEMOVE:
        //            scrollBar._bTrackingMouse = true;
        //            scrollBar.drawScrollBar();
        //            scrollBar.scrollFader();
        //            base.WndProc(ref m);
        //            break;

        //        case WM_MOUSEMOVE:
        //            if (scrollBar._bTrackingMouse)
        //                scrollBar._bTrackingMouse = false;
        //            //base.WndProc(ref m);
        //            break;


        //        case WM_SIZE:
        //        case WM_MOVE:
        //            scrollBar.reSizeMask();
        //            //base.WndProc(ref m);
        //            break;

        //        default:
        //            scrollBar.checkBarState();
        //            //base.WndProc(ref m);
        //            break;
        //    }

        //}




       
        




        //try
        //{
        //    if (m.Msg == 0xF && mouseIsMoving)
        //    {
        //        mouseIsMoving = false;
        //        this.Parent.Refresh();
        //        needUpdate = true;
        //    }
        //    if ((m.Msg == 0x83 || m.Msg == 0x5) && (this.Visible || this.Focused))
        //    {

        //        needUpdate = true;
        //        //this.Visible = false;
        //        //this.Visible = true;

        //    }
        //    if (m.Msg == 0x0133 || needUpdate) //NCCALCSIZE = 0x0083, WM_CREATE = 0x0001, DRAWITEM = 0x002B, MEASUREITEM = 0x002C, CTLCOLOREDIT  = 0x0133, WM_PAINT = 0x000F
        //    {


        //        needUpdate = false;

        //        //_cRcm = new ProGammaX.cRCM(this.Handle);
        //        //// skin controls in containers [tabcontrol]
        //        //_cRcm.SkinChildControls = true;
        //        //// fade graphic
        //        //_cRcm.TransitionGraphic = ProGammaX.Properties.Resources.fader;
        //        //// use custom tooltips
        //        //_cRcm.UseCustomTips = true;

        //        //MainForm.mainFormRef._cRcm = new ProGammaX.cRCM(this.Handle);
        //        //MainForm.mainFormRef._cRcm.Add(ProGammaX.ControlType.ListView, MainForm.mainFormRef.vienna_header, MainForm.mainFormRef.vienna_ScrollHorzShaft, MainForm.mainFormRef.vienna_ScrollHorzArrow, MainForm.mainFormRef.vienna_ScrollHorzThumb, MainForm.mainFormRef.vienna_ScrollVertShaft, MainForm.mainFormRef.vienna_ScrollVertArrow, MainForm.mainFormRef.vienna_ScrollVertThumb);



        //        //visible = false;
        //        //if (scrollBar == null)
        //        //{
        //        //scrollBar.Dispose();
        //        if (this.Visible)
        //        {
        //            scrollBar = new cInternalScrollBar(this.Handle, MainForm.mainFormRef.vienna_ScrollHorzShaft, MainForm.mainFormRef.vienna_ScrollHorzArrow, MainForm.mainFormRef.vienna_ScrollHorzThumb, MainForm.mainFormRef.vienna_ScrollVertShaft, MainForm.mainFormRef.vienna_ScrollVertArrow, MainForm.mainFormRef.vienna_ScrollVertThumb, MainForm.mainFormRef.vienna_ScrollHorzShaft);
        //        }
        //        //if (this.Visible)
        //        //{
        //        //    scrollBar = new cInternalScrollBar(this.Handle); //, MainForm.mainFormRef.vienna_combo, MainForm.mainFormRef.vienna_ScrollHorzArrow, MainForm.mainFormRef.vienna_ScrollHorzThumb, MainForm.mainFormRef.vienna_ScrollVertShaft, MainForm.mainFormRef.vienna_ScrollVertArrow, MainForm.mainFormRef.vienna_ScrollVertThumb, MainForm.mainFormRef.vienna_ScrollHorzShaft, true);
        //        //    scrollBar.drawScrollBar();

        //        //} 

        //        //}   



        //    }
        //    //else
        //    //{
        //    //    if (this.Visible == false)
        //    //    {


        //    //        scrollBar = null;
        //    //    }


        //    //}

        

        //}
        //catch
        //{

        //}




        // Suppress mouse messages that are OUTSIDE of the items area
        if (m.Msg >= 0x201 && m.Msg <= 0x209)
        {
            Point pos = new Point(m.LParam.ToInt32() & 0xffff, m.LParam.ToInt32() >> 16);
            var hit = this.HitTest(pos);
            switch (hit.Location)
            {
                case ListViewHitTestLocations.AboveClientArea:
                case ListViewHitTestLocations.BelowClientArea:
                case ListViewHitTestLocations.LeftOfClientArea:
                case ListViewHitTestLocations.RightOfClientArea:
                case ListViewHitTestLocations.None:
                    return;
            }
        }
        //if (m.Msg == WM_VSCROLL)
        //{
        //    try
        //    {
        //        //int type = m.WParam.ToInt32() & 0xffff;
        //        //int position = m.WParam.ToInt32() >> 16;
        //        //this.BeginInvoke((MethodInvoker)delegate { this.FocusedItem.EnsureVisible(); });


        //    }
        //    catch
        //    {

        //    }


        //}


            //base.WndProc(ref m);

        base.WndProc(ref m);
        //System.Threading.Thread.Sleep(1);


    }
  




            private void myListView_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
            {
                e.Graphics.FillRectangle(new SolidBrush(ThemedColors.MenuColor), e.Bounds);
                e.Graphics.DrawString(e.Header.Text, e.Font, new SolidBrush(ThemedColors.MenuTextColor), e.Bounds);
                //this.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
                //this.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            }
            private void myListView_DrawItem(object sender, DrawListViewItemEventArgs e)
           {
               e.DrawDefault = true;
           }




        










    //    protected override void OnDrawItem(DrawListViewItemEventArgs e)
    //    {
    //        if (this.SelectedItems.Contains(e.Item))
    //        {
               
    //            e.Graphics.FillRectangle(new SolidBrush(Color.Blue), e.Bounds);
    //            e.DrawFocusRectangle();
    //            this.BackColor = Color.Yellow;
    //            //e.DrawDefault = true;
              
    //        }
    //        else
    //        {
                

    //        }
    //        e.DrawDefault = true;
    //        base.OnDrawItem(e);
    //    }

    //    protected override void OnDrawSubItem(DrawListViewSubItemEventArgs e)
    //    {

    //        if (e.ColumnIndex == 1)

    //            e.Graphics.FillRectangle(new SolidBrush(Color.Blue), e.Bounds);

    //        else

    //            e.DrawDefault = true;

    //    }

    //    protected override void OnDrawColumnHeader(DrawListViewColumnHeaderEventArgs e)
    //    {

    //        e.DrawDefault = true;

    //    }

    







    /// <summary>
    /// This class is an implementation of the 'IComparer' interface.
    /// </summary>









    [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust")]
    class cListView : IDisposable
    {
        #region Constants
        private const int LVM_FIRST = 0x1000;
        private const int LVM_GETHEADER = (LVM_FIRST + 31);
        #endregion

        #region API
        [DllImport("user32.dll")]
        private static extern IntPtr SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        #endregion

        #region Fields
        private IntPtr _hListviewWnd = IntPtr.Zero;
        private cHeader _cHeaderSkin;
        private cInternalScrollBar _cInternalScroll;
        #endregion

        #region Constructor
        public cListView(IntPtr handle, Bitmap header, Bitmap hztrack, Bitmap hzarrow, Bitmap hzthumb, Bitmap vttrack, Bitmap vtarrow, Bitmap vtthumb, Bitmap fader)
        {
            if (handle == IntPtr.Zero)
                throw new Exception("The listview handle is invalid.");
            _hListviewWnd = handle;

            if (headerWnd != IntPtr.Zero)
                _cHeaderSkin = new cHeader(headerWnd, header);

            if (hztrack != null && hzarrow != null && hzthumb != null && vttrack != null && vtarrow != null && vtthumb != null)
                _cInternalScroll = new cInternalScrollBar(_hListviewWnd, hztrack, hzarrow, hzthumb, vttrack, vtarrow, vtthumb, fader);
            else
                throw new Exception("The listview image(s) are invalid");
        }

        private IntPtr headerWnd
        {
            get { return (SendMessage(_hListviewWnd, LVM_GETHEADER, 0, 0)); }
        }
        #endregion

        #region Methods
        public void Dispose()
        {
            try
            {
                if (_cHeaderSkin != null) _cHeaderSkin.Dispose();
                if (_cInternalScroll != null) _cInternalScroll.Dispose();
            }
            catch { }
        }
        #endregion
    }












    [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust")]
    class cHeader : NativeWindow, IDisposable
    {
        #region Constants
        // misc
        private const int GWL_STYLE = (-16);
        private const int GWL_EXSTYLE = (-20);
        private const int WS_ENABLED = 0x02;
        private const int CLR_NONE = -1;
        private const int ILD_TRANSPARENT = 0x1;
        // mouse buttons
        private const int VK_LBUTTON = 0x1;
        private const int VK_RBUTTON = 0x2;
        private const int SM_SWAPBUTTON = 23;
        // theming
        private const int HP_HEADERSORTARROW = 4;
        private const int HSAS_SORTEDUP = 1;
        private const int HSAS_SORTEDDOWN = 2;

        // header flags
        private const int HDF_LEFT = 0x0000;
        private const int HDF_RIGHT = 0x0001;
        private const int HDF_CENTER = 0x0002;
        private const int HDF_JUSTIFYMASK = 0x0003;
        private const int HDF_RTLREADING = 4;
        private const int HDF_OWNERDRAW = 0x8000;
        private const int HDF_STRING = 0x4000;
        private const int HDF_BITMAP = 0x2000;
        private const int HDF_BITMAP_ON_RIGHT = 0x1000;
        private const int HDF_IMAGE = 0x0800;
        private const int HDF_SORTUP = 0x0400;
        private const int HDF_SORTDOWN = 0x0200;
        private const int HDF_CHECKBOX = 0x0040;
        private const int HDF_CHECKED = 0x0080;
        private const int HDF_FIXEDWIDTH = 0x0100;
        private const int HDF_SPLITBUTTON = 0x1000000;
        private const int HDFT_ISSTRING = 0x0000;
        private const int HDFT_ISNUMBER = 0x0001;
        private const int HDFT_ISDATE = 0x0002;
        private const int HDFT_HASNOVALUE = 0x8000;
        // header item
        private const int HDI_WIDTH = 0x0001;
        private const int HDI_HEIGHT = HDI_WIDTH;
        private const int HDI_TEXT = 0x0002;
        private const int HDI_FORMAT = 0x0004;
        private const int HDI_LPARAM = 0x0008;
        private const int HDI_BITMAP = 0x0010;
        private const int HDI_IMAGE = 0x0020;
        private const int HDI_DI_SETITEM = 0x0040;
        private const int HDI_ORDER = 0x0080;
        private const int HDI_FILTER = 0x0100;
        private const int HDI_STATE = 0x0200;
        private const int HDIS_FOCUSED = 0x00000001;
        // header messages  
        private const int HDIS_STATE = 0x8;
        private const int HDM_FIRST = 0x1200;
        private const int HDM_GETITEMCOUNT = (HDM_FIRST + 0);
        private const int HDM_INSERTITEMA = (HDM_FIRST + 1);
        private const int HDM_INSERTITEMW = (HDM_FIRST + 10);
        private const int HDM_DELETEITEM = (HDM_FIRST + 2);
        private const int HDM_GETITEMA = (HDM_FIRST + 3);
        private const int HDM_GETITEMW = (HDM_FIRST + 11);
        private const int HDM_SETITEMA = (HDM_FIRST + 4);
        private const int HDM_SETITEMW = (HDM_FIRST + 12);
        private const int HDM_LAYOUT = (HDM_FIRST + 5);
        private const int HDM_HITTEST = (HDM_FIRST + 6);
        private const int HDM_GETITEMRECT = (HDM_FIRST + 7);
        private const int HDM_SETIMAGELIST = (HDM_FIRST + 8);
        private const int HDM_GETIMAGELIST = (HDM_FIRST + 9);
        private const int HDM_ORDERTOINDEX = (HDM_FIRST + 15);
        private const int HDM_CREATEDRAGIMAGE = (HDM_FIRST + 16);
        private const int HDM_GETORDERARRAY = (HDM_FIRST + 17);
        private const int HDM_SETORDERARRAY = (HDM_FIRST + 18);
        private const int HDM_SETHOTDIVIDER = (HDM_FIRST + 19);
        private const int HDM_SETBITMAPMARGIN = (HDM_FIRST + 20);
        private const int HDM_GETBITMAPMARGIN = (HDM_FIRST + 21);
        private const int HDM_SETFILTERCHANGETIMEOUT = (HDM_FIRST + 22);
        private const int HDM_EDITFILTER = (HDM_FIRST + 23);
        private const int HDM_CLEARFILTER = (HDM_FIRST + 24);
        private const int HDM_GETITEMDROPDOWNRECT = (HDM_FIRST + 25);
        private const int HDM_GETOVERFLOWRECT = (HDM_FIRST + 26);
        private const int HDM_GETFOCUSEDITEM = (HDM_FIRST + 27);
        private const int HDM_SETFOCUSEDITEM = (HDM_FIRST + 28);
        // header notify
        private const int HDN_FIRST = -300;
        private const int HDN_LAST = -399;
        private const int HDN_ITEMCHANGINGA = (HDN_FIRST - 0);
        private const int HDN_ITEMCHANGINGW = (HDN_FIRST - 20);
        private const int HDN_ITEMCHANGEDA = (HDN_FIRST - 1);
        private const int HDN_ITEMCHANGEDW = (HDN_FIRST - 21);
        private const int HDN_ITEMCLICKA = (HDN_FIRST - 2);
        private const int HDN_ITEMCLICKW = (HDN_FIRST - 22);
        private const int HDN_ITEMDBLCLICKA = (HDN_FIRST - 3);
        private const int HDN_ITEMDBLCLICKW = (HDN_FIRST - 23);
        private const int HDN_DIVIDERDBLCLICKA = (HDN_FIRST - 5);
        private const int HDN_DIVIDERDBLCLICKW = (HDN_FIRST - 25);
        private const int HDN_BEGINTRACKA = (HDN_FIRST - 6);
        private const int HDN_BEGINTRACKW = (HDN_FIRST - 26);
        private const int HDN_ENDTRACKA = (HDN_FIRST - 7);
        private const int HDN_ENDTRACKW = (HDN_FIRST - 27);
        private const int HDN_TRACKA = (HDN_FIRST - 8);
        private const int HDN_TRACKW = (HDN_FIRST - 28);
        private const int HDN_GETDISPINFOA = (HDN_FIRST - 9);
        private const int HDN_GETDISPINFOW = (HDN_FIRST - 29);
        private const int HDN_BEGINDRAG = (HDN_FIRST - 10);
        private const int HDN_ENDDRAG = (HDN_FIRST - 11);
        private const int HDN_FILTERCHANGE = (HDN_FIRST - 12);
        private const int HDN_FILTERBTNCLICK = (HDN_FIRST - 13);
        private const int HDN_BEGINFILTEREDIT = (HDN_FIRST - 14);
        private const int HDN_ENDFILTEREDIT = (HDN_FIRST - 15);
        private const int HDN_ITEMSTATEICONCLICK = (HDN_FIRST - 16);
        private const int HDN_ITEMKEYDOWN = (HDN_FIRST - 17);
        private const int HDN_DROPDOWN = (HDN_FIRST - 18);
        private const int HDN_OVERFLOWCLICK = (HDN_FIRST - 19);
        // header styles
        private const int HDS_HORZ = 0x0000;
        private const int HDS_BUTTONS = 0x0002;
        private const int HDS_HOTTRACK = 0x0004;
        private const int HDS_HIDDEN = 0x0008;
        private const int HDS_DRAGDROP = 0x0040;
        private const int HDS_FULLDRAG = 0x0080;
        private const int HDS_FILTERBAR = 0x0100;
        private const int HDS_FLAT = 0x0200;
        private const int HDS_CHECKBOXES = 0x0400;
        private const int HDS_NOSIZING = 0x0800;
        private const int HDS_OVERFLOW = 0x1000;
        // window messages
        private const int WM_MOUSEMOVE = 0x200;
        private const int WM_MOUSELEAVE = 0x2A3;
        private const int WM_LBUTTONDOWN = 0x201;
        private const int WM_LBUTTONUP = 0x202;
        private const int WM_MOUSEHOVER = 0x2A1;
        private const int WM_PAINT = 0xF;
        // message handler
        private static IntPtr MSG_HANDLED = new IntPtr(1);
        #endregion

        #region Enums
        private enum HITEST : int
        {
            HHT_NOWHERE = 0x0001,
            HHT_ONHEADER = 0x0002,
            HHT_ONDIVIDER = 0x0004,
            HHT_ONDIVOPEN = 0x0008,
            HHT_ONFILTER = 0x0010,
            HHT_ONFILTERBUTTON = 0x0020,
            HHT_ABOVE = 0x0100,
            HHT_BELOW = 0x0200,
            HHT_TORIGHT = 0x0400,
            HHT_TOLEFT = 0x0800,
            HHT_ONITEMSTATEICON = 0x1000,
            HHT_ONDROPDOWN = 0x2000,
            HHT_ONOVERFLOW = 0x4000
        }
        #endregion

        #region Structs
        [StructLayout(LayoutKind.Sequential)]
        private struct PAINTSTRUCT
        {
            internal IntPtr hdc;
            internal int fErase;
            internal RECT rcPaint;
            internal int fRestore;
            internal int fIncUpdate;
            internal int Reserved1;
            internal int Reserved2;
            internal int Reserved3;
            internal int Reserved4;
            internal int Reserved5;
            internal int Reserved6;
            internal int Reserved7;
            internal int Reserved8;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct RECT
        {
            internal RECT(int X, int Y, int Width, int Height)
            {
                this.Left = X;
                this.Top = Y;
                this.Right = Width;
                this.Bottom = Height;
            }
            internal int Left;
            internal int Top;
            internal int Right;
            internal int Bottom;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct HDITEM
        {
            internal int mask;
            internal int cxy;
            internal string pszText;
            internal IntPtr hbm;
            internal int cchTextMax;
            internal int fmt;
            internal IntPtr lParam;
            internal int iImage;
            internal int iOrder;
            internal uint type;
            internal IntPtr pvFilter;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct HDHITTESTINFO
        {
            internal Point pt;
            internal HITEST flags;
            internal int iItem;
        }
        #endregion

        #region API
        [DllImport("user32.dll")]
        private static extern IntPtr BeginPaint(IntPtr hWnd, ref PAINTSTRUCT ps);

        [DllImport("user32.dll")]
        private static extern bool EndPaint(IntPtr hWnd, ref PAINTSTRUCT ps);

        [DllImport("gdi32.dll")]
        private static extern IntPtr SelectObject(IntPtr hdc, IntPtr hObject);

        [DllImport("gdi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool BitBlt(IntPtr hdc, int nXDest, int nYDest, int nWidth, int nHeight, IntPtr hdcSrc, int nXSrc, int nYSrc, int dwRop);

        [DllImport("user32.dll")]
        private static extern IntPtr GetDC(IntPtr handle);

        [DllImport("user32.dll")]
        private static extern int ReleaseDC(IntPtr handle, IntPtr hdc);

        [DllImport("gdi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool StretchBlt(IntPtr hDest, int X, int Y, int nWidth, int nHeight, IntPtr hdcSrc,
        int sX, int sY, int nWidthSrc, int nHeightSrc, int dwRop);

        [DllImport("user32.dll")]
        private static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImport("user32.dll")]
        private static extern IntPtr SendMessage(IntPtr hwnd, int msg, int wParam, ref HDITEM lParam);
        [DllImport("user32.dll")]
        private static extern IntPtr SendMessage(IntPtr hwnd, int msg, int wParam, ref RECT lParam);
        [DllImport("user32.dll")]
        private static extern IntPtr SendMessage(IntPtr hwnd, int msg, int wParam, ref HDHITTESTINFO lParam);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetClientRect(IntPtr hWnd, ref RECT r);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetCursorPos(ref Point lpPoint);

        [DllImport("user32.dll")]
        private static extern int ScreenToClient(IntPtr hwnd, ref Point lpPoint);

        [DllImport("user32.dll")]
        private static extern int GetWindowLong(IntPtr hwnd, int nIndex);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool PtInRect([In] ref RECT lprc, Point pt);

        [DllImport("user32.dll")]
        private static extern bool ValidateRect(IntPtr hWnd, ref RECT lpRect);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetWindowRect(IntPtr hWnd, ref RECT lpRect);

        [DllImport("user32.dll")]
        private static extern int OffsetRect(ref RECT lpRect, int x, int y);

        [DllImport("uxtheme.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool IsAppThemed();

        [DllImport("uxtheme.dll", ExactSpelling = true, CharSet = CharSet.Unicode)]
        private static extern IntPtr OpenThemeData(IntPtr hWnd, String classList);

        [DllImport("uxtheme.dll", ExactSpelling = true)]
        private static extern int DrawThemeBackground(IntPtr hTheme, IntPtr hdc, int iPartId,
           int iStateId, ref RECT pRect, ref RECT pClipRect);

        [DllImport("uxtheme.dll", ExactSpelling = true)]
        private static extern int CloseThemeData(IntPtr hTheme);

        [DllImport("user32.dll", ExactSpelling = true, CharSet = CharSet.Auto)]
        private static extern IntPtr GetParent(IntPtr hWnd);

        [DllImport("comctl32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool ImageList_DrawEx(IntPtr himl, int i, IntPtr hdcDst, int x, int y, int dx, int dy, int rgbBk, int rgbFg, int fStyle);

        [DllImport("comctl32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool ImageList_GetIconSize(IntPtr himl, out int cx, out int cy);

        [DllImport("user32.dll")]
        private static extern int GetSystemMetrics(int smIndex);

        [DllImport("user32.dll")]
        private static extern short GetKeyState(int nVirtKey);
        #endregion

        #region Fields
        private bool _bPainting = false;
        private IntPtr _hHeaderWnd = IntPtr.Zero;
        private cStoreDc _cHeaderDc = new cStoreDc();
        private Bitmap _oHeaderBitmap;
        #endregion

        #region Constructor
        public cHeader(IntPtr hWnd, Bitmap skin)
        {
            if (hWnd == IntPtr.Zero)
                throw new Exception("The header handle is invalid.");
            if (skin == null)
                throw new Exception("The header image is invalid.");
            HeaderGraphic = skin;
            _hHeaderWnd = hWnd;
            this.AssignHandle(_hHeaderWnd);
        }
        #endregion

        #region Properties
        private int ColumnCount
        {
            get { return (SendMessage(_hHeaderWnd, HDM_GETITEMCOUNT, 0, 0)); }
        }

        /// <summary>
        /// Get/Set Header bitmap.
        /// </summary>
        public Bitmap HeaderGraphic
        {
            get { return _oHeaderBitmap; }
            set
            {
                _oHeaderBitmap = value;
                if (_cHeaderDc.Hdc != IntPtr.Zero)
                {
                    _cHeaderDc.Dispose();
                    _cHeaderDc = new cStoreDc();
                }
                _cHeaderDc.Width = _oHeaderBitmap.Width;
                _cHeaderDc.Height = _oHeaderBitmap.Height;
                SelectObject(_cHeaderDc.Hdc, _oHeaderBitmap.GetHbitmap());
            }
        }
        #endregion

        #region Methods
        private int columnAtIndex(int column)
        {
            HDITEM hd = new HDITEM();
            hd.mask = HDI_ORDER;
            for (int i = 0; i < ColumnCount; i++)
            {
                if (SendMessage(_hHeaderWnd, HDM_GETITEMA, column, ref hd) != IntPtr.Zero)
                    return hd.iOrder;
            }
            return 0;
        }

        private void drawHeader()
        {
            RECT tr = new RECT();
            RECT wr = new RECT();
            cStoreDc tempDc = new cStoreDc();
            int offset = 0;

            // set up the temp dc
            GetWindowRect(_hHeaderWnd, ref wr);
            OffsetRect(ref wr, -wr.Left, -wr.Top);
            tempDc.Height = wr.Bottom;
            tempDc.Width = wr.Right;

            int width = _cHeaderDc.Width / 4;

            for (int i = 0; i < ColumnCount; i++)
            {
                if (!Enabled())
                {
                    offset = width * 3;
                }
                else if (i == focusedColumn())
                {
                    if (leftKeyPressed())
                        offset = width * 2;
                    else
                        offset = width;
                }
                else
                {
                    offset = 0;
                }
                SendMessage(_hHeaderWnd, HDM_GETITEMRECT, i, ref tr);
                using (StretchImage st = new StretchImage(_cHeaderDc.Hdc, tempDc.Hdc, new Rectangle(offset, 0, width, _cHeaderDc.Height), new Rectangle(tr.Left, tr.Top, tr.Right - tr.Left, tr.Bottom - tr.Top), 3, StretchModeEnum.STRETCH_HALFTONE)) { }
                // button, icon, sort arrows and text
                if (hasButton(i))
                    drawButton(tempDc.Hdc, i, tr);
                if (hasSort(i))
                    drawSortArrow(tempDc.Hdc, i, tr);
                if (hasIcon(i))
                    drawIcon(tempDc.Hdc, i, tr);
                drawText(tempDc.Hdc, i, tr);
            }
            // draw the end piece
            SendMessage(_hHeaderWnd, HDM_GETITEMRECT, columnAtIndex(ColumnCount - 1), ref tr);
            int left = tr.Right;
            GetWindowRect(_hHeaderWnd, ref tr);
            OffsetRect(ref tr, -tr.Left, -tr.Top);
            tr.Left = left;
            tr.Right += 10;
            using (StretchImage st = new StretchImage(_cHeaderDc.Hdc, tempDc.Hdc, new Rectangle(0, 0, (_cHeaderDc.Width / 3) - 2, _cHeaderDc.Height), new Rectangle(tr.Left, tr.Top, tr.Right - tr.Left + 2, tr.Bottom - tr.Top), 3, StretchModeEnum.STRETCH_HALFTONE)) { }
            // blit the temp dc
            IntPtr hdc = GetDC(_hHeaderWnd);
            BitBlt(hdc, 0, 0, wr.Right, wr.Bottom, tempDc.Hdc, 0, 0, 0xCC0020);
            ReleaseDC(_hHeaderWnd, hdc);
            tempDc.Dispose();
        }

        private bool leftKeyPressed()
        {
            if (mouseButtonsSwitched())
                return (GetKeyState(VK_RBUTTON) < 0);
            else
                return (GetKeyState(VK_LBUTTON) < 0);
        }

        private bool mouseButtonsSwitched()
        {
            return (GetSystemMetrics(SM_SWAPBUTTON) != 0);
        }

        private void drawButton(IntPtr hdc, int column, RECT tr)
        {
            HITEST state = columnState();
            if (state == HITEST.HHT_ONHEADER || state == HITEST.HHT_ONDROPDOWN)
            {
                // as with windows implementation, only draw if column is in focus
                if (column == focusedColumn())
                {
                    RECT br = new RECT();
                    int offset = _cHeaderDc.Width / 3;
                    Point pt = new Point();

                    // get the button size
                    SendMessage(_hHeaderWnd, HDM_GETITEMDROPDOWNRECT, column, ref br);
                    int height = br.Bottom - br.Top;
                    int width = br.Right - br.Left;
                    // fix for position bug?
                    br.Left = tr.Right - (width + 2);
                    br.Right = br.Left + width;
                    using (Graphics g = Graphics.FromHdc(hdc))
                    {
                        using (Pen borderPen = new Pen(Color.FromKnownColor(KnownColor.ControlDark), 0.5f))
                            g.DrawRectangle(borderPen, new Rectangle(br.Left, br.Top, width, height - 3));
                    }
                    GetCursorPos(ref pt);
                    ScreenToClient(_hHeaderWnd, ref pt);
                    // if selected shade the button
                    if (PtInRect(ref br, pt))
                        drawFocusedButton(hdc, new Rectangle(br.Left, br.Top, width, height - 3), LinearGradientMode.Vertical);
                    br.Left += (width - 5) / 2;
                    br.Bottom = br.Top + 6;
                    br.Top = (height / 2) - 3;
                    // draw the arrow
                    drawArrow(hdc, new Rectangle(br.Left, br.Top, 12, 6), true);
                }
            }
        }

        private void drawFocusedButton(IntPtr hdc, Rectangle bounds, LinearGradientMode gradient)
        {
            using (Graphics g = Graphics.FromHdc(hdc))
            {
                // draw using anti alias
                using (GraphicsMode mode = new GraphicsMode(g, SmoothingMode.AntiAlias))
                {
                    // create the path
                    using (GraphicsPath buttonPath = createRoundRectanglePath(
                        g,
                        bounds.X, bounds.Y,
                        bounds.Width, bounds.Height,
                        1.0f))
                    {
                        // draw the outer edge
                        using (Pen borderPen = new Pen(Color.FromArgb(150, Color.SlateGray), 1f))
                            g.DrawPath(borderPen, buttonPath);
                    }
                    bounds.Inflate(-1, -1);

                    using (GraphicsPath buttonPath = createRoundRectanglePath(
                        g,
                        bounds.X, bounds.Y,
                        bounds.Width, bounds.Height,
                        1.0f))
                    {
                        // draw the inner edge
                        using (Pen borderPen = new Pen(Color.FromArgb(150, Color.DarkGray), 1.5f))
                            g.DrawPath(borderPen, buttonPath);

                        // create a thin gradient cover
                        using (LinearGradientBrush fillBrush = new LinearGradientBrush(
                            bounds,
                            Color.FromArgb(50, Color.White),
                            Color.FromArgb(50, Color.SlateGray),
                            gradient))
                        {
                            // shift the blend factors
                            Blend blend = new Blend();
                            blend.Positions = new float[] { 0f, .3f, .6f, 1f };
                            blend.Factors = new float[] { 0f, .5f, .8f, .2f };
                            fillBrush.Blend = blend;
                            // fill the path
                            g.FillPath(fillBrush, buttonPath);
                        }
                    }
                }
            }
        }

        private void drawSortArrow(IntPtr hdc, int column, RECT tr)
        {
            if (Environment.OSVersion.Version.Major > 5)
            {
                HDITEM hi = new HDITEM();
                hi.mask = HDI_FORMAT;
                SendMessage(_hHeaderWnd, HDM_GETITEMA, column, ref hi);
                int left = (tr.Right - tr.Left) / 2 - 4;
                Rectangle ar = new Rectangle(tr.Left + left, 0, 12, 6);
                if ((hi.fmt & (HDF_SORTDOWN)) == HDF_SORTDOWN)
                {
                    if (!drawThemeArrow(hdc, ar, true))
                        drawArrow(hdc, ar, true);
                }
                else
                {
                    if (!drawThemeArrow(hdc, ar, false))
                        drawArrow(hdc, ar, false);
                }
            }
        }

        private GraphicsPath createRoundRectanglePath(Graphics g, float X, float Y, float width, float height, float radius)
        {
            // create a path
            GraphicsPath pathBounds = new GraphicsPath();
            pathBounds.AddLine(X + radius, Y, X + width - (radius * 2), Y);
            pathBounds.AddArc(X + width - (radius * 2), Y, radius * 2, radius * 2, 270, 90);
            pathBounds.AddLine(X + width, Y + radius, X + width, Y + height - (radius * 2));
            pathBounds.AddArc(X + width - (radius * 2), Y + height - (radius * 2), radius * 2, radius * 2, 0, 90);
            pathBounds.AddLine(X + width - (radius * 2), Y + height, X + radius, Y + height);
            pathBounds.AddArc(X, Y + height - (radius * 2), radius * 2, radius * 2, 90, 90);
            pathBounds.AddLine(X, Y + height - (radius * 2), X, Y + radius);
            pathBounds.AddArc(X, Y, radius * 2, radius * 2, 180, 90);
            pathBounds.CloseFigure();
            return pathBounds;
        }

        private bool drawThemeArrow(IntPtr hdc, Rectangle bounds, bool down)
        {
            if (IsAppThemed())
            {
                IntPtr hTheme = OpenThemeData(GetParent(_hHeaderWnd), "Header");
                if (hTheme != IntPtr.Zero)
                {
                    RECT tr = new RECT(bounds.Left, bounds.Top, bounds.Right, bounds.Bottom);
                    // draw part
                    DrawThemeBackground(hTheme, hdc, HP_HEADERSORTARROW, down ? HSAS_SORTEDDOWN : HSAS_SORTEDUP, ref tr, ref tr);
                    CloseThemeData(hTheme);
                    return true;
                }
            }
            return false;
        }

        private void drawArrow(IntPtr hdc, Rectangle bounds, bool down)
        {
            using (Graphics g = Graphics.FromHdc(hdc))
            {
                using (GraphicsMode mode = new GraphicsMode(g, SmoothingMode.AntiAlias))
                {
                    using (GraphicsPath gp = new GraphicsPath())
                    {
                        if (down)
                        {
                            // draw the frame
                            gp.AddLine(new Point(bounds.X, bounds.Top), new Point(bounds.X + 4, bounds.Top));
                            gp.AddLine(new Point(bounds.X, bounds.Top), new Point(bounds.X + 2, bounds.Top + 2));
                            gp.AddLine(new Point(bounds.X + 2, bounds.Top + 2), new Point(bounds.X + 4, bounds.Top));
                            gp.CloseFigure();
                        }
                        else
                        {
                            gp.AddLine(new Point(bounds.X, bounds.Top + 4), new Point(bounds.X + 4, bounds.Top + 4));
                            gp.AddLine(new Point(bounds.X, bounds.Top + 4), new Point(bounds.X + 2, bounds.Top + 2));
                            gp.AddLine(new Point(bounds.X + 4, bounds.Top + 4), new Point(bounds.X + 2, bounds.Top + 2));
                            gp.CloseFigure();
                        }
                        // draw border
                        using (Pen borderPen = new Pen(Color.FromArgb(240, Color.Black), 0.5f))
                            g.DrawPath(borderPen, gp);
                        // fill path
                        using (Brush backBrush = new SolidBrush(Color.SlateGray))
                            g.FillPath(backBrush, gp);
                    }
                }
            }
        }

        private void drawIcon(IntPtr hdc, int column, RECT tr)
        {
            IntPtr iml = (IntPtr)SendMessage(_hHeaderWnd, HDM_GETIMAGELIST, HDIS_STATE, 0);
            if (iml != IntPtr.Zero)
            {
                int cx = 0;
                int cy = 0;
                HDITEM hi = new HDITEM();
                ImageList_GetIconSize(iml, out cx, out cy);
                hi.mask = HDI_IMAGE;
                SendMessage(_hHeaderWnd, HDM_GETITEMA, column, ref hi);
                tr.Top = (tr.Bottom - cy) / 2;
                tr.Left += 4;
                ImageList_DrawEx(iml, hi.iImage, hdc, tr.Left, tr.Top, cx, cy, CLR_NONE, 0, ILD_TRANSPARENT);
            }
        }

        private void drawText(IntPtr hdc, int column, RECT tr)
        {
            HDITEM hi = new HDITEM();
            hi.mask = HDI_TEXT | HDI_FORMAT | HDI_IMAGE;
            hi.cchTextMax = 255;
            hi.pszText = new String('0', 255);
            SendMessage(_hHeaderWnd, HDM_GETITEMA, column, ref hi);

            if (String.IsNullOrEmpty(hi.pszText))
                return;

            string text = hi.pszText;

            using (Graphics g = Graphics.FromHdc(hdc))
            {
                using (StringFormat sf = new StringFormat())
                {
                    g.SmoothingMode = SmoothingMode.AntiAlias;
                    g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
                    sf.Trimming = StringTrimming.EllipsisCharacter;
                    if ((hi.fmt & HDF_RIGHT) == HDF_RIGHT)
                    {
                        sf.Alignment = StringAlignment.Far;
                        sf.LineAlignment = StringAlignment.Center;
                    }
                    else if ((hi.fmt & HDF_CENTER) == HDF_CENTER)
                    {
                        sf.Alignment = StringAlignment.Center;
                        sf.LineAlignment = StringAlignment.Center;
                    }
                    else
                    {
                        sf.Alignment = StringAlignment.Near;
                        sf.LineAlignment = StringAlignment.Center;
                    }
                    if ((hi.fmt & HDF_IMAGE) == HDF_IMAGE)
                        tr.Left += 18;

                    if ((hi.fmt & HDF_RTLREADING) == HDF_RTLREADING)
                        sf.FormatFlags = StringFormatFlags.DirectionRightToLeft;

                    if (hasButton(column))
                        tr.Right -= 12;

                    Control ct = (ListView)Control.FromHandle(GetParent(_hHeaderWnd));
                    SizeF sz = g.MeasureString(text, ct.Font);
                    tr.Top = (tr.Bottom / 2) - (int)sz.Height;
                    tr.Bottom = tr.Top + (int)sz.Height;
                    // draw the text
                    using (Brush captionBrush = new SolidBrush(ct.ForeColor))
                        g.DrawString(text, ct.Font, captionBrush, new RectangleF(tr.Left, tr.Top + 5, tr.Right - tr.Left, tr.Bottom - tr.Top), sf);
                }
            }
        }

        private bool Enabled()
        {
            if ((GetWindowLong(_hHeaderWnd, GWL_STYLE) & WS_ENABLED) == WS_ENABLED)
                return true;
            return false;
        }

        private bool hasButton(int column)
        {
            if (Environment.OSVersion.Version.Major > 5)
            {
                HDITEM hi = new HDITEM();
                hi.mask = HDI_FORMAT;
                SendMessage(_hHeaderWnd, HDM_GETITEMA, columnAtIndex(column), ref hi);
                if ((hi.fmt & HDF_SPLITBUTTON) == HDF_SPLITBUTTON)
                    return true;
            }
            return false;
        }

        private bool hasIcon(int column)
        {
            HDITEM hi = new HDITEM();
            hi.mask = HDI_FORMAT;
            SendMessage(_hHeaderWnd, HDM_GETITEMA, column, ref hi);
            if ((hi.fmt & HDF_IMAGE) == HDF_IMAGE)
                return true;
            return false;
        }

        private bool hasSort(int column)
        {
            if (Environment.OSVersion.Version.Major > 5)
            {
                HDITEM hi = new HDITEM();
                hi.mask = HDI_FORMAT;
                SendMessage(_hHeaderWnd, HDM_GETITEMA, column, ref hi);
                if ((hi.fmt & (HDF_SORTUP | HDF_SORTDOWN)) > 0)
                    return true;
            }
            return false;
        }

        private HITEST columnState()
        {
            HDHITTESTINFO ht = new HDHITTESTINFO();
            GetCursorPos(ref ht.pt);
            ScreenToClient(_hHeaderWnd, ref ht.pt);
            SendMessage(_hHeaderWnd, HDM_HITTEST, 0, ref ht);

            if (ht.iItem != -1)
                return ht.flags;
            return HITEST.HHT_NOWHERE;
        }

        private int focusedColumn()
        {
            HDHITTESTINFO ht = new HDHITTESTINFO();
            GetCursorPos(ref ht.pt);
            ScreenToClient(_hHeaderWnd, ref ht.pt);
            SendMessage(_hHeaderWnd, HDM_HITTEST, 0, ref ht);
            return ht.iItem;
        }

        public void Dispose()
        {
            try
            {
                this.ReleaseHandle();
                if (_oHeaderBitmap != null) _oHeaderBitmap.Dispose();
                if (_cHeaderDc != null) _cHeaderDc.Dispose();
            }
            catch { }
            GC.SuppressFinalize(this);
        }
        #endregion

        #region WndProc
        protected override void WndProc(ref Message m)
        {
            PAINTSTRUCT ps = new PAINTSTRUCT();
            switch (m.Msg)
            {
                case WM_PAINT:
                    if (!_bPainting)
                    {
                        _bPainting = true;
                        // start painting engine
                        BeginPaint(m.HWnd, ref ps);
                        drawHeader();
                        ValidateRect(m.HWnd, ref ps.rcPaint);
                        // done
                        EndPaint(m.HWnd, ref ps);
                        _bPainting = false;
                        m.Result = MSG_HANDLED;
                    }
                    else
                    {
                        base.WndProc(ref m);
                    }
                    break;

                default:
                    base.WndProc(ref m);
                    break;
            }
        }
        #endregion
    }














    internal class cInternalScrollBar : NativeWindow, IDisposable
    {
        #region Constants
        // style
        private const int GWL_STYLE = (-16);
        private const int GWL_EXSTYLE = (-20);
        private const int WS_EX_TOPMOST = 0x8;
        private const int WS_EX_TOOLWINDOW = 0x80;
        private const int WS_CHILD = 0x40000000;
        private const int WS_OVERLAPPED = 0x0;
        private const int WS_CLIPSIBLINGS = 0x4000000;
        private const int WS_VISIBLE = 0x10000000;
        private const int WS_HSCROLL = 0x100000;
        private const int WS_VSCROLL = 0x200000;
        private const int SS_OWNERDRAW = 0xD;
        // showwindow
        private const int SW_HIDE = 0x0;
        private const int SW_NORMAL = 0x1;
        // size/move
        private const uint SWP_NOSIZE = 0x0001;
        private const uint SWP_NOMOVE = 0x0002;
        private const uint SWP_NOZORDER = 0x0004;
        private const uint SWP_NOREDRAW = 0x0008;
        private const uint SWP_NOACTIVATE = 0x0010;
        private const uint SWP_FRAMECHANGED = 0x0020;
        private const uint SWP_SHOWWINDOW = 0x0040;
        private const uint SWP_HIDEWINDOW = 0x0080;
        private const uint SWP_NOCOPYBITS = 0x0100;
        private const uint SWP_NOOWNERZORDER = 0x0200;
        private const uint SWP_NOSENDCHANGING = 0x0400;
        // setwindowpos
        static readonly IntPtr HWND_NOTOPMOST = new IntPtr(-2);
        static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
        static readonly IntPtr HWND_TOP = new IntPtr(0);
        static readonly IntPtr HWND_BOTTOM = new IntPtr(1);
        // scroll messages
        private const int WM_HSCROLL = 0x114;
        private const int WM_VSCROLL = 0x115;
        private const int SB_LINEUP = 0;
        private const int SB_LINEDOWN = 1;
        private const int SB_LINELEFT = 0;
        private const int SB_LINERIGHT = 1;
        private const int SB_PAGEUP = 2;
        private const int SB_PAGEDOWN = 3;
        private const int SB_PAGELEFT = 2;
        private const int SB_PAGERIGHT = 3;
        // mouse buttons
        private const int VK_LBUTTON = 0x1;
        private const int VK_RBUTTON = 0x2;
        // redraw
        private const int RDW_INVALIDATE = 0x0001;
        private const int RDW_INTERNALPAINT = 0x0002;
        private const int RDW_ERASE = 0x0004;
        private const int RDW_VALIDATE = 0x0008;
        private const int RDW_NOINTERNALPAINT = 0x0010;
        private const int RDW_NOERASE = 0x0020;
        private const int RDW_NOCHILDREN = 0x0040;
        private const int RDW_ALLCHILDREN = 0x0080;
        private const int RDW_UPDATENOW = 0x0100;
        private const int RDW_ERASENOW = 0x0200;
        private const int RDW_FRAME = 0x0400;
        private const int RDW_NOFRAME = 0x0800;
        // scroll bar messages
        private const int SB_HORZ = 0x0;
        private const int SB_VERT = 0x1;
        private const int SBM_SETPOS = 0x00E0;
        private const int SBM_GETPOS = 0x00E1;
        private const int SBM_SETRANGE = 0x00E2;
        private const int SBM_SETRANGEREDRAW = 0x00E6;
        private const int SBM_GETRANGE = 0x00E3;
        private const int SBM_ENABLE_ARROWS = 0x00E4;
        private const int SBM_SETSCROLLINFO = 0x00E9;
        private const int SBM_GETSCROLLINFO = 0x00EA;
        private const int SBM_GETSCROLLBARINFO = 0x00EB;
        private const int SIF_RANGE = 0x0001;
        private const int SIF_PAGE = 0x0002;
        private const int SIF_POS = 0x0004;
        private const int SIF_DISABLENOSCROLL = 0x0008;
        private const int SIF_TRACKPOS = 0x0010;
        private const int SIF_ALL = (SIF_RANGE | SIF_PAGE | SIF_POS | SIF_TRACKPOS);
        // scrollbar states
        private const int STATE_SYSTEM_INVISIBLE = 0x00008000;
        private const int STATE_SYSTEM_OFFSCREEN = 0x00010000;
        private const int STATE_SYSTEM_PRESSED = 0x00000008;
        private const int STATE_SYSTEM_UNAVAILABLE = 0x00000001;
        private const uint OBJID_HSCROLL = 0xFFFFFFFA;
        private const uint OBJID_VSCROLL = 0xFFFFFFFB;
        private const uint OBJID_CLIENT = 0xFFFFFFFC;
        // window messages
        private const int WM_PAINT = 0xF;
        private const int WM_NCPAINT = 0x85;
        private const int WM_NCMOUSEMOVE = 0xA0;
        private const int WM_MOUSEMOVE = 0x200;
        private const int WM_MOUSELEAVE = 0x2A3;
        private const int WM_LBUTTONDOWN = 0x201;
        private const int WM_LBUTTONUP = 0x202;
        private const int WM_LBUTTONDBLCLK = 0x203;
        private const int WM_RBUTTONDOWN = 0x204;
        private const int WM_RBUTTONUP = 0x205;
        private const int WM_RBUTTONDBLCLK = 0x206;
        private const int WM_MBUTTONDOWN = 0x207;
        private const int WM_MBUTTONUP = 0x208;
        private const int WM_MBUTTONDBLCLK = 0x209;
        private const int WM_MOUSEWHEEL = 0x20A;
        private const int WM_MOUSEHWHEEL = 0x020E;
        private const int WM_STYLECHANGED = 0x7D;
        private const int WM_SIZE = 0x5;
        private const int WM_MOVE = 0x3;
        // message handler
        private static IntPtr MSG_HANDLED = new IntPtr(1);
        #endregion

        #region Enums
        private enum SB_HITEST : int
        {
            offControl = 0,
            topArrow,
            bottomArrow,
            leftArrow,
            rightArrow,
            button,
            track
        }

        private enum SYSTEM_METRICS : int
        {
            SM_CXSCREEN = 0,
            SM_CYSCREEN = 1,
            SM_CXVSCROLL = 2,
            SM_CYHSCROLL = 3,
            SM_CYCAPTION = 4,
            SM_CXBORDER = 5,
            SM_CYBORDER = 6,
            SM_CYVTHUMB = 9,
            SM_CXHTHUMB = 10,
            SM_CXICON = 11,
            SM_CYICON = 12,
            SM_CXCURSOR = 13,
            SM_CYCURSOR = 14,
            SM_CYMENU = 15,
            SM_CXFULLSCREEN = 16,
            SM_CYFULLSCREEN = 17,
            SM_CYKANJIWINDOW = 18,
            SM_MOUSEPRESENT = 19,
            SM_CYVSCROLL = 20,
            SM_CXHSCROLL = 21,
            SM_SWAPBUTTON = 23,
            SM_CXMIN = 28,
            SM_CYMIN = 29,
            SM_CXSIZE = 30,
            SM_CYSIZE = 31,
            SM_CXFRAME = 32,
            SM_CYFRAME = 33,
            SM_CXMINTRACK = 34,
            SM_CYMINTRACK = 35,
            SM_CYSMCAPTION = 51,
            SM_CXMINIMIZED = 57,
            SM_CYMINIMIZED = 58,
            SM_CXMAXTRACK = 59,
            SM_CYMAXTRACK = 60,
            SM_CXMAXIMIZED = 61,
            SM_CYMAXIMIZED = 62
        }
        #endregion

        #region Structs
        [StructLayout(LayoutKind.Sequential)]
        private struct PAINTSTRUCT
        {
            internal IntPtr hdc;
            internal int fErase;
            internal RECT rcPaint;
            internal int fRestore;
            internal int fIncUpdate;
            internal int Reserved1;
            internal int Reserved2;
            internal int Reserved3;
            internal int Reserved4;
            internal int Reserved5;
            internal int Reserved6;
            internal int Reserved7;
            internal int Reserved8;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct RECT
        {
            internal RECT(int X, int Y, int Width, int Height)
            {
                this.Left = X;
                this.Top = Y;
                this.Right = Width;
                this.Bottom = Height;
            }
            internal int Left;
            internal int Top;
            internal int Right;
            internal int Bottom;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct SCROLLINFO
        {
            internal uint cbSize;
            internal uint fMask;
            internal int nMin;
            internal int nMax;
            internal uint nPage;
            internal int nPos;
            internal int nTrackPos;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct SCROLLBARINFO
        {
            internal int cbSize;
            internal RECT rcScrollBar;
            internal int dxyLineButton;
            internal int xyThumbTop;
            internal int xyThumbBottom;
            internal int reserved;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            internal int[] rgstate;
        }
        #endregion

        #region API
        [DllImport("user32.dll")]
        private static extern IntPtr BeginPaint(IntPtr hWnd, ref PAINTSTRUCT ps);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool EndPaint(IntPtr hWnd, ref PAINTSTRUCT ps);

        [DllImport("gdi32.dll")]
        private static extern IntPtr SelectObject(IntPtr hdc, IntPtr hObject);

        [DllImport("gdi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool BitBlt(IntPtr hdc, int nXDest, int nYDest, int nWidth, int nHeight, IntPtr hdcSrc, int nXSrc, int nYSrc, int dwRop);

        [DllImport("gdi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool StretchBlt(IntPtr hDest, int X, int Y, int nWidth, int nHeight, IntPtr hdcSrc,
        int sX, int sY, int nWidthSrc, int nHeightSrc, int dwRop);

        [DllImport("user32.dll")]
        private static extern IntPtr GetDC(IntPtr handle);

        [DllImport("user32.dll")]
        private static extern int ReleaseDC(IntPtr handle, IntPtr hdc);

        [DllImport("user32.dll")]
        private static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImport("user32.dll")]
        private static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, ref SCROLLBARINFO lParam);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetWindowRect(IntPtr hWnd, ref RECT lpRect);

        [DllImport("user32.dll")]
        private extern static int OffsetRect(ref RECT lpRect, int x, int y);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool ValidateRect(IntPtr hWnd, ref RECT lpRect);

        [DllImport("user32.dll")]
        private static extern int GetSystemMetrics(SYSTEM_METRICS smIndex);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool RedrawWindow(IntPtr hWnd, IntPtr lprcUpdate, IntPtr hrgnUpdate, uint flags);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetCursorPos(ref Point lpPoint);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool PtInRect([In] ref RECT lprc, Point pt);

        [DllImport("user32.dll")]
        private static extern int ScreenToClient(IntPtr hwnd, ref Point lpPoint);

        [DllImport("user32.dll")]
        private static extern short GetKeyState(int nVirtKey);

        [DllImport("user32.dll")]
        private static extern int SetScrollPos(IntPtr hWnd, int nBar, int nPos, bool bRedraw);

        [DllImport("user32.dll")]
        private static extern int GetScrollPos(IntPtr hWnd, int nBar);

        [DllImport("user32.dll")]
        private static extern IntPtr GetParent(IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern IntPtr CreateWindowEx(int exstyle, string lpClassName, string lpWindowName, int dwStyle,
            int x, int y, int nWidth, int nHeight, IntPtr hwndParent, IntPtr Menu, IntPtr hInstance, IntPtr lpParam);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool DestroyWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndAfter, int x, int y, int cx, int cy, uint flags);


        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool EqualRect([In] ref RECT lprc1, [In] ref RECT lprc2);

        [DllImport("user32.dll")]
        private static extern int GetWindowLong(IntPtr hwnd, int nIndex);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll")]
        private static extern int GetScrollBarInfo(IntPtr hWnd, uint idObject, ref SCROLLBARINFO psbi);
        #endregion

        #region Fields
        public bool _bTrackingMouse = false;
        private int _iArrowCx = 0;
        private int _iArrowCy = 0;
        private IntPtr _hVerticalMaskWnd = IntPtr.Zero;
        private IntPtr _hHorizontalMaskWnd = IntPtr.Zero;
        private IntPtr _hSizerMaskWnd = IntPtr.Zero;
        private IntPtr _hControlWnd = IntPtr.Zero;
        private cStoreDc _cHorizontalArrowDc = new cStoreDc();
        private cStoreDc _cHorizontalThumbDc = new cStoreDc();
        private cStoreDc _cHorizontalTrackDc = new cStoreDc();
        private cStoreDc _cVerticalArrowDc = new cStoreDc();
        private cStoreDc _cVerticalThumbDc = new cStoreDc();
        private cStoreDc _cVerticalTrackDc = new cStoreDc();
        private Bitmap _oHorizontalArrowBitmap;
        private Bitmap _oHorizontalThumbBitmap;
        private Bitmap _oHorizontalTrackBitmap;
        private Bitmap _oVerticalArrowBitmap;
        private Bitmap _oVerticalThumbBitmap;
        private Bitmap _oVerticalTrackBitmap;
        private Bitmap _oMask;
        #endregion

        #region Constructor
        public cInternalScrollBar(IntPtr hWnd, Bitmap hztrack, Bitmap hzarrow, Bitmap hzthumb, Bitmap vttrack, Bitmap vtarrow, Bitmap vtthumb, Bitmap fader)
        {
            //if (hWnd == IntPtr.Zero)
            //    throw new Exception("The control handle is invalid.");
            //if (hztrack == null)
            //    throw new Exception("The Horizontal Track image is invalid.");
            //if (hzarrow == null)
            //    throw new Exception("The Horizontal Arrow image is invalid.");
            //if (hzthumb == null)
            //    throw new Exception("The Horizontal Thumb image is invalid.");
            //if (vttrack == null)
            //    throw new Exception("The Vertical Track image is invalid.");
            //if (vtarrow == null)
            //    throw new Exception("The Vertical Arrow image is invalid.");
            //if (vtthumb == null)
            //    throw new Exception("The Vertical Thumb image is invalid.");
            try
            {

                //bool isHwndVisible = ((Control.FromHandle(hWnd).Visible));// || (Control.FromHandle(hWnd).GetType() == typeof(myPanel)))); //(IsScrollbarVisible(hWnd) && );
                ////bool hwndContainsProperControls = (Control.FromHandle(hWnd).GetType() == typeof(richTextBox) || Control.FromHandle(hWnd).GetType() == typeof(textBox) || Control.FromHandle(hWnd).GetType() == typeof(listBox) || Control.FromHandle(hWnd).GetType() == typeof(listView) || Control.FromHandle(hWnd).GetType() == typeof(treeView) || Control.FromHandle(hWnd).GetType() == typeof(myPanel) || Control.FromHandle(hWnd).GetType() == typeof(myTabControl));

                //if (isHwndVisible)
                //{
                    HorizontalArrowGraphic = hzarrow;
                    HorizontalThumbGraphic = hzthumb;
                    HorizontalTrackGraphic = hztrack;
                    VerticalArrowGraphic = vtarrow;
                    VerticalThumbGraphic = vtthumb;
                    VerticalTrackGraphic = vttrack;

                    // the fader for this class would require
                    // some additional code, with the fader inclass
                    if (fader != null)
                        TransitionGraphic = fader;
                    scrollbarMetrics();
                    _hControlWnd = hWnd;
                    createScrollBarMask();
                    this.AssignHandle(_hControlWnd);
                //}
                //else
                //{


                //}
            }
            catch
            {

            }
        }
        #endregion

        #region Properties

        //private static bool IsScrollbarVisible(IntPtr hWnd)
        //{
        //    // Determines whether the vertical scrollbar is visible for the specified control
        //    bool bVisible = false;
        //    int nMessage = GWL_STYLE;
        //    int nStyle = GetWindowLong(hWnd, nMessage);
        //    bVisible = ((nStyle & nMessage) != 0);
        //    return bVisible;
        //}



        private Bitmap HorizontalArrowGraphic
        {
            get { return _oHorizontalArrowBitmap; }
            set
            {
                _oHorizontalArrowBitmap = value;
                if (_cHorizontalArrowDc.Hdc != IntPtr.Zero)
                {
                    _cHorizontalArrowDc.Dispose();
                    _cHorizontalArrowDc = new cStoreDc();
                }
                _cHorizontalArrowDc.Width = _oHorizontalArrowBitmap.Width;
                _cHorizontalArrowDc.Height = _oHorizontalArrowBitmap.Height;
                //SelectObject(_cHorizontalArrowDc.Hdc, _oHorizontalArrowBitmap.GetHbitmap());


                Thread selectBmp = new Thread(() => SelectObject(_cHorizontalArrowDc.Hdc, _oHorizontalArrowBitmap.GetHbitmap()));
                selectBmp.Start();
                selectBmp.Join();

            }
        }

        private Bitmap HorizontalThumbGraphic
        {
            get { return _oHorizontalThumbBitmap; }
            set
            {
                _oHorizontalThumbBitmap = value;
                if (_cHorizontalThumbDc.Hdc != IntPtr.Zero)
                {
                    _cHorizontalThumbDc.Dispose();
                    _cHorizontalThumbDc = new cStoreDc();
                }
                _cHorizontalThumbDc.Width = _oHorizontalThumbBitmap.Width;
                _cHorizontalThumbDc.Height = _oHorizontalThumbBitmap.Height;
                //SelectObject(_cHorizontalThumbDc.Hdc, _oHorizontalThumbBitmap.GetHbitmap());


                Thread selectBmp = new Thread(() => SelectObject(_cHorizontalThumbDc.Hdc, _oHorizontalThumbBitmap.GetHbitmap()));
                selectBmp.Start();
                selectBmp.Join();

            }
        }

        private Bitmap HorizontalTrackGraphic
        {
            get { return _oHorizontalTrackBitmap; }
            set
            {
                _oHorizontalTrackBitmap = value;
                if (_cHorizontalTrackDc.Hdc != IntPtr.Zero)
                {
                    _cHorizontalTrackDc.Dispose();
                    _cHorizontalTrackDc = new cStoreDc();
                }
                _cHorizontalTrackDc.Width = _oHorizontalTrackBitmap.Width;
                _cHorizontalTrackDc.Height = _oHorizontalTrackBitmap.Height;
                //SelectObject(_cHorizontalTrackDc.Hdc, _oHorizontalTrackBitmap.GetHbitmap());




                Thread selectBmp = new Thread(() => SelectObject(_cHorizontalTrackDc.Hdc, _oHorizontalTrackBitmap.GetHbitmap()));
                selectBmp.Start();
                selectBmp.Join();
            }
        }

        private Bitmap VerticalArrowGraphic
        {
            get { return _oVerticalArrowBitmap; }
            set
            {
                _oVerticalArrowBitmap = value;
                if (_cVerticalArrowDc.Hdc != IntPtr.Zero)
                {
                    _cVerticalArrowDc.Dispose();
                    _cVerticalArrowDc = new cStoreDc();
                }
                _cVerticalArrowDc.Width = _oVerticalArrowBitmap.Width;
                _cVerticalArrowDc.Height = _oVerticalArrowBitmap.Height;
                //SelectObject(_cVerticalArrowDc.Hdc, _oVerticalArrowBitmap.GetHbitmap());

                Thread selectBmp = new Thread(() => SelectObject(_cVerticalArrowDc.Hdc, _oVerticalArrowBitmap.GetHbitmap()));
                selectBmp.Start();
                selectBmp.Join();

            }
        }

        private Bitmap VerticalThumbGraphic
        {
            get { return _oVerticalThumbBitmap; }
            set
            {
                _oVerticalThumbBitmap = value;
                if (_cVerticalThumbDc.Hdc != IntPtr.Zero)
                {
                    _cVerticalThumbDc.Dispose();
                    _cVerticalThumbDc = new cStoreDc();
                }
                _cVerticalThumbDc.Width = _oVerticalThumbBitmap.Width;
                _cVerticalThumbDc.Height = _oVerticalThumbBitmap.Height;
                //SelectObject(_cVerticalThumbDc.Hdc, _oVerticalThumbBitmap.GetHbitmap());


                Thread selectBmp = new Thread(() => SelectObject(_cVerticalThumbDc.Hdc, _oVerticalThumbBitmap.GetHbitmap()));
                selectBmp.Start();
                selectBmp.Join();
            }
        }

        private Bitmap VerticalTrackGraphic
        {
            get { return _oVerticalTrackBitmap; }
            set
            {
                _oVerticalTrackBitmap = value;
                if (_cVerticalTrackDc.Hdc != IntPtr.Zero)
                {
                    _cVerticalTrackDc.Dispose();
                    _cVerticalTrackDc = new cStoreDc();
                }
                _cVerticalTrackDc.Width = _oVerticalTrackBitmap.Width;
                _cVerticalTrackDc.Height = _oVerticalTrackBitmap.Height;
                //SelectObject(_cVerticalTrackDc.Hdc, _oVerticalTrackBitmap.GetHbitmap());


                Thread selectBmp = new Thread(() => SelectObject(_cVerticalTrackDc.Hdc, _oVerticalTrackBitmap.GetHbitmap()));
                selectBmp.Start();
                selectBmp.Join();
            }
        }

        private Bitmap TransitionGraphic
        {
            get { return _oMask; }
            set { _oMask = value; }
        }

        private int HScrollPos
        {
            get { return GetScrollPos((IntPtr)this.Handle, SB_HORZ); }
            set { SetScrollPos((IntPtr)this.Handle, SB_HORZ, value, true); }
        }

        private int VScrollPos
        {
            get { return GetScrollPos((IntPtr)this.Handle, SB_VERT); }
            set { SetScrollPos((IntPtr)this.Handle, SB_VERT, value, true); }
        }
        #endregion

        #region Methods
        public void checkBarState()
        {
            if ((GetWindowLong(_hControlWnd, GWL_STYLE) & WS_VISIBLE) == WS_VISIBLE)
            {
                if (hasHorizontal())
                    ShowWindow(_hHorizontalMaskWnd, SW_NORMAL);
                else
                    ShowWindow(_hHorizontalMaskWnd, SW_HIDE);

                if (hasVertical())
                    ShowWindow(_hVerticalMaskWnd, SW_NORMAL);
                else
                    ShowWindow(_hVerticalMaskWnd, SW_HIDE);

                if (hasSizer())
                    ShowWindow(_hSizerMaskWnd, SW_NORMAL);
                else
                    ShowWindow(_hSizerMaskWnd, SW_HIDE);
            }
            else
            {
                ShowWindow(_hHorizontalMaskWnd, SW_HIDE);
                ShowWindow(_hVerticalMaskWnd, SW_HIDE);
                ShowWindow(_hSizerMaskWnd, SW_HIDE);
            }
        }

        private void createScrollBarMask()
        {
            Type t = typeof(cScrollBar);
            Module m = t.Module;
            IntPtr hInstance = Marshal.GetHINSTANCE(m);
            IntPtr hParent = GetParent(_hControlWnd);
            RECT tr = new RECT();
            Point pt = new Point();
            SCROLLBARINFO sb = new SCROLLBARINFO();
            sb.cbSize = Marshal.SizeOf(sb);

            // vertical scrollbar
            // get the size and position
            GetScrollBarInfo(_hControlWnd, OBJID_VSCROLL, ref sb);
            tr = sb.rcScrollBar;
            pt.X = tr.Left;
            pt.Y = tr.Top;
            ScreenToClient(hParent, ref pt);

            // create the window
            _hVerticalMaskWnd = CreateWindowEx(WS_EX_TOPMOST | WS_EX_TOOLWINDOW,
                "STATIC", "",
                SS_OWNERDRAW | WS_CHILD | WS_CLIPSIBLINGS | WS_OVERLAPPED | WS_VISIBLE,
                pt.X, pt.Y,
                (tr.Right - tr.Left), (tr.Bottom - tr.Top),
                hParent,
                IntPtr.Zero, hInstance, IntPtr.Zero);

            // set z-order
            SetWindowPos(_hVerticalMaskWnd, HWND_TOP,
                0, 0,
                0, 0,
                SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE | SWP_NOOWNERZORDER);

            // horizontal scrollbar
            GetScrollBarInfo(_hControlWnd, OBJID_HSCROLL, ref sb);
            tr = sb.rcScrollBar;
            pt.X = tr.Left;
            pt.Y = tr.Top;
            ScreenToClient(hParent, ref pt);

            _hHorizontalMaskWnd = CreateWindowEx(WS_EX_TOPMOST | WS_EX_TOOLWINDOW,
                "STATIC", "",
                SS_OWNERDRAW | WS_CHILD | WS_CLIPSIBLINGS | WS_OVERLAPPED | WS_VISIBLE,
                pt.X, pt.Y,
                (tr.Right - tr.Left), (tr.Bottom - tr.Top),
                hParent,
                IntPtr.Zero, hInstance, IntPtr.Zero);

            SetWindowPos(_hHorizontalMaskWnd, HWND_TOP,
                0, 0,
                0, 0,
                SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE | SWP_NOOWNERZORDER);

            // sizer
            _hSizerMaskWnd = CreateWindowEx(WS_EX_TOPMOST | WS_EX_TOOLWINDOW,
                "STATIC", "",
                SS_OWNERDRAW | WS_CHILD | WS_CLIPSIBLINGS | WS_OVERLAPPED | WS_VISIBLE,
                pt.X + (tr.Right - tr.Left), pt.Y,
                _iArrowCx, _iArrowCy,
                hParent,
                IntPtr.Zero, hInstance, IntPtr.Zero);

            SetWindowPos(_hSizerMaskWnd, HWND_TOP,
                0, 0,
                0, 0,
                SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE | SWP_NOOWNERZORDER);
            reSizeMask();
        }

        public void drawScrollBar()
        {
            RECT tr = new RECT();
            Point pst = new Point();
            cStoreDc tempDc = new cStoreDc();
            IntPtr hdc = IntPtr.Zero;
            int offset = 0;
            int width = 0;
            int section = 0;
            SCROLLBARINFO sb = new SCROLLBARINFO();
            sb.cbSize = Marshal.SizeOf(sb);

            if (hasHorizontal())
            {
                GetScrollBarInfo(_hControlWnd, OBJID_HSCROLL, ref sb);
                tr = sb.rcScrollBar;
                OffsetRect(ref tr, -tr.Left, -tr.Top);
                tempDc.Width = tr.Right;
                tempDc.Height = tr.Bottom;
                SB_HITEST hitTest = scrollbarHitTest(Orientation.Horizontal);

                // draw the track
                using (StretchImage si = new StretchImage(_cHorizontalTrackDc.Hdc, tempDc.Hdc, new Rectangle(0, 0, _cHorizontalTrackDc.Width, _cHorizontalTrackDc.Height), new Rectangle(_iArrowCx, 0, tr.Right - (2 * _iArrowCx), tr.Bottom), 2, StretchModeEnum.STRETCH_HALFTONE)) { }
                // draw the arrows
                section = 7;
                width = _cHorizontalArrowDc.Width / section;
                // left arrow
                if (hitTest == SB_HITEST.leftArrow)
                {
                    if (leftKeyPressed())
                        offset = 2;
                    else
                        offset = 1;

                }
                else
                {
                    offset = 0;
                }
                using (StretchImage si = new StretchImage(_cHorizontalArrowDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cHorizontalArrowDc.Height), new Rectangle(0, 0, _iArrowCx, tr.Bottom), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                // right arrow
                if (hitTest == SB_HITEST.rightArrow)
                {
                    if (leftKeyPressed())
                        offset = 5;
                    else
                        offset = 4;

                }
                else
                {
                    offset = 3;
                }
                using (StretchImage si = new StretchImage(_cHorizontalArrowDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cHorizontalArrowDc.Height), new Rectangle(tr.Right - _iArrowCx, 0, _iArrowCx, tr.Bottom), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                // draw the thumb
                section = 3;
                width = _cHorizontalThumbDc.Width / section;
                if (hitTest == SB_HITEST.button)
                {
                    if (leftKeyPressed())
                        offset = 2;
                    else
                        offset = 1;

                }
                else
                {
                    offset = 0;
                }
                pst.X = sb.xyThumbTop;
                pst.Y = sb.xyThumbBottom;







                using (StretchImage si = new StretchImage(_cHorizontalThumbDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cHorizontalThumbDc.Height), new Rectangle(pst.X, 2, pst.Y - pst.X, tr.Bottom), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                hdc = GetDC(_hHorizontalMaskWnd);
                BitBlt(hdc, 0, 0, tr.Right, tr.Bottom, tempDc.Hdc, 0, 0, 0xCC0020);
                ReleaseDC(_hHorizontalMaskWnd, hdc);
            }

            if (hasSizer())
            {
                tempDc.Width = _iArrowCx;
                tempDc.Height = _iArrowCy;
                offset = 6;
                section = 7;
                width = _cHorizontalArrowDc.Width / section;

                using (StretchImage si = new StretchImage(_cHorizontalArrowDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cHorizontalArrowDc.Height), new Rectangle(0, 0, _iArrowCx, _iArrowCy), 0, StretchModeEnum.STRETCH_HALFTONE)) { }
                hdc = GetDC(_hSizerMaskWnd);
                BitBlt(hdc, 0, 0, _iArrowCx, _iArrowCy, tempDc.Hdc, 0, 0, 0xCC0020);
                ReleaseDC(_hSizerMaskWnd, hdc);
            }

            if (hasVertical())
            {
                GetScrollBarInfo(_hControlWnd, OBJID_VSCROLL, ref sb);
                tr = sb.rcScrollBar;
                OffsetRect(ref tr, -tr.Left, -tr.Top);
                tempDc.Width = tr.Right;
                tempDc.Height = tr.Bottom;
                SB_HITEST hitTest = scrollbarHitTest(Orientation.Vertical);

                // draw the track
                using (StretchImage si = new StretchImage(_cVerticalTrackDc.Hdc, tempDc.Hdc, new Rectangle(0, 0, _cVerticalTrackDc.Width, _cVerticalTrackDc.Height), new Rectangle(0, _iArrowCy, tr.Right, tr.Bottom - (2 * _iArrowCy)), 2, StretchModeEnum.STRETCH_HALFTONE)) { }
                section = 6;
                width = _cVerticalArrowDc.Width / section;

                // top arrow
                if (hitTest == SB_HITEST.topArrow)
                {
                    if (leftKeyPressed())
                        offset = 2;
                    else
                        offset = 1;

                }
                else
                {
                    offset = 0;
                }
                using (StretchImage si = new StretchImage(_cVerticalArrowDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cVerticalArrowDc.Height), new Rectangle(0, 0, tr.Right, _iArrowCy), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                // bottom arrow
                if (hitTest == SB_HITEST.bottomArrow)
                {
                    if (leftKeyPressed())
                        offset = 5;
                    else
                        offset = 4;

                }
                else
                {
                    offset = 3;
                }
                using (StretchImage si = new StretchImage(_cVerticalArrowDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cVerticalArrowDc.Height), new Rectangle(0, tr.Bottom - _iArrowCy, tr.Right, _iArrowCy), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                // draw the thumb
                section = 3;
                width = _cVerticalThumbDc.Width / section;
                if (hitTest == SB_HITEST.button)
                {
                    if (leftKeyPressed())
                        offset = 2;
                    else
                        offset = 1;

                }
                else
                {
                    offset = 0;
                }

                pst.X = sb.xyThumbTop;
                pst.Y = sb.xyThumbBottom;
                using (StretchImage si = new StretchImage(_cVerticalThumbDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cVerticalThumbDc.Height), new Rectangle(0, pst.X, _iArrowCx, pst.Y - pst.X), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                hdc = GetDC(_hVerticalMaskWnd);
                BitBlt(hdc, 0, 0, tr.Right, tr.Bottom, tempDc.Hdc, 0, 0, 0xCC0020);
                ReleaseDC(_hVerticalMaskWnd, hdc);
            }
            tempDc.Dispose();
        }

        public void scrollFader()
        {
            if (TransitionGraphic != null)
            {
                SB_HITEST hitTest;
                SCROLLBARINFO sb = new SCROLLBARINFO();
                sb.cbSize = Marshal.SizeOf(sb);
                if (hasHorizontal())
                {
                    hitTest = scrollbarHitTest(Orientation.Horizontal);

                    if ((hitTest == SB_HITEST.button) && (!leftKeyPressed()))
                    {
                        GetScrollBarInfo(_hControlWnd, OBJID_HSCROLL, ref sb);
                        // start the transition routines here
                        // size of mask - new Rectangle(sb.xyThumbTop, 2, sb.xyThumbBottom - sb.xyThumbTop, sb.rcScrollBar.Bottom)
                    }
                }
                if (hasVertical())
                {
                    hitTest = scrollbarHitTest(Orientation.Vertical);

                    if ((hitTest == SB_HITEST.button) && (!leftKeyPressed()))
                    {
                        GetScrollBarInfo(_hControlWnd, OBJID_VSCROLL, ref sb);
                        // start the transition routines here
                        // size of mask - new Rectangle(sb.xyThumbTop, 2, sb.xyThumbBottom - sb.xyThumbTop, sb.rcScrollBar.Bottom)
                    }
                }
            }


        }

        private bool hasHorizontal()
        {
            return ((GetWindowLong(_hControlWnd, GWL_STYLE) & WS_HSCROLL) == WS_HSCROLL);
        }

        private bool hasSizer()
        {
            return (hasHorizontal() && hasVertical());
        }

        private bool hasVertical()
        {
            return ((GetWindowLong(_hControlWnd, GWL_STYLE) & WS_VSCROLL) == WS_VSCROLL);
        }

        public void invalidateWindow(bool messaged)
        {
            if (messaged)
                RedrawWindow(_hControlWnd, IntPtr.Zero, IntPtr.Zero, RDW_INTERNALPAINT);
            else
                RedrawWindow(_hControlWnd, IntPtr.Zero, IntPtr.Zero, RDW_INVALIDATE | RDW_UPDATENOW);
        }

        private bool leftKeyPressed()
        {
            if (mouseButtonsSwitched())
                return (GetKeyState(VK_RBUTTON) < 0);
            else
                return (GetKeyState(VK_LBUTTON) < 0);
        }

        private bool mouseButtonsSwitched()
        {
            return (GetSystemMetrics(SYSTEM_METRICS.SM_SWAPBUTTON) != 0);
        }

        private SB_HITEST scrollbarHitTest(Orientation orient)
        {
            Point pt = new Point();
            RECT tr = new RECT();
            RECT tp = new RECT();
            SCROLLBARINFO sb = new SCROLLBARINFO();
            sb.cbSize = Marshal.SizeOf(sb);

            GetCursorPos(ref pt);

            if (orient == Orientation.Horizontal)
            {
                GetScrollBarInfo(_hControlWnd, OBJID_HSCROLL, ref sb);
                tr = sb.rcScrollBar;
                //OffsetRect(ref tr, -tr.Left, -tr.Top);
                tp = tr;
                if (PtInRect(ref tr, pt))
                {
                    // left arrow
                    tp.Right = tp.Left + _iArrowCx;
                    if (PtInRect(ref tp, pt))
                        return SB_HITEST.leftArrow;
                    // right arrow
                    tp.Left = tr.Right - _iArrowCx;
                    tp.Right = tr.Right;
                    if (PtInRect(ref tp, pt))
                        return SB_HITEST.rightArrow;
                    // button
                    tp.Left = tr.Left + sb.xyThumbTop;
                    tp.Right = tr.Left + sb.xyThumbBottom;
                    if (PtInRect(ref tp, pt))
                        return SB_HITEST.button;
                    // track
                    return SB_HITEST.track;
                }
            }
            else
            {
                GetScrollBarInfo(_hControlWnd, OBJID_VSCROLL, ref sb);
                tr = sb.rcScrollBar;
                tp = tr;

                if (PtInRect(ref tr, pt))
                {
                    // top arrow
                    tp.Bottom = tr.Top + _iArrowCy;
                    if (PtInRect(ref tp, pt))
                        return SB_HITEST.topArrow;
                    // bottom arrow
                    tp.Top = tr.Bottom - _iArrowCy;
                    tp.Bottom = tr.Bottom;
                    if (PtInRect(ref tp, pt))
                        return SB_HITEST.bottomArrow;
                    // button
                    tp.Top = tr.Top + sb.xyThumbTop;
                    tp.Bottom = tr.Bottom + sb.xyThumbBottom;
                    if (PtInRect(ref tp, pt))
                        return SB_HITEST.button;
                    // track
                    return SB_HITEST.track;
                }
            }
            return SB_HITEST.offControl;
        }

        public void reSizeMask()
        {
            RECT tr = new RECT();
            SCROLLBARINFO sb = new SCROLLBARINFO();
            sb.cbSize = Marshal.SizeOf(sb);
            IntPtr hParent = GetParent(_hControlWnd);
            Point pt = new Point();

            if (hasVertical())
            {
                GetScrollBarInfo(_hControlWnd, OBJID_VSCROLL, ref sb);
                tr = sb.rcScrollBar;
                pt.X = tr.Left;
                pt.Y = tr.Top;
                ScreenToClient(hParent, ref pt);
                SetWindowPos(_hVerticalMaskWnd, IntPtr.Zero, pt.X, pt.Y, tr.Right - tr.Left, tr.Bottom - tr.Top, SWP_NOACTIVATE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_SHOWWINDOW);
            }
            if (hasHorizontal())
            {
                GetScrollBarInfo(_hControlWnd, OBJID_HSCROLL, ref sb);
                tr = sb.rcScrollBar;
                pt.X = tr.Left;
                pt.Y = tr.Top;
                ScreenToClient(hParent, ref pt);
                SetWindowPos(_hHorizontalMaskWnd, IntPtr.Zero, pt.X, pt.Y, tr.Right - tr.Left, tr.Bottom - tr.Top, SWP_NOACTIVATE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_SHOWWINDOW);
            }
            if (hasSizer())
            {
                GetScrollBarInfo(_hControlWnd, OBJID_HSCROLL, ref sb);
                tr = new RECT(sb.rcScrollBar.Right, sb.rcScrollBar.Top, sb.rcScrollBar.Right + _iArrowCx, sb.rcScrollBar.Bottom);
                pt.X = tr.Left;
                pt.Y = tr.Top;
                ScreenToClient(hParent, ref pt);
                SetWindowPos(_hSizerMaskWnd, IntPtr.Zero, pt.X, pt.Y, tr.Right - tr.Left, tr.Bottom - tr.Top, SWP_NOACTIVATE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_SHOWWINDOW);
            }
        }

        private void scrollbarMetrics()
        {
            _iArrowCx = GetSystemMetrics(SYSTEM_METRICS.SM_CXVSCROLL);
            _iArrowCy = GetSystemMetrics(SYSTEM_METRICS.SM_CYVSCROLL);
        }

        private void scrollHorizontal(bool Right)
        {
            if (Right)
                SendMessage(_hControlWnd, WM_HSCROLL, SB_LINERIGHT, 0);
            else
                SendMessage(_hControlWnd, WM_HSCROLL, SB_LINELEFT, 0);

        }

        private void scrollVertical(bool Down)
        {
            if (Down)
                SendMessage(_hControlWnd, WM_VSCROLL, SB_LINEDOWN, 0);
            else
                SendMessage(_hControlWnd, WM_VSCROLL, SB_LINEUP, 0);
        }

        public void Dispose()
        {
            try
            {
                this.ReleaseHandle();
                if (_oVerticalArrowBitmap != null) _oVerticalArrowBitmap.Dispose();
                if (_cVerticalArrowDc != null) _cVerticalArrowDc.Dispose();
                if (_oVerticalThumbBitmap != null) _oVerticalThumbBitmap.Dispose();
                if (_cVerticalThumbDc != null) _cVerticalThumbDc.Dispose();
                if (_oVerticalTrackBitmap != null) _oVerticalTrackBitmap.Dispose();
                if (_cVerticalTrackDc != null) _cVerticalTrackDc.Dispose();
                if (_oHorizontalArrowBitmap != null) _oHorizontalArrowBitmap.Dispose();
                if (_cHorizontalArrowDc != null) _cHorizontalArrowDc.Dispose();
                if (_oHorizontalThumbBitmap != null) _oHorizontalThumbBitmap.Dispose();
                if (_cHorizontalThumbDc != null) _cHorizontalThumbDc.Dispose();
                if (_oHorizontalTrackBitmap != null) _oHorizontalTrackBitmap.Dispose();
                if (_cHorizontalTrackDc != null) _cHorizontalTrackDc.Dispose();
                if (_hVerticalMaskWnd != IntPtr.Zero) DestroyWindow(_hVerticalMaskWnd);
                if (_hHorizontalMaskWnd != IntPtr.Zero) DestroyWindow(_hHorizontalMaskWnd);
                if (_hSizerMaskWnd != IntPtr.Zero) DestroyWindow(_hSizerMaskWnd);
            }
            catch { }
            GC.SuppressFinalize(this);
        }
        #endregion


        ////  Scrollbar direction
        ////  All these constents can be found in WinUser.h
        //// 
        //private const int SBS_HORZ = 0;
        //private const int SBS_VERT = 1;
        ////  Windows Messages
        ////  All these constents can be found in WinUser.h
        //// 
        //private const int WM_VSCROLL = 277;
        //private const int WM_HSCROLL = 276;
        //private const int SB_THUMBPOSITION = 4;
        //public enum eScrollAction
        //{

        //    Jump = 0,

        //    Relative = 1,
        //}
        //public enum eScrollDirection
        //{

        //    Vertical = 0,

        //    Horizontal = 1,
        //}


        ////  API Function: GetScrollPos
        ////  Returns an integer of the position of the scrollbar
        //// 
        //[DllImport("user32.dll")]
        //private static extern int GetScrollPos(IntPtr hWnd, int nBar);

        ////  API Function: SetScrollPos
        ////  Sets ONLY the scrollbar DOES NOT change the control object
        //// 
        //[DllImport("user32.dll")]
        //private static extern int SetScrollPos(IntPtr hWnd, int nBar, int nPos, bool bRedraw);

        //  API Function: PostMessageA
        //  Sends a message to a control (We are going to tell it to synch
        //  with the scrollbar)
        // 
        //[DllImport("user32.dll")]
        //private static extern bool PostMessageA(IntPtr hwnd, int wMsg, int wParam, int lParam);

        ////      Sub: scrollControl
        ////  Purpose: All functions to control the scroll are in here
        //// 
        //private void scrollControl(IntPtr hWnd, eScrollDirection Direction, eScrollAction Action, int Amount)
        //{
        //    int position;
        //    //  What direction are we going
        //    if ((Direction == eScrollDirection.Horizontal))
        //    {
        //        //  What action are we taking (Jumping or Relative)
        //        if ((Action == eScrollAction.Relative))
        //        {
        //            position = (GetScrollPos(hWnd, SBS_HORZ) + Amount);
        //        }
        //        else
        //        {
        //            position = Amount;
        //        }
        //        //  Make it so
        //        if ((SetScrollPos(hWnd, SBS_HORZ, position, true) != -1))
        //        {
        //            PostMessageA(hWnd, WM_HSCROLL, (SB_THUMBPOSITION + (65536 * position)), 0);
        //        }
        //        else
        //        {
        //            //MsgBox(("Can\'t set info (Err: "
        //            //                + (GetLastWin32Error() + ")")));
        //        }
        //    }
        //    else
        //    {
        //        //  What action are we taking (Jumping or Relative)
        //        if ((Action == eScrollAction.Relative))
        //        {
        //            position = (GetScrollPos(hWnd, SBS_VERT) + Amount);
        //        }
        //        else
        //        {
        //            position = Amount;
        //        }
        //        //  Make it so
        //        if ((SetScrollPos(hWnd, SBS_VERT, position, true) != -1))
        //        {
        //            PostMessageA(hWnd, WM_VSCROLL, (SB_THUMBPOSITION + (65536 * position)), 0);
        //        }
        //        else
        //        {
        //            //MsgBox(("Can\'t set info (Err: "
        //            //                + (GetLastWin32Error() + ")")));
        //        }
        //    }
        //}




        //#region WndProc
        //protected override void WndProc(ref Message m)
        //{

        //    switch (m.Msg)
        //    {

        //        case WM_VSCROLL:
        //            this.WndProc(ref m);



        //            break;
        //        case WM_HSCROLL:

        //            this.WndProc(ref m);



        //            break;
        //        default:
        //            this.WndProc(ref m);
        //            break;
        //    }
        //}
        //#endregion


        //public bool PreFilterMessage(ref Message m)
        //{
        //    switch (m.Msg)
        //    {
        //        case WM_MOUSEWHEEL:   // 0x020A
        //        case WM_MOUSEHWHEEL:  // 0x020E
        //            IntPtr hControlUnderMouse = WindowFromPoint(new Point((int)m.LParam));
        //            if (hControlUnderMouse == m.HWnd)
        //                return false; // already headed for the right control
        //            else
        //            {
        //                // redirect the message to the control under the mouse
        //                SendMessage(hControlUnderMouse, m.Msg, m.WParam, m.LParam);
        //                return true;
        //            }
        //        default:
        //            return false;
        //    }
        //}








        //protected override void OnResize(EventArgs e)
        //{
        //    this.OnResize(e);
        //    VisibleScrollbars = GetVisibleScrollbars(this);
        //}




        #region WndProc
        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {

                //case 0x000F: //WM_PAINT
                case 0x0083:
                    reSizeMask();
                    base.WndProc(ref m);

                    break;
                case WM_NCPAINT:
                    drawScrollBar();
                    base.WndProc(ref m);
                    break;

                case WM_MOUSEHWHEEL:  // 0x020E
                case WM_HSCROLL:
                    //scrollControl(m.WParam, eScrollDirection.Vertical, eScrollAction.Relative, 5);
                    _bTrackingMouse = true;
                    drawScrollBar();
                    scrollFader();

                    base.WndProc(ref m);
                    break;
                case WM_MOUSEWHEEL:   // 0x020A
                case WM_VSCROLL:
                    //scrollControl(m.WParam, eScrollDirection.Vertical, eScrollAction.Relative, -5);

                    _bTrackingMouse = true;
                    drawScrollBar();
                    scrollFader();
                    base.WndProc(ref m);
                    break;
                case WM_NCMOUSEMOVE:
                    _bTrackingMouse = true;
                    drawScrollBar();
                    scrollFader();
                    base.WndProc(ref m);
                    break;

                case WM_MOUSEMOVE:
                    if (_bTrackingMouse)
                        _bTrackingMouse = false;
                    base.WndProc(ref m);
                    break;


                case WM_SIZE:
                case WM_MOVE:
                    reSizeMask();
                    base.WndProc(ref m);
                    break;

                default:
                    checkBarState();
                    base.WndProc(ref m);
                    break;
            }
        }
        #endregion














        [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust")]
        class cScrollBar : NativeWindow, IDisposable
        {
            #region Constants
            // showwindow
            private const int SW_HIDE = 0x0;
            private const int SW_NORMAL = 0x1;
            // window styles
            private const int GWL_STYLE = (-16);
            private const int GWL_EXSTYLE = (-20);
            private const int WS_EX_TOPMOST = 0x8;
            private const int WS_EX_TOOLWINDOW = 0x80;
            private const int WS_CHILD = 0x40000000;
            private const int WS_OVERLAPPED = 0x0;
            private const int WS_CLIPCHILDREN = 0x2000000;
            private const int WS_CLIPSIBLINGS = 0x4000000;
            private const int WS_VISIBLE = 0x10000000;
            private const int SS_OWNERDRAW = 0xD;
            // size/move
            private const uint SWP_NOSIZE = 0x0001;
            private const uint SWP_NOMOVE = 0x0002;
            private const uint SWP_NOZORDER = 0x0004;
            private const uint SWP_NOREDRAW = 0x0008;
            private const uint SWP_NOACTIVATE = 0x0010;
            private const uint SWP_FRAMECHANGED = 0x0020;
            private const uint SWP_SHOWWINDOW = 0x0040;
            private const uint SWP_HIDEWINDOW = 0x0080;
            private const uint SWP_NOCOPYBITS = 0x0100;
            private const uint SWP_NOOWNERZORDER = 0x0200;
            private const uint SWP_NOSENDCHANGING = 0x0400;
            // setwindowpos
            static readonly IntPtr HWND_NOTOPMOST = new IntPtr(-2);
            static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
            static readonly IntPtr HWND_TOP = new IntPtr(0);
            static readonly IntPtr HWND_BOTTOM = new IntPtr(1);
            // scroll messages
            private const int WM_HSCROLL = 0x114;
            private const int WM_VSCROLL = 0x115;
            private const int SB_LINEUP = 0;
            private const int SB_LINEDOWN = 1;
            private const int SB_LINELEFT = 0;
            private const int SB_LINERIGHT = 1;
            private const int SB_PAGEUP = 2;
            private const int SB_PAGEDOWN = 3;
            private const int SB_PAGELEFT = 2;
            private const int SB_PAGERIGHT = 3;
            // mouse buttons
            private const int VK_LBUTTON = 0x1;
            private const int VK_RBUTTON = 0x2;
            // redraw
            private const int RDW_INVALIDATE = 0x0001;
            private const int RDW_INTERNALPAINT = 0x0002;
            private const int RDW_ERASE = 0x0004;
            private const int RDW_VALIDATE = 0x0008;
            private const int RDW_NOINTERNALPAINT = 0x0010;
            private const int RDW_NOERASE = 0x0020;
            private const int RDW_NOCHILDREN = 0x0040;
            private const int RDW_ALLCHILDREN = 0x0080;
            private const int RDW_UPDATENOW = 0x0100;
            private const int RDW_ERASENOW = 0x0200;
            private const int RDW_FRAME = 0x0400;
            private const int RDW_NOFRAME = 0x0800;
            // scroll bar messages
            private const int SB_HORZ = 0x0;
            private const int SB_VERT = 0x1;
            private const int SBM_SETPOS = 0x00E0;
            private const int SBM_GETPOS = 0x00E1;
            private const int SBM_SETRANGE = 0x00E2;
            private const int SBM_SETRANGEREDRAW = 0x00E6;
            private const int SBM_GETRANGE = 0x00E3;
            private const int SBM_ENABLE_ARROWS = 0x00E4;
            private const int SBM_SETSCROLLINFO = 0x00E9;
            private const int SBM_GETSCROLLINFO = 0x00EA;
            private const int SBM_GETSCROLLBARINFO = 0x00EB;
            private const int SIF_RANGE = 0x0001;
            private const int SIF_PAGE = 0x0002;
            private const int SIF_POS = 0x0004;
            private const int SIF_DISABLENOSCROLL = 0x0008;
            private const int SIF_TRACKPOS = 0x0010;
            private const int SIF_ALL = (SIF_RANGE | SIF_PAGE | SIF_POS | SIF_TRACKPOS);
            // scrollbar states
            private const int STATE_SYSTEM_INVISIBLE = 0x00008000;
            private const int STATE_SYSTEM_OFFSCREEN = 0x00010000;
            private const int STATE_SYSTEM_PRESSED = 0x00000008;
            private const int STATE_SYSTEM_UNAVAILABLE = 0x00000001;
            private const uint OBJID_HSCROLL = 0xFFFFFFFA;
            private const uint OBJID_VSCROLL = 0xFFFFFFFB;
            private const uint OBJID_CLIENT = 0xFFFFFFFC;
            // window messages
            private const int WM_PAINT = 0xF;
            private const int WM_MOUSEMOVE = 0x200;
            private const int WM_MOUSELEAVE = 0x2A3;
            private const int WM_LBUTTONDOWN = 0x201;
            private const int WM_LBUTTONUP = 0x202;
            private const int WM_LBUTTONDBLCLK = 0x203;
            private const int WM_RBUTTONDOWN = 0x204;
            private const int WM_RBUTTONUP = 0x205;
            private const int WM_RBUTTONDBLCLK = 0x206;
            private const int WM_MBUTTONDOWN = 0x207;
            private const int WM_MBUTTONUP = 0x208;
            private const int WM_MBUTTONDBLCLK = 0x209;
            private const int WM_MOUSEWHEEL = 0x20A;
            private const int WM_SIZE = 0x5;
            private const int WM_MOVE = 0x3;
            // message handler
            private static IntPtr MSG_HANDLED = new IntPtr(1);
            #endregion

            #region Enums
            private enum SB_HITEST : int
            {
                offControl = 0,
                topArrow,
                bottomArrow,
                leftArrow,
                rightArrow,
                button,
                track
            }

            private enum SYSTEM_METRICS : int
            {
                SM_CXSCREEN = 0,
                SM_CYSCREEN = 1,
                SM_CXVSCROLL = 2,
                SM_CYHSCROLL = 3,
                SM_CYCAPTION = 4,
                SM_CXBORDER = 5,
                SM_CYBORDER = 6,
                SM_CYVTHUMB = 9,
                SM_CXHTHUMB = 10,
                SM_CXICON = 11,
                SM_CYICON = 12,
                SM_CXCURSOR = 13,
                SM_CYCURSOR = 14,
                SM_CYMENU = 15,
                SM_CXFULLSCREEN = 16,
                SM_CYFULLSCREEN = 17,
                SM_CYKANJIWINDOW = 18,
                SM_MOUSEPRESENT = 19,
                SM_CYVSCROLL = 20,
                SM_CXHSCROLL = 21,
                SM_SWAPBUTTON = 23,
                SM_CXMIN = 28,
                SM_CYMIN = 29,
                SM_CXSIZE = 30,
                SM_CYSIZE = 31,
                SM_CXFRAME = 32,
                SM_CYFRAME = 33,
                SM_CXMINTRACK = 34,
                SM_CYMINTRACK = 35,
                SM_CYSMCAPTION = 51,
                SM_CXMINIMIZED = 57,
                SM_CYMINIMIZED = 58,
                SM_CXMAXTRACK = 59,
                SM_CYMAXTRACK = 60,
                SM_CXMAXIMIZED = 61,
                SM_CYMAXIMIZED = 62
            }
            #endregion

            #region Structs
            [StructLayout(LayoutKind.Sequential)]
            private struct PAINTSTRUCT
            {
                internal IntPtr hdc;
                internal int fErase;
                internal RECT rcPaint;
                internal int fRestore;
                internal int fIncUpdate;
                internal int Reserved1;
                internal int Reserved2;
                internal int Reserved3;
                internal int Reserved4;
                internal int Reserved5;
                internal int Reserved6;
                internal int Reserved7;
                internal int Reserved8;
            }

            [StructLayout(LayoutKind.Sequential)]
            private struct RECT
            {
                internal RECT(int X, int Y, int Width, int Height)
                {
                    this.Left = X;
                    this.Top = Y;
                    this.Right = Width;
                    this.Bottom = Height;
                }
                internal int Left;
                internal int Top;
                internal int Right;
                internal int Bottom;
            }

            [StructLayout(LayoutKind.Sequential)]
            private struct SCROLLINFO
            {
                internal uint cbSize;
                internal uint fMask;
                internal int nMin;
                internal int nMax;
                internal uint nPage;
                internal int nPos;
                internal int nTrackPos;
            }

            [StructLayout(LayoutKind.Sequential)]
            private struct SCROLLBARINFO
            {
                internal int cbSize;
                internal RECT rcScrollBar;
                internal int dxyLineButton;
                internal int xyThumbTop;
                internal int xyThumbBottom;
                internal int reserved;
                [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
                internal int[] rgstate;
            }
            #endregion

            #region API
            [DllImport("user32.dll")]
            private static extern IntPtr BeginPaint(IntPtr hWnd, ref PAINTSTRUCT ps);

            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool EndPaint(IntPtr hWnd, ref PAINTSTRUCT ps);

            [DllImport("gdi32.dll")]
            private static extern IntPtr SelectObject(IntPtr hdc, IntPtr hObject);

            [DllImport("gdi32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool BitBlt(IntPtr hdc, int nXDest, int nYDest, int nWidth, int nHeight, IntPtr hdcSrc, int nXSrc, int nYSrc, int dwRop);

            [DllImport("gdi32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool StretchBlt(IntPtr hDest, int X, int Y, int nWidth, int nHeight, IntPtr hdcSrc,
            int sX, int sY, int nWidthSrc, int nHeightSrc, int dwRop);

            [DllImport("user32.dll")]
            private static extern IntPtr GetDC(IntPtr handle);

            [DllImport("user32.dll")]
            private static extern int ReleaseDC(IntPtr handle, IntPtr hdc);

            [DllImport("user32.dll")]
            private static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
            [DllImport("user32.dll")]
            private static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, ref SCROLLBARINFO lParam);

            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool GetWindowRect(IntPtr hWnd, ref RECT lpRect);

            [DllImport("user32.dll")]
            private extern static int OffsetRect(ref RECT lpRect, int x, int y);

            [DllImport("user32.dll")]
            private static extern bool ValidateRect(IntPtr hWnd, ref RECT lpRect);

            [DllImport("user32.dll")]
            private static extern int GetSystemMetrics(SYSTEM_METRICS smIndex);

            [DllImport("uxtheme.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private extern static bool IsAppThemed();

            [DllImport("uxtheme.dll", ExactSpelling = true, CharSet = CharSet.Unicode)]
            private static extern int SetWindowTheme(IntPtr hWnd, String pszSubAppName, String pszSubIdList);

            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool RedrawWindow(IntPtr hWnd, IntPtr lprcUpdate, IntPtr hrgnUpdate, uint flags);

            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool GetCursorPos(ref Point lpPoint);

            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool PtInRect([In] ref RECT lprc, Point pt);

            [DllImport("user32.dll")]
            private static extern int ScreenToClient(IntPtr hwnd, ref Point lpPoint);

            [DllImport("user32.dll")]
            private static extern short GetKeyState(int nVirtKey);

            [DllImport("user32.dll")]
            private static extern int SetScrollPos(IntPtr hWnd, int nBar, int nPos, bool bRedraw);

            [DllImport("user32.dll")]
            private static extern int GetScrollPos(IntPtr hWnd, int nBar);

            [DllImport("user32.dll")]
            private static extern IntPtr GetParent(IntPtr hWnd);

            [DllImport("user32.dll", SetLastError = true)]
            private static extern IntPtr CreateWindowEx(int exstyle, string lpClassName, string lpWindowName, int dwStyle,
                int x, int y, int nWidth, int nHeight, IntPtr hwndParent, IntPtr Menu, IntPtr hInstance, IntPtr lpParam);

            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool DestroyWindow(IntPtr hWnd);

            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndAfter, int x, int y, int cx, int cy, uint flags);

            [DllImport("user32.dll")]
            static extern bool EqualRect([In] ref RECT lprc1, [In] ref RECT lprc2);

            [DllImport("user32.dll")]
            private static extern int GetWindowLong(IntPtr hwnd, int nIndex);

            [DllImport("user32.dll")]
            private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
            #endregion

            #region Fields
            public bool _bPainting = false;
            //private bool _bMoved = false;
            //private bool _bFading = false;
            private IntPtr _hMaskWnd = IntPtr.Zero;
            private int _iArrowCx = 14;
            private int _iArrowCy = 14;
            private Orientation _eScrollbarOrientation;
            private IntPtr _hScrollBarWnd = IntPtr.Zero;
            private cStoreDc _cArrowDc = new cStoreDc();
            private cStoreDc _cThumbDc = new cStoreDc();
            private cStoreDc _cTrackDc = new cStoreDc();
            private Bitmap _oArrowBitmap;
            private Bitmap _oThumbBitmap;
            private Bitmap _oTrackBitmap;
            private Bitmap _oMask;
            #endregion

            #region Constructor
            public cScrollBar(IntPtr hWnd, Orientation orientation, Bitmap thumb, Bitmap track, Bitmap arrow, Bitmap fader)
            {
                if (hWnd == IntPtr.Zero)
                    throw new Exception("The scrollbar handle is invalid.");
                ArrowGraphic = arrow;
                ThumbGraphic = thumb;
                TrackGraphic = track;
                _hScrollBarWnd = hWnd;
                Direction = orientation;
                scrollbarMetrics();
                if (Environment.OSVersion.Version.Major > 5)
                {
                    if (IsAppThemed())
                        SetWindowTheme(_hScrollBarWnd, "", "");
                }
                createScrollBarMask();
                ScrollBar sc = (ScrollBar)Control.FromHandle(_hScrollBarWnd);
                sc.Scroll += new ScrollEventHandler(sc_Scroll);
                sc.MouseEnter += new EventHandler(sc_MouseEnter);
                Control ct = Control.FromHandle(GetParent(_hScrollBarWnd));
                ct.Paint += new PaintEventHandler(ct_Paint);
                this.AssignHandle(hWnd);
                if (fader != null)
                    TransitionGraphic = fader;
            }

            void sc_MouseEnter(object sender, EventArgs e)
            {
                scrollFader();
            }

            public void Dispose()
            {
                try
                {
                    this.ReleaseHandle();
                    if (_oArrowBitmap != null) _oArrowBitmap.Dispose();
                    if (_cArrowDc != null) _cArrowDc.Dispose();
                    if (_oThumbBitmap != null) _oThumbBitmap.Dispose();
                    if (_cThumbDc != null) _cThumbDc.Dispose();
                    if (_oTrackBitmap != null) _oTrackBitmap.Dispose();
                    if (_cTrackDc != null) _cTrackDc.Dispose();
                    if (_hMaskWnd != IntPtr.Zero) DestroyWindow(_hMaskWnd);
                    //if (_oMask != null) _oMask.Dispose();
                }
                catch { }
                GC.SuppressFinalize(this);
            }
            #endregion

            #region Events
            private void ct_Paint(object sender, PaintEventArgs e)
            {
                invalidateWindow(false);
            }

            private void sc_Scroll(object sender, ScrollEventArgs e)
            {
                invalidateWindow(false);
            }
            #endregion

            #region Properties
            private Bitmap ArrowGraphic
            {
                get { return _oArrowBitmap; }
                set
                {
                    _oArrowBitmap = value;
                    if (_cArrowDc.Hdc != IntPtr.Zero)
                    {
                        _cArrowDc.Dispose();
                        _cArrowDc = new cStoreDc();
                    }
                    _cArrowDc.Width = _oArrowBitmap.Width;
                    _cArrowDc.Height = _oArrowBitmap.Height;
                    SelectObject(_cArrowDc.Hdc, _oArrowBitmap.GetHbitmap());

                }
            }

            private Bitmap ThumbGraphic
            {
                get { return _oThumbBitmap; }
                set
                {
                    _oThumbBitmap = value;
                    if (_cThumbDc.Hdc != IntPtr.Zero)
                    {
                        _cThumbDc.Dispose();
                        _cThumbDc = new cStoreDc();
                    }
                    _cThumbDc.Width = _oThumbBitmap.Width;
                    _cThumbDc.Height = _oThumbBitmap.Height;
                    SelectObject(_cThumbDc.Hdc, _oThumbBitmap.GetHbitmap());

                }
            }

            private Bitmap TrackGraphic
            {
                get { return _oTrackBitmap; }
                set
                {
                    _oTrackBitmap = value;
                    if (_cTrackDc.Hdc != IntPtr.Zero)
                    {
                        _cTrackDc.Dispose();
                        _cTrackDc = new cStoreDc();
                    }
                    _cTrackDc.Width = _oTrackBitmap.Width;
                    _cTrackDc.Height = _oTrackBitmap.Height;
                    SelectObject(_cTrackDc.Hdc, _oTrackBitmap.GetHbitmap());

                }
            }

            private Bitmap TransitionGraphic
            {
                get { return _oMask; }
                set { _oMask = value; }
            }

            private Orientation Direction
            {
                get { return _eScrollbarOrientation; }
                set { _eScrollbarOrientation = value; }
            }

            private int HScrollPos
            {
                get { return GetScrollPos((IntPtr)this.Handle, SB_HORZ); }
                set { SetScrollPos((IntPtr)this.Handle, SB_HORZ, value, true); }
            }

            private int VScrollPos
            {
                get { return GetScrollPos((IntPtr)this.Handle, SB_VERT); }
                set { SetScrollPos((IntPtr)this.Handle, SB_VERT, value, true); }
            }
            #endregion

            #region Methods
            private void checkBarState()
            {
                if ((GetWindowLong(_hScrollBarWnd, GWL_STYLE) & WS_VISIBLE) == WS_VISIBLE)
                    ShowWindow(_hMaskWnd, SW_NORMAL);
                else
                    ShowWindow(_hMaskWnd, SW_HIDE);
            }

            private void createScrollBarMask()
            {
                Type t = typeof(cScrollBar);
                Module m = t.Module;
                IntPtr hInstance = Marshal.GetHINSTANCE(m);
                IntPtr hParent = GetParent(_hScrollBarWnd);
                RECT tr = new RECT();
                Point pt = new Point();

                GetWindowRect(_hScrollBarWnd, ref tr);
                pt.X = tr.Left;
                pt.Y = tr.Top;
                ScreenToClient(hParent, ref pt);

                _hMaskWnd = CreateWindowEx(WS_EX_TOPMOST | WS_EX_TOOLWINDOW,
                    "STATIC", "",
                    SS_OWNERDRAW | WS_CHILD | WS_CLIPSIBLINGS | WS_OVERLAPPED | WS_VISIBLE,
                    pt.X, pt.Y,
                    (tr.Right - tr.Left), (tr.Bottom - tr.Top),
                    hParent,
                    IntPtr.Zero, hInstance, IntPtr.Zero);

                // set z-order
                SetWindowPos(_hMaskWnd, HWND_TOP,
                    0, 0,
                    0, 0,
                    SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE | SWP_NOOWNERZORDER);
            }

            public void drawScrollBar()
            {
                SCROLLBARINFO sbi = new SCROLLBARINFO();
                RECT tr = new RECT();
                cStoreDc tempDc = new cStoreDc();
                int offset = 0;
                int width = 0;
                int section = 0;

                GetWindowRect(_hScrollBarWnd, ref tr);
                OffsetRect(ref tr, -tr.Left, -tr.Top);
                tempDc.Width = tr.Right;
                tempDc.Height = tr.Bottom;
                SB_HITEST hitTest = scrollbarHitTest();

                sbi.cbSize = Marshal.SizeOf(sbi);
                SendMessage(_hScrollBarWnd, SBM_GETSCROLLBARINFO, 0, ref sbi);

                if (Direction == Orientation.Horizontal)
                {
                    // draw the track
                    using (StretchImage si = new StretchImage(_cTrackDc.Hdc, tempDc.Hdc, new Rectangle(0, 0, _cTrackDc.Width, _cTrackDc.Height), new Rectangle(_iArrowCx, 0, tr.Right - (2 * _iArrowCx), tr.Bottom), 2, StretchModeEnum.STRETCH_HALFTONE)) { }
                    // draw the arrows
                    section = 7;
                    width = _cArrowDc.Width / section;
                    // left arrow
                    if (hitTest == SB_HITEST.leftArrow)
                    {
                        if (leftKeyPressed())
                            offset = 2;
                        else
                            offset = 1;
                    }
                    else
                    {
                        offset = 0;
                    }
                    using (StretchImage si = new StretchImage(_cArrowDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cArrowDc.Height), new Rectangle(0, 0, _iArrowCx, tr.Bottom), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                    // right arrow
                    if (hitTest == SB_HITEST.rightArrow)
                    {
                        if (leftKeyPressed())
                            offset = 5;
                        else
                            offset = 4;
                    }
                    else
                    {
                        offset = 3;
                    }
                    using (StretchImage si = new StretchImage(_cArrowDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cArrowDc.Height), new Rectangle(tr.Right - _iArrowCx, 0, _iArrowCx, tr.Bottom), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                    // draw the thumb
                    section = 3;
                    width = _cThumbDc.Width / section;
                    if (hitTest == SB_HITEST.button)
                    {
                        if (leftKeyPressed())
                            offset = 2;
                        else
                            offset = 1;
                    }
                    else
                    {
                        offset = 0;
                    }
                    Point pst = getScrollBarThumb();
                    using (StretchImage si = new StretchImage(_cThumbDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cThumbDc.Height), new Rectangle(pst.X, 0, pst.Y - pst.X, tr.Bottom), 2, StretchModeEnum.STRETCH_HALFTONE)) { }
                }
                else
                {
                    // draw the track
                    using (StretchImage si = new StretchImage(_cTrackDc.Hdc, tempDc.Hdc, new Rectangle(0, 0, _cTrackDc.Width, _cTrackDc.Height), new Rectangle(0, _iArrowCy, tr.Right, tr.Bottom - (2 * _iArrowCy)), 2, StretchModeEnum.STRETCH_HALFTONE)) { }
                    section = 6;
                    width = _cArrowDc.Width / section;

                    // top arrow
                    if (hitTest == SB_HITEST.topArrow)
                    {
                        if (leftKeyPressed())
                            offset = 2;
                        else
                            offset = 1;
                    }
                    else
                    {
                        offset = 0;
                    }
                    using (StretchImage si = new StretchImage(_cArrowDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cArrowDc.Height), new Rectangle(0, 0, tr.Right, _iArrowCy), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                    // bottom arrow
                    if (hitTest == SB_HITEST.bottomArrow)
                    {
                        if (leftKeyPressed())
                            offset = 5;
                        else
                            offset = 4;
                    }
                    else
                    {
                        offset = 3;
                    }
                    using (StretchImage si = new StretchImage(_cArrowDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cArrowDc.Height), new Rectangle(0, tr.Bottom - _iArrowCy, tr.Right, _iArrowCy), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                    // draw the thumb
                    section = 3;
                    width = _cThumbDc.Width / section;
                    if (hitTest == SB_HITEST.button)
                    {
                        if (leftKeyPressed())
                            offset = 2;
                        else
                            offset = 1;
                    }
                    else
                    {
                        offset = 0;
                    }
                    Point pst = getScrollBarThumb();
                    using (StretchImage si = new StretchImage(_cThumbDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cThumbDc.Height), new Rectangle(0, pst.X, _iArrowCx, pst.Y - pst.X), 2, StretchModeEnum.STRETCH_HALFTONE)) { }
                }
                IntPtr hdc = GetDC(_hMaskWnd);
                BitBlt(hdc, 0, 0, tr.Right, tr.Bottom, tempDc.Hdc, 0, 0, 0xCC0020);
                ReleaseDC(_hMaskWnd, hdc);
                tempDc.Dispose();
            }

            private RECT focusedPartSize()
            {
                RECT tr = new RECT();
                GetWindowRect(_hScrollBarWnd, ref tr);
                OffsetRect(ref tr, -tr.Left, -tr.Top);

                switch (scrollbarHitTest())
                {
                    case SB_HITEST.leftArrow:
                        tr = new RECT(0, 0, _iArrowCx, tr.Bottom);
                        break;

                    case SB_HITEST.rightArrow:
                        tr = new RECT(tr.Right - _iArrowCx, 0, tr.Right, tr.Bottom);
                        break;

                    case SB_HITEST.topArrow:
                        tr = new RECT(0, 0, tr.Right, _iArrowCy);
                        break;

                    case SB_HITEST.bottomArrow:
                        tr = new RECT(0, tr.Bottom - _iArrowCy, tr.Right, _iArrowCy);
                        break;

                    case SB_HITEST.button:
                        Point pst = getScrollBarThumb();
                        if (Direction == Orientation.Horizontal)
                            tr = new RECT(pst.X, 2, pst.Y - pst.X, tr.Bottom);
                        else
                            tr = new RECT(0, pst.X, _iArrowCx, pst.Y - pst.X);
                        break;

                }
                return tr;
            }

            private RECT getScrollBarRect()
            {
                SCROLLBARINFO sbi = new SCROLLBARINFO();
                sbi.cbSize = Marshal.SizeOf(sbi);
                SendMessage(_hScrollBarWnd, SBM_GETSCROLLBARINFO, 0, ref sbi);
                return sbi.rcScrollBar;
            }

            public void sizeCheck()
            {
                RECT tr = new RECT();
                RECT tw = new RECT();
                GetWindowRect(_hMaskWnd, ref tw);
                GetWindowRect(_hScrollBarWnd, ref tr);
                if (!EqualRect(ref tr, ref tw))
                    SetWindowPos(_hMaskWnd, IntPtr.Zero, tr.Left, tr.Top, tr.Right - tr.Left, tr.Bottom - tr.Top, SWP_NOACTIVATE | SWP_NOOWNERZORDER | SWP_NOZORDER);
            }

            private Point getScrollBarThumb()
            {
                Point pt = new Point();
                ScrollBar sc = (ScrollBar)Control.FromHandle(_hScrollBarWnd);
                float incr = 0;

                if (sc.ClientRectangle.Width > sc.ClientRectangle.Height)
                {
                    incr = ((float)(sc.ClientRectangle.Width - (_iArrowCx * 2)) / sc.Maximum);
                    pt.X = (int)(incr * sc.Value) + _iArrowCx;
                    pt.Y = pt.X + (int)(incr * sc.LargeChange);
                }
                else
                {
                    incr = ((float)(sc.ClientRectangle.Height - (_iArrowCy * 2)) / sc.Maximum);
                    pt.X = (int)(incr * sc.Value) + _iArrowCy;
                    pt.Y = pt.X + (int)(incr * sc.LargeChange);
                }
                // fix for ?bug? in scrollbar
                if (sc.Value < sc.Maximum)
                {
                    sc.Value++;
                    sc.Value--;
                }
                else
                {
                    sc.Value--;
                    sc.Value++;
                }
                invalidateWindow(true);
                return pt;
            }

            private void invalidateWindow(bool messaged)
            {
                if (messaged)
                    RedrawWindow(_hScrollBarWnd, IntPtr.Zero, IntPtr.Zero, RDW_INTERNALPAINT);
                else
                    RedrawWindow(_hScrollBarWnd, IntPtr.Zero, IntPtr.Zero, RDW_INVALIDATE | RDW_UPDATENOW);
            }

            private bool leftKeyPressed()
            {
                if (mouseButtonsSwitched())
                    return (GetKeyState(VK_RBUTTON) < 0);
                else
                    return (GetKeyState(VK_LBUTTON) < 0);
            }

            private bool mouseButtonsSwitched()
            {
                return (GetSystemMetrics(SYSTEM_METRICS.SM_SWAPBUTTON) != 0);
            }

            private SB_HITEST scrollbarHitTest()
            {
                Point pt = new Point();
                RECT tr = new RECT();

                GetWindowRect(_hScrollBarWnd, ref tr);
                OffsetRect(ref tr, -tr.Left, -tr.Top);

                RECT tp = tr;
                GetCursorPos(ref pt);
                ScreenToClient(_hScrollBarWnd, ref pt);

                if (Direction == Orientation.Horizontal)
                {
                    if (PtInRect(ref tr, pt))
                    {
                        // left arrow
                        tp.Right = _iArrowCx;
                        if (PtInRect(ref tp, pt))
                            return SB_HITEST.leftArrow;
                        // right arrow
                        tp.Left = tr.Right - _iArrowCx;
                        tp.Right = tr.Right;
                        if (PtInRect(ref tp, pt))
                            return SB_HITEST.rightArrow;
                        // button
                        Point pb = getScrollBarThumb();
                        tp.Left = pb.X;
                        tp.Right = pb.Y;
                        if (PtInRect(ref tp, pt))
                            return SB_HITEST.button;
                        // track
                        return SB_HITEST.track;
                    }
                }
                else
                {
                    if (PtInRect(ref tr, pt))
                    {
                        // top arrow
                        tp.Bottom = _iArrowCy;
                        if (PtInRect(ref tp, pt))
                            return SB_HITEST.topArrow;
                        // bottom arrow
                        tp.Top = tr.Bottom - _iArrowCy;
                        tp.Bottom = tr.Bottom;
                        if (PtInRect(ref tp, pt))
                            return SB_HITEST.bottomArrow;
                        // button
                        Point pb = getScrollBarThumb();
                        tp.Top = pb.X;
                        tp.Bottom = pb.Y;
                        if (PtInRect(ref tp, pt))
                            return SB_HITEST.button;
                        // track
                        return SB_HITEST.track;
                    }
                }
                return SB_HITEST.offControl;
            }

            private void scrollbarMetrics()
            {
                if (Direction == Orientation.Horizontal)
                {
                    _iArrowCx = GetSystemMetrics(SYSTEM_METRICS.SM_CXHSCROLL);
                    _iArrowCy = GetSystemMetrics(SYSTEM_METRICS.SM_CYHSCROLL);
                }
                else
                {
                    _iArrowCx = GetSystemMetrics(SYSTEM_METRICS.SM_CXVSCROLL);
                    _iArrowCy = GetSystemMetrics(SYSTEM_METRICS.SM_CYVSCROLL);
                }
            }

            private void scrollHorizontal(bool Right)
            {
                if (Right)
                    SendMessage(_hScrollBarWnd, WM_HSCROLL, SB_LINERIGHT, 0);
                else
                    SendMessage(_hScrollBarWnd, WM_HSCROLL, SB_LINELEFT, 0);

            }

            private void scrollVertical(bool Down)
            {
                if (Down)
                    SendMessage(_hScrollBarWnd, WM_VSCROLL, SB_LINEDOWN, 0);
                else
                    SendMessage(_hScrollBarWnd, WM_VSCROLL, SB_LINEUP, 0);
            }

            private void scrollFader()
            {

                SB_HITEST hitTest = scrollbarHitTest();
                if (hitTest == SB_HITEST.button)
                {
                    Point pst = getScrollBarThumb();
                    if (TransitionGraphic != null)
                    {
                        cTransition ts;
                        RECT tr = new RECT();
                        if (Direction == Orientation.Horizontal)
                        {
                            GetWindowRect(_hScrollBarWnd, ref tr);
                            ts = new cTransition(_hMaskWnd, _hScrollBarWnd, TransitionGraphic, new Rectangle(pst.X, 0, pst.Y - pst.X, tr.Bottom));
                        }
                        else
                        {
                            ts = new cTransition(_hMaskWnd, _hScrollBarWnd, TransitionGraphic, new Rectangle(0, pst.X, _iArrowCx, pst.Y - pst.X));
                        }
                    }
                }
            }
            #endregion

            #region WndProc
            protected override void WndProc(ref Message m)
            {
                PAINTSTRUCT ps = new PAINTSTRUCT();

                switch (m.Msg)
                {

                    case 0x83:
                    case WM_PAINT:
                        if (!_bPainting)
                        {
                            _bPainting = true;
                            // start painting engine
                            BeginPaint(m.HWnd, ref ps);
                            drawScrollBar();
                            ValidateRect(m.HWnd, ref ps.rcPaint);
                            // done
                            EndPaint(m.HWnd, ref ps);
                            _bPainting = false;
                            m.Result = MSG_HANDLED;
                        }
                        else
                        {
                            base.WndProc(ref m);
                        }
                        break;

                    case WM_SIZE:
                    case WM_MOVE:
                        sizeCheck();
                        base.WndProc(ref m);
                        break;

                    default:
                        base.WndProc(ref m);
                        break;
                }
            }
            #endregion
        }










    }
















    #region StoreDc
    [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust")]
    public class cStoreDc
    {
        [DllImport("gdi32.dll")]
        private static extern IntPtr CreateDCA([MarshalAs(UnmanagedType.LPStr)]string lpszDriver, [MarshalAs(UnmanagedType.LPStr)]string lpszDevice, [MarshalAs(UnmanagedType.LPStr)]string lpszOutput, int lpInitData);

        [DllImport("gdi32.dll")]
        private static extern IntPtr CreateDCW([MarshalAs(UnmanagedType.LPWStr)]string lpszDriver, [MarshalAs(UnmanagedType.LPWStr)]string lpszDevice, [MarshalAs(UnmanagedType.LPWStr)]string lpszOutput, int lpInitData);

        [DllImport("gdi32.dll")]
        private static extern IntPtr CreateDC(string lpszDriver, string lpszDevice, string lpszOutput, int lpInitData);

        [DllImport("gdi32.dll")]
        private static extern IntPtr CreateCompatibleDC(IntPtr hdc);

        [DllImport("gdi32.dll")]
        private static extern IntPtr CreateCompatibleBitmap(IntPtr hdc, int nWidth, int nHeight);

        [DllImport("gdi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool DeleteDC(IntPtr hdc);

        [DllImport("gdi32.dll", PreserveSig = true)]
        private static extern IntPtr SelectObject(IntPtr hdc, IntPtr hgdiobj);

        [DllImport("gdi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool DeleteObject(IntPtr hObject);

        private int _iHeight = 0;
        private int _iWidth = 0;
        private IntPtr _pHdc = IntPtr.Zero;
        private IntPtr _pBmp = IntPtr.Zero;
        private IntPtr _pBmpOld = IntPtr.Zero;

        public IntPtr Hdc
        {
            get { return _pHdc; }
        }

        public IntPtr HBmp
        {
            get { return _pBmp; }
        }

        public int Height
        {
            get { return _iHeight; }
            set
            {
                if (_iHeight != value)
                {
                    _iHeight = value;
                    ImageCreate(_iWidth, _iHeight);
                }
            }
        }

        public int Width
        {
            get { return _iWidth; }
            set
            {
                if (_iWidth != value)
                {
                    _iWidth = value;
                    ImageCreate(_iWidth, _iHeight);
                }
            }
        }

        private void ImageCreate(int width, int height)
        {
            IntPtr pHdc = IntPtr.Zero;

            ImageDestroy();
            pHdc = CreateDCA("DISPLAY", "", "", 0);
            _pHdc = CreateCompatibleDC(pHdc);
            _pBmp = CreateCompatibleBitmap(pHdc, _iWidth, _iHeight);
            _pBmpOld = SelectObject(_pHdc, _pBmp);
            if (_pBmpOld == IntPtr.Zero)
            {
                ImageDestroy();
            }
            else
            {
                _iWidth = width;
                _iHeight = height;
            }
            DeleteDC(pHdc);
            pHdc = IntPtr.Zero;
        }

        private void ImageDestroy()
        {
            if (_pBmpOld != IntPtr.Zero)
            {
                SelectObject(_pHdc, _pBmpOld);
                _pBmpOld = IntPtr.Zero;
            }
            if (_pBmp != IntPtr.Zero)
            {
                DeleteObject(_pBmp);
                _pBmp = IntPtr.Zero;
            }
            if (_pHdc != IntPtr.Zero)
            {
                DeleteDC(_pHdc);
                _pHdc = IntPtr.Zero;
            }
        }

        public void Dispose()
        {
            ImageDestroy();
        }
    }
    #endregion























    }



   
   
}
