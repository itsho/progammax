﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ProGammaX
{
    public class myPanel : Panel
    {
        public myPanel()
        {
            
            

        }

        //// window messages
        //private const int WM_PAINT = 0xF;
        //private const int WM_NCPAINT = 0x85;
        //private const int WM_NCMOUSEMOVE = 0xA0;
        //private const int WM_MOUSEMOVE = 0x200;
        //private const int WM_MOUSELEAVE = 0x2A3;
        //private const int WM_LBUTTONDOWN = 0x201;
        //private const int WM_LBUTTONUP = 0x202;
        //private const int WM_LBUTTONDBLCLK = 0x203;
        //private const int WM_RBUTTONDOWN = 0x204;
        //private const int WM_RBUTTONUP = 0x205;
        //private const int WM_RBUTTONDBLCLK = 0x206;
        //private const int WM_MBUTTONDOWN = 0x207;
        //private const int WM_MBUTTONUP = 0x208;
        //private const int WM_MBUTTONDBLCLK = 0x209;
        //private const int WM_MOUSEWHEEL = 0x20A;
        //private const int WM_MOUSEHWHEEL = 0x020E;
        //private const int WM_STYLECHANGED = 0x7D;
        //private const int WM_SIZE = 0x5;
        //private const int WM_MOVE = 0x3;
        //private const int WM_VSCROLL = 0x0115;
        //private const int WM_HSCROLL = 0x0114;




        //internal cInternalScrollBar scrollBar;

        //protected override void OnHandleCreated(EventArgs e)
        //{


        //    base.OnHandleCreated(e);

        //    try
        //    {
        //        scrollBar = new cInternalScrollBar(this.Handle, MainForm.mainFormRef.vienna_ScrollHorzShaft, MainForm.mainFormRef.vienna_ScrollHorzArrow, MainForm.mainFormRef.vienna_ScrollHorzThumb, MainForm.mainFormRef.vienna_ScrollVertShaft, MainForm.mainFormRef.vienna_ScrollVertArrow, MainForm.mainFormRef.vienna_ScrollVertThumb, MainForm.mainFormRef.vienna_ScrollHorzShaft);

        //    }
        //    catch
        //    {

        //    }

        //}




        //protected override void OnHandleDestroyed(EventArgs e)
        //{
        //    try
        //    {
        //        scrollBar.Dispose();
        //    }
        //    catch
        //    {

        //    }

        //    base.OnHandleDestroyed(e);
        //}



        //protected override void OnVisibleChanged(EventArgs e)
        //{
        //    base.OnVisibleChanged(e);
        //    try
        //    {

        //        if (this.Visible)
        //        {
        //            scrollBar = new cInternalScrollBar(this.Handle, MainForm.mainFormRef.vienna_ScrollHorzShaft, MainForm.mainFormRef.vienna_ScrollHorzArrow, MainForm.mainFormRef.vienna_ScrollHorzThumb, MainForm.mainFormRef.vienna_ScrollVertShaft, MainForm.mainFormRef.vienna_ScrollVertArrow, MainForm.mainFormRef.vienna_ScrollVertThumb, MainForm.mainFormRef.vienna_ScrollHorzShaft);

        //        }
        //        else
        //        {
        //            scrollBar = null;

        //        }


        //        if (this.Visible)
        //        {
        //            this.Parent.Refresh();
        //        }


        //    }
        //    catch
        //    {


        //    }


        //}




        //protected override void WndProc(ref Message m)
        //{




        //    if (scrollBar != null && this.Visible)
        //    {




        //        switch (m.Msg)
        //        {




        //            //case 0x0083:
        //            //    //case 0x000F: //WM_PAINT
        //            //    scrollBar.checkBarState();

        //            //    base.WndProc(ref m);
        //            //    break;
        //            case WM_NCPAINT:


        //                scrollBar.drawScrollBar();
        //                //base.WndProc(ref m);

        //                break;

        //            case WM_MOUSEHWHEEL:  // 0x020E
        //            case WM_HSCROLL:
        //                //scrollControl(m.WParam, eScrollDirection.Vertical, eScrollAction.Relative, 5);
        //                scrollBar._bTrackingMouse = true;
        //                scrollBar.drawScrollBar();
        //                scrollBar.scrollFader();

        //                //base.WndProc(ref m);
        //                break;
        //            case WM_MOUSEWHEEL:   // 0x020A
        //            case WM_VSCROLL:
        //                //scrollControl(m.WParam, eScrollDirection.Vertical, eScrollAction.Relative, -5);

        //                scrollBar._bTrackingMouse = true;
        //                scrollBar.drawScrollBar();
        //                scrollBar.scrollFader();
        //                //base.WndProc(ref m);
        //                break;
        //            case WM_NCMOUSEMOVE:
        //                scrollBar._bTrackingMouse = true;
        //                scrollBar.drawScrollBar();
        //                scrollBar.scrollFader();
        //                base.WndProc(ref m);
        //                break;

        //            case WM_MOUSEMOVE:
        //                if (scrollBar._bTrackingMouse)
        //                    scrollBar._bTrackingMouse = false;
        //                //base.WndProc(ref m);
        //                break;


        //            case WM_SIZE:
        //            case WM_MOVE:
        //                scrollBar.reSizeMask();
        //                //base.WndProc(ref m);
        //                break;

        //            default:
        //                scrollBar.checkBarState();
        //                //base.WndProc(ref m);
        //                break;
        //        }






        //    }

        //    base.WndProc(ref m);
        //    //System.Threading.Thread.Sleep(1);

        //}


















        [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust")]
        private class cPanel : IDisposable
        {
            #region Fields
            private IntPtr _hPanelWnd = IntPtr.Zero;
            private cInternalScrollBar _cInternalScroll;
            #endregion

            #region Constructor
            public cPanel(IntPtr handle, Bitmap hztrack, Bitmap hzarrow, Bitmap hzthumb, Bitmap vttrack, Bitmap vtarrow, Bitmap vtthumb, Bitmap fader)
            {
                if (handle == IntPtr.Zero)
                    throw new Exception("The Panel handle is invalid.");
                _hPanelWnd = handle;
                if (hztrack != null && hzarrow != null && hzthumb != null && vttrack != null && vtarrow != null && vtthumb != null)
                    _cInternalScroll = new cInternalScrollBar(_hPanelWnd, hztrack, hzarrow, hzthumb, vttrack, vtarrow, vtthumb, fader);
                else
                    throw new Exception("The Panel image(s) are invalid");
            }

            public void Dispose()
            {
                try
                {
                    if (_cInternalScroll != null) _cInternalScroll.Dispose();
                }
                catch { }
            }
            #endregion
        }



















        private Bitmap _oTransitionMask;



        public Bitmap TransitionGraphic
        {
            get { return _oTransitionMask; }
            set { _oTransitionMask = value; }
        }











        //fixes incorrect anchor property
        //protected override void OnSizeChanged(EventArgs e)
        //{

        //    if (this.Handle != null)
        //    {

        //        this.BeginInvoke((MethodInvoker)delegate
        //        {

        //            base.OnSizeChanged(e);

        //        });

        //    }

        //} 











        protected override void OnVisibleChanged(EventArgs e)
        {
            base.OnVisibleChanged(e);
            try
            {



                if (this.FindForm().Visible)
                {

                    start();
                }
                else
                {
                    //
                }

                if (this.Visible)
                {
                    this.Refresh();
                }


            }
            catch
            {


            }


        }

        private void start()
        {



            try
            {
                if (!MainForm.mainFormRef.checkBox_OPTIONS_Themes_UseSkinOnControls.Checked)
                {
                    return;

                }
            }
            catch
            {
                return;
            }



            try
            {
                if (this.Visible)
                {

                    TransitionGraphic = null;

                    //Add(ControlType ct, Bitmap hztrack, Bitmap hzarrow, Bitmap hzthumb, Bitmap vttrack, Bitmap vtarrow, Bitmap vtthumb)
                    new cPanel(this.Handle, ProGammaX.MainForm.mainFormRef.vienna_ScrollHorzShaft, ProGammaX.MainForm.mainFormRef.vienna_ScrollHorzArrow, ProGammaX.MainForm.mainFormRef.vienna_ScrollHorzThumb, ProGammaX.MainForm.mainFormRef.vienna_ScrollVertShaft, ProGammaX.MainForm.mainFormRef.vienna_ScrollVertArrow, ProGammaX.MainForm.mainFormRef.vienna_ScrollVertThumb, TransitionGraphic);

                }
            }
            catch
            {

            }

        }


































        internal class cInternalScrollBar : NativeWindow, IDisposable
        {
            #region Constants
            // style
            private const int GWL_STYLE = (-16);
            private const int GWL_EXSTYLE = (-20);
            private const int WS_EX_TOPMOST = 0x8;
            private const int WS_EX_TOOLWINDOW = 0x80;
            private const int WS_CHILD = 0x40000000;
            private const int WS_OVERLAPPED = 0x0;
            private const int WS_CLIPSIBLINGS = 0x4000000;
            private const int WS_VISIBLE = 0x10000000;
            private const int WS_HSCROLL = 0x100000;
            private const int WS_VSCROLL = 0x200000;
            private const int SS_OWNERDRAW = 0xD;
            // showwindow
            private const int SW_HIDE = 0x0;
            private const int SW_NORMAL = 0x1;
            // size/move
            private const uint SWP_NOSIZE = 0x0001;
            private const uint SWP_NOMOVE = 0x0002;
            private const uint SWP_NOZORDER = 0x0004;
            private const uint SWP_NOREDRAW = 0x0008;
            private const uint SWP_NOACTIVATE = 0x0010;
            private const uint SWP_FRAMECHANGED = 0x0020;
            private const uint SWP_SHOWWINDOW = 0x0040;
            private const uint SWP_HIDEWINDOW = 0x0080;
            private const uint SWP_NOCOPYBITS = 0x0100;
            private const uint SWP_NOOWNERZORDER = 0x0200;
            private const uint SWP_NOSENDCHANGING = 0x0400;
            // setwindowpos
            static readonly IntPtr HWND_NOTOPMOST = new IntPtr(-2);
            static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
            static readonly IntPtr HWND_TOP = new IntPtr(0);
            static readonly IntPtr HWND_BOTTOM = new IntPtr(1);
            // scroll messages
            private const int WM_HSCROLL = 0x114;
            private const int WM_VSCROLL = 0x115;
            private const int SB_LINEUP = 0;
            private const int SB_LINEDOWN = 1;
            private const int SB_LINELEFT = 0;
            private const int SB_LINERIGHT = 1;
            private const int SB_PAGEUP = 2;
            private const int SB_PAGEDOWN = 3;
            private const int SB_PAGELEFT = 2;
            private const int SB_PAGERIGHT = 3;
            // mouse buttons
            private const int VK_LBUTTON = 0x1;
            private const int VK_RBUTTON = 0x2;
            // redraw
            private const int RDW_INVALIDATE = 0x0001;
            private const int RDW_INTERNALPAINT = 0x0002;
            private const int RDW_ERASE = 0x0004;
            private const int RDW_VALIDATE = 0x0008;
            private const int RDW_NOINTERNALPAINT = 0x0010;
            private const int RDW_NOERASE = 0x0020;
            private const int RDW_NOCHILDREN = 0x0040;
            private const int RDW_ALLCHILDREN = 0x0080;
            private const int RDW_UPDATENOW = 0x0100;
            private const int RDW_ERASENOW = 0x0200;
            private const int RDW_FRAME = 0x0400;
            private const int RDW_NOFRAME = 0x0800;
            // scroll bar messages
            private const int SB_HORZ = 0x0;
            private const int SB_VERT = 0x1;
            private const int SBM_SETPOS = 0x00E0;
            private const int SBM_GETPOS = 0x00E1;
            private const int SBM_SETRANGE = 0x00E2;
            private const int SBM_SETRANGEREDRAW = 0x00E6;
            private const int SBM_GETRANGE = 0x00E3;
            private const int SBM_ENABLE_ARROWS = 0x00E4;
            private const int SBM_SETSCROLLINFO = 0x00E9;
            private const int SBM_GETSCROLLINFO = 0x00EA;
            private const int SBM_GETSCROLLBARINFO = 0x00EB;
            private const int SIF_RANGE = 0x0001;
            private const int SIF_PAGE = 0x0002;
            private const int SIF_POS = 0x0004;
            private const int SIF_DISABLENOSCROLL = 0x0008;
            private const int SIF_TRACKPOS = 0x0010;
            private const int SIF_ALL = (SIF_RANGE | SIF_PAGE | SIF_POS | SIF_TRACKPOS);
            // scrollbar states
            private const int STATE_SYSTEM_INVISIBLE = 0x00008000;
            private const int STATE_SYSTEM_OFFSCREEN = 0x00010000;
            private const int STATE_SYSTEM_PRESSED = 0x00000008;
            private const int STATE_SYSTEM_UNAVAILABLE = 0x00000001;
            private const uint OBJID_HSCROLL = 0xFFFFFFFA;
            private const uint OBJID_VSCROLL = 0xFFFFFFFB;
            private const uint OBJID_CLIENT = 0xFFFFFFFC;
            // window messages
            private const int WM_PAINT = 0xF;
            private const int WM_NCPAINT = 0x85;
            private const int WM_NCMOUSEMOVE = 0xA0;
            private const int WM_MOUSEMOVE = 0x200;
            private const int WM_MOUSELEAVE = 0x2A3;
            private const int WM_LBUTTONDOWN = 0x201;
            private const int WM_LBUTTONUP = 0x202;
            private const int WM_LBUTTONDBLCLK = 0x203;
            private const int WM_RBUTTONDOWN = 0x204;
            private const int WM_RBUTTONUP = 0x205;
            private const int WM_RBUTTONDBLCLK = 0x206;
            private const int WM_MBUTTONDOWN = 0x207;
            private const int WM_MBUTTONUP = 0x208;
            private const int WM_MBUTTONDBLCLK = 0x209;
            private const int WM_MOUSEWHEEL = 0x20A;
            private const int WM_MOUSEHWHEEL = 0x020E;
            private const int WM_STYLECHANGED = 0x7D;
            private const int WM_SIZE = 0x5;
            private const int WM_MOVE = 0x3;
            // message handler
            private static IntPtr MSG_HANDLED = new IntPtr(1);
            #endregion

            #region Enums
            private enum SB_HITEST : int
            {
                offControl = 0,
                topArrow,
                bottomArrow,
                leftArrow,
                rightArrow,
                button,
                track
            }

            private enum SYSTEM_METRICS : int
            {
                SM_CXSCREEN = 0,
                SM_CYSCREEN = 1,
                SM_CXVSCROLL = 2,
                SM_CYHSCROLL = 3,
                SM_CYCAPTION = 4,
                SM_CXBORDER = 5,
                SM_CYBORDER = 6,
                SM_CYVTHUMB = 9,
                SM_CXHTHUMB = 10,
                SM_CXICON = 11,
                SM_CYICON = 12,
                SM_CXCURSOR = 13,
                SM_CYCURSOR = 14,
                SM_CYMENU = 15,
                SM_CXFULLSCREEN = 16,
                SM_CYFULLSCREEN = 17,
                SM_CYKANJIWINDOW = 18,
                SM_MOUSEPRESENT = 19,
                SM_CYVSCROLL = 20,
                SM_CXHSCROLL = 21,
                SM_SWAPBUTTON = 23,
                SM_CXMIN = 28,
                SM_CYMIN = 29,
                SM_CXSIZE = 30,
                SM_CYSIZE = 31,
                SM_CXFRAME = 32,
                SM_CYFRAME = 33,
                SM_CXMINTRACK = 34,
                SM_CYMINTRACK = 35,
                SM_CYSMCAPTION = 51,
                SM_CXMINIMIZED = 57,
                SM_CYMINIMIZED = 58,
                SM_CXMAXTRACK = 59,
                SM_CYMAXTRACK = 60,
                SM_CXMAXIMIZED = 61,
                SM_CYMAXIMIZED = 62
            }
            #endregion

            #region Structs
            [StructLayout(LayoutKind.Sequential)]
            private struct PAINTSTRUCT
            {
                internal IntPtr hdc;
                internal int fErase;
                internal RECT rcPaint;
                internal int fRestore;
                internal int fIncUpdate;
                internal int Reserved1;
                internal int Reserved2;
                internal int Reserved3;
                internal int Reserved4;
                internal int Reserved5;
                internal int Reserved6;
                internal int Reserved7;
                internal int Reserved8;
            }

            [StructLayout(LayoutKind.Sequential)]
            private struct RECT
            {
                internal RECT(int X, int Y, int Width, int Height)
                {
                    this.Left = X;
                    this.Top = Y;
                    this.Right = Width;
                    this.Bottom = Height;
                }
                internal int Left;
                internal int Top;
                internal int Right;
                internal int Bottom;
            }

            [StructLayout(LayoutKind.Sequential)]
            private struct SCROLLINFO
            {
                internal uint cbSize;
                internal uint fMask;
                internal int nMin;
                internal int nMax;
                internal uint nPage;
                internal int nPos;
                internal int nTrackPos;
            }

            [StructLayout(LayoutKind.Sequential)]
            private struct SCROLLBARINFO
            {
                internal int cbSize;
                internal RECT rcScrollBar;
                internal int dxyLineButton;
                internal int xyThumbTop;
                internal int xyThumbBottom;
                internal int reserved;
                [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
                internal int[] rgstate;
            }
            #endregion

            #region API
            [DllImport("user32.dll")]
            private static extern IntPtr BeginPaint(IntPtr hWnd, ref PAINTSTRUCT ps);

            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool EndPaint(IntPtr hWnd, ref PAINTSTRUCT ps);

            [DllImport("gdi32.dll")]
            private static extern IntPtr SelectObject(IntPtr hdc, IntPtr hObject);

            [DllImport("gdi32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool BitBlt(IntPtr hdc, int nXDest, int nYDest, int nWidth, int nHeight, IntPtr hdcSrc, int nXSrc, int nYSrc, int dwRop);

            [DllImport("gdi32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool StretchBlt(IntPtr hDest, int X, int Y, int nWidth, int nHeight, IntPtr hdcSrc,
            int sX, int sY, int nWidthSrc, int nHeightSrc, int dwRop);

            [DllImport("user32.dll")]
            private static extern IntPtr GetDC(IntPtr handle);

            [DllImport("user32.dll")]
            private static extern int ReleaseDC(IntPtr handle, IntPtr hdc);

            [DllImport("user32.dll")]
            private static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
            [DllImport("user32.dll")]
            private static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, ref SCROLLBARINFO lParam);

            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool GetWindowRect(IntPtr hWnd, ref RECT lpRect);

            [DllImport("user32.dll")]
            private extern static int OffsetRect(ref RECT lpRect, int x, int y);

            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool ValidateRect(IntPtr hWnd, ref RECT lpRect);

            [DllImport("user32.dll")]
            private static extern int GetSystemMetrics(SYSTEM_METRICS smIndex);

            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool RedrawWindow(IntPtr hWnd, IntPtr lprcUpdate, IntPtr hrgnUpdate, uint flags);

            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool GetCursorPos(ref Point lpPoint);

            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool PtInRect([In] ref RECT lprc, Point pt);

            [DllImport("user32.dll")]
            private static extern int ScreenToClient(IntPtr hwnd, ref Point lpPoint);

            [DllImport("user32.dll")]
            private static extern short GetKeyState(int nVirtKey);

            [DllImport("user32.dll")]
            private static extern int SetScrollPos(IntPtr hWnd, int nBar, int nPos, bool bRedraw);

            [DllImport("user32.dll")]
            private static extern int GetScrollPos(IntPtr hWnd, int nBar);

            [DllImport("user32.dll")]
            private static extern IntPtr GetParent(IntPtr hWnd);

            [DllImport("user32.dll", SetLastError = true)]
            private static extern IntPtr CreateWindowEx(int exstyle, string lpClassName, string lpWindowName, int dwStyle,
                int x, int y, int nWidth, int nHeight, IntPtr hwndParent, IntPtr Menu, IntPtr hInstance, IntPtr lpParam);

            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool DestroyWindow(IntPtr hWnd);

            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndAfter, int x, int y, int cx, int cy, uint flags);


            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool EqualRect([In] ref RECT lprc1, [In] ref RECT lprc2);

            [DllImport("user32.dll")]
            private static extern int GetWindowLong(IntPtr hwnd, int nIndex);

            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

            [DllImport("user32.dll")]
            private static extern int GetScrollBarInfo(IntPtr hWnd, uint idObject, ref SCROLLBARINFO psbi);
            #endregion

            #region Fields
            public bool _bTrackingMouse = false;
            private int _iArrowCx = 0;
            private int _iArrowCy = 0;
            private IntPtr _hVerticalMaskWnd = IntPtr.Zero;
            private IntPtr _hHorizontalMaskWnd = IntPtr.Zero;
            private IntPtr _hSizerMaskWnd = IntPtr.Zero;
            private IntPtr _hControlWnd = IntPtr.Zero;
            private cStoreDc _cHorizontalArrowDc = new cStoreDc();
            private cStoreDc _cHorizontalThumbDc = new cStoreDc();
            private cStoreDc _cHorizontalTrackDc = new cStoreDc();
            private cStoreDc _cVerticalArrowDc = new cStoreDc();
            private cStoreDc _cVerticalThumbDc = new cStoreDc();
            private cStoreDc _cVerticalTrackDc = new cStoreDc();
            private Bitmap _oHorizontalArrowBitmap;
            private Bitmap _oHorizontalThumbBitmap;
            private Bitmap _oHorizontalTrackBitmap;
            private Bitmap _oVerticalArrowBitmap;
            private Bitmap _oVerticalThumbBitmap;
            private Bitmap _oVerticalTrackBitmap;
            private Bitmap _oMask;
            #endregion

            #region Constructor
            public cInternalScrollBar(IntPtr hWnd, Bitmap hztrack, Bitmap hzarrow, Bitmap hzthumb, Bitmap vttrack, Bitmap vtarrow, Bitmap vtthumb, Bitmap fader)
            {
                //if (hWnd == IntPtr.Zero)
                //    throw new Exception("The control handle is invalid.");
                //if (hztrack == null)
                //    throw new Exception("The Horizontal Track image is invalid.");
                //if (hzarrow == null)
                //    throw new Exception("The Horizontal Arrow image is invalid.");
                //if (hzthumb == null)
                //    throw new Exception("The Horizontal Thumb image is invalid.");
                //if (vttrack == null)
                //    throw new Exception("The Vertical Track image is invalid.");
                //if (vtarrow == null)
                //    throw new Exception("The Vertical Arrow image is invalid.");
                //if (vtthumb == null)
                //    throw new Exception("The Vertical Thumb image is invalid.");
                try
                {

                    //bool isHwndVisible = ((Control.FromHandle(hWnd).Visible));// || (Control.FromHandle(hWnd).GetType() == typeof(myPanel)))); //(IsScrollbarVisible(hWnd) && );
                    ////bool hwndContainsProperControls = (Control.FromHandle(hWnd).GetType() == typeof(richTextBox) || Control.FromHandle(hWnd).GetType() == typeof(textBox) || Control.FromHandle(hWnd).GetType() == typeof(listBox) || Control.FromHandle(hWnd).GetType() == typeof(listView) || Control.FromHandle(hWnd).GetType() == typeof(treeView) || Control.FromHandle(hWnd).GetType() == typeof(myPanel) || Control.FromHandle(hWnd).GetType() == typeof(myTabControl));

                    //if (isHwndVisible)
                    //{
                    HorizontalArrowGraphic = hzarrow;
                    HorizontalThumbGraphic = hzthumb;
                    HorizontalTrackGraphic = hztrack;
                    VerticalArrowGraphic = vtarrow;
                    VerticalThumbGraphic = vtthumb;
                    VerticalTrackGraphic = vttrack;

                    // the fader for this class would require
                    // some additional code, with the fader inclass
                    if (fader != null)
                        TransitionGraphic = fader;
                    scrollbarMetrics();
                    _hControlWnd = hWnd;
                    createScrollBarMask();
                    this.AssignHandle(_hControlWnd);
                    //}
                    //else
                    //{


                    //}
                }
                catch
                {

                }
            }
            #endregion

            #region Properties

            //private static bool IsScrollbarVisible(IntPtr hWnd)
            //{
            //    // Determines whether the vertical scrollbar is visible for the specified control
            //    bool bVisible = false;
            //    int nMessage = GWL_STYLE;
            //    int nStyle = GetWindowLong(hWnd, nMessage);
            //    bVisible = ((nStyle & nMessage) != 0);
            //    return bVisible;
            //}



            private Bitmap HorizontalArrowGraphic
            {
                get { return _oHorizontalArrowBitmap; }
                set
                {
                    _oHorizontalArrowBitmap = value;
                    if (_cHorizontalArrowDc.Hdc != IntPtr.Zero)
                    {
                        _cHorizontalArrowDc.Dispose();
                        _cHorizontalArrowDc = new cStoreDc();
                    }
                    _cHorizontalArrowDc.Width = _oHorizontalArrowBitmap.Width;
                    _cHorizontalArrowDc.Height = _oHorizontalArrowBitmap.Height;
                    //SelectObject(_cHorizontalArrowDc.Hdc, _oHorizontalArrowBitmap.GetHbitmap());


                    Thread selectBmp = new Thread(() => SelectObject(_cHorizontalArrowDc.Hdc, _oHorizontalArrowBitmap.GetHbitmap()));
                    selectBmp.Start();
                    selectBmp.Join();

                }
            }

            private Bitmap HorizontalThumbGraphic
            {
                get { return _oHorizontalThumbBitmap; }
                set
                {
                    _oHorizontalThumbBitmap = value;
                    if (_cHorizontalThumbDc.Hdc != IntPtr.Zero)
                    {
                        _cHorizontalThumbDc.Dispose();
                        _cHorizontalThumbDc = new cStoreDc();
                    }
                    _cHorizontalThumbDc.Width = _oHorizontalThumbBitmap.Width;
                    _cHorizontalThumbDc.Height = _oHorizontalThumbBitmap.Height;
                    //SelectObject(_cHorizontalThumbDc.Hdc, _oHorizontalThumbBitmap.GetHbitmap());


                    Thread selectBmp = new Thread(() => SelectObject(_cHorizontalThumbDc.Hdc, _oHorizontalThumbBitmap.GetHbitmap()));
                    selectBmp.Start();
                    selectBmp.Join();

                }
            }

            private Bitmap HorizontalTrackGraphic
            {
                get { return _oHorizontalTrackBitmap; }
                set
                {
                    _oHorizontalTrackBitmap = value;
                    if (_cHorizontalTrackDc.Hdc != IntPtr.Zero)
                    {
                        _cHorizontalTrackDc.Dispose();
                        _cHorizontalTrackDc = new cStoreDc();
                    }
                    _cHorizontalTrackDc.Width = _oHorizontalTrackBitmap.Width;
                    _cHorizontalTrackDc.Height = _oHorizontalTrackBitmap.Height;
                    //SelectObject(_cHorizontalTrackDc.Hdc, _oHorizontalTrackBitmap.GetHbitmap());




                    Thread selectBmp = new Thread(() => SelectObject(_cHorizontalTrackDc.Hdc, _oHorizontalTrackBitmap.GetHbitmap()));
                    selectBmp.Start();
                    selectBmp.Join();
                }
            }

            private Bitmap VerticalArrowGraphic
            {
                get { return _oVerticalArrowBitmap; }
                set
                {
                    _oVerticalArrowBitmap = value;
                    if (_cVerticalArrowDc.Hdc != IntPtr.Zero)
                    {
                        _cVerticalArrowDc.Dispose();
                        _cVerticalArrowDc = new cStoreDc();
                    }
                    _cVerticalArrowDc.Width = _oVerticalArrowBitmap.Width;
                    _cVerticalArrowDc.Height = _oVerticalArrowBitmap.Height;
                    //SelectObject(_cVerticalArrowDc.Hdc, _oVerticalArrowBitmap.GetHbitmap());

                    Thread selectBmp = new Thread(() => SelectObject(_cVerticalArrowDc.Hdc, _oVerticalArrowBitmap.GetHbitmap()));
                    selectBmp.Start();
                    selectBmp.Join();

                }
            }

            private Bitmap VerticalThumbGraphic
            {
                get { return _oVerticalThumbBitmap; }
                set
                {
                    _oVerticalThumbBitmap = value;
                    if (_cVerticalThumbDc.Hdc != IntPtr.Zero)
                    {
                        _cVerticalThumbDc.Dispose();
                        _cVerticalThumbDc = new cStoreDc();
                    }
                    _cVerticalThumbDc.Width = _oVerticalThumbBitmap.Width;
                    _cVerticalThumbDc.Height = _oVerticalThumbBitmap.Height;
                    //SelectObject(_cVerticalThumbDc.Hdc, _oVerticalThumbBitmap.GetHbitmap());


                    Thread selectBmp = new Thread(() => SelectObject(_cVerticalThumbDc.Hdc, _oVerticalThumbBitmap.GetHbitmap()));
                    selectBmp.Start();
                    selectBmp.Join();
                }
            }

            private Bitmap VerticalTrackGraphic
            {
                get { return _oVerticalTrackBitmap; }
                set
                {
                    _oVerticalTrackBitmap = value;
                    if (_cVerticalTrackDc.Hdc != IntPtr.Zero)
                    {
                        _cVerticalTrackDc.Dispose();
                        _cVerticalTrackDc = new cStoreDc();
                    }
                    _cVerticalTrackDc.Width = _oVerticalTrackBitmap.Width;
                    _cVerticalTrackDc.Height = _oVerticalTrackBitmap.Height;
                    //SelectObject(_cVerticalTrackDc.Hdc, _oVerticalTrackBitmap.GetHbitmap());


                    Thread selectBmp = new Thread(() => SelectObject(_cVerticalTrackDc.Hdc, _oVerticalTrackBitmap.GetHbitmap()));
                    selectBmp.Start();
                    selectBmp.Join();
                }
            }

            private Bitmap TransitionGraphic
            {
                get { return _oMask; }
                set { _oMask = value; }
            }

            private int HScrollPos
            {
                get { return GetScrollPos((IntPtr)this.Handle, SB_HORZ); }
                set { SetScrollPos((IntPtr)this.Handle, SB_HORZ, value, true); }
            }

            private int VScrollPos
            {
                get { return GetScrollPos((IntPtr)this.Handle, SB_VERT); }
                set { SetScrollPos((IntPtr)this.Handle, SB_VERT, value, true); }
            }
            #endregion

            #region Methods
            public void checkBarState()
            {
                if ((GetWindowLong(_hControlWnd, GWL_STYLE) & WS_VISIBLE) == WS_VISIBLE)
                {
                    if (hasHorizontal())
                        ShowWindow(_hHorizontalMaskWnd, SW_NORMAL);
                    else
                        ShowWindow(_hHorizontalMaskWnd, SW_HIDE);

                    if (hasVertical())
                        ShowWindow(_hVerticalMaskWnd, SW_NORMAL);
                    else
                        ShowWindow(_hVerticalMaskWnd, SW_HIDE);

                    if (hasSizer())
                        ShowWindow(_hSizerMaskWnd, SW_NORMAL);
                    else
                        ShowWindow(_hSizerMaskWnd, SW_HIDE);
                }
                else
                {
                    ShowWindow(_hHorizontalMaskWnd, SW_HIDE);
                    ShowWindow(_hVerticalMaskWnd, SW_HIDE);
                    ShowWindow(_hSizerMaskWnd, SW_HIDE);
                }
            }

            private void createScrollBarMask()
            {
                Type t = typeof(cScrollBar);
                Module m = t.Module;
                IntPtr hInstance = Marshal.GetHINSTANCE(m);
                IntPtr hParent = GetParent(_hControlWnd);
                RECT tr = new RECT();
                Point pt = new Point();
                SCROLLBARINFO sb = new SCROLLBARINFO();
                sb.cbSize = Marshal.SizeOf(sb);

                // vertical scrollbar
                // get the size and position
                GetScrollBarInfo(_hControlWnd, OBJID_VSCROLL, ref sb);
                tr = sb.rcScrollBar;
                pt.X = tr.Left;
                pt.Y = tr.Top;
                ScreenToClient(hParent, ref pt);

                // create the window
                _hVerticalMaskWnd = CreateWindowEx(WS_EX_TOPMOST | WS_EX_TOOLWINDOW,
                    "STATIC", "",
                    SS_OWNERDRAW | WS_CHILD | WS_CLIPSIBLINGS | WS_OVERLAPPED | WS_VISIBLE,
                    pt.X, pt.Y,
                    (tr.Right - tr.Left), (tr.Bottom - tr.Top),
                    hParent,
                    IntPtr.Zero, hInstance, IntPtr.Zero);

                // set z-order
                SetWindowPos(_hVerticalMaskWnd, HWND_TOP,
                    0, 0,
                    0, 0,
                    SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE | SWP_NOOWNERZORDER);

                // horizontal scrollbar
                GetScrollBarInfo(_hControlWnd, OBJID_HSCROLL, ref sb);
                tr = sb.rcScrollBar;
                pt.X = tr.Left;
                pt.Y = tr.Top;
                ScreenToClient(hParent, ref pt);

                _hHorizontalMaskWnd = CreateWindowEx(WS_EX_TOPMOST | WS_EX_TOOLWINDOW,
                    "STATIC", "",
                    SS_OWNERDRAW | WS_CHILD | WS_CLIPSIBLINGS | WS_OVERLAPPED | WS_VISIBLE,
                    pt.X, pt.Y,
                    (tr.Right - tr.Left), (tr.Bottom - tr.Top),
                    hParent,
                    IntPtr.Zero, hInstance, IntPtr.Zero);

                SetWindowPos(_hHorizontalMaskWnd, HWND_TOP,
                    0, 0,
                    0, 0,
                    SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE | SWP_NOOWNERZORDER);

                // sizer
                _hSizerMaskWnd = CreateWindowEx(WS_EX_TOPMOST | WS_EX_TOOLWINDOW,
                    "STATIC", "",
                    SS_OWNERDRAW | WS_CHILD | WS_CLIPSIBLINGS | WS_OVERLAPPED | WS_VISIBLE,
                    pt.X + (tr.Right - tr.Left), pt.Y,
                    _iArrowCx, _iArrowCy,
                    hParent,
                    IntPtr.Zero, hInstance, IntPtr.Zero);

                SetWindowPos(_hSizerMaskWnd, HWND_TOP,
                    0, 0,
                    0, 0,
                    SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE | SWP_NOOWNERZORDER);
                reSizeMask();
            }

            public void drawScrollBar()
            {
                RECT tr = new RECT();
                Point pst = new Point();
                cStoreDc tempDc = new cStoreDc();
                IntPtr hdc = IntPtr.Zero;
                int offset = 0;
                int width = 0;
                int section = 0;
                SCROLLBARINFO sb = new SCROLLBARINFO();
                sb.cbSize = Marshal.SizeOf(sb);

                if (hasHorizontal())
                {
                    GetScrollBarInfo(_hControlWnd, OBJID_HSCROLL, ref sb);
                    tr = sb.rcScrollBar;
                    OffsetRect(ref tr, -tr.Left, -tr.Top);
                    tempDc.Width = tr.Right;
                    tempDc.Height = tr.Bottom;
                    SB_HITEST hitTest = scrollbarHitTest(Orientation.Horizontal);

                    // draw the track
                    using (StretchImage si = new StretchImage(_cHorizontalTrackDc.Hdc, tempDc.Hdc, new Rectangle(0, 0, _cHorizontalTrackDc.Width, _cHorizontalTrackDc.Height), new Rectangle(_iArrowCx, 0, tr.Right - (2 * _iArrowCx), tr.Bottom), 2, StretchModeEnum.STRETCH_HALFTONE)) { }
                    // draw the arrows
                    section = 7;
                    width = _cHorizontalArrowDc.Width / section;
                    // left arrow
                    if (hitTest == SB_HITEST.leftArrow)
                    {
                        if (leftKeyPressed())
                            offset = 2;
                        else
                            offset = 1;

                    }
                    else
                    {
                        offset = 0;
                    }
                    using (StretchImage si = new StretchImage(_cHorizontalArrowDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cHorizontalArrowDc.Height), new Rectangle(0, 0, _iArrowCx, tr.Bottom), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                    // right arrow
                    if (hitTest == SB_HITEST.rightArrow)
                    {
                        if (leftKeyPressed())
                            offset = 5;
                        else
                            offset = 4;

                    }
                    else
                    {
                        offset = 3;
                    }
                    using (StretchImage si = new StretchImage(_cHorizontalArrowDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cHorizontalArrowDc.Height), new Rectangle(tr.Right - _iArrowCx, 0, _iArrowCx, tr.Bottom), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                    // draw the thumb
                    section = 3;
                    width = _cHorizontalThumbDc.Width / section;
                    if (hitTest == SB_HITEST.button)
                    {
                        if (leftKeyPressed())
                            offset = 2;
                        else
                            offset = 1;

                    }
                    else
                    {
                        offset = 0;
                    }
                    pst.X = sb.xyThumbTop;
                    pst.Y = sb.xyThumbBottom;







                    using (StretchImage si = new StretchImage(_cHorizontalThumbDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cHorizontalThumbDc.Height), new Rectangle(pst.X, 2, pst.Y - pst.X, tr.Bottom), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                    hdc = GetDC(_hHorizontalMaskWnd);
                    BitBlt(hdc, 0, 0, tr.Right, tr.Bottom, tempDc.Hdc, 0, 0, 0xCC0020);
                    ReleaseDC(_hHorizontalMaskWnd, hdc);
                }

                if (hasSizer())
                {
                    tempDc.Width = _iArrowCx;
                    tempDc.Height = _iArrowCy;
                    offset = 6;
                    section = 7;
                    width = _cHorizontalArrowDc.Width / section;

                    using (StretchImage si = new StretchImage(_cHorizontalArrowDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cHorizontalArrowDc.Height), new Rectangle(0, 0, _iArrowCx, _iArrowCy), 0, StretchModeEnum.STRETCH_HALFTONE)) { }
                    hdc = GetDC(_hSizerMaskWnd);
                    BitBlt(hdc, 0, 0, _iArrowCx, _iArrowCy, tempDc.Hdc, 0, 0, 0xCC0020);
                    ReleaseDC(_hSizerMaskWnd, hdc);
                }

                if (hasVertical())
                {
                    GetScrollBarInfo(_hControlWnd, OBJID_VSCROLL, ref sb);
                    tr = sb.rcScrollBar;
                    OffsetRect(ref tr, -tr.Left, -tr.Top);
                    tempDc.Width = tr.Right;
                    tempDc.Height = tr.Bottom;
                    SB_HITEST hitTest = scrollbarHitTest(Orientation.Vertical);

                    // draw the track
                    using (StretchImage si = new StretchImage(_cVerticalTrackDc.Hdc, tempDc.Hdc, new Rectangle(0, 0, _cVerticalTrackDc.Width, _cVerticalTrackDc.Height), new Rectangle(0, _iArrowCy, tr.Right, tr.Bottom - (2 * _iArrowCy)), 2, StretchModeEnum.STRETCH_HALFTONE)) { }
                    section = 6;
                    width = _cVerticalArrowDc.Width / section;

                    // top arrow
                    if (hitTest == SB_HITEST.topArrow)
                    {
                        if (leftKeyPressed())
                            offset = 2;
                        else
                            offset = 1;

                    }
                    else
                    {
                        offset = 0;
                    }
                    using (StretchImage si = new StretchImage(_cVerticalArrowDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cVerticalArrowDc.Height), new Rectangle(0, 0, tr.Right, _iArrowCy), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                    // bottom arrow
                    if (hitTest == SB_HITEST.bottomArrow)
                    {
                        if (leftKeyPressed())
                            offset = 5;
                        else
                            offset = 4;

                    }
                    else
                    {
                        offset = 3;
                    }
                    using (StretchImage si = new StretchImage(_cVerticalArrowDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cVerticalArrowDc.Height), new Rectangle(0, tr.Bottom - _iArrowCy, tr.Right, _iArrowCy), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                    // draw the thumb
                    section = 3;
                    width = _cVerticalThumbDc.Width / section;
                    if (hitTest == SB_HITEST.button)
                    {
                        if (leftKeyPressed())
                            offset = 2;
                        else
                            offset = 1;

                    }
                    else
                    {
                        offset = 0;
                    }

                    pst.X = sb.xyThumbTop;
                    pst.Y = sb.xyThumbBottom;
                    using (StretchImage si = new StretchImage(_cVerticalThumbDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cVerticalThumbDc.Height), new Rectangle(0, pst.X, _iArrowCx, pst.Y - pst.X), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                    hdc = GetDC(_hVerticalMaskWnd);
                    BitBlt(hdc, 0, 0, tr.Right, tr.Bottom, tempDc.Hdc, 0, 0, 0xCC0020);
                    ReleaseDC(_hVerticalMaskWnd, hdc);
                }
                tempDc.Dispose();
            }

            public void scrollFader()
            {
                if (TransitionGraphic != null)
                {
                    SB_HITEST hitTest;
                    SCROLLBARINFO sb = new SCROLLBARINFO();
                    sb.cbSize = Marshal.SizeOf(sb);
                    if (hasHorizontal())
                    {
                        hitTest = scrollbarHitTest(Orientation.Horizontal);

                        if ((hitTest == SB_HITEST.button) && (!leftKeyPressed()))
                        {
                            GetScrollBarInfo(_hControlWnd, OBJID_HSCROLL, ref sb);
                            // start the transition routines here
                            // size of mask - new Rectangle(sb.xyThumbTop, 2, sb.xyThumbBottom - sb.xyThumbTop, sb.rcScrollBar.Bottom)
                        }
                    }
                    if (hasVertical())
                    {
                        hitTest = scrollbarHitTest(Orientation.Vertical);

                        if ((hitTest == SB_HITEST.button) && (!leftKeyPressed()))
                        {
                            GetScrollBarInfo(_hControlWnd, OBJID_VSCROLL, ref sb);
                            // start the transition routines here
                            // size of mask - new Rectangle(sb.xyThumbTop, 2, sb.xyThumbBottom - sb.xyThumbTop, sb.rcScrollBar.Bottom)
                        }
                    }
                }


            }

            private bool hasHorizontal()
            {
                return ((GetWindowLong(_hControlWnd, GWL_STYLE) & WS_HSCROLL) == WS_HSCROLL);
            }

            private bool hasSizer()
            {
                return (hasHorizontal() && hasVertical());
            }

            private bool hasVertical()
            {
                return ((GetWindowLong(_hControlWnd, GWL_STYLE) & WS_VSCROLL) == WS_VSCROLL);
            }

            public void invalidateWindow(bool messaged)
            {
                if (messaged)
                    RedrawWindow(_hControlWnd, IntPtr.Zero, IntPtr.Zero, RDW_INTERNALPAINT);
                else
                    RedrawWindow(_hControlWnd, IntPtr.Zero, IntPtr.Zero, RDW_INVALIDATE | RDW_UPDATENOW);
            }

            private bool leftKeyPressed()
            {
                if (mouseButtonsSwitched())
                    return (GetKeyState(VK_RBUTTON) < 0);
                else
                    return (GetKeyState(VK_LBUTTON) < 0);
            }

            private bool mouseButtonsSwitched()
            {
                return (GetSystemMetrics(SYSTEM_METRICS.SM_SWAPBUTTON) != 0);
            }

            private SB_HITEST scrollbarHitTest(Orientation orient)
            {
                Point pt = new Point();
                RECT tr = new RECT();
                RECT tp = new RECT();
                SCROLLBARINFO sb = new SCROLLBARINFO();
                sb.cbSize = Marshal.SizeOf(sb);

                GetCursorPos(ref pt);

                if (orient == Orientation.Horizontal)
                {
                    GetScrollBarInfo(_hControlWnd, OBJID_HSCROLL, ref sb);
                    tr = sb.rcScrollBar;
                    //OffsetRect(ref tr, -tr.Left, -tr.Top);
                    tp = tr;
                    if (PtInRect(ref tr, pt))
                    {
                        // left arrow
                        tp.Right = tp.Left + _iArrowCx;
                        if (PtInRect(ref tp, pt))
                            return SB_HITEST.leftArrow;
                        // right arrow
                        tp.Left = tr.Right - _iArrowCx;
                        tp.Right = tr.Right;
                        if (PtInRect(ref tp, pt))
                            return SB_HITEST.rightArrow;
                        // button
                        tp.Left = tr.Left + sb.xyThumbTop;
                        tp.Right = tr.Left + sb.xyThumbBottom;
                        if (PtInRect(ref tp, pt))
                            return SB_HITEST.button;
                        // track
                        return SB_HITEST.track;
                    }
                }
                else
                {
                    GetScrollBarInfo(_hControlWnd, OBJID_VSCROLL, ref sb);
                    tr = sb.rcScrollBar;
                    tp = tr;

                    if (PtInRect(ref tr, pt))
                    {
                        // top arrow
                        tp.Bottom = tr.Top + _iArrowCy;
                        if (PtInRect(ref tp, pt))
                            return SB_HITEST.topArrow;
                        // bottom arrow
                        tp.Top = tr.Bottom - _iArrowCy;
                        tp.Bottom = tr.Bottom;
                        if (PtInRect(ref tp, pt))
                            return SB_HITEST.bottomArrow;
                        // button
                        tp.Top = tr.Top + sb.xyThumbTop;
                        tp.Bottom = tr.Bottom + sb.xyThumbBottom;
                        if (PtInRect(ref tp, pt))
                            return SB_HITEST.button;
                        // track
                        return SB_HITEST.track;
                    }
                }
                return SB_HITEST.offControl;
            }

            public void reSizeMask()
            {
                RECT tr = new RECT();
                SCROLLBARINFO sb = new SCROLLBARINFO();
                sb.cbSize = Marshal.SizeOf(sb);
                IntPtr hParent = GetParent(_hControlWnd);
                Point pt = new Point();

                if (hasVertical())
                {
                    GetScrollBarInfo(_hControlWnd, OBJID_VSCROLL, ref sb);
                    tr = sb.rcScrollBar;
                    pt.X = tr.Left;
                    pt.Y = tr.Top;
                    ScreenToClient(hParent, ref pt);
                    SetWindowPos(_hVerticalMaskWnd, IntPtr.Zero, pt.X, pt.Y, tr.Right - tr.Left, tr.Bottom - tr.Top, SWP_NOACTIVATE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_SHOWWINDOW);
                }
                if (hasHorizontal())
                {
                    GetScrollBarInfo(_hControlWnd, OBJID_HSCROLL, ref sb);
                    tr = sb.rcScrollBar;
                    pt.X = tr.Left;
                    pt.Y = tr.Top;
                    ScreenToClient(hParent, ref pt);
                    SetWindowPos(_hHorizontalMaskWnd, IntPtr.Zero, pt.X, pt.Y, tr.Right - tr.Left, tr.Bottom - tr.Top, SWP_NOACTIVATE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_SHOWWINDOW);
                }
                if (hasSizer())
                {
                    GetScrollBarInfo(_hControlWnd, OBJID_HSCROLL, ref sb);
                    tr = new RECT(sb.rcScrollBar.Right, sb.rcScrollBar.Top, sb.rcScrollBar.Right + _iArrowCx, sb.rcScrollBar.Bottom);
                    pt.X = tr.Left;
                    pt.Y = tr.Top;
                    ScreenToClient(hParent, ref pt);
                    SetWindowPos(_hSizerMaskWnd, IntPtr.Zero, pt.X, pt.Y, tr.Right - tr.Left, tr.Bottom - tr.Top, SWP_NOACTIVATE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_SHOWWINDOW);
                }
            }

            private void scrollbarMetrics()
            {
                _iArrowCx = GetSystemMetrics(SYSTEM_METRICS.SM_CXVSCROLL);
                _iArrowCy = GetSystemMetrics(SYSTEM_METRICS.SM_CYVSCROLL);
            }

            private void scrollHorizontal(bool Right)
            {
                if (Right)
                    SendMessage(_hControlWnd, WM_HSCROLL, SB_LINERIGHT, 0);
                else
                    SendMessage(_hControlWnd, WM_HSCROLL, SB_LINELEFT, 0);

            }

            private void scrollVertical(bool Down)
            {
                if (Down)
                    SendMessage(_hControlWnd, WM_VSCROLL, SB_LINEDOWN, 0);
                else
                    SendMessage(_hControlWnd, WM_VSCROLL, SB_LINEUP, 0);
            }

            public void Dispose()
            {
                try
                {
                    this.ReleaseHandle();
                    if (_oVerticalArrowBitmap != null) _oVerticalArrowBitmap.Dispose();
                    if (_cVerticalArrowDc != null) _cVerticalArrowDc.Dispose();
                    if (_oVerticalThumbBitmap != null) _oVerticalThumbBitmap.Dispose();
                    if (_cVerticalThumbDc != null) _cVerticalThumbDc.Dispose();
                    if (_oVerticalTrackBitmap != null) _oVerticalTrackBitmap.Dispose();
                    if (_cVerticalTrackDc != null) _cVerticalTrackDc.Dispose();
                    if (_oHorizontalArrowBitmap != null) _oHorizontalArrowBitmap.Dispose();
                    if (_cHorizontalArrowDc != null) _cHorizontalArrowDc.Dispose();
                    if (_oHorizontalThumbBitmap != null) _oHorizontalThumbBitmap.Dispose();
                    if (_cHorizontalThumbDc != null) _cHorizontalThumbDc.Dispose();
                    if (_oHorizontalTrackBitmap != null) _oHorizontalTrackBitmap.Dispose();
                    if (_cHorizontalTrackDc != null) _cHorizontalTrackDc.Dispose();
                    if (_hVerticalMaskWnd != IntPtr.Zero) DestroyWindow(_hVerticalMaskWnd);
                    if (_hHorizontalMaskWnd != IntPtr.Zero) DestroyWindow(_hHorizontalMaskWnd);
                    if (_hSizerMaskWnd != IntPtr.Zero) DestroyWindow(_hSizerMaskWnd);
                }
                catch { }
                GC.SuppressFinalize(this);
            }
            #endregion


            ////  Scrollbar direction
            ////  All these constents can be found in WinUser.h
            //// 
            //private const int SBS_HORZ = 0;
            //private const int SBS_VERT = 1;
            ////  Windows Messages
            ////  All these constents can be found in WinUser.h
            //// 
            //private const int WM_VSCROLL = 277;
            //private const int WM_HSCROLL = 276;
            //private const int SB_THUMBPOSITION = 4;
            //public enum eScrollAction
            //{

            //    Jump = 0,

            //    Relative = 1,
            //}
            //public enum eScrollDirection
            //{

            //    Vertical = 0,

            //    Horizontal = 1,
            //}


            ////  API Function: GetScrollPos
            ////  Returns an integer of the position of the scrollbar
            //// 
            //[DllImport("user32.dll")]
            //private static extern int GetScrollPos(IntPtr hWnd, int nBar);

            ////  API Function: SetScrollPos
            ////  Sets ONLY the scrollbar DOES NOT change the control object
            //// 
            //[DllImport("user32.dll")]
            //private static extern int SetScrollPos(IntPtr hWnd, int nBar, int nPos, bool bRedraw);

            //  API Function: PostMessageA
            //  Sends a message to a control (We are going to tell it to synch
            //  with the scrollbar)
            // 
            //[DllImport("user32.dll")]
            //private static extern bool PostMessageA(IntPtr hwnd, int wMsg, int wParam, int lParam);

            ////      Sub: scrollControl
            ////  Purpose: All functions to control the scroll are in here
            //// 
            //private void scrollControl(IntPtr hWnd, eScrollDirection Direction, eScrollAction Action, int Amount)
            //{
            //    int position;
            //    //  What direction are we going
            //    if ((Direction == eScrollDirection.Horizontal))
            //    {
            //        //  What action are we taking (Jumping or Relative)
            //        if ((Action == eScrollAction.Relative))
            //        {
            //            position = (GetScrollPos(hWnd, SBS_HORZ) + Amount);
            //        }
            //        else
            //        {
            //            position = Amount;
            //        }
            //        //  Make it so
            //        if ((SetScrollPos(hWnd, SBS_HORZ, position, true) != -1))
            //        {
            //            PostMessageA(hWnd, WM_HSCROLL, (SB_THUMBPOSITION + (65536 * position)), 0);
            //        }
            //        else
            //        {
            //            //MsgBox(("Can\'t set info (Err: "
            //            //                + (GetLastWin32Error() + ")")));
            //        }
            //    }
            //    else
            //    {
            //        //  What action are we taking (Jumping or Relative)
            //        if ((Action == eScrollAction.Relative))
            //        {
            //            position = (GetScrollPos(hWnd, SBS_VERT) + Amount);
            //        }
            //        else
            //        {
            //            position = Amount;
            //        }
            //        //  Make it so
            //        if ((SetScrollPos(hWnd, SBS_VERT, position, true) != -1))
            //        {
            //            PostMessageA(hWnd, WM_VSCROLL, (SB_THUMBPOSITION + (65536 * position)), 0);
            //        }
            //        else
            //        {
            //            //MsgBox(("Can\'t set info (Err: "
            //            //                + (GetLastWin32Error() + ")")));
            //        }
            //    }
            //}




            //#region WndProc
            //protected override void WndProc(ref Message m)
            //{

            //    switch (m.Msg)
            //    {

            //        case WM_VSCROLL:
            //            this.WndProc(ref m);



            //            break;
            //        case WM_HSCROLL:

            //            this.WndProc(ref m);



            //            break;
            //        default:
            //            this.WndProc(ref m);
            //            break;
            //    }
            //}
            //#endregion


            //public bool PreFilterMessage(ref Message m)
            //{
            //    switch (m.Msg)
            //    {
            //        case WM_MOUSEWHEEL:   // 0x020A
            //        case WM_MOUSEHWHEEL:  // 0x020E
            //            IntPtr hControlUnderMouse = WindowFromPoint(new Point((int)m.LParam));
            //            if (hControlUnderMouse == m.HWnd)
            //                return false; // already headed for the right control
            //            else
            //            {
            //                // redirect the message to the control under the mouse
            //                SendMessage(hControlUnderMouse, m.Msg, m.WParam, m.LParam);
            //                return true;
            //            }
            //        default:
            //            return false;
            //    }
            //}








            //protected override void OnResize(EventArgs e)
            //{
            //    this.OnResize(e);
            //    VisibleScrollbars = GetVisibleScrollbars(this);
            //}




            #region WndProc
            protected override void WndProc(ref Message m)
            {
                switch (m.Msg)
                {

                    //case 0x000F: //WM_PAINT
                    case 0x0083:
                        reSizeMask();
                        base.WndProc(ref m);

                        break;
                    case WM_NCPAINT:
                        drawScrollBar();
                        base.WndProc(ref m);
                        break;

                    case WM_MOUSEHWHEEL:  // 0x020E
                    case WM_HSCROLL:
                        //scrollControl(m.WParam, eScrollDirection.Vertical, eScrollAction.Relative, 5);
                        _bTrackingMouse = true;
                        drawScrollBar();
                        scrollFader();

                        base.WndProc(ref m);
                        break;
                    case WM_MOUSEWHEEL:   // 0x020A
                    case WM_VSCROLL:
                        //scrollControl(m.WParam, eScrollDirection.Vertical, eScrollAction.Relative, -5);

                        _bTrackingMouse = true;
                        drawScrollBar();
                        scrollFader();
                        base.WndProc(ref m);
                        break;
                    case WM_NCMOUSEMOVE:
                        _bTrackingMouse = true;
                        drawScrollBar();
                        scrollFader();
                        base.WndProc(ref m);
                        break;

                    case WM_MOUSEMOVE:
                        if (_bTrackingMouse)
                            _bTrackingMouse = false;
                        base.WndProc(ref m);
                        break;


                    case WM_SIZE:
                    case WM_MOVE:
                        reSizeMask();
                        base.WndProc(ref m);
                        break;

                    default:
                        checkBarState();
                        base.WndProc(ref m);
                        break;
                }
            }
            #endregion














            [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust")]
            class cScrollBar : NativeWindow, IDisposable
            {
                #region Constants
                // showwindow
                private const int SW_HIDE = 0x0;
                private const int SW_NORMAL = 0x1;
                // window styles
                private const int GWL_STYLE = (-16);
                private const int GWL_EXSTYLE = (-20);
                private const int WS_EX_TOPMOST = 0x8;
                private const int WS_EX_TOOLWINDOW = 0x80;
                private const int WS_CHILD = 0x40000000;
                private const int WS_OVERLAPPED = 0x0;
                private const int WS_CLIPCHILDREN = 0x2000000;
                private const int WS_CLIPSIBLINGS = 0x4000000;
                private const int WS_VISIBLE = 0x10000000;
                private const int SS_OWNERDRAW = 0xD;
                // size/move
                private const uint SWP_NOSIZE = 0x0001;
                private const uint SWP_NOMOVE = 0x0002;
                private const uint SWP_NOZORDER = 0x0004;
                private const uint SWP_NOREDRAW = 0x0008;
                private const uint SWP_NOACTIVATE = 0x0010;
                private const uint SWP_FRAMECHANGED = 0x0020;
                private const uint SWP_SHOWWINDOW = 0x0040;
                private const uint SWP_HIDEWINDOW = 0x0080;
                private const uint SWP_NOCOPYBITS = 0x0100;
                private const uint SWP_NOOWNERZORDER = 0x0200;
                private const uint SWP_NOSENDCHANGING = 0x0400;
                // setwindowpos
                static readonly IntPtr HWND_NOTOPMOST = new IntPtr(-2);
                static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
                static readonly IntPtr HWND_TOP = new IntPtr(0);
                static readonly IntPtr HWND_BOTTOM = new IntPtr(1);
                // scroll messages
                private const int WM_HSCROLL = 0x114;
                private const int WM_VSCROLL = 0x115;
                private const int SB_LINEUP = 0;
                private const int SB_LINEDOWN = 1;
                private const int SB_LINELEFT = 0;
                private const int SB_LINERIGHT = 1;
                private const int SB_PAGEUP = 2;
                private const int SB_PAGEDOWN = 3;
                private const int SB_PAGELEFT = 2;
                private const int SB_PAGERIGHT = 3;
                // mouse buttons
                private const int VK_LBUTTON = 0x1;
                private const int VK_RBUTTON = 0x2;
                // redraw
                private const int RDW_INVALIDATE = 0x0001;
                private const int RDW_INTERNALPAINT = 0x0002;
                private const int RDW_ERASE = 0x0004;
                private const int RDW_VALIDATE = 0x0008;
                private const int RDW_NOINTERNALPAINT = 0x0010;
                private const int RDW_NOERASE = 0x0020;
                private const int RDW_NOCHILDREN = 0x0040;
                private const int RDW_ALLCHILDREN = 0x0080;
                private const int RDW_UPDATENOW = 0x0100;
                private const int RDW_ERASENOW = 0x0200;
                private const int RDW_FRAME = 0x0400;
                private const int RDW_NOFRAME = 0x0800;
                // scroll bar messages
                private const int SB_HORZ = 0x0;
                private const int SB_VERT = 0x1;
                private const int SBM_SETPOS = 0x00E0;
                private const int SBM_GETPOS = 0x00E1;
                private const int SBM_SETRANGE = 0x00E2;
                private const int SBM_SETRANGEREDRAW = 0x00E6;
                private const int SBM_GETRANGE = 0x00E3;
                private const int SBM_ENABLE_ARROWS = 0x00E4;
                private const int SBM_SETSCROLLINFO = 0x00E9;
                private const int SBM_GETSCROLLINFO = 0x00EA;
                private const int SBM_GETSCROLLBARINFO = 0x00EB;
                private const int SIF_RANGE = 0x0001;
                private const int SIF_PAGE = 0x0002;
                private const int SIF_POS = 0x0004;
                private const int SIF_DISABLENOSCROLL = 0x0008;
                private const int SIF_TRACKPOS = 0x0010;
                private const int SIF_ALL = (SIF_RANGE | SIF_PAGE | SIF_POS | SIF_TRACKPOS);
                // scrollbar states
                private const int STATE_SYSTEM_INVISIBLE = 0x00008000;
                private const int STATE_SYSTEM_OFFSCREEN = 0x00010000;
                private const int STATE_SYSTEM_PRESSED = 0x00000008;
                private const int STATE_SYSTEM_UNAVAILABLE = 0x00000001;
                private const uint OBJID_HSCROLL = 0xFFFFFFFA;
                private const uint OBJID_VSCROLL = 0xFFFFFFFB;
                private const uint OBJID_CLIENT = 0xFFFFFFFC;
                // window messages
                private const int WM_PAINT = 0xF;
                private const int WM_MOUSEMOVE = 0x200;
                private const int WM_MOUSELEAVE = 0x2A3;
                private const int WM_LBUTTONDOWN = 0x201;
                private const int WM_LBUTTONUP = 0x202;
                private const int WM_LBUTTONDBLCLK = 0x203;
                private const int WM_RBUTTONDOWN = 0x204;
                private const int WM_RBUTTONUP = 0x205;
                private const int WM_RBUTTONDBLCLK = 0x206;
                private const int WM_MBUTTONDOWN = 0x207;
                private const int WM_MBUTTONUP = 0x208;
                private const int WM_MBUTTONDBLCLK = 0x209;
                private const int WM_MOUSEWHEEL = 0x20A;
                private const int WM_SIZE = 0x5;
                private const int WM_MOVE = 0x3;
                // message handler
                private static IntPtr MSG_HANDLED = new IntPtr(1);
                #endregion

                #region Enums
                private enum SB_HITEST : int
                {
                    offControl = 0,
                    topArrow,
                    bottomArrow,
                    leftArrow,
                    rightArrow,
                    button,
                    track
                }

                private enum SYSTEM_METRICS : int
                {
                    SM_CXSCREEN = 0,
                    SM_CYSCREEN = 1,
                    SM_CXVSCROLL = 2,
                    SM_CYHSCROLL = 3,
                    SM_CYCAPTION = 4,
                    SM_CXBORDER = 5,
                    SM_CYBORDER = 6,
                    SM_CYVTHUMB = 9,
                    SM_CXHTHUMB = 10,
                    SM_CXICON = 11,
                    SM_CYICON = 12,
                    SM_CXCURSOR = 13,
                    SM_CYCURSOR = 14,
                    SM_CYMENU = 15,
                    SM_CXFULLSCREEN = 16,
                    SM_CYFULLSCREEN = 17,
                    SM_CYKANJIWINDOW = 18,
                    SM_MOUSEPRESENT = 19,
                    SM_CYVSCROLL = 20,
                    SM_CXHSCROLL = 21,
                    SM_SWAPBUTTON = 23,
                    SM_CXMIN = 28,
                    SM_CYMIN = 29,
                    SM_CXSIZE = 30,
                    SM_CYSIZE = 31,
                    SM_CXFRAME = 32,
                    SM_CYFRAME = 33,
                    SM_CXMINTRACK = 34,
                    SM_CYMINTRACK = 35,
                    SM_CYSMCAPTION = 51,
                    SM_CXMINIMIZED = 57,
                    SM_CYMINIMIZED = 58,
                    SM_CXMAXTRACK = 59,
                    SM_CYMAXTRACK = 60,
                    SM_CXMAXIMIZED = 61,
                    SM_CYMAXIMIZED = 62
                }
                #endregion

                #region Structs
                [StructLayout(LayoutKind.Sequential)]
                private struct PAINTSTRUCT
                {
                    internal IntPtr hdc;
                    internal int fErase;
                    internal RECT rcPaint;
                    internal int fRestore;
                    internal int fIncUpdate;
                    internal int Reserved1;
                    internal int Reserved2;
                    internal int Reserved3;
                    internal int Reserved4;
                    internal int Reserved5;
                    internal int Reserved6;
                    internal int Reserved7;
                    internal int Reserved8;
                }

                [StructLayout(LayoutKind.Sequential)]
                private struct RECT
                {
                    internal RECT(int X, int Y, int Width, int Height)
                    {
                        this.Left = X;
                        this.Top = Y;
                        this.Right = Width;
                        this.Bottom = Height;
                    }
                    internal int Left;
                    internal int Top;
                    internal int Right;
                    internal int Bottom;
                }

                [StructLayout(LayoutKind.Sequential)]
                private struct SCROLLINFO
                {
                    internal uint cbSize;
                    internal uint fMask;
                    internal int nMin;
                    internal int nMax;
                    internal uint nPage;
                    internal int nPos;
                    internal int nTrackPos;
                }

                [StructLayout(LayoutKind.Sequential)]
                private struct SCROLLBARINFO
                {
                    internal int cbSize;
                    internal RECT rcScrollBar;
                    internal int dxyLineButton;
                    internal int xyThumbTop;
                    internal int xyThumbBottom;
                    internal int reserved;
                    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
                    internal int[] rgstate;
                }
                #endregion

                #region API
                [DllImport("user32.dll")]
                private static extern IntPtr BeginPaint(IntPtr hWnd, ref PAINTSTRUCT ps);

                [DllImport("user32.dll")]
                [return: MarshalAs(UnmanagedType.Bool)]
                private static extern bool EndPaint(IntPtr hWnd, ref PAINTSTRUCT ps);

                [DllImport("gdi32.dll")]
                private static extern IntPtr SelectObject(IntPtr hdc, IntPtr hObject);

                [DllImport("gdi32.dll")]
                [return: MarshalAs(UnmanagedType.Bool)]
                private static extern bool BitBlt(IntPtr hdc, int nXDest, int nYDest, int nWidth, int nHeight, IntPtr hdcSrc, int nXSrc, int nYSrc, int dwRop);

                [DllImport("gdi32.dll")]
                [return: MarshalAs(UnmanagedType.Bool)]
                private static extern bool StretchBlt(IntPtr hDest, int X, int Y, int nWidth, int nHeight, IntPtr hdcSrc,
                int sX, int sY, int nWidthSrc, int nHeightSrc, int dwRop);

                [DllImport("user32.dll")]
                private static extern IntPtr GetDC(IntPtr handle);

                [DllImport("user32.dll")]
                private static extern int ReleaseDC(IntPtr handle, IntPtr hdc);

                [DllImport("user32.dll")]
                private static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
                [DllImport("user32.dll")]
                private static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, ref SCROLLBARINFO lParam);

                [DllImport("user32.dll")]
                [return: MarshalAs(UnmanagedType.Bool)]
                private static extern bool GetWindowRect(IntPtr hWnd, ref RECT lpRect);

                [DllImport("user32.dll")]
                private extern static int OffsetRect(ref RECT lpRect, int x, int y);

                [DllImport("user32.dll")]
                private static extern bool ValidateRect(IntPtr hWnd, ref RECT lpRect);

                [DllImport("user32.dll")]
                private static extern int GetSystemMetrics(SYSTEM_METRICS smIndex);

                [DllImport("uxtheme.dll")]
                [return: MarshalAs(UnmanagedType.Bool)]
                private extern static bool IsAppThemed();

                [DllImport("uxtheme.dll", ExactSpelling = true, CharSet = CharSet.Unicode)]
                private static extern int SetWindowTheme(IntPtr hWnd, String pszSubAppName, String pszSubIdList);

                [DllImport("user32.dll")]
                [return: MarshalAs(UnmanagedType.Bool)]
                private static extern bool RedrawWindow(IntPtr hWnd, IntPtr lprcUpdate, IntPtr hrgnUpdate, uint flags);

                [DllImport("user32.dll")]
                [return: MarshalAs(UnmanagedType.Bool)]
                private static extern bool GetCursorPos(ref Point lpPoint);

                [DllImport("user32.dll")]
                [return: MarshalAs(UnmanagedType.Bool)]
                private static extern bool PtInRect([In] ref RECT lprc, Point pt);

                [DllImport("user32.dll")]
                private static extern int ScreenToClient(IntPtr hwnd, ref Point lpPoint);

                [DllImport("user32.dll")]
                private static extern short GetKeyState(int nVirtKey);

                [DllImport("user32.dll")]
                private static extern int SetScrollPos(IntPtr hWnd, int nBar, int nPos, bool bRedraw);

                [DllImport("user32.dll")]
                private static extern int GetScrollPos(IntPtr hWnd, int nBar);

                [DllImport("user32.dll")]
                private static extern IntPtr GetParent(IntPtr hWnd);

                [DllImport("user32.dll", SetLastError = true)]
                private static extern IntPtr CreateWindowEx(int exstyle, string lpClassName, string lpWindowName, int dwStyle,
                    int x, int y, int nWidth, int nHeight, IntPtr hwndParent, IntPtr Menu, IntPtr hInstance, IntPtr lpParam);

                [DllImport("user32.dll")]
                [return: MarshalAs(UnmanagedType.Bool)]
                private static extern bool DestroyWindow(IntPtr hWnd);

                [DllImport("user32.dll")]
                [return: MarshalAs(UnmanagedType.Bool)]
                private static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndAfter, int x, int y, int cx, int cy, uint flags);

                [DllImport("user32.dll")]
                static extern bool EqualRect([In] ref RECT lprc1, [In] ref RECT lprc2);

                [DllImport("user32.dll")]
                private static extern int GetWindowLong(IntPtr hwnd, int nIndex);

                [DllImport("user32.dll")]
                private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
                #endregion

                #region Fields
                public bool _bPainting = false;
                //private bool _bMoved = false;
                //private bool _bFading = false;
                private IntPtr _hMaskWnd = IntPtr.Zero;
                private int _iArrowCx = 14;
                private int _iArrowCy = 14;
                private Orientation _eScrollbarOrientation;
                private IntPtr _hScrollBarWnd = IntPtr.Zero;
                private cStoreDc _cArrowDc = new cStoreDc();
                private cStoreDc _cThumbDc = new cStoreDc();
                private cStoreDc _cTrackDc = new cStoreDc();
                private Bitmap _oArrowBitmap;
                private Bitmap _oThumbBitmap;
                private Bitmap _oTrackBitmap;
                private Bitmap _oMask;
                #endregion

                #region Constructor
                public cScrollBar(IntPtr hWnd, Orientation orientation, Bitmap thumb, Bitmap track, Bitmap arrow, Bitmap fader)
                {
                    if (hWnd == IntPtr.Zero)
                        throw new Exception("The scrollbar handle is invalid.");
                    ArrowGraphic = arrow;
                    ThumbGraphic = thumb;
                    TrackGraphic = track;
                    _hScrollBarWnd = hWnd;
                    Direction = orientation;
                    scrollbarMetrics();
                    if (Environment.OSVersion.Version.Major > 5)
                    {
                        if (IsAppThemed())
                            SetWindowTheme(_hScrollBarWnd, "", "");
                    }
                    createScrollBarMask();
                    ScrollBar sc = (ScrollBar)Control.FromHandle(_hScrollBarWnd);
                    sc.Scroll += new ScrollEventHandler(sc_Scroll);
                    sc.MouseEnter += new EventHandler(sc_MouseEnter);
                    Control ct = Control.FromHandle(GetParent(_hScrollBarWnd));
                    ct.Paint += new PaintEventHandler(ct_Paint);
                    this.AssignHandle(hWnd);
                    if (fader != null)
                        TransitionGraphic = fader;
                }

                void sc_MouseEnter(object sender, EventArgs e)
                {
                    scrollFader();
                }

                public void Dispose()
                {
                    try
                    {
                        this.ReleaseHandle();
                        if (_oArrowBitmap != null) _oArrowBitmap.Dispose();
                        if (_cArrowDc != null) _cArrowDc.Dispose();
                        if (_oThumbBitmap != null) _oThumbBitmap.Dispose();
                        if (_cThumbDc != null) _cThumbDc.Dispose();
                        if (_oTrackBitmap != null) _oTrackBitmap.Dispose();
                        if (_cTrackDc != null) _cTrackDc.Dispose();
                        if (_hMaskWnd != IntPtr.Zero) DestroyWindow(_hMaskWnd);
                        //if (_oMask != null) _oMask.Dispose();
                    }
                    catch { }
                    GC.SuppressFinalize(this);
                }
                #endregion

                #region Events
                private void ct_Paint(object sender, PaintEventArgs e)
                {
                    invalidateWindow(false);
                }

                private void sc_Scroll(object sender, ScrollEventArgs e)
                {
                    invalidateWindow(false);
                }
                #endregion

                #region Properties
                private Bitmap ArrowGraphic
                {
                    get { return _oArrowBitmap; }
                    set
                    {
                        _oArrowBitmap = value;
                        if (_cArrowDc.Hdc != IntPtr.Zero)
                        {
                            _cArrowDc.Dispose();
                            _cArrowDc = new cStoreDc();
                        }
                        _cArrowDc.Width = _oArrowBitmap.Width;
                        _cArrowDc.Height = _oArrowBitmap.Height;
                        SelectObject(_cArrowDc.Hdc, _oArrowBitmap.GetHbitmap());

                    }
                }

                private Bitmap ThumbGraphic
                {
                    get { return _oThumbBitmap; }
                    set
                    {
                        _oThumbBitmap = value;
                        if (_cThumbDc.Hdc != IntPtr.Zero)
                        {
                            _cThumbDc.Dispose();
                            _cThumbDc = new cStoreDc();
                        }
                        _cThumbDc.Width = _oThumbBitmap.Width;
                        _cThumbDc.Height = _oThumbBitmap.Height;
                        SelectObject(_cThumbDc.Hdc, _oThumbBitmap.GetHbitmap());

                    }
                }

                private Bitmap TrackGraphic
                {
                    get { return _oTrackBitmap; }
                    set
                    {
                        _oTrackBitmap = value;
                        if (_cTrackDc.Hdc != IntPtr.Zero)
                        {
                            _cTrackDc.Dispose();
                            _cTrackDc = new cStoreDc();
                        }
                        _cTrackDc.Width = _oTrackBitmap.Width;
                        _cTrackDc.Height = _oTrackBitmap.Height;
                        SelectObject(_cTrackDc.Hdc, _oTrackBitmap.GetHbitmap());

                    }
                }

                private Bitmap TransitionGraphic
                {
                    get { return _oMask; }
                    set { _oMask = value; }
                }

                private Orientation Direction
                {
                    get { return _eScrollbarOrientation; }
                    set { _eScrollbarOrientation = value; }
                }

                private int HScrollPos
                {
                    get { return GetScrollPos((IntPtr)this.Handle, SB_HORZ); }
                    set { SetScrollPos((IntPtr)this.Handle, SB_HORZ, value, true); }
                }

                private int VScrollPos
                {
                    get { return GetScrollPos((IntPtr)this.Handle, SB_VERT); }
                    set { SetScrollPos((IntPtr)this.Handle, SB_VERT, value, true); }
                }
                #endregion

                #region Methods
                private void checkBarState()
                {
                    if ((GetWindowLong(_hScrollBarWnd, GWL_STYLE) & WS_VISIBLE) == WS_VISIBLE)
                        ShowWindow(_hMaskWnd, SW_NORMAL);
                    else
                        ShowWindow(_hMaskWnd, SW_HIDE);
                }

                private void createScrollBarMask()
                {
                    Type t = typeof(cScrollBar);
                    Module m = t.Module;
                    IntPtr hInstance = Marshal.GetHINSTANCE(m);
                    IntPtr hParent = GetParent(_hScrollBarWnd);
                    RECT tr = new RECT();
                    Point pt = new Point();

                    GetWindowRect(_hScrollBarWnd, ref tr);
                    pt.X = tr.Left;
                    pt.Y = tr.Top;
                    ScreenToClient(hParent, ref pt);

                    _hMaskWnd = CreateWindowEx(WS_EX_TOPMOST | WS_EX_TOOLWINDOW,
                        "STATIC", "",
                        SS_OWNERDRAW | WS_CHILD | WS_CLIPSIBLINGS | WS_OVERLAPPED | WS_VISIBLE,
                        pt.X, pt.Y,
                        (tr.Right - tr.Left), (tr.Bottom - tr.Top),
                        hParent,
                        IntPtr.Zero, hInstance, IntPtr.Zero);

                    // set z-order
                    SetWindowPos(_hMaskWnd, HWND_TOP,
                        0, 0,
                        0, 0,
                        SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE | SWP_NOOWNERZORDER);
                }

                public void drawScrollBar()
                {
                    SCROLLBARINFO sbi = new SCROLLBARINFO();
                    RECT tr = new RECT();
                    cStoreDc tempDc = new cStoreDc();
                    int offset = 0;
                    int width = 0;
                    int section = 0;

                    GetWindowRect(_hScrollBarWnd, ref tr);
                    OffsetRect(ref tr, -tr.Left, -tr.Top);
                    tempDc.Width = tr.Right;
                    tempDc.Height = tr.Bottom;
                    SB_HITEST hitTest = scrollbarHitTest();

                    sbi.cbSize = Marshal.SizeOf(sbi);
                    SendMessage(_hScrollBarWnd, SBM_GETSCROLLBARINFO, 0, ref sbi);

                    if (Direction == Orientation.Horizontal)
                    {
                        // draw the track
                        using (StretchImage si = new StretchImage(_cTrackDc.Hdc, tempDc.Hdc, new Rectangle(0, 0, _cTrackDc.Width, _cTrackDc.Height), new Rectangle(_iArrowCx, 0, tr.Right - (2 * _iArrowCx), tr.Bottom), 2, StretchModeEnum.STRETCH_HALFTONE)) { }
                        // draw the arrows
                        section = 7;
                        width = _cArrowDc.Width / section;
                        // left arrow
                        if (hitTest == SB_HITEST.leftArrow)
                        {
                            if (leftKeyPressed())
                                offset = 2;
                            else
                                offset = 1;
                        }
                        else
                        {
                            offset = 0;
                        }
                        using (StretchImage si = new StretchImage(_cArrowDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cArrowDc.Height), new Rectangle(0, 0, _iArrowCx, tr.Bottom), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                        // right arrow
                        if (hitTest == SB_HITEST.rightArrow)
                        {
                            if (leftKeyPressed())
                                offset = 5;
                            else
                                offset = 4;
                        }
                        else
                        {
                            offset = 3;
                        }
                        using (StretchImage si = new StretchImage(_cArrowDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cArrowDc.Height), new Rectangle(tr.Right - _iArrowCx, 0, _iArrowCx, tr.Bottom), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                        // draw the thumb
                        section = 3;
                        width = _cThumbDc.Width / section;
                        if (hitTest == SB_HITEST.button)
                        {
                            if (leftKeyPressed())
                                offset = 2;
                            else
                                offset = 1;
                        }
                        else
                        {
                            offset = 0;
                        }
                        Point pst = getScrollBarThumb();
                        using (StretchImage si = new StretchImage(_cThumbDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cThumbDc.Height), new Rectangle(pst.X, 0, pst.Y - pst.X, tr.Bottom), 2, StretchModeEnum.STRETCH_HALFTONE)) { }
                    }
                    else
                    {
                        // draw the track
                        using (StretchImage si = new StretchImage(_cTrackDc.Hdc, tempDc.Hdc, new Rectangle(0, 0, _cTrackDc.Width, _cTrackDc.Height), new Rectangle(0, _iArrowCy, tr.Right, tr.Bottom - (2 * _iArrowCy)), 2, StretchModeEnum.STRETCH_HALFTONE)) { }
                        section = 6;
                        width = _cArrowDc.Width / section;

                        // top arrow
                        if (hitTest == SB_HITEST.topArrow)
                        {
                            if (leftKeyPressed())
                                offset = 2;
                            else
                                offset = 1;
                        }
                        else
                        {
                            offset = 0;
                        }
                        using (StretchImage si = new StretchImage(_cArrowDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cArrowDc.Height), new Rectangle(0, 0, tr.Right, _iArrowCy), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                        // bottom arrow
                        if (hitTest == SB_HITEST.bottomArrow)
                        {
                            if (leftKeyPressed())
                                offset = 5;
                            else
                                offset = 4;
                        }
                        else
                        {
                            offset = 3;
                        }
                        using (StretchImage si = new StretchImage(_cArrowDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cArrowDc.Height), new Rectangle(0, tr.Bottom - _iArrowCy, tr.Right, _iArrowCy), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                        // draw the thumb
                        section = 3;
                        width = _cThumbDc.Width / section;
                        if (hitTest == SB_HITEST.button)
                        {
                            if (leftKeyPressed())
                                offset = 2;
                            else
                                offset = 1;
                        }
                        else
                        {
                            offset = 0;
                        }
                        Point pst = getScrollBarThumb();
                        using (StretchImage si = new StretchImage(_cThumbDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cThumbDc.Height), new Rectangle(0, pst.X, _iArrowCx, pst.Y - pst.X), 2, StretchModeEnum.STRETCH_HALFTONE)) { }
                    }
                    IntPtr hdc = GetDC(_hMaskWnd);
                    BitBlt(hdc, 0, 0, tr.Right, tr.Bottom, tempDc.Hdc, 0, 0, 0xCC0020);
                    ReleaseDC(_hMaskWnd, hdc);
                    tempDc.Dispose();
                }

                private RECT focusedPartSize()
                {
                    RECT tr = new RECT();
                    GetWindowRect(_hScrollBarWnd, ref tr);
                    OffsetRect(ref tr, -tr.Left, -tr.Top);

                    switch (scrollbarHitTest())
                    {
                        case SB_HITEST.leftArrow:
                            tr = new RECT(0, 0, _iArrowCx, tr.Bottom);
                            break;

                        case SB_HITEST.rightArrow:
                            tr = new RECT(tr.Right - _iArrowCx, 0, tr.Right, tr.Bottom);
                            break;

                        case SB_HITEST.topArrow:
                            tr = new RECT(0, 0, tr.Right, _iArrowCy);
                            break;

                        case SB_HITEST.bottomArrow:
                            tr = new RECT(0, tr.Bottom - _iArrowCy, tr.Right, _iArrowCy);
                            break;

                        case SB_HITEST.button:
                            Point pst = getScrollBarThumb();
                            if (Direction == Orientation.Horizontal)
                                tr = new RECT(pst.X, 2, pst.Y - pst.X, tr.Bottom);
                            else
                                tr = new RECT(0, pst.X, _iArrowCx, pst.Y - pst.X);
                            break;

                    }
                    return tr;
                }

                private RECT getScrollBarRect()
                {
                    SCROLLBARINFO sbi = new SCROLLBARINFO();
                    sbi.cbSize = Marshal.SizeOf(sbi);
                    SendMessage(_hScrollBarWnd, SBM_GETSCROLLBARINFO, 0, ref sbi);
                    return sbi.rcScrollBar;
                }

                public void sizeCheck()
                {
                    RECT tr = new RECT();
                    RECT tw = new RECT();
                    GetWindowRect(_hMaskWnd, ref tw);
                    GetWindowRect(_hScrollBarWnd, ref tr);
                    if (!EqualRect(ref tr, ref tw))
                        SetWindowPos(_hMaskWnd, IntPtr.Zero, tr.Left, tr.Top, tr.Right - tr.Left, tr.Bottom - tr.Top, SWP_NOACTIVATE | SWP_NOOWNERZORDER | SWP_NOZORDER);
                }

                private Point getScrollBarThumb()
                {
                    Point pt = new Point();
                    ScrollBar sc = (ScrollBar)Control.FromHandle(_hScrollBarWnd);
                    float incr = 0;

                    if (sc.ClientRectangle.Width > sc.ClientRectangle.Height)
                    {
                        incr = ((float)(sc.ClientRectangle.Width - (_iArrowCx * 2)) / sc.Maximum);
                        pt.X = (int)(incr * sc.Value) + _iArrowCx;
                        pt.Y = pt.X + (int)(incr * sc.LargeChange);
                    }
                    else
                    {
                        incr = ((float)(sc.ClientRectangle.Height - (_iArrowCy * 2)) / sc.Maximum);
                        pt.X = (int)(incr * sc.Value) + _iArrowCy;
                        pt.Y = pt.X + (int)(incr * sc.LargeChange);
                    }
                    // fix for ?bug? in scrollbar
                    if (sc.Value < sc.Maximum)
                    {
                        sc.Value++;
                        sc.Value--;
                    }
                    else
                    {
                        sc.Value--;
                        sc.Value++;
                    }
                    invalidateWindow(true);
                    return pt;
                }

                private void invalidateWindow(bool messaged)
                {
                    if (messaged)
                        RedrawWindow(_hScrollBarWnd, IntPtr.Zero, IntPtr.Zero, RDW_INTERNALPAINT);
                    else
                        RedrawWindow(_hScrollBarWnd, IntPtr.Zero, IntPtr.Zero, RDW_INVALIDATE | RDW_UPDATENOW);
                }

                private bool leftKeyPressed()
                {
                    if (mouseButtonsSwitched())
                        return (GetKeyState(VK_RBUTTON) < 0);
                    else
                        return (GetKeyState(VK_LBUTTON) < 0);
                }

                private bool mouseButtonsSwitched()
                {
                    return (GetSystemMetrics(SYSTEM_METRICS.SM_SWAPBUTTON) != 0);
                }

                private SB_HITEST scrollbarHitTest()
                {
                    Point pt = new Point();
                    RECT tr = new RECT();

                    GetWindowRect(_hScrollBarWnd, ref tr);
                    OffsetRect(ref tr, -tr.Left, -tr.Top);

                    RECT tp = tr;
                    GetCursorPos(ref pt);
                    ScreenToClient(_hScrollBarWnd, ref pt);

                    if (Direction == Orientation.Horizontal)
                    {
                        if (PtInRect(ref tr, pt))
                        {
                            // left arrow
                            tp.Right = _iArrowCx;
                            if (PtInRect(ref tp, pt))
                                return SB_HITEST.leftArrow;
                            // right arrow
                            tp.Left = tr.Right - _iArrowCx;
                            tp.Right = tr.Right;
                            if (PtInRect(ref tp, pt))
                                return SB_HITEST.rightArrow;
                            // button
                            Point pb = getScrollBarThumb();
                            tp.Left = pb.X;
                            tp.Right = pb.Y;
                            if (PtInRect(ref tp, pt))
                                return SB_HITEST.button;
                            // track
                            return SB_HITEST.track;
                        }
                    }
                    else
                    {
                        if (PtInRect(ref tr, pt))
                        {
                            // top arrow
                            tp.Bottom = _iArrowCy;
                            if (PtInRect(ref tp, pt))
                                return SB_HITEST.topArrow;
                            // bottom arrow
                            tp.Top = tr.Bottom - _iArrowCy;
                            tp.Bottom = tr.Bottom;
                            if (PtInRect(ref tp, pt))
                                return SB_HITEST.bottomArrow;
                            // button
                            Point pb = getScrollBarThumb();
                            tp.Top = pb.X;
                            tp.Bottom = pb.Y;
                            if (PtInRect(ref tp, pt))
                                return SB_HITEST.button;
                            // track
                            return SB_HITEST.track;
                        }
                    }
                    return SB_HITEST.offControl;
                }

                private void scrollbarMetrics()
                {
                    if (Direction == Orientation.Horizontal)
                    {
                        _iArrowCx = GetSystemMetrics(SYSTEM_METRICS.SM_CXHSCROLL);
                        _iArrowCy = GetSystemMetrics(SYSTEM_METRICS.SM_CYHSCROLL);
                    }
                    else
                    {
                        _iArrowCx = GetSystemMetrics(SYSTEM_METRICS.SM_CXVSCROLL);
                        _iArrowCy = GetSystemMetrics(SYSTEM_METRICS.SM_CYVSCROLL);
                    }
                }

                private void scrollHorizontal(bool Right)
                {
                    if (Right)
                        SendMessage(_hScrollBarWnd, WM_HSCROLL, SB_LINERIGHT, 0);
                    else
                        SendMessage(_hScrollBarWnd, WM_HSCROLL, SB_LINELEFT, 0);

                }

                private void scrollVertical(bool Down)
                {
                    if (Down)
                        SendMessage(_hScrollBarWnd, WM_VSCROLL, SB_LINEDOWN, 0);
                    else
                        SendMessage(_hScrollBarWnd, WM_VSCROLL, SB_LINEUP, 0);
                }

                private void scrollFader()
                {

                    SB_HITEST hitTest = scrollbarHitTest();
                    if (hitTest == SB_HITEST.button)
                    {
                        Point pst = getScrollBarThumb();
                        if (TransitionGraphic != null)
                        {
                            cTransition ts;
                            RECT tr = new RECT();
                            if (Direction == Orientation.Horizontal)
                            {
                                GetWindowRect(_hScrollBarWnd, ref tr);
                                ts = new cTransition(_hMaskWnd, _hScrollBarWnd, TransitionGraphic, new Rectangle(pst.X, 0, pst.Y - pst.X, tr.Bottom));
                            }
                            else
                            {
                                ts = new cTransition(_hMaskWnd, _hScrollBarWnd, TransitionGraphic, new Rectangle(0, pst.X, _iArrowCx, pst.Y - pst.X));
                            }
                        }
                    }
                }
                #endregion

                #region WndProc
                protected override void WndProc(ref Message m)
                {
                    PAINTSTRUCT ps = new PAINTSTRUCT();

                    switch (m.Msg)
                    {

                        case 0x83:
                        case WM_PAINT:
                            if (!_bPainting)
                            {
                                _bPainting = true;
                                // start painting engine
                                BeginPaint(m.HWnd, ref ps);
                                drawScrollBar();
                                ValidateRect(m.HWnd, ref ps.rcPaint);
                                // done
                                EndPaint(m.HWnd, ref ps);
                                _bPainting = false;
                                m.Result = MSG_HANDLED;
                            }
                            else
                            {
                                base.WndProc(ref m);
                            }
                            break;

                        case WM_SIZE:
                        case WM_MOVE:
                            sizeCheck();
                            base.WndProc(ref m);
                            break;

                        default:
                            base.WndProc(ref m);
                            break;
                    }
                }
                #endregion
            }










        }























        #region Enums
        public enum StretchModeEnum : int
        {
            STRETCH_ANDSCANS = 1,
            STRETCH_ORSCANS = 2,
            STRETCH_DELETESCANS = 3,
            STRETCH_HALFTONE = 4,
        }
        #endregion

        #region GraphicsMode
        public class GraphicsMode : IDisposable
        {
            #region Instance Fields
            private Graphics _cGraphicCopy;
            private SmoothingMode _eOldMode;
            #endregion

            #region Identity
            /// <summary>
            /// Initialize a new instance of the class.
            /// </summary>
            /// <param name="g">Graphics instance.</param>
            /// <param name="mode">Desired Smoothing mode.</param>
            public GraphicsMode(Graphics g, SmoothingMode mode)
            {
                _cGraphicCopy = g;
                _eOldMode = _cGraphicCopy.SmoothingMode;
                _cGraphicCopy.SmoothingMode = mode;
            }

            /// <summary>
            /// Revert the SmoothingMode to original setting.
            /// </summary>
            public void Dispose()
            {
                _cGraphicCopy.SmoothingMode = _eOldMode;
            }
            #endregion
        }
        #endregion

        #region StretchMode
        public class StretchMode : IDisposable
        {
            #region API
            [DllImport("gdi32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool SetStretchBltMode(IntPtr hdc, StretchModeEnum iStretchMode);

            [DllImport("gdi32.dll")]
            private static extern int GetStretchBltMode(IntPtr hdc);
            #endregion

            #region Fields
            private StretchModeEnum _eOldMode = StretchModeEnum.STRETCH_ANDSCANS;
            private IntPtr _pHdc = IntPtr.Zero;
            #endregion

            public StretchMode(IntPtr hdc, StretchModeEnum mode)
            {
                _eOldMode = (StretchModeEnum)GetStretchBltMode(hdc);
                _pHdc = hdc;
                SetStretchBltMode(hdc, mode);
            }

            public void Dispose()
            {
                SetStretchBltMode(_pHdc, _eOldMode);
            }
        }
        #endregion

        #region StretchImage
        public class StretchImage : IDisposable
        {
            #region API
            [DllImport("gdi32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool SetStretchBltMode(IntPtr hdc, StretchModeEnum eStretchMode);

            [DllImport("gdi32.dll")]
            private static extern int GetStretchBltMode(IntPtr hdc);

            [DllImport("gdi32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool StretchBlt(IntPtr hDest, int X, int Y, int nWidth, int nHeight, IntPtr hdcSrc,
            int sX, int sY, int nWidthSrc, int nHeightSrc, int dwRop);
            #endregion

            #region Fields
            private StretchModeEnum _eOldMode = StretchModeEnum.STRETCH_ANDSCANS;
            private IntPtr _pHdc = IntPtr.Zero;
            #endregion

            public StretchImage(IntPtr sourceDc, IntPtr destDc, Rectangle src, Rectangle dest, int depth, StretchModeEnum eStretchMode)
            {
                _eOldMode = (StretchModeEnum)GetStretchBltMode(sourceDc);
                _pHdc = sourceDc;
                SetStretchBltMode(sourceDc, eStretchMode);

                // left
                StretchBlt(destDc, dest.Left, dest.Top, depth, dest.Height, sourceDc, src.Left, 0, depth, src.Height, 0xCC0020);
                // right
                StretchBlt(destDc, dest.Right - depth, dest.Top, depth, dest.Height, sourceDc, src.Right - depth, 0, depth, src.Height, 0xCC0020);
                // top
                StretchBlt(destDc, dest.Left + depth, dest.Top, dest.Width - (2 * depth), depth, sourceDc, src.Left + depth, 0, src.Width - (2 * depth), depth, 0xCC0020);
                // bottom
                StretchBlt(destDc, dest.Left + depth, dest.Bottom - depth, dest.Width - (2 * depth), depth, sourceDc, src.Left + depth, src.Bottom - depth, src.Width - (2 * depth), depth, 0xCC0020);
                // center
                StretchBlt(destDc, dest.Left + depth, dest.Top + depth, dest.Width - (2 * depth), dest.Height - (2 * depth), sourceDc, src.Left + depth, depth, src.Width - (2 * depth), src.Height - (2 * depth), 0xCC0020);
            }

            public void Dispose()
            {
                SetStretchBltMode(_pHdc, _eOldMode);
            }
        }
        #endregion

        #region AlphaStretch
        public class AlphaStretch : IDisposable
        {
            private const byte AC_SRC_OVER = 0x00;
            private const byte AC_SRC_ALPHA = 0x01;

            [StructLayout(LayoutKind.Sequential)]
            private struct BLENDFUNCTION
            {
                byte BlendOp;
                byte BlendFlags;
                byte SourceConstantAlpha;
                byte AlphaFormat;

                internal BLENDFUNCTION(byte op, byte flags, byte alpha, byte format)
                {
                    BlendOp = op;
                    BlendFlags = flags;
                    SourceConstantAlpha = alpha;
                    AlphaFormat = format;
                }
            }

            [DllImport("gdi32.dll", EntryPoint = "GdiAlphaBlend")]
            private static extern bool AlphaBlend(IntPtr hdcDest, int nXOriginDest, int nYOriginDest, int nWidthDest, int nHeightDest,
            IntPtr hdcSrc, int nXOriginSrc, int nYOriginSrc, int nWidthSrc, int nHeightSrc, BLENDFUNCTION blendFunction);

            public AlphaStretch(IntPtr sourceDc, IntPtr destDc, Rectangle src, Rectangle dest, int depth, byte opacity)
            {
                BLENDFUNCTION bf = new BLENDFUNCTION(AC_SRC_OVER, 0x0, opacity, 0x0);
                // left
                AlphaBlend(destDc, dest.Left, dest.Top, depth, dest.Height, sourceDc, src.Left, 0, depth, src.Height, bf);
                // right
                AlphaBlend(destDc, dest.Right - depth, dest.Top, depth, dest.Height, sourceDc, src.Right - depth, 0, depth, src.Height, bf);
                // top
                AlphaBlend(destDc, dest.Left + depth, dest.Top, dest.Width - (2 * depth), depth, sourceDc, src.Left + depth, 0, src.Width - (2 * depth), depth, bf);
                // bottom
                AlphaBlend(destDc, dest.Left + depth, dest.Bottom - depth, dest.Width - (2 * depth), depth, sourceDc, src.Left + depth, src.Bottom - depth, src.Width - (2 * depth), depth, bf);
                // center
                AlphaBlend(destDc, dest.Left + depth, dest.Top + depth, dest.Width - (2 * depth), dest.Height - (2 * depth), sourceDc, src.Left + depth, depth, src.Width - (2 * depth), src.Height - (2 * depth), bf);
            }

            public void Dispose()
            {
                //
            }
        }
        #endregion























        [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust")]
        public class cTransition : NativeWindow, IDisposable
        {
            #region Constants
            private const int WM_PAINT = 0xF;
            private const int WM_NCPAINT = 0x85;
            private const int WM_TIMER = 0x113;
            private const int WM_NCMOUSEMOVE = 0xA0;
            private const int WM_MOUSEMOVE = 0x200;
            private const int WM_MOUSELEAVE = 0x2A3;
            private const int WM_LBUTTONDOWN = 0x201;
            private const int WM_RBUTTONDOWN = 0x204;
            #endregion

            #region Structs
            [StructLayout(LayoutKind.Sequential)]
            internal struct RECT
            {
                internal RECT(int X, int Y, int Width, int Height)
                {
                    this.Left = X;
                    this.Top = Y;
                    this.Right = Width;
                    this.Bottom = Height;
                }
                internal int Left;
                internal int Top;
                internal int Right;
                internal int Bottom;
            }

            [StructLayout(LayoutKind.Sequential)]
            private struct PAINTSTRUCT
            {
                internal IntPtr hdc;
                internal int fErase;
                internal RECT rcPaint;
                internal int fRestore;
                internal int fIncUpdate;
                internal int Reserved1;
                internal int Reserved2;
                internal int Reserved3;
                internal int Reserved4;
                internal int Reserved5;
                internal int Reserved6;
                internal int Reserved7;
                internal int Reserved8;
            }
            #endregion

            #region API
            [DllImport("user32.dll")]
            private static extern IntPtr BeginPaint(IntPtr hWnd, ref PAINTSTRUCT ps);

            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool EndPaint(IntPtr hWnd, ref PAINTSTRUCT ps);

            [DllImport("user32.dll")]
            private static extern IntPtr GetDC(IntPtr handle);

            [DllImport("user32.dll")]
            private static extern int ReleaseDC(IntPtr handle, IntPtr hdc);

            [DllImport("gdi32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool BitBlt(IntPtr hdc, int nXDest, int nYDest, int nWidth, int nHeight, IntPtr hdcSrc, int nXSrc, int nYSrc, int dwRop);

            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool PtInRect([In] ref RECT lprc, Point pt);

            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool GetCursorPos(ref Point lpPoint);

            [DllImport("user32.dll")]
            private static extern int ScreenToClient(IntPtr hwnd, ref Point lpPoint);

            [DllImport("gdi32.dll")]
            private static extern IntPtr SelectObject(IntPtr hdc, IntPtr hObject);

            [DllImport("user32.dll")]
            private static extern IntPtr GetDesktopWindow();

            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool GetWindowRect(IntPtr hWnd, ref RECT lpRect);

            [DllImport("user32.dll")]
            private extern static int OffsetRect(ref RECT lpRect, int x, int y);

            [DllImport("user32.dll")]
            private static extern IntPtr SetTimer(IntPtr hWnd, int nIDEvent, uint uElapse, IntPtr lpTimerFunc);

            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool KillTimer(IntPtr hWnd, uint uIDEvent);

            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool ValidateRect(IntPtr hWnd, ref RECT lpRect);
            #endregion

            #region Fields
            private bool _bPainting = false;
            private bool _bFadeIn = false;
            private int _maskTimer = 0;
            private int _safeTimer = 0;
            private Rectangle _areaRect;
            private IntPtr _hControlWnd = IntPtr.Zero;
            private IntPtr _hParentWnd = IntPtr.Zero;
            private Bitmap _oMask;
            private cStoreDc _cMaskDc = new cStoreDc();
            private cStoreDc _cTransitionDc = new cStoreDc();
            #endregion

            #region Events
            public delegate void DisposingDelegate();
            public event DisposingDelegate Disposing;
            #endregion

            #region Constructor
            public cTransition(IntPtr hWnd, IntPtr hParent, Bitmap mask, Rectangle area)
            {
                if (hWnd == IntPtr.Zero)
                    throw new Exception("The control handle is invalid.");
                _hControlWnd = hWnd;
                if (mask != null)
                {
                    TransitionGraphic = mask;
                    _areaRect = area;
                    if (hParent != IntPtr.Zero)
                    {
                        _hParentWnd = hParent;
                        this.AssignHandle(_hParentWnd);
                    }
                    else
                    {
                        _hParentWnd = _hControlWnd;
                        this.AssignHandle(_hControlWnd);
                    }
                    startTimer();
                }
            }

            public void Dispose()
            {
                this.ReleaseHandle();
                if (Disposing != null) Disposing();
                stopTimer();
                if (_cTransitionDc != null) _cTransitionDc.Dispose();
                if (_cMaskDc != null) _cMaskDc.Dispose();
                GC.SuppressFinalize(this);
            }
            #endregion

            #region Properties
            private Bitmap TransitionGraphic
            {
                get { return _oMask; }
                set
                {
                    _oMask = value;
                    if (_cTransitionDc.Hdc != IntPtr.Zero)
                    {
                        _cTransitionDc.Dispose();
                        _cTransitionDc = new cStoreDc();
                    }
                    _cTransitionDc.Width = _oMask.Width;
                    _cTransitionDc.Height = _oMask.Height;
                    SelectObject(_cTransitionDc.Hdc, _oMask.GetHbitmap());
                }
            }
            #endregion

            #region Methods
            private void startTimer()
            {
                if (_safeTimer > 0)
                    stopTimer();
                SetTimer(_hParentWnd, 66, 25, IntPtr.Zero);
            }

            private void stopTimer()
            {
                if (_safeTimer > 0)
                {
                    KillTimer(_hParentWnd, 66);
                    _safeTimer = 0;
                }
            }

            private void fadeIn()
            {
                if (_bFadeIn == false)
                {
                    _bFadeIn = true;
                    captureDc();
                }
                if (_maskTimer < 10)
                    _maskTimer++;
                drawMask();

            }

            private void fadeOut()
            {
                if (_bFadeIn == true)
                    _bFadeIn = false;
                if (_maskTimer > 1)
                {
                    _maskTimer--;
                    drawMask();
                }
                else
                {
                    Control ct = Control.FromHandle(_hParentWnd);
                    if (ct != null)
                        ct.Refresh();
                    this.Dispose();
                }
            }

            private void drawMask()
            {
                byte bt = 0;
                IntPtr hdc = IntPtr.Zero;
                cStoreDc tempDc = new cStoreDc();
                RECT tr = new RECT();

                GetWindowRect(_hControlWnd, ref tr);
                OffsetRect(ref tr, -tr.Left, -tr.Top);

                Rectangle bounds = new Rectangle(_areaRect.Left, _areaRect.Top, _areaRect.Right - _areaRect.Left, _areaRect.Bottom - _areaRect.Top);
                tempDc.Width = tr.Right;
                tempDc.Height = tr.Bottom;

                bt = (byte)(_maskTimer * 15);
                BitBlt(tempDc.Hdc, 0, 0, tr.Right, tr.Bottom, _cMaskDc.Hdc, 0, 0, 0xCC0020);
                using (AlphaStretch al = new AlphaStretch(_cTransitionDc.Hdc, tempDc.Hdc, new Rectangle(0, 0, _cTransitionDc.Width, _cTransitionDc.Height), bounds, 2, bt)) { }
                hdc = GetDC(_hControlWnd);
                BitBlt(hdc, 0, 0, tr.Right, tr.Bottom, tempDc.Hdc, 0, 0, 0xCC0020);
                ReleaseDC(_hControlWnd, hdc);
                tempDc.Dispose();
                ValidateRect(_hControlWnd, ref tr);
            }

            private void captureDc()
            {
                RECT tr = new RECT();
                GetWindowRect(_hControlWnd, ref tr);

                _cMaskDc.Width = tr.Right - tr.Left;
                _cMaskDc.Height = tr.Bottom - tr.Top;
                if (_cMaskDc.Hdc != IntPtr.Zero)
                {
                    using (Graphics g = Graphics.FromHdc(_cMaskDc.Hdc))
                        g.CopyFromScreen(tr.Left, tr.Top, 0, 0, new Size(_cMaskDc.Width, _cMaskDc.Height), CopyPixelOperation.SourceCopy);
                }
            }

            private bool inArea()
            {
                Point pt = new Point();
                RECT tr = new RECT(_areaRect.Left, _areaRect.Top, _areaRect.Right, _areaRect.Bottom);
                GetCursorPos(ref pt);
                ScreenToClient(_hParentWnd, ref pt);
                if (PtInRect(ref tr, pt))
                    return true;
                return false;
            }
            #endregion

            #region WndProc
            protected override void WndProc(ref Message m)
            {
                PAINTSTRUCT ps = new PAINTSTRUCT();

                switch (m.Msg)
                {
                    case WM_PAINT:
                        if (!_bPainting && _maskTimer > 1)
                        {
                            _bPainting = true;
                            // start painting engine
                            BeginPaint(m.HWnd, ref ps);
                            drawMask();
                            // done
                            EndPaint(m.HWnd, ref ps);
                            _bPainting = false;
                        }
                        else
                        {
                            base.WndProc(ref m);
                        }
                        break;

                    case WM_TIMER:
                        if ((_safeTimer > 50) && (!inArea()))
                        {
                            stopTimer();
                        }
                        else
                        {
                            if (_bFadeIn == true)
                                fadeIn();
                            else
                                fadeOut();
                        }
                        _safeTimer++;
                        base.WndProc(ref m);
                        break;

                    case WM_MOUSEMOVE:
                        if (inArea())
                            fadeIn();
                        else
                            fadeOut();
                        base.WndProc(ref m);
                        break;

                    case WM_MOUSELEAVE:
                        fadeOut();
                        base.WndProc(ref m);
                        break;

                    case WM_LBUTTONDOWN:
                        Dispose();
                        base.WndProc(ref m);
                        break;

                    default:
                        base.WndProc(ref m);
                        break;
                }
            }
            #endregion
        }



























        #region StoreDc
        [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust")]
        public class cStoreDc
        {
            [DllImport("gdi32.dll")]
            private static extern IntPtr CreateDCA([MarshalAs(UnmanagedType.LPStr)]string lpszDriver, [MarshalAs(UnmanagedType.LPStr)]string lpszDevice, [MarshalAs(UnmanagedType.LPStr)]string lpszOutput, int lpInitData);

            [DllImport("gdi32.dll")]
            private static extern IntPtr CreateDCW([MarshalAs(UnmanagedType.LPWStr)]string lpszDriver, [MarshalAs(UnmanagedType.LPWStr)]string lpszDevice, [MarshalAs(UnmanagedType.LPWStr)]string lpszOutput, int lpInitData);

            [DllImport("gdi32.dll")]
            private static extern IntPtr CreateDC(string lpszDriver, string lpszDevice, string lpszOutput, int lpInitData);

            [DllImport("gdi32.dll")]
            private static extern IntPtr CreateCompatibleDC(IntPtr hdc);

            [DllImport("gdi32.dll")]
            private static extern IntPtr CreateCompatibleBitmap(IntPtr hdc, int nWidth, int nHeight);

            [DllImport("gdi32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool DeleteDC(IntPtr hdc);

            [DllImport("gdi32.dll", PreserveSig = true)]
            private static extern IntPtr SelectObject(IntPtr hdc, IntPtr hgdiobj);

            [DllImport("gdi32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool DeleteObject(IntPtr hObject);

            private int _iHeight = 0;
            private int _iWidth = 0;
            private IntPtr _pHdc = IntPtr.Zero;
            private IntPtr _pBmp = IntPtr.Zero;
            private IntPtr _pBmpOld = IntPtr.Zero;

            public IntPtr Hdc
            {
                get { return _pHdc; }
            }

            public IntPtr HBmp
            {
                get { return _pBmp; }
            }

            public int Height
            {
                get { return _iHeight; }
                set
                {
                    if (_iHeight != value)
                    {
                        _iHeight = value;
                        ImageCreate(_iWidth, _iHeight);
                    }
                }
            }

            public int Width
            {
                get { return _iWidth; }
                set
                {
                    if (_iWidth != value)
                    {
                        _iWidth = value;
                        ImageCreate(_iWidth, _iHeight);
                    }
                }
            }

            private void ImageCreate(int width, int height)
            {
                IntPtr pHdc = IntPtr.Zero;

                ImageDestroy();
                pHdc = CreateDCA("DISPLAY", "", "", 0);
                _pHdc = CreateCompatibleDC(pHdc);
                _pBmp = CreateCompatibleBitmap(pHdc, _iWidth, _iHeight);
                _pBmpOld = SelectObject(_pHdc, _pBmp);
                if (_pBmpOld == IntPtr.Zero)
                {
                    ImageDestroy();
                }
                else
                {
                    _iWidth = width;
                    _iHeight = height;
                }
                DeleteDC(pHdc);
                pHdc = IntPtr.Zero;
            }

            private void ImageDestroy()
            {
                if (_pBmpOld != IntPtr.Zero)
                {
                    SelectObject(_pHdc, _pBmpOld);
                    _pBmpOld = IntPtr.Zero;
                }
                if (_pBmp != IntPtr.Zero)
                {
                    DeleteObject(_pBmp);
                    _pBmp = IntPtr.Zero;
                }
                if (_pHdc != IntPtr.Zero)
                {
                    DeleteDC(_pHdc);
                    _pHdc = IntPtr.Zero;
                }
            }

            public void Dispose()
            {
                ImageDestroy();
            }
        }
        #endregion



























    }
}