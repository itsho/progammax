﻿
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
namespace ProGammaX
{
    public class ShowMessage : Form
    {
        private Panel pnlShowMessage;
        private PictureBox pictureBox1;
        private System.ComponentModel.IContainer components;
        //private ImageList imageList1;
        private Label lblMessageText;
        //public System.Windows.Forms.Form noHost;
        #region properties
        public enumMessageButton ButtonYesNo
        {
            get
            {
                return enumMessageButton.YesNo;
            }
            set
            {
                ButtonYesNo = value;
            }
        }
        public enumMessageButton ButtonOK
        {
            get
            {
                return enumMessageButton.OK;
            }
            set
            {
                ButtonOK = value;

            }
        }
        public enumMessageButton ButtonYesNoCancel
        {
            get
            {
                return enumMessageButton.YesNoCancel;
            }
            set
            {
                ButtonYesNoCancel = value;
            }
        }
        public enumMessageButton ButtonAbortRetryIgnore
        {
            get
            {
                return enumMessageButton.AbortRetryIgnore;
            }
            set
            {
                ButtonAbortRetryIgnore = value;
            }
        }
        public enumMessageButton ButtonOKCancel
        {
            get
            {
                return enumMessageButton.OKCancel;
            }

            set
            {
                ButtonOKCancel = value;
            }
        }
        public enumMessageIcon IconError
        {
            get
            {
                return enumMessageIcon.Error;
            }

            set
            {
                IconError = value;
            }
        }
        public enumMessageIcon IconQuestion
        {
            get
            {
                return enumMessageIcon.Question;
            }

            set
            {
                IconQuestion = value;
            }
        }
        public enumMessageIcon IconInformation
        {
            get
            {
                return enumMessageIcon.Information;
            }

            set
            {
                IconInformation = value;
            }
        }
        public enumMessageIcon IconWarning
        {
            get
            {
                return enumMessageIcon.Warning;
            }

            set
            {
                IconWarning = value;
            }
        }

        #endregion
        public ShowMessage()
        {
            InitializeComponent();
            
            ProGammaX.MainForm.mainFormRef.applySelectedTheme(this);
        }


        //private System.ComponentModel.IContainer components = null;

        /// Here I am overriding Paint method of form object
        /// and set it's background color as gradient. Here I am
        /// using LinearGradientBrush class object to make gradient
        /// color which comes in System.Drawing.Drawing2D namespace.
        ///
        ///
        //protected override void OnPaint(PaintEventArgs e)
        //{
        //    Rectangle rect = this.ClientRectangle;
        //    LinearGradientBrush brush = new LinearGradientBrush(rect, Color.SkyBlue, Color.AliceBlue, 60);
        //    e.Graphics.FillRectangle(brush, rect);
        //    base.OnPaint(e);
        //}

        ///
        /// setMessage method is used to display message
        /// on form and it's height adjust automatically.
        /// I am displaying message in a Label control.
        ///
        /// Message which needs to be displayed to user.
        private void setMessage(string messageText)
        {
            int number = Math.Abs(messageText.Length / 30);
            if (number != 0)
                this.lblMessageText.Height = number * 25;
            this.lblMessageText.Text = messageText;
        }

        ///
        /// This method is used to add button on message box.
        ///
        /// MessageButton is type of enumMessageButton
        /// through which I get type of button which needs to be displayed.
        private void addButton(enumMessageButton MessageButton)
        {
            switch (MessageButton)
            {
                case enumMessageButton.OK:
                    {
                        //If type of enumButton is OK then we add OK button only.
                        button btnOk = new button(); //Create object of Button.
                        btnOk.Text = "OK"; //Here we set text of Button.
                        btnOk.DialogResult = DialogResult.OK; //Set DialogResult property of button.
                        btnOk.FlatStyle = FlatStyle.Popup; //Set flat appearence of button.
                        btnOk.FlatAppearance.BorderSize = 0;
                        btnOk.SetBounds(pnlShowMessage.ClientSize.Width - 80, 5, 75, 25); // Set bounds of button.
                        pnlShowMessage.Controls.Add(btnOk); //Finally Add button control on panel.
                    }
                    break;
                case enumMessageButton.OKCancel:
                    {
                        button btnOk = new button();
                        btnOk.Text = "OK";
                        btnOk.DialogResult = DialogResult.OK;
                        btnOk.FlatStyle = FlatStyle.Popup;
                        btnOk.FlatAppearance.BorderSize = 0;
                        btnOk.SetBounds((pnlShowMessage.ClientSize.Width - 70), 5, 65, 25);
                        pnlShowMessage.Controls.Add(btnOk);

                        button btnCancel = new button();
                        btnCancel.Text = "Cancel";
                        btnCancel.DialogResult = DialogResult.Cancel;
                        btnCancel.FlatStyle = FlatStyle.Popup;
                        btnCancel.FlatAppearance.BorderSize = 0;
                        btnCancel.SetBounds((pnlShowMessage.ClientSize.Width - (btnOk.ClientSize.Width + 5 + 80)), 5, 75, 25);
                        pnlShowMessage.Controls.Add(btnCancel);

                    }
                    break;
                case enumMessageButton.YesNo:
                    {
                        button btnNo = new button();
                        btnNo.Text = "No";
                        btnNo.DialogResult = DialogResult.No;
                        btnNo.FlatStyle = FlatStyle.Popup;
                        btnNo.FlatAppearance.BorderSize = 0;
                        btnNo.SetBounds((pnlShowMessage.ClientSize.Width - 70), 5, 65, 25);
                        pnlShowMessage.Controls.Add(btnNo);

                        button btnYes = new button();
                        btnYes.Text = "Yes";
                        btnYes.DialogResult = DialogResult.Yes;
                        btnYes.FlatStyle = FlatStyle.Popup;
                        btnYes.FlatAppearance.BorderSize = 0;
                        btnYes.SetBounds((pnlShowMessage.ClientSize.Width - (btnNo.ClientSize.Width + 5 + 80)), 5, 75, 25);
                        pnlShowMessage.Controls.Add(btnYes);
                    }
                    break;
                case enumMessageButton.YesNoCancel:
                    {
                        button btnCancel = new button();
                        btnCancel.Text = "Cancel";
                        btnCancel.DialogResult = DialogResult.Cancel;
                        btnCancel.FlatStyle = FlatStyle.Popup;
                        btnCancel.FlatAppearance.BorderSize = 0;
                        btnCancel.SetBounds((pnlShowMessage.ClientSize.Width - 70), 5, 65, 25);
                        pnlShowMessage.Controls.Add(btnCancel);

                        button btnNo = new button();
                        btnNo.Text = "No";
                        btnNo.DialogResult = DialogResult.No;
                        btnNo.FlatStyle = FlatStyle.Popup;
                        btnNo.FlatAppearance.BorderSize = 0;
                        btnNo.SetBounds((pnlShowMessage.ClientSize.Width - (btnCancel.ClientSize.Width + 5 + 80)), 5, 75, 25);
                        pnlShowMessage.Controls.Add(btnNo);

                        button btnYes = new button();
                        btnYes.Text = "Yes";
                        btnYes.DialogResult = DialogResult.Yes;
                        btnYes.FlatStyle = FlatStyle.Popup;
                        btnYes.FlatAppearance.BorderSize = 0;
                        btnYes.SetBounds((pnlShowMessage.ClientSize.Width - (btnCancel.ClientSize.Width + btnNo.ClientSize.Width + 10 + 80)), 5, 75, 25);
                        pnlShowMessage.Controls.Add(btnYes);
                    }
                    break;
                case enumMessageButton.AbortRetryIgnore:
                    {
                        button btnCancel = new button();
                        btnCancel.Text = "Ignore";
                        btnCancel.DialogResult = DialogResult.Ignore;
                        btnCancel.FlatStyle = FlatStyle.Popup;
                        btnCancel.FlatAppearance.BorderSize = 0;
                        btnCancel.SetBounds((pnlShowMessage.ClientSize.Width - 70), 5, 65, 25);
                        pnlShowMessage.Controls.Add(btnCancel);

                        button btnNo = new button();
                        btnNo.Text = "Retry";
                        btnNo.DialogResult = DialogResult.Retry;
                        btnNo.FlatStyle = FlatStyle.Popup;
                        btnNo.FlatAppearance.BorderSize = 0;
                        btnNo.SetBounds((pnlShowMessage.ClientSize.Width - (btnCancel.ClientSize.Width + 5 + 80)), 5, 75, 25);
                        pnlShowMessage.Controls.Add(btnNo);

                        button btnYes = new button();
                        btnYes.Text = "Abort";
                        btnYes.DialogResult = DialogResult.Abort;
                        btnYes.FlatStyle = FlatStyle.Popup;
                        btnYes.FlatAppearance.BorderSize = 0;
                        btnYes.SetBounds((pnlShowMessage.ClientSize.Width - (btnCancel.ClientSize.Width + btnNo.ClientSize.Width + 10 + 80)), 5, 75, 25);
                        pnlShowMessage.Controls.Add(btnYes);
                    }
                    break;
            }
        }
        //private string getPath()
        //{
        //    return Application.StartupPath + @"\";
        //}
        ///
        /// We can use this method to add image on message box.
        /// I had taken all images in ImageList control so that
        /// I can eaily add images. Image is displayed in
        /// PictureBox control.
        ///
        /// Type of image to be displayed.
        private void addIconImage(enumMessageIcon MessageIcon)
        {


            //string path = getPath();
            //path = path + "Icons\\";
            switch (MessageIcon)
            {
                // imageList1 pictures are not crisp
                case enumMessageIcon.Error:
                    pictureBox1.Image = (Image)Properties.Resources.exclamation_mark; // Image.FromFile(path + "error.ico");//imageList1.Images["Error"]; //Error is key name in imagelist control which uniqly identified images in ImageList control.
                    break;
                case enumMessageIcon.Information:
                    pictureBox1.Image = (Image)Properties.Resources.info; //Image.FromFile(path + "information.ico");//imageList1.Images["Information"];
                    break;
                case enumMessageIcon.Question:
                    pictureBox1.Image = (Image)Properties.Resources.question_mark1; //Image.FromFile(path + "question.ico");//imageList1.Images["Question"];
                    break;
                case enumMessageIcon.Warning:
                    pictureBox1.Image = (Image)Properties.Resources.exclamation_mark_yellow; //Image.FromFile(path + "exclamation.ico");//imageList1.Images["Warning"];
                    break;
            }
        }

        #region Overloaded Show message to display message box.

        ///
        /// Show method is overloaded which is used to display message
        ///
        ///
        //public DialogResult Show(string messageText)
        //{
        // frmShowMessage frmMessage = new frmShowMessage();
        // frmMessage.Text = "Message Box";
        // frmMessage.setMessage(messageText);
        // frmMessage.addIconImage(enumMessageIcon.Information);
        // frmMessage.addButton(enumMessageButton.OK);
        // frmMessage.ShowDialog();
        // return frmMessage.DialogResult;
        //}

        //public DialogResult Show(string messageText, string messageTitle)
        //{
        // frmShowMessage frmMessage = new frmShowMessage();
        // frmMessage.Text = messageTitle;
        // frmMessage.setMessage(messageText);
        // frmMessage.addIconImage(enumMessageIcon.Information);
        // frmMessage.addButton(enumMessageButton.OK);
        // frmMessage.ShowDialog();
        // return frmMessage.DialogResult;
        //}
        // Initialize parameter values for the lazy
        public DialogResult Show(string messageText, string messageTitle = "Message Box",
        enumMessageButton messageButton = enumMessageButton.OK, enumMessageIcon messageIcon = enumMessageIcon.Information)
        {
       
            ShowMessage frmMessage = new ShowMessage();
            frmMessage.Left += frmMessage.Left + 100;
            frmMessage.Top += frmMessage.Top + 50;
            frmMessage.StartPosition = FormStartPosition.Manual;
            frmMessage.Location = new System.Drawing.Point((MainForm.mainFormRef.Location.X + MainForm.mainFormRef.Size.Width / 2 - this.Size.Width / 2), (MainForm.mainFormRef.Location.Y + MainForm.mainFormRef.Size.Height / 2) - this.Size.Height / 2); // center parent (MainForm)
            frmMessage.setMessage(messageText);
            frmMessage.Text = messageTitle;
            frmMessage.addIconImage(messageIcon);
            frmMessage.addButton(messageButton);
            frmMessage.ShowDialog();
            return frmMessage.DialogResult;
        }
        public DialogResult Show(Form parent, string messageText, string messageTitle = "Message Box",
            enumMessageButton messageButton = enumMessageButton.OK, enumMessageIcon messageIcon = enumMessageIcon.Information)
        {

            ShowMessage frmMessage = new ShowMessage();
            frmMessage.Left += frmMessage.Left + 100;
            frmMessage.Top += frmMessage.Top + 50;
            frmMessage.StartPosition = FormStartPosition.Manual;
            frmMessage.Location = new System.Drawing.Point((parent.Location.X + parent.Size.Width / 2 - this.Size.Width / 2), (parent.Location.Y + parent.Size.Height / 2) - this.Size.Height / 2); // center parent (MainForm)
            frmMessage.setMessage(messageText);
            frmMessage.Text = messageTitle;
            frmMessage.addIconImage(messageIcon);
            frmMessage.addButton(messageButton);
            frmMessage.ShowDialog();
            return frmMessage.DialogResult;
        }
        #endregion

        # region Initalization
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ShowMessage));
            this.lblMessageText = new System.Windows.Forms.Label();
            this.pnlShowMessage = new myPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            //this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            //
            // lblMessageText
            //
            this.lblMessageText.BackColor = System.Drawing.Color.Transparent;
            this.lblMessageText.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessageText.Location = new System.Drawing.Point(73, 9);
            this.lblMessageText.Name = "lblMessageText";
            this.lblMessageText.Size = new System.Drawing.Size(324, 23);
            this.lblMessageText.TabIndex = 0;
            this.lblMessageText.Text = "label1";
            //
            // pnlShowMessage
            //
            this.pnlShowMessage.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pnlShowMessage.BackColor = System.Drawing.Color.Transparent;
            this.pnlShowMessage.Location = new System.Drawing.Point(74, 46);
            this.pnlShowMessage.Name = "pnlShowMessage";
            this.pnlShowMessage.Size = new System.Drawing.Size(332, 36);
            this.pnlShowMessage.TabIndex = 1;
            //
            // pictureBox1
            //
            this.pictureBox1.Location = new System.Drawing.Point(13, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(32, 32);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            ////
            //// imageList1
            ////
            //this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            //this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            //this.imageList1.Images.SetKeyName(0, "Error");
            //this.imageList1.Images.SetKeyName(1, "Information");
            //this.imageList1.Images.SetKeyName(2, "Question");
            //this.imageList1.Images.SetKeyName(3, "Warning");
            //
            // frmShowMessage
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(409, 85);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pnlShowMessage);
            this.Controls.Add(this.lblMessageText);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            //this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmShowMessage";
            this.Opacity = 0.98D;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

    }
        #endregion
    #region constant defiend in form of enumration which is used in showMessage class.

    public enum enumMessageIcon
    {
        Error,
        Warning,
        Information,
        Question,
    }

    public enum enumMessageButton
    {
        OK,
        YesNo,
        YesNoCancel,
        OKCancel,
        AbortRetryIgnore
    }

    #endregion
}
