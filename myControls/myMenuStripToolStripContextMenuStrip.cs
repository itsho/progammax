﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProGammaX
{
    class myMenuStripToolStripContextMenuStrip
    {


        //Source:
        //http://www.codeproject.com/Articles/70204/Custom-VisualStudio-2008-style-MenuStrip-and-ToolS


        public class myMenuStrip : MenuStrip
        {
            public myMenuStrip()
            {
                this.Renderer = new myMenuRenderer();

            }
        }

        public class myContextMenuStrip : ContextMenuStrip
        {
            public myContextMenuStrip()
            {
                this.Renderer = new myMenuRenderer();
                
            }
        }




        public class myToolStripRenderer : ToolStripProfessionalRenderer
        {

            // Render custom background gradient
            protected override void OnRenderToolStripBackground(ToolStripRenderEventArgs e)
            {
                base.OnRenderToolStripBackground(e);
                
                using (var b = new LinearGradientBrush(e.AffectedBounds,
                    clsColor.clrVerBG_White, clsColor.clrVerBG_GrayBlue,
                    LinearGradientMode.Vertical))
                {
                    using (var shadow = new SolidBrush(clsColor.clrVerBG_Shadow))
                    {
                        var rect = new Rectangle
                (0, e.ToolStrip.Height - 2, e.ToolStrip.Width, 1);
                        e.Graphics.FillRectangle(b, e.AffectedBounds);
                        e.Graphics.FillRectangle(shadow, rect);
                    }
                }
            }


            // Render button selected and pressed state
            protected override void OnRenderButtonBackground(ToolStripItemRenderEventArgs e)
            {
                base.OnRenderButtonBackground(e);
                var rectBorder = new Rectangle(0, 0, e.Item.Width - 1, e.Item.Height - 1);
                var rect = new Rectangle(1, 1, e.Item.Width - 2, e.Item.Height - 2);

                if (e.Item.Selected == true || (e.Item as ToolStripButton).Checked)
                {
                    using (var b = new LinearGradientBrush
                (rect, clsColor.clrToolstripBtnGrad_White,
                        clsColor.clrToolstripBtnGrad_Blue, LinearGradientMode.Vertical))
                    {
                        using (var b2 = new SolidBrush(clsColor.clrToolstripBtn_Border))
                        {
                            e.Graphics.FillRectangle(b2, rectBorder);
                            e.Graphics.FillRectangle(b, rect);
                        }
                    }
                }
                if (e.Item.Pressed)
                {
                    using (var b = new LinearGradientBrush
                (rect, clsColor.clrToolstripBtnGrad_White_Pressed,
                        clsColor.clrToolstripBtnGrad_Blue_Pressed,
                    LinearGradientMode.Vertical))
                    {
                        using (var b2 = new SolidBrush(clsColor.clrToolstripBtn_Border))
                        {
                            e.Graphics.FillRectangle(b2, rectBorder);
                            e.Graphics.FillRectangle(b, rect);
                        }
                    }
                }
            }
        }




        public class clsColor
        {
            public static Color clrHorBG_GrayBlue { get { return ThemedColors.MenuColor; } set { ThemedColors.MenuColor = value; } } //Color.FromArgb(255, 233, 236, 250);
            public static Color clrHorBG_White { get { return ThemedColors.BackColor; } set { ThemedColors.BackColor = value; } } //Color.FromArgb(255, 244, 247, 252);
            public static Color clrSubmenuBG { get { return ThemedColors.MenuColor; } set { ThemedColors.MenuColor = value; } } //Color.FromArgb(255, 240, 240, 240);
            public static Color clrImageMarginBlue { get { return ThemedColors.MenuColor; } set { ThemedColors.MenuColor = value; } } //Color.FromArgb(255, 212, 216, 230);
            public static Color clrImageMarginWhite { get { return ThemedColors.WindowColor; } set { ThemedColors.WindowColor = value; } } //Color.FromArgb(255, 244, 247, 252);
            public static Color clrImageMarginLine { get { return ThemedColors.GridColor; } set { ThemedColors.MenuTextColor = value; } } // Color.FromArgb(255, 160, 160, 180);
            public static Color clrSelectedBG_Blue { get { return ThemedColors.SelectionColor; } set { ThemedColors.SelectionColor = value; } } //Color.FromArgb(255, 186, 228, 246);
            public static Color clrSelectedBG_Header_Blue { get { return ThemedColors.MenuColor; } set { ThemedColors.MenuColor = value; } } // Color.FromArgb(255, 146, 202, 230);
            public static Color clrSelectedBG_White { get { return ThemedColors.WindowColor; } set { ThemedColors.WindowColor = value; } } //ThemedColors.MenuColor; //Color.FromArgb(255, 241, 248, 251);
            public static Color clrSelectedBG_Border { get { return ThemedColors.WindowColor; } set { ThemedColors.WindowColor = value; } } //Color.FromArgb(255, 150, 217, 249);
            public static Color clrSelectedBG_Drop_Blue { get { return ThemedColors.SelectionColor; } set { ThemedColors.SelectionColor = value; } } //Color.FromArgb(255, 139, 195, 225);
            public static Color clrSelectedBG_Drop_Border { get { return ThemedColors.MenuTextColor; } set { ThemedColors.MenuTextColor = value; } } // Color.FromArgb(255, 48, 127, 177);
            public static Color clrMenuBorder { get { return ThemedColors.MenuTextColor; } set { ThemedColors.MenuTextColor = value; } } //Color.FromArgb(255, 160, 160, 160);
            public static Color clrCheckBG { get { return ThemedColors.GridColor; } set { ThemedColors.GridColor = value; } } //Color.FromArgb(255, 206, 237, 250);

            public static Color clrVerBG_GrayBlue { get { return ThemedColors.SelectionColor; } set { ThemedColors.SelectionColor = value; } } //Color.FromArgb(255, 196, 203, 219);
            public static Color clrVerBG_White { get { return ThemedColors.MenuColor; } set { ThemedColors.MenuColor = value; } } // Color.FromArgb(255, 250, 250, 253);
            public static Color clrVerBG_Shadow { get { return ThemedColors.MenuTextColor; } set { ThemedColors.MenuTextColor = value; } } //Color.FromArgb(255, 181, 190, 206);

            public static Color clrToolstripBtnGrad_Blue { get { return ThemedColors.SelectionColor; } set { ThemedColors.SelectionColor = value; } } //Color.FromArgb(255, 129, 192, 224);
            public static Color clrToolstripBtnGrad_White { get { return ThemedColors.MenuColor; } set { ThemedColors.MenuColor = value; } } //Color.FromArgb(255, 237, 248, 253);
            public static Color clrToolstripBtn_Border { get { return ThemedColors.MenuTextColor; } set { ThemedColors.MenuTextColor = value; } } //Color.FromArgb(255, 41, 153, 255);
            public static Color clrToolstripBtnGrad_Blue_Pressed { get { return ThemedColors.SelectionColor; } set { ThemedColors.SelectionColor = value; } } //Color.FromArgb(255, 124, 177, 204);
            public static Color clrToolstripBtnGrad_White_Pressed { get { return ThemedColors.MenuColor; } set { ThemedColors.MenuColor = value; } } //Color.FromArgb(255, 228, 245, 252);

            public static void DrawRoundedRectangle(Graphics g, int x, int y,
                int width, int height, int m_diameter, Color color)
            {

                using (Pen pen = new Pen(color))
                {
                    //Dim g As Graphics
                    var BaseRect = new RectangleF(x, y, width, height);
                    var ArcRect = new RectangleF(BaseRect.Location,
                    new SizeF(m_diameter, m_diameter));
                    //top left Arc
                    g.DrawArc(pen, ArcRect, 180, 90);
                    g.DrawLine(pen, x + Convert.ToInt32(m_diameter / 2),
                y, x + width - Convert.ToInt32(m_diameter / 2), y);

                    // top right arc
                    ArcRect.X = BaseRect.Right - m_diameter;
                    g.DrawArc(pen, ArcRect, 270, 90);
                    g.DrawLine(pen, x + width, y + Convert.ToInt32(m_diameter / 2),
                x + width, y + height - Convert.ToInt32(m_diameter / 2));

                    // bottom right arc
                    ArcRect.Y = BaseRect.Bottom - m_diameter;
                    g.DrawArc(pen, ArcRect, 0, 90);
                    g.DrawLine(pen, x + Convert.ToInt32(m_diameter / 2),
            y + height, x + width - Convert.ToInt32(m_diameter / 2), y + height);

                    // bottom left arc
                    ArcRect.X = BaseRect.Left;
                    g.DrawArc(pen, ArcRect, 90, 90);
                    g.DrawLine(pen, x, y + Convert.ToInt32(m_diameter / 2),
                x, y + height - Convert.ToInt32(m_diameter / 2));
                }
            }
        }





        public class myMenuRenderer : ToolStripProfessionalRenderer
        {
            // Make sure the textcolor is black
            protected override void InitializeItem(ToolStripItem item)
            {
                
                base.InitializeItem(item);
                //if (item.Enabled)
                //{
                //    item.ForeColor = ThemedColors.MenuTextColor;
                //    //item.BackColor = ThemedColors.MenuColor;
                //}
                //else
                //{
                //    item.ForeColor = ThemedColors.SelectionColor;
                //    //item.BackColor = ThemedColors.MenuColor;
                //}
            }

            protected override void Initialize(ToolStrip toolStrip)
            {
                base.Initialize(toolStrip);
                //toolStrip.ForeColor = ThemedColors.MenuTextColor;

            }
            public myMenuRenderer()
            {

            }
  
            private bool orientationIsHorizontal = true;
            protected override void OnRenderGrip(ToolStripGripRenderEventArgs e)
            {
                //base.OnRenderGrip(e);
                
                
                var rect = new Rectangle(e.GripBounds.X, e.GripBounds.Y, e.GripBounds.Width, e.GripBounds.Height);
     

                    using (var b = new LinearGradientBrush
                (rect, ThemedColors.MenuTextColor,
                        ThemedColors.MenuTextColor,
                         LinearGradientMode.Vertical))
                    {

                        if (e.GripDisplayStyle == ToolStripGripDisplayStyle.Vertical)
                        {
                            orientationIsHorizontal = true;
                            Pen blackPen = new Pen(b, e.GripBounds.Width / 2);
                            blackPen.DashStyle = DashStyle.Dot;
                            blackPen.DashCap = DashCap.Round;
                            e.Graphics.DrawLine(blackPen, new Point(e.GripBounds.Width / 2, e.GripBounds.Height / 6), new Point(e.GripBounds.Width / 2, e.GripBounds.Height - e.GripBounds.Height / 6));
                        
                        }
                        else
                        {
                            orientationIsHorizontal = false;
                            Pen blackPen = new Pen(b, e.GripBounds.Height / 2);
                            blackPen.DashStyle = DashStyle.Dot;
                            blackPen.DashCap = DashCap.Round;
                            e.Graphics.DrawLine(blackPen, new Point(e.GripBounds.Width / 6, e.GripBounds.Height / 2), new Point(e.GripBounds.Width - e.GripBounds.Width / 6, e.GripBounds.Height / 2));

                        }

                    }

                    



            }
            protected override void OnRenderOverflowButtonBackground(ToolStripItemRenderEventArgs e)
            {

                          
               
                    //base.OnRenderOverflowButtonBackground(e);

                    // Create the first pathright side up triangle.
                    Point[] myArray =
                         {
                             new Point(e.Item.Width / 4, e.Item.Height / 2 + e.Item.Height / 3),
                             new Point(e.Item.Width / 2 + e.Item.Width / 4, e.Item.Height / 2 + e.Item.Height / 3),
                             new Point(e.Item.Width / 2, e.Item.Height - e.Item.Height / 8),
                             new Point(e.Item.Width / 4, e.Item.Height / 2 + e.Item.Height / 3)
                         };
                    Point[] myArray1 =
                         {
                             new Point(e.Item.Width / 2 - e.Item.Width / 32, e.Item.Height / 2 - e.Item.Height / 8),
                             new Point(e.Item.Width / 2 + e.Item.Width / 32, e.Item.Height / 2 - e.Item.Height / 8),
                             new Point(e.Item.Width / 2, e.Item.Height / 2 + e.Item.Height / 8),
                             new Point(e.Item.Width / 2 - e.Item.Width / 32, e.Item.Height / 2 - e.Item.Height / 8),
                         };
                    GraphicsPath myPath = new GraphicsPath();

                    if (orientationIsHorizontal)
                    {
                        myPath.AddLines(myArray);
                    }
                    else
                    {
                        myPath.AddLines(myArray1);
                    }

                    // Draw the combined path to the screen.
                    Brush myBrush = new LinearGradientBrush(myArray[0], myArray[2], ThemedColors.MenuTextColor, ThemedColors.MenuTextColor);
                    
          

                    var rectBorder = new Rectangle(0, 0, e.Item.Width - 1, e.Item.Height - 1);
                    var rect = new Rectangle(1, 1, e.Item.Width - 2, e.Item.Height - 2);
                    

                    if (e.Item.Pressed)
                    {
                        using (var b = new LinearGradientBrush
                    (rect, clsColor.clrToolstripBtnGrad_White,
                            ThemedColors.DisabledMenuTextColor,
                             LinearGradientMode.Vertical))
                        {
                            using (var b2 = new SolidBrush(clsColor.clrToolstripBtn_Border))
                            {
                                e.Graphics.FillRectangle(b2, rectBorder);
                                e.Graphics.FillRectangle(b, rect);
                            }
                        }
                    }
                    else
                    {
                        using (var b = new LinearGradientBrush(rect, clsColor.clrToolstripBtnGrad_White,
                                        ThemedColors.DisabledMenuTextColor, LinearGradientMode.Vertical))
                        {
                            using (var b2 = new SolidBrush(clsColor.clrToolstripBtn_Border))
                            {
                                //e.Graphics.FillRectangle(b2, rectBorder);
                                e.Graphics.FillRectangle(b, rect);
                            }
                        }
                    }



                    e.Graphics.FillPath(myBrush, myPath);



            }



            // Render horizontal background gradient
            protected override void OnRenderToolStripBackground(ToolStripRenderEventArgs e)
            {
                
                base.OnRenderToolStripBackground(e);
                LinearGradientBrush b = new LinearGradientBrush(e.AffectedBounds,
                    clsColor.clrHorBG_GrayBlue, clsColor.clrHorBG_White,
                        LinearGradientMode.Horizontal);
                e.Graphics.FillRectangle(b, e.AffectedBounds);
            }

            // Render image margin and gray ItemBackground
            protected override void OnRenderImageMargin(ToolStripRenderEventArgs e)
            {
                base.OnRenderImageMargin(e);

                // Draw ImageMargin background gradient
                LinearGradientBrush b = new LinearGradientBrush
                (e.AffectedBounds, clsColor.clrImageMarginWhite,
                    clsColor.clrImageMarginBlue, LinearGradientMode.Horizontal);

                // Shadow at the right of image margin
                var DarkLine = new SolidBrush(clsColor.clrImageMarginLine);
                var WhiteLine = new SolidBrush(ThemedColors.WindowColor);
                var rect = new Rectangle(e.AffectedBounds.Width,
                    2, 1, e.AffectedBounds.Height);
                var rect2 = new Rectangle(e.AffectedBounds.Width + 1,
                    2, 1, e.AffectedBounds.Height);

                // Gray background
                var SubmenuBGbrush = new SolidBrush(clsColor.clrSubmenuBG);
                var rect3 = new Rectangle(0, 0, e.ToolStrip.Width, e.ToolStrip.Height);

                // Border
                var borderPen = new Pen(clsColor.clrMenuBorder);
                var rect4 = new Rectangle
            (0, 1, e.ToolStrip.Width - 1, e.ToolStrip.Height - 2);

                e.Graphics.FillRectangle(SubmenuBGbrush, rect3);
                e.Graphics.FillRectangle(b, e.AffectedBounds);
                e.Graphics.FillRectangle(DarkLine, rect);
                e.Graphics.FillRectangle(WhiteLine, rect2);
                e.Graphics.DrawRectangle(borderPen, rect4);
            }

            // Render checkmark
            protected override void OnRenderItemCheck(ToolStripItemImageRenderEventArgs e)
            {
                base.OnRenderItemCheck(e);

                if (e.Item.Selected)
                {
                    var rect = new Rectangle(3, 1, 20, 20);
                    var rect2 = new Rectangle(4, 2, 18, 18);
                    SolidBrush b = new SolidBrush(clsColor.clrToolstripBtn_Border);
                    SolidBrush b2 = new SolidBrush(clsColor.clrCheckBG);

                    e.Graphics.FillRectangle(b, rect);
                    e.Graphics.FillRectangle(b2, rect2);
                    e.Graphics.DrawImage(e.Image, new Point(5, 3));
                }
                else
                {
                    var rect = new Rectangle(3, 1, 20, 20);
                    var rect2 = new Rectangle(4, 2, 18, 18);
                    SolidBrush b = new SolidBrush(clsColor.clrSelectedBG_Drop_Border);
                    SolidBrush b2 = new SolidBrush(clsColor.clrCheckBG);

                    e.Graphics.FillRectangle(b, rect);
                    e.Graphics.FillRectangle(b2, rect2);
                    e.Graphics.DrawImage(e.Image, new Point(5, 3));
                }
            }

            // Render separator
            protected override void OnRenderSeparator(ToolStripSeparatorRenderEventArgs e)
            {
                base.OnRenderSeparator(e);

                var DarkLine = new SolidBrush(clsColor.clrImageMarginLine);
                var WhiteLine = new SolidBrush(ThemedColors.WindowColor);
                var rect = new Rectangle(32, 3, e.Item.Width - 32, 1);
                e.Graphics.FillRectangle(DarkLine, rect);
                e.Graphics.FillRectangle(WhiteLine, rect);
            }

            // Render arrow
            protected override void OnRenderArrow(ToolStripArrowRenderEventArgs e)
            {
                e.ArrowColor = ThemedColors.MenuTextColor;
                base.OnRenderArrow(e);
            }

            protected override void OnRenderItemText(ToolStripItemTextRenderEventArgs e)
            {


                //draw with solid red
          

                //base.OnRenderItemText(e);
                //if( e.ForeColor == ThemedColors.MenuTextColor )
                //{
                if (e.Item.Enabled)
                {
                    //e.TextFormat |= TextFormatFlags.HorizontalCenter;
                    e.Graphics.DrawString(e.Item.Text, e.Item.Font, new SolidBrush(ThemedColors.MenuTextColor), e.TextRectangle);
                }
                else
                {
                    e.Graphics.DrawString(e.Item.Text, e.Item.Font, new SolidBrush(ThemedColors.DisabledMenuTextColor), e.TextRectangle);
                }
                //}

            }
             //Render  MenuItem background: lightblue if selected, darkblue if dropped down
            protected override void OnRenderMenuItemBackground(ToolStripItemRenderEventArgs e)
            {
                base.OnRenderMenuItemBackground(e);

                if (e.Item.Enabled)
                {
                    if (e.Item.IsOnDropDown == false && e.Item.Selected)
                    {
                        // If item is MenuHeader and selected: draw darkblue color
                        var rect = new Rectangle(3, 2, e.Item.Width - 6, e.Item.Height - 4);
                        using (var b = new LinearGradientBrush
                    (rect, clsColor.clrSelectedBG_White,
                            clsColor.clrSelectedBG_Header_Blue,
                LinearGradientMode.Vertical))
                        {
                            using (var b2 = new SolidBrush(clsColor.clrCheckBG))
                            {
                                e.Graphics.FillRectangle(b, rect);
                                clsColor.DrawRoundedRectangle(e.Graphics, rect.Left - 1,
                    rect.Top - 1, rect.Width, rect.Height + 1, 4,
                    clsColor.clrToolstripBtn_Border);
                                clsColor.DrawRoundedRectangle(e.Graphics, rect.Left - 2,
                    rect.Top - 2, rect.Width + 2, rect.Height + 3, 4,
                    ThemedColors.WindowColor);
                                //e.Item.ForeColor = ThemedColors.MenuTextColor;
                            }
                        }
                    }
                    else if (e.Item.IsOnDropDown && e.Item.Selected)
                    {
                        // If item is NOT menuheader (but subitem); 
                        // and selected: draw lightblue border

                        var rect = new Rectangle(4, 2, e.Item.Width - 6, e.Item.Height - 4);
                        using (var b = new LinearGradientBrush
                    (rect, clsColor.clrSelectedBG_White,
                            clsColor.clrSelectedBG_Blue, LinearGradientMode.Vertical))
                        {
                            using (var b2 = new SolidBrush(clsColor.clrSelectedBG_Border))
                            {

                                e.Graphics.FillRectangle(b, rect);
                                clsColor.DrawRoundedRectangle(e.Graphics,
                                    rect.Left - 1, rect.Top - 1, rect.Width,
                                rect.Height + 1, 6, clsColor.clrSelectedBG_Border);
                                //e.Item.ForeColor = ThemedColors.MenuTextColor;
                            }
                        }
                    }

                    // If item is MenuHeader and menu is dropped down; 
                    // selection rectangle is now darker
                    if ((e.Item as ToolStripMenuItem).DropDown.Visible &&
                        e.Item.IsOnDropDown == false)
                    {
                        // (e.Item as ToolStripMenuItem).OwnerItem == null
                        var rect = new Rectangle(3, 2, e.Item.Width - 6, e.Item.Height - 4);
                        using (var b = new LinearGradientBrush(rect, ThemedColors.WindowColor, clsColor.clrSelectedBG_Drop_Blue, LinearGradientMode.Vertical))
                        {
                            using (var b2 = new SolidBrush(clsColor.clrSelectedBG_Drop_Border))
                            {
                                e.Graphics.FillRectangle(b, rect);
                                clsColor.DrawRoundedRectangle(e.Graphics, rect.Left - 1, rect.Top - 1, rect.Width, rect.Height + 1,
                                4, clsColor.clrSelectedBG_Drop_Border);
                                clsColor.DrawRoundedRectangle(
                                    e.Graphics, rect.Left - 2, rect.Top - 2,
                        rect.Width + 2, rect.Height + 3, 4,
                                    ThemedColors.WindowColor);
                                //e.Item.ForeColor = ThemedColors.MenuTextColor;
                            }
                        }
                    }
                    //e.Item.ForeColor = ThemedColors.MenuTextColor;
                }
                //else
                //{
                //    e.Item.ForeColor = Color.OrangeRed;
                //}



                    //draw background
                    //base.OnRenderMenuItemBackground(e);
                   



            }




  







        }










        //public class myToolStripMenuItem : ToolStripMenuItem
        //{
        //    private Font font;
        //    private Color foreColor;
        //    private Image image;
        //    public void InitializeItem(myToolStripMenuItem item)
        //    {
        //        this.InitializeItem(item);
        //        item.ForeColor = ThemedColors.MenuTextColor;
        //    }
        //    public override Font Font
        //    {
        //        get
        //        {
        //            return font;
        //        }
        //        set
        //        {
        //            font = value;
        //        }
        //    }

        //    public override Image Image
        //            {
        //                  get 
        //                { 
        //                     return base.Image;
        //                }
        //                  set 
        //                { 
        //                    base.Image = value;
        //                }
        //            }


        //    public override Color ForeColor

        //    {
        //        get
        //        {
        //            return foreColor;
        //        }
        //        set
        //        {
        //            foreColor = value;
        //        }
        //    }

        //    public myToolStripMenuItem(string text, Font font, Image image, Color foreColor)
        //        : base(text)
        //    {
        //        this.Font = font;
        //        this.Image = image;
        //        this.ForeColor = foreColor;
        //        //this.OwnerDraw = true;
        //    }

        //    public myToolStripMenuItem(string text, Image image)
        //        : base(text)
        //    {
        //        // Choose a suitable default color and font.
        //        this.Font = new Font("Tahoma", 8);
        //        this.Image = image;
        //        this.ForeColor = SystemColors.MenuText;
        //        //this.OwnerDraw = true;
        //    }

            //protected override void OnMeasureItem(System.Windows.Forms.MeasureItemEventArgs e)
            //{
            //    this.OnMeasureItem(e);

            //    // Measure size needed to display text.
            //    e.ItemHeight = (int)e.Graphics.MeasureString(this.Text, this.Font).Height + 5;
            //    e.ItemWidth = (int)e.Graphics.MeasureString(this.Text, this.Font).Width + 30;
            //}

            //protected override void OnDrawItem(System.Windows.Forms.DrawItemEventArgs e)
            //{
            //    this.OnDrawItem(e);

            //    // Determine whether disabled text is needed.
            //    Color textColor;
            //    if (this.Enabled == false)
            //    {
            //        textColor = SystemColors.GrayText;
            //    }
            //    else
            //    {
            //        e.DrawBackground();
            //        if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
            //        {
            //            textColor = SystemColors.HighlightText;
            //        }
            //        else
            //        {
            //            textColor = this.ForeColor;
            //        }
            //    }


            //    // Draw the image.
            //    if (Image != null)
            //    {
            //        if (this.Enabled == false)
            //        {
            //            ControlPaint.DrawImageDisabled(e.Graphics, Image,
            //                e.Bounds.Left + 3, e.Bounds.Top + 2, SystemColors.Menu);
            //        }
            //        else
            //        {
            //            e.Graphics.DrawImage(Image, e.Bounds.Left + 3, e.Bounds.Top + 2);
            //        }
            //    }

            //    // Draw the text with the supplied colors and in the set region.
            //    e.Graphics.DrawString(this.Text, this.Font, new SolidBrush(textColor),
            //        e.Bounds.Left + 25, e.Bounds.Top + 3);

            //}
        //}














        public class myToolStrip : ToolStrip
        {
            public myToolStrip()
            {
                this.Renderer = new myToolStripRenderer();
            }
        }

    

        public class myToolStripContainer : ToolStripContainer
        {
            public myToolStripContainer()
            {
                this.TopToolStripPanel.Paint +=
            new PaintEventHandler(TopToolStripPanel_Paint);
                this.TopToolStripPanel.SizeChanged +=
            new EventHandler(TopToolStripPanel_SizeChanged);
            }

            void TopToolStripPanel_SizeChanged(object sender, EventArgs e)
            {
                this.Invalidate();
            }

            void TopToolStripPanel_Paint(object sender, PaintEventArgs e)
            {
                Graphics g = e.Graphics;
                var rect = new Rectangle(0, 0, this.Width, this.FindForm().Height);
                using (LinearGradientBrush b = new LinearGradientBrush(
                    rect, clsColor.clrHorBG_GrayBlue,
            clsColor.clrHorBG_White, LinearGradientMode.Horizontal))
                {
                    g.FillRectangle(b, rect);
                }
            }
        }





    }
}
