﻿namespace ProGammaX
{
    using System.Windows.Forms;

    public partial class Dialog_CreateShortcut
    {

        private static Dialog_CreateShortcut Dialog_CreateShortcutRef;
        public static Dialog_CreateShortcut dialog_CreateShortcutRef
        {
        get
            {
            return Dialog_CreateShortcutRef;
            }
        }

        #region Constructors

        public Dialog_CreateShortcut()
        {
            InitializeComponent();
            ProGammaX.MainForm.mainFormRef.applySelectedTheme(this);
            Dialog_CreateShortcutRef = this;
            this.Load += new System.EventHandler(Dialog6_Load);
        }

        #endregion Constructors

        #region Methods

        // ERROR: Handles clauses are not supported in C#
        private void Cancel_Button_Click(System.Object sender, System.EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        // ERROR: Handles clauses are not supported in C#
        private void CheckBox2_CheckedChanged(System.Object sender, System.EventArgs e)
        {
            this.TextBox2.Enabled = this.CheckBox2.Checked;
        }

        // ERROR: Handles clauses are not supported in C#
        private void Dialog6_Load(System.Object sender, System.EventArgs e)
        {
            this.Text = MainForm.StringToBeTranslated[172] + MainForm.mainFormRef.GetProfileNameFrom_MAIN_LIST_Column2();
            // "Create a shortcut to " & MainForm.GetProfileNameFrom_MAIN_LIST_Column2()
            this.TextBox2.Enabled = this.CheckBox2.Checked;
            this.TextBox2.Text = MainForm.StringToBeTranslated[173];
            // "Old games"
        }

        // ERROR: Handles clauses are not supported in C#
        private void OK_Button_Click(System.Object sender, System.EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        #endregion Methods
    }
}